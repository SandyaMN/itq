/*!
 * 
 * DOCUMENTATION SCRIPT
 *
 */
$(function() {
	$('a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash,
	    $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
            $(document).on("scroll", onScroll);
	    });
	});

    $(document).on("scroll", onScroll);
    //smoothscroll
	function onScroll(event){
	    var scrollPos = $(document).scrollTop();
	    $('#documentation-navigation ul li a').each(function () {
	        var currLink = $(this);
	        var refElement = $(currLink.attr("href"));
	        if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
	            $('#documentation-navigation ul li a').removeClass("active");
	            currLink.addClass("active");
	        }
	        else{
	            currLink.removeClass("active");
	        }
	    });
	}
});