<?php
/**
 * Template Name: about-us
 */

get_header(); ?>

    <!--<div id="header"></div>	-->	

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet';?>">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>" type="text/javascript"></script>
    <!--<script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>    
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>-->
		
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="bold">About Us</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="about active company-profile"><a href="<?php echo get_home_url(); ?>/company-profile/">Company Profile</a></li>
                        <li class="leadershipTeam"><a class="about " href="<?php echo get_home_url(); ?>/leadershipteam/">Leadership Team</a></li>
                        <li class="itqJourney"><a class="about" href="<?php echo get_home_url(); ?>/itqjourney/">ITQ Journey </a></li>
                        <li class="awards"><a class="about " href="<?php echo get_home_url(); ?>/awards">Awards</a></li>
                        <li class="csr"><a class="about " href="<?php echo get_home_url(); ?>/csr/">CSR</a></li>
                        <li class="blogs"><a class="about " href="">Blogs</a></li>
                        <li class="events"><a class="about " href="<?php echo get_home_url(); ?>/events/">Events</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="company-profile" class="tab-pane fade in active">
                <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/company-profile/">About Us</a> / <a class="current-page">Company
                        Profile</a></p>
                <div class="profile">
                    <!-- <h3>Company Profile</h3> -->
                    <div class="about clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                            <h4>About ITQ</h4>
                            <p>InterGlobe Technology Quotient (ITQ), a strategic business unit of InterGlobe
                                Enterprises is a large Indian conglomerate holding leadership positions in Aviation,
                                Hospitality and Travel related services. Established in 1989 and headquartered in
                                Gurugram, InterGlobe through its various businesses employs more than 22,000
                                professionals globally.</p>
                            <p>&nbsp;</p>
                            <p>ITQ is an official distributor of Travelport in 6 markets across Asia Pacific region
                                including India, Sri Lanka. Headquartered in Gurgaon, the company provides cutting edge
                                travel technology solutions with unmatched inventory options to its customers helping
                                them increase their productivity and business efficiency. With an extended network
                                nearly 400 cities having 19 dedicated offices and 14 REPs, InterGlobe Technology
                                Quotient reaches out to over 36,000 agency terminals, with two nationalised service
                                centres and 16 training centres having state-of-the-art infrastructure and facilities
                                matching international standards.</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="100%" /><br />
                        </div><br />
                    </div>
                    <p>&nbsp;</p>
                    <div class="travelport">
                        <h4>Travelport</h4>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                        <p>Travelport is the technology company which makes the experience of buying and managing
                            travel continually better. It operates a travel commerce platform providing distribution,
                            technology, payment and other solutions for the global travel and tourism industry. The
                            company facilitates travel commerce by connecting the world?s leading travel providers with
                            online and offline travel buyers in a proprietary business-to-business (B2B) travel
                            marketplace.</p>
                        <br /><!-- <p>&nbsp;</p> -->
                        <p>Travelport has a leadership position in airline merchandising, hotel content and
                            distribution, car rental, mobile commerce and B2B payment solutions. The company also
                            provides critical IT services to airlines, such as shopping, ticketing, departure control
                            and other solutions. With net revenue of over $2.4 billion in 2017, Travelport is
                            headquartered in Langley, UK, has approximately 4,000 staff and is represented in 180
                            countries and territories. </p>
                        <br /><!-- <p>&nbsp;</p> -->
                        <p>In India, Travelport partners with InterGlobe Technology Quotient (ITQ), a strategic
                            business unit of InterGlobe Enterprises, is an official distributor of Travelport in 6
                            markets across Asia Pacific region including India, Sri Lanka. Headquartered in Gurgaon,
                            the company provides cutting edge travel technology solutions with unmatched inventory
                            options to its customers helping them increase their productivity and business efficiency.
                            With an extended network nearly 400 cities having 19 dedicated offices and 14 REPs,
                            InterGlobe Technology Quotient reaches out to over 36,000 agency terminals, with two
                            nationalised service centres and 16 training centres having state-of-the-art infrastructure
                            and facilities matching international standards.</p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  p-l-30">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/travelport-logo.png';?>" width="100%" />
                        </div><br />
                        
                        
                    </div>
                </div>
            </div>
            <!--<div class="tab-pane fade" id="itqJourney">

            </div>
            <div class="tab-pane fade" id="leadershipTeam">

            </div>
            <div class="tab-pane fade" id="awards">

            </div>
            <div class="tab-pane fade" id="csr">

            </div>
            <div class="tab-pane fade" id="blogs">

            </div>
            <div class="tab-pane fade" id="events">

            </div>-->
        </div>
    </div>

    <!--  our business -->
    <div class="container business">
        <h3 class="blue-text">Interglobe businesses</h3><br />
        <div class="business-div">
            <div class="row business-row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/interglobe.png';?>" width="85" />
                        <h5>InterGlobe Enterprises</h5>
                        <p>InterGlobe Enterprises is a large Indian conglomerate holding leadership positions...</p>
                        <a class="next" href="https://www.interglobe.com/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/Aviation.png';?>" width="85" />
                        <h5>InterGlobe Aviation</h5>
                        <p>IndiGo commenced operations in August, 2006 with a single aircraft and is today India's...</p>
                        <a class="next" href="https://www.goindigo.in/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/hotel.png';?>" width="85" />
                        <h5>InterGlobe Hotels</h5>
                        <p>InterGlobe Hotels, a joint venture between InterGlobe Enterprises and Accor Hotels was...</p>
                        <a class="next" href="https://www.interglobehotels.com/about.html" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/air-transport.png';?>" width="85" />
                        <h5>InterGlobe Air Transport</h5>
                        <p>Since 1989, InterGlobe Air Transport (IGAT) has assisted the world's leading airlines...</p>
                        <a class="next" href="https://www.interglobe.com/Business/Loadview?Cat=Airline%20Management" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <!-- <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Technology Quotient</h5>
                        <p>InterGlobe Technology Quotient (ITQ), is an official distributor of Travelport in 6
                            markets...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div> -->
            </div>
            <div class="row business-row">
                <!-- <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Technologies</h5>
                        <p>InterGlobe Technologies is a leading provider of IT, BPM and Digital Services & Solutions...</p>
                        <a class="next" href=""><img src="/assets/next.png';?>" width="40" /></a>
                    </div>
                </div> -->
               
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/cae-logo.png';?>" width="85" />
                        <h5>CAE Simulation Training Private Limited</h5>
                        <p>CAE Simulation Training Private Limited (CSTPL), is a joint venture between...</p>
                        <a class="next" href="https://www.interglobe.com/Business/Loadview?Cat=Advanced%20Pilot%20Training" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/intsame-logo.png';?>" width="85" />
                        <h5>SAME (InterGlobe Education)</h5>
                        <p>School for Aircraft Maintenance Engineering (SAME) is a DGCA approved school offering...</p>
                        <a class="next" href="https://www.interglobe.com/Business/Loadview?Cat=AIRCRAFT%20MAINTENANCE%20ENGINEERING" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/itq-logos/interglobe-real-state-logo.png';?>" width="85" />
                        <h5>InterGlobe Real Estate</h5>
                        <p>InterGlobe Real Estate, new business venture of InterGlobe Enterprises started operations...</p>
                        <a class="next" href="https://www.interglobe.com/Business/Loadview?Cat=Real%20Estate" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
            </div>
            <!-- <div class="row business-row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Real Estate</h5>
                        <p>InterGlobe Real Estate, new business venture of InterGlobe Enterprises started operations...</p>
                        <a class="next" href=""><img src="/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

  <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>