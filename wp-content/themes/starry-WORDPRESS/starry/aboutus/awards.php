<?php
/**
 * Template Name: awards
 */

get_header(); ?>
    <!--<div id="header"></div> --> 

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet';?>">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>" type="text/javascript"></script>
    <!--<script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>    
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>-->
        
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="bold">About Us</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="about company-profile"><a href="<?php echo get_home_url(); ?>/company-profile/">Company Profile</a></li>
                        <li class="leadershipTeam"><a class="about " href="<?php echo get_home_url(); ?>/leadershipteam/">Leadership Team</a></li>                        
                        <li class="itqJourney"><a class="about" href="<?php echo get_home_url(); ?>/itqjourney/">ITQ Journey </a></li>
                        <li class="active awards"><a class="about " href="<?php echo get_home_url(); ?>/awards">Awards</a></li>
                        <li class="csr"><a class="about " href="<?php echo get_home_url(); ?>/csr/">CSR</a></li>
                        <li class="blogs"><a class="about " href="">Blogs</a></li>
                        <li class="events"><a class="about " href="<?php echo get_home_url(); ?>/events/">Events</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="awards" class="tab-pane fade in active">
<p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/company-profile/">About Us</a> / <a class="current-page">ITQ
        Awards</a></p>
<div class="team">
    <h3>Awards and Recognitions</h3>
    <div class="awadrs clear-fix">
        <div class="award-year">
            <h4>2018</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="" />
                    <p>ITQ awarded Golden Peacock Award 2018 by Institute of Directors for effective Risk Management</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="" />
                    <p>ITQ awarded ICONIC 2018 at Redhat Communications Award Ceremony</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="" />
                    <p>ITQ awarded Best GDS title for Travelport at India Travel Awards - South 2018</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2018/Global-Star-Awards-2018.jpg';?>" />
                    <p>ITQ conferred with 'Most Professional GDS' award for Travelport Smartpoint (Galileo) at Global
                        Star Awards 2018</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2018/TnH awards.png';?>" />
                    <p>ITQ wins the ‘Most Outstanding Travel Technology Provider' award at 3rd Travel & Hospitality
                        Award</p>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="" />
                    <p> ITQ awarded Most Prefered GDS title for Travelport at the VETA 2019</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="" />
                    <p>ITQ awarded 2018 Star of the Industry title for effective Marketing & Communication presented by
                        ET Now</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="" />
                    <p>ITQ awarded Best GDS title for Travelport in North, East and West regions at India Travel Awards
                        2018</p>
                </div>

            </div>


        </div>
        <div class="award-year">
            <h4>2017</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2017/India Travel Awards South 2017.png';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport awarded the "Best GDS" title at India Travel Awards South 2017</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2017/31-2017.jpg';?>" />
                    <br /><br />
                    <p>Travelport awarded the Best GDS in India Travel Awards - East 2017</p>
                </div>
            </div>
        </div>
        <div class="award-year">
            <h4>2016</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2016/india travel awards east-2016.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport Wins the Best GDS Title at East India Travel Awards 2016</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2016/west-2016.png';?>" />
                    <br /><br />
                    <p>Travelport Wins the Best GDS Title at West India Travel Awards 2016</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <br />
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2016/Todays traveller Award-2016.jpg';?>" />
                    <p>&nbsp;</p><br />
                    <p>ITQ wins Today's Traveller Award 2016 for Best Technology Solution Provider</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2016/raw2016.gif';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>InterGlobe Adjudged Asia's Best Companies to Work For 2016</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2016/south-award-2016.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport awarded the "Best GDS for South India" 2016</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2016/jury choice award logo - 2016.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport Wins the Most Innovative Travel Technology Partner Award 2016</p>
                </div>
            </div>
        </div>
        <div class="award-year">
            <h4>2015</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/india travel awards north 2015.png';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport awarded the "Best GDS for North India" 2015</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/India Hospitality Awards 2015.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>Travelport Rooms and More awarded the Best Technology Provider in India Hospitality Awards,
                        South & West 2015</p>

                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/great place to work 2015.png';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>InterGlobe amongst India's Best Companies to Work For 2015</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/India Travel Awards-west-2015.png';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport awarded the "Best GDS for West India" 2015</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/india travel awards east 2015.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport awarded the "Best GDS for East India" 2015</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/TTG Travel Awards 2015.png';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport wins the 2015 TTG Asia Travel Award for being the Best GDS</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/tnh 2015.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>InterGlobe wins Most Outstanding Travel Technology Provider in TnH Award in 2015</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2015/travel-awards-img 2015.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport Galileo awarded the "Best GDS for South India" at the India Travel Awards 2015</p>
                </div>
            </div>
        </div>
        <div class="award-year">
            <h4>2014</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2014/south-award-2014.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>"Travelport Galileo awarded the "Best GDS for South India" 2014</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2014/ttg-2014.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport wins the 2014 TTG Asia Travel Award</p>

                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2014/great place to work 2014.png';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>InterGlobe amongst India's Best Companies to Work For 2014</p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2014/taai-2014.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>Travelport Galileo wins 'Best GDS of the Year' award 2014 at TAAI Travel Awards</p>
                </div>
            </div>
        </div>
        <div class="award-year">
            <h4>2013</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2013/great place to work-2013.png';?>" />
                    <p>&nbsp;</p>
                    <p>InterGlobe amongst India's Best Companies to Work For 2013</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2013/taai-2013.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport Galileo wins 'Best GDS of the Year' award 2013 at TAAI Travel Awards</p>

                </div>

            </div>
        </div>
        <div class="award-year">
            <h4>2012</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2012/trophy-2012.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport named Asia-Pacific's Best GDS at TTG Awards for the fourth year in a row!</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <p>&nbsp;</p>
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2012/great place to work-2012.png';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>InterGlobe amongst India's Best Companies to Work For 2012</p>

                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <p>&nbsp;</p>
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2012/taai-2012.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport Galileo wins 'Best GDS of the Year' award 2012 at TAAI Travel Awards</p>

                </div>
            </div>
        </div>
        <div class="award-year">
            <h4>2011</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <p>&nbsp;</p>
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2011/great place to work-2011.png';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>InterGlobe amongst India's Best Companies to Work For 2011</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2011/TTG-logo-2011.jpg';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport named Asia-Pacific's Best GDS at TTG Awards for the third year in a
                        row!</p>

                </div>

            </div>
        </div>
        <div class="award-year">
            <h4>2010</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <p>&nbsp;</p>
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2010/bestcompanies-2010.gif';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>Great Place to Work 2010</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2010/TTG-2010.jpg';?>" />
                    <p>Travelport wins the 2010 TTG Asia Travel Award</p>

                </div>

            </div>
        </div>
        <div class="award-year">
            <h4>2009</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <p>&nbsp;</p>
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2009/great place to work-2009.png';?>" />
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                    <p>InterGlobe amongst India's Best Companies to Work For 2009</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2009/TTG Awards 2009.gif';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport wins the 2009 TTG Asia Travel Award</p>

                </div>

            </div>
        </div>
        <div class="award-year">
            <h4>2008</h4>
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <p>&nbsp;</p>
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2008/great place to work-2008.png';?>" />
                    <p>&nbsp;</p>
                    <p>Great Place to Work 2008</p>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-6 award">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/awards/year-2008/TravelWeekly 2008.gif';?>" />
                    <p>&nbsp;</p>
                    <p>Travelport Galileo Named 'Best GDS' Asia for Second Year Running</p>

                </div>

            </div>
        </div>
    </div>
</div>
                            </div>
                            </div>
                            </div>

<!--  our business -->
    <div class="container business">
        <h3 class="blue-text">Our Business</h3><br />
        <div class="business-div">
            <div class="row business-row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Enterprises</h5>
                        <p>InterGlobe Enterprises is a large Indian conglomerate holding leadership positions...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Aviation</h5>
                        <p>IndiGo commenced operations in August, 2006 with a single aircraft and is today India's...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Hotels</h5>
                        <p>InterGlobe Hotels, a joint venture between InterGlobe Enterprises and Accor Hotels was...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Technology Quotient</h5>
                        <p>InterGlobe Technology Quotient (ITQ), is an official distributor of Travelport in 6
                            markets...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
            </div>
            <div class="row business-row">
                <!-- <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Technologies</h5>
                        <p>InterGlobe Technologies is a leading provider of IT, BPM and Digital Services & Solutions...</p>
                        <a class="next" href=""><img src="/assets/next.png';?>" width="40" /></a>
                    </div>
                </div> -->
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Air Transport</h5>
                        <p>Since 1989, InterGlobe Air Transport (IGAT) has assisted the world's leading airlines...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>CAE Simulation Training Private Limited</h5>
                        <p>CAE Simulation Training Private Limited (CSTPL), is a joint venture between...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>SAME (InterGlobe Education)</h5>
                        <p>School for Aircraft Maintenance Engineering (SAME) is a DGCA approved school offering...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Real Estate</h5>
                        <p>InterGlobe Real Estate, new business venture of InterGlobe Enterprises started operations...</p>
                        <a class="next" href=""><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
            </div>
            <!-- <div class="row business-row">
                <div class="col-lg-3 col-md-3 col-sm-3">
                    <div class="individual">
                        <img src="/assets/interglobe_logo.png';?>" width="85" />
                        <h5>InterGlobe Real Estate</h5>
                        <p>InterGlobe Real Estate, new business venture of InterGlobe Enterprises started operations...</p>
                        <a class="next" href=""><img src="/assets/next.png';?>" width="40" /></a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>