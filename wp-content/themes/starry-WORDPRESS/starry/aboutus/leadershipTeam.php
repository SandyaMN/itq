<?php
/**
 * Template Name: leadershipTeam
 */

get_header(); ?>
    <!--<div id="header"></div> --> 

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet';?>">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>" type="text/javascript"></script>
    <!--<script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>    
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>-->
        
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="bold">About Us</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="about company-profile"><a href="<?php echo get_home_url(); ?>/company-profile/">Company Profile</a></li>
                        
                        <li class="itqJourney"><a class="about" href="<?php echo get_home_url(); ?>/itqjourney/">ITQ Journey </a></li>
                        
                        <li class="awards"><a class="about " href="<?php echo get_home_url(); ?>/awards">Awards</a></li>
                        <li class="csr"><a class="about " href="<?php echo get_home_url(); ?>/csr/">CSR</a></li>
                        <li class="blogs"><a class="about " href="">Blogs</a></li>
                        <li class="events"><a class="about " href="<?php echo get_home_url(); ?>/events/">Events</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="leadershipTeam" class="tab-pane fade in active">
<p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/company-profile/">About Us</a> / <a class="current-page">Leadership Team</a></p>
<div class="team">
    <h3>Leadership Team</h3>
    <div class="about clear-fix">
        <div class="row team-row clear-fix">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="details">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/Anil-Parashar.png';?>" />
                    <p class="name">Anil Parashar</p>
                    <p class="designation">President and CEO,</p>
                    <p class="company">InterGlobe Technology Quotient</p>
                    <div class="linkedIn">
                        <a href="https://www.linkedin.com/in/anil-parashar-061634/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/linkedIn.png';?>" /></a>
                    
                    </div>
                    <button type="button" class="btn btn-lg read-more" data-toggle="modal" data-target="#anilParashar">View
                        Details</button>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="details">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/Sandeep-Dwivedi.png';?>" />
                    
                    

                    <p class="name">Sandeep Dwivedi</p>
                    <p class="designation">Chief Operating Officer,</p>
                    <p class="company">InterGlobe Technology Quotient</p>
                    <div class="linkedIn">
                        <a href="https://www.linkedin.com/in/sandeep-dwivedi-3013416/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/linkedIn.png';?>" /></a>
                    
                    </div>
                    
                    <button type="button" class="btn btn-lg read-more" data-toggle="modal" data-target="#sandeepDwivedi">View
                        Details</button>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="details">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/Prashant-Deo-Singh.png';?>" />
                    <p class="name">Prashant Deo Singh</p>
                    <p class="designation">Head- Human Resources,</p>
                    <p class="company">InterGlobe Technology Quotient</p>
                    <div class="linkedIn">
                        <a href="https://in.linkedin.com/in/prashant-deo-singh-5241902" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/linkedIn.png';?>" /></a>
                    
                    </div>
                    <button type="button" class="btn btn-lg read-more" data-toggle="modal" data-target="#prashant">View
                        Details</button>
                </div>
            </div>
        </div>
        <div class="row team-row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="details">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/Mohammad-Mujtaba.png';?>" />
                    <p class="name">Mohammad Mujtaba</p>
                    <p class="designation">Chief Innovation Officer,</p>
                    <p class="company">InterGlobe Technology Quotient</p>
                    <div class="linkedIn">
                        <a href="https://www.linkedin.com/in/mohammad-mujtaba-05002998/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/linkedIn.png';?>" /></a>
                    
                    </div>
                    <button type="button" class="btn btn-lg read-more" data-toggle="modal" data-target="#mujtaba">View
                        Details</button>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="details">
                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/Anil-Porter.png';?>" />
                    <p class="name">Anil Porter</p>
                    <p class="designation">Chief Technology Officer,</p>
                    <p class="company">InterGlobe Technology Quotient</p>
                    <div class="linkedIn">
                        <a href="https://www.linkedin.com/in/anil-porter-5a83324/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/linkedIn.png';?>" /></a>
                    
                    </div>
                    <button type="button" class="btn btn-lg read-more" data-toggle="modal" data-target="#anilPorter">View
                        Details</button>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <!-- <div class="details">
                            <img src="../../assets/about-us/Munish-Gupta.png" />
                            <p class="name">Munish Gupta</p>
                            <p class="designation">Principal Finance Officer,</p>
                            <p class="company">InterGlobe Technology Quotient</p>
                            <a class="read-more">View Details</a>
                        </div> -->
            </div>
        </div>
    </div>

    <div class="modal fade" id="anilParashar" role="dialog">
        <div class="modal-dialog w-50 ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title leader-title name">Anil Parashar</h2>
                    <h4 class="leader-title designation"> President & CEO, InterGlobe Technology Quotient</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Anil Parashar has extensive operational and financial experience in handling various aspects of
                        business including raising finances, capital restructuring, mergers & acquisitions, statutory
                        compliances, investor relations and long term planning. </p>
                    <p> Prior to President and CEO of InterGlobe Technology Quotient (ITQ), he was Group CFO at
                        InterGlobe Enterprise, where he led transformational transactions including the Worldspan
                        distribution rights in India. He successfully demonstrated his financial acumen by leading
                        InterGlobe through various stages of its evolution where he was responsible for providing
                        strategic and financial leadership to all the Group businesses.</p>
                    <p> He is a member of Group’s Executive Committee and sits on the board of several InterGlobe
                        companies, including ITQ and IndiGo.</p>
                    <p> Anil is credited with over 30 years of rich experience including leadership positions at Swiss
                        Air, Asbestos Cement Company and Delhi Express Travels. He is also a representative on PHD
                        Chamber of Commerce, ASSOCHAM and FICCI Forums on Taxation and Travel & Tourism.</p>
                       <p> He holds a professional degree in Chartered Accountancy and is a graduate in Economic (Hons.)
                        from Delhi University.</p>

                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" id="sandeepDwivedi" role="dialog">
        <div class="modal-dialog w-50 ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title leader-title name">Sandeep Dwivedi</h2>
                    <h4 class="leader-title designation"> Chief Operating Officer, InterGlobe Technology Quotient</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Sandeep Dwivedi is Chief Operating Officer at InterGlobe Technology Quotient Pvt Ltd (ITQ), an
                        official distributor of Travelport in 6 markets across Asia Pacific region including India, Sri
                        Lanka. In his current role, he spearheads overall responsibility of ITQ's growing operations
                        including strategic business development and implementing growth strategies. His focus is on
                        building a strong, customer-focused organization, while increasing efficiency across all
                        aspects of operations. He is responsible for Products; E-commerce & Innovation; Network & IT
                        infrastructure; Relationship Management; Marketing; Distribution; Business Excellence; Planning
                        & Strategy verticals.</p>

                       <p> Having previously led the position as Chief Commercial Officer at ITQ and served in InterGlobe
                        group companies in different capacity, Sandeep has demonstrated spectacular leadership
                        competency with company's growth in the market. With over 25 years in executive management and
                        business supervisory roles, he led commercial strategy and business growth in the organization.
                        He has had a successful track record of providing strong corporate leadership, management and
                        direction with a keen understanding of applying tactical business and technological strategies
                        that create new business offerings, advanced capabilities, market differentiation and
                        competitive advantages.</p>

                        <p> Prior to this, he has worked with Business Standard as head of finance, accounts and
                        administration. He has had the pleasure to work in various cross functional industries ranging
                        from manufacturing to consulting to trading.</p>

                        <p> He holds a bachelor's degree in Commerce (B.Com) from University of Rajasthan and is also a
                        Chartered Accountant. With an experience as rich & diverse as his, he brings with him a rich
                        pool of knowledge and experiences from various sectors.</p>

                        <p> Adventurous by nature, Sandeep likes to explore road journeys and quite passionate about
                        vintage cars.</p>

                   
                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" id="prashant" role="dialog">
        <div class="modal-dialog w-50 ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title leader-title name">Prashant Deo Singh</h2>
                    <h4 class="leader-title designation"> Head- Human Resources</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Prashant is the head of Human Resources at ITQ and governs the Organizational Development &
                        People Agenda. He brings with him rich industry experience of more than two decades in
                        Strategic Human Resources, building businesses and making cultural transformation across India,
                        Africa, Middle East & GCC in various sectors such as manufacturing, retail business, banking,
                        telecom, IT and hospitality. Prior to joining ITQ, he held corporate HR leadership position at
                        Sharaf DG in UAE.</p>

                        <p>  Prashant holds a degree in BA (Hons) Political Science from Delhi University and a Post
                        Graduate degree in Personnel Management & Industrial Relations with specialization in HRD from
                        Xavier Institute of Social Service (XISS). </p>

                        <p> Besides his strong passion for fostering organisational growth, Prashant is fond of travelling,
                        reading and driving.</p>
                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" id="mujtaba" role="dialog">
        <div class="modal-dialog w-50 ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title leader-title name">Mohammad Mujtaba</h2>
                    <h4 class="leader-title designation"> Chief Innovation Officer</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Mohammad Mujtaba is the Chief Innovation Officer and is responsible for software development
                        and E commerce support functions. In his current role, Mujtaba is primarily responsible for end
                        to end solutioning & enhancing customer experience in the OTA space.</p>

                     <p>   Mujtaba brings in 15 years of extensive experience in leading E commerce, multi channel
                        marketing, sales & operations across travel organisations including Indian Airlines and
                        Amadeus. He has been a key professional in the travel industry and has worked across various
                        GDSs. He's had an entrepreneurial stint too and founded a technology consultancy start up based
                        out of Gurgaon. He has been invaluable in migrating Dnata into online space & achieving
                        complete business model transformation in online space.</p>
                        <p> Fond of adventure, Mujtaba loves hitch hiking and draws inspiration from listening to classical
                        music & resides in Gurgaon with his family.

                    </p>
                </div>

            </div>

        </div>
    </div>

    <div class="modal fade" id="anilPorter" role="dialog">
        <div class="modal-dialog w-50 ">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h2 class="modal-title leader-title name">Anil Porter</h2>
                    <h4 class="leader-title designation"> Chief Technology Officer</h4>
                </div>
                <div class="modal-body">
                    <p>
                        Anil Porter is the Head- IT & GDS Services at InterGlobe Technology Quotient Pvt Ltd. In his
                        current role, Anil brings in focus on service delivery, products & IT Infrastructure thereby
                        capitalizing resources and creating efficiencies in the organization on a larger scale.</p>

                       <p>  Prior to this he has worked in various industries like Electronic media, Banking, Aviation and
                        Travel and comes with an experience as vast as 21 years. Previously, he has worked as System
                        Architect at Sahara Net for over 7 years and was instrumental in setting up Asia's largest VSAT
                        Network, World's largest private emailing system & India's largest leased line VPN based
                        network for Travelport Galileo in India.</p>

                        <p> Anil is the winner of India's Best CIO 2016 from the CIO association of India and holder of
                        NEXT100 2015 Award as one of future Indian CIOs and Most Innovative CIOs/CTOs of India 2016. He
                        is a graduate in Electronics and Telecom. Anil loves to travel uphill and explore new places.
                        He lives in NCR, India with his family.</p>

                        <p> He holds a professional degree in Chartered Accountancy and is a graduate in Economic (Hons.)
                        from Delhi University.</p>
                
                </div>

            </div>

        </div>
    </div>

</div>
</div>
</div>
</div>
<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>