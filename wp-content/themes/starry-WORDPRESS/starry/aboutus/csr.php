
<?php
/**
 * Template Name: csr
 */

get_header(); ?>
    <!--<div id="header"></div> --> 

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet';?>">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>" type="text/javascript"></script>

 <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="bold">About Us</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="about company-profile"><a href="<?php echo get_home_url(); ?>/company-profile">Company Profile</a></li>
                        <li class="leadershipTeam"><a class="about " href="<?php echo get_home_url(); ?>/leadershipteam/">Leadership Team</a></li>
                        <li class="itqJourney"><a class="about" href="<?php echo get_home_url(); ?>/itqjourney/">ITQ Journey </a></li>
                       
                        <li class="awards"><a class="about " href="<?php echo get_home_url(); ?>/awards">Awards</a></li>
                        <li class="active csr"><a class="about " href="<?php echo get_home_url(); ?>/csr/">CSR</a></li>
                        <li class="blogs"><a class="about " href="">Blogs</a></li>
                        <li class="events"><a class="about " href="<?php echo get_home_url(); ?>/events/">Events</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="awards" class="tab-pane fade in active">
<p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/company-profile">About Us</a> / <a class="current-page">ITQ CSR</a></p>

<div class="subheading-us">
    <div class="container">
        <div class="subheading-menu">
            <ul class="nav sub nav-tabs">
                <li class="active"><a data-toggle="tab" href="#csrActivities">CSR Activities
                    </a>
                </li>
                <li><a data-toggle="tab" href="#csrPolicy">CSR Policy</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="container subheading-tabs">
    <div class="tab-content">
        <div id="csrActivities" class="tab-pane fade active in">
            <div class="csr-tab">
                <h3>What drives us..</h3>
                <!-- <h6>Overview (What drives us / Belief / Objective)</h6> -->
                <p>Corporate Social Responsibility (CSR) is a broad term used to describe a company's efforts to
                    improve
                    society in some way. These efforts can range from donating money to nonprofits to implementing
                    environmentally-friendly
                    policies in the workplace. CSR is important for companies, nonprofits, and employees alike.
                    Likewise,
                    here at ITQ we look forward to employee engagement and encourage our employees to contribute to the
                    society
                    in any which way they like. It may not only be monetary, but supporting the needy either by
                    donation,
                    help and support, spending time with the children & elderly, monetary support or just involving
                    themselves
                    into basic art and craft. The list is endless and there are way too many options for the employees
                    to
                    choose from. Every month/quarter, various different activities are run across for employees to
                    choose
                    from based on their time and interest. Some of the major CSR initiatives which ITQ has been
                    following
                    over the last few years are as under:
                </p>
                <div class="container">
                    <!-- ========= -->
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>ITQ lends a helping hand to Friendicoes</h5>
                                <a class="read-more" target="blank" href="http://www.itq.in/%20cms_uploads/ITQ%20lends%20a%20helping%20hand%20to%20Friendicoes.jpg">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/ITQ-lends-a-helping.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>Blood Donation Camp at ITQ, Gurgaon</h5>
                                <p>Giving blood is quick, easy and it saves lives. InterGlobe Technology Quotient in
                                    collaboration
                                    with E-meditek organized a blood donation camp
                                </p><br />
                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/Blood%20Donation%20at%20Gurgaon%20office_October%202016.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/BloodDonationOctober.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>De-Stress Camp & Health Check Up at Apeejay House-Mumbai</h5>
                                <p>InterGlobe Technology Quotient Mumbai team arranged a De-stress camp and health
                                    check-up
                                    for ITQ and for other offices in premises of Apeejay House, Mumbai
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/De-stress%20camp%20and%20health%20check-up%20at%20Appejay%20House%20-Mumbai_October.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/DestresscampMumbaiOctober.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>"Each One Teach One"</h5>
                                <p>As part of the Joy of Giving festival, InterGlobe Technology Quotient in
                                    collaboration
                                    with Uday Foundation organized an activity of Each one Teach One
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/Each%20One%20Teach%20One%20at%20Uday%20Foundation_October.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/EachOneTeachOneFoundationOctober.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>"HAMARA STATION HAMARI SHAAN"</h5>
                                <p>InterGlobe Technology Quotient Mumbai team got associated with ?MAD Foundation? and
                                    participated
                                    in ?HAMARA STATION HAMARI SHAAN? event.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/Hamara%20Station%20Hamari%20Shann%20with%20MAD%20Foundation_October.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/HamaraStationHamariShannOctober.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>Visit to Asha Bhavan by Team Trivandrum</h5>
                                <p>ITQ Trivandrum team visited Asha Bhavan, Vazhuthacaud,Trivandrum on 30th September
                                    2016
                                    as a part of i-Serve.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/Visit%20to%20Asha%20Bhavan%20by%20Team%20TRV.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/ashabhawan.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>A Visit to Old Age Home & Carnival for Children by ITQ, Kolkata</h5>
                                <p>InterGlobe Technology Quotient Kolkata office collaborated with an NGO in Kolkata
                                    that
                                    takes care of both Children & Old Age Home.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/CSR%20at%20ITQ%20Kolkata_September.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/CSRITQKolkataSeptember.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>CSR with Blind Relief Association</h5>
                                <p>With our endeavor to help the pupils of Blind Relief Association we assisted the
                                    blind
                                    individuals in preparing their artifacts for their Diwali Mela in Aug & Sep 2016.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/CSR-Blind%20Relief%20Association_August%202016.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/CSR-Blind-August-2016.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>ITQ Guwahati team participated in Walk for Swachh Rail Swachh Bharat campaign</h5>
                                <p>The Guwahati team of ITQ participated in 'walkathon' on December 2015 in order to
                                    support
                                    the Swachh Rail Swachh Bharat campaign initiated by North East Frontier Railway.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/ITQ%20Guwahati%20team%20participated%20in%20Walkathon%20to%20support%20Swachh%20Rail%20Swachh%20Bharat%20campaign.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/thumbnail.png';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>ITQ Delhi Team organizes a cricket match for blind kids</h5>
                                <p>The Delhi team of InterGlobe Technology Quotient had organized a cricket match for
                                    blind
                                    kids on October 2015.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/ITQ%20Delhi%20Team%20organizes%20a%20cricket%20match%20for%20blind%20kids.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/ITQ Delhi Team organizes a cricket match for blind kids.png';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>ITQ takes on tree plantation initiative at Aravali Biodiversity Park</h5>
                                <p>InterGlobe Technology Quotient initiated a project to plant trees in Aravali
                                    Biodiversity
                                    Park, Gurgaon, on September 2015.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/ITQ%20takes%20on%20tree%20plantation%20initiative%20at%20Aravali%20Biodiversity%20Park.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/ITQ GurgaonTeam undertakes a tree plantation project ar Aravalli.png';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>ITQ Chennai Team reaches out to the physical and mentally challenged children</h5>
                                <p>Our Chennai Team offered help to the young children at various stages of physical
                                    and
                                    mental disabilities in Mithra on July 2015.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/ITQ%20Chennai%20Team%20reaches%20out%20to%20the%20physical%20and%20mentally%20challenged%20children.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Day two.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>ITQ Noida Team undertakes a tree plantation project</h5>
                                <p>The Noida team of InterGlobe Technology Quotient participated in the afforestation
                                    activity
                                    by planting trees in certain key areas of the city on March 2015.
                                </p><br />

                                <a class="read-more" target="blank" href="http://www.itq.in/cms_uploads/ITQ%20Noida%20Team%20undertakes%20a%20tree%20plantation%20project.pdf">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/ITQ Noida Team undertakes a tree plantation project.png';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>InterGlobe Foundation and the Aga Khan</h5>
                                <p>InterGlobe Foundation and the Aga Khan Trust for Culture to collaborate on
                                    conservation of Suﬁ poet Abdul Rahim Khan I Khanan's Tomb</p><br />

                                <a class="read-more" target="blank" data-toggle="modal" data-target="#aga-khan">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/aga-khan-img.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                            <div class="row">
                                <h5>I Serve National Event</h5>
                                <p>This Independence day InterGlobe Technology Quotient alongwith its travel partners
                                    undertook a very unique initiative.</p><br />

                                <a class="read-more" target="blank" data-toggle="modal" data-target="#serve-nation">Read
                                    More..
                                </a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/serve-nation.jpg';?>" />
                        </div>
                    </div>
                    <hr class="initiative-bottom-line">
                    </hr>
                    <div class="row csr-initiative clear-fix">
                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                                <div class="row">
                                    <h5>I Serve @ InterGlobe Technology Quotient East 13-14</h5>
                                    <p>At 'SMILE' we along with some of the TAFI agents went and spent time with the children out there and interacted with them through quiz sessions, general discussion, got some of them participated in different activities.</p><br />
    
                                    <a class="read-more" target="blank" data-toggle="modal" data-target="#serve-east">Read
                                        More..
                                    </a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                                <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-east.jpg';?>" />
                            </div>
                        </div>
                        <hr class="initiative-bottom-line">
                        </hr>
                        <div class="row csr-initiative clear-fix">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                                    <div class="row">
                                        <h5>Blood Donation Camp</h5>
                                        <p>Blood Donation Camp was organised jointly with InterGlobe Technology Quotient and TAFI East chapter</p><br />
        
                                        <a class="read-more" target="blank" data-toggle="modal" data-target="#blood-camp">Read
                                            More..
                                        </a>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/blood-donation.jpg';?>" />
                                </div>
                            </div>
                            <hr class="initiative-bottom-line">
                            </hr>
                            <div class="row csr-initiative clear-fix">
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 csr-left">
                                        <div class="row">
                                            <h5>I Serve @ InterGlobe Technology Quotient West 13-14</h5>
                                            <ul>
                                                <li>Collectively 42 days of service</li>
                                                <li>Activities</li>
                                                <li>Blood Donation Camp</li>
                                                <li>Don Bosco Yuva Sanstha</li>
                                                <li>Nirmalya Trust</li>
                                                <li>Helpers of Mary</li>
                                                <li>Environment- Cleaning of Sanjay Gandhi National Park</li>

                                            </ul><br/>
                                            <a class="read-more" target="blank" data-toggle="modal" data-target="#serve-west">Read
                                                More..
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 csr-right">
                                        <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-west.jpg';?>" />
                                    </div>
                                </div>
                                <hr class="initiative-bottom-line">
                                </hr>
                </div>
            </div>
        </div>
        <div id="csrPolicy" class="tab-pane fade">
            <h5>CORPORATE SOCIAL RESPONSIBILITY (CSR) POLICY</h5>
            <p>OF INTERGLOBE TECHNOLOGY QUOTIENT PRIVATE LTD.</p>
            <br/>
            <p>This CSR Policy is formulated in compliance with Section 135 (3) (a) of the Companies Act, 2013 read with Companies (Corporate Social responsibility  Policy) Rules, 2014 and has been approved by the Board of Directors of InterGlobe Technology Quotient  Private Ltd. {"InterGlobe" or "Company").</p>
            <br/>
            <p class="paragraph-heading">Philosophy:</p>
            <p>InterGlobe  has been  inspired  by  the  vision  to  serve a larger  national  purpose.  InterGlobe identifies  its core values as: highest standards of integrity, respecting  its customers'  interest, and future-mindedness. InterGlobe  is committed to  being  a good  corporate citizen  as it is believed that this would  help it to achieve its goals and objectives that  it has set for itself and build a sustainable business for its current  and future  stakeholders. A conscious strategy will be followed  to design and implement CSR programs under the CSR Policy to create shared values with its stakeholders.</p>
            <br/>
            <p>InterGlobe's  vision is to contribute to the social and economic development of communities  in which  it operates  and  to  participate   in  providing  education,  skill  development, conserving environment  and promoting national heritage.</p>
            <br/>
            <p class="paragraph-heading">Implementation process: Identification of projects I processes:</p>
            <p>All CSR  activities  undertaken  by InterGlobe  will follow  certain  rules and procedures  to ensure that the chosen programs reach the intended target and create tangible impact. The process for identification and implementation of a program here will include:</p>
            <br/>
            <ul>
                <li>Identification of priority areas</li>
                <li>Stakeholder identification</li>
                <li>Due diligence of implementation partner</li>
                <li>Project preparation/ project evaluation</li>
                <li>Budgeting</li>
                <li>Project implementation and Review</li>

            </ul>
            <br/>
            <p>A program picked up for implementation by a company will be cleared by the CSR Committee of the company and will be in line with  this CSR Policy. The Company may carry out CSR activity either itself or through lnterGlobe Foundation, an associate company formed  under Section 8 of the Companies Act, 2013 or through  any other  registered trust, registered  society, registered
                    foundation, etc.</p>
                    <br/>
                    <p class="paragraph-heading">Selecting CSR Program:</p>
                   <p>The Company shall, while  selecting a CSR activity, give preference  to the  local area and areas around which it operates in. For an activity to be included by the Company as its CSR activity, it should be relating  to any one or more of the following  activities, as listed in the Schedule VII to the Companies Act, 2013:</p>
                    <br/>
                    <ol>
                        <li> Promotion  of education;</li>
                        <li> Promoting gender equality and empowering women;</li>
                        <li> Conducting workshop and training programs for women;</li>
                        <li> Providing Education to under privileged children;</li>
                        <li> Focus on educating the girl child and the underprivileged by providing appropriate infrastructure, and groom them as future value creators;</li>
                        <li> Empowerment of women for education, health & self-employment;</li>
                        <li> Conserving National Heritage and Environment;</li>
                        <li> Measures for the benefit of armed forces veterans, war widows and their dependents;</li>
                        <li> To provide  training  to  promote rural  sports, nationally  recognized  sports, and Para­ Olympic sports and Olympic sports;</li>
                        <li> To contribute to the Prime Minister's National Relief Fund or any other  fund set up by the  Central Government  or  the  State Governments  for  socio-economic  development and relief  and welfare  of the Scheduled Castes, the Scheduled Tribes, other  backward classes, minorities and women;</li>
                        <li> To provide  contributions or  funds  provided  to  technology  incubators  located  within academic institutions which are approved by the Central Government;</li>
                        <li> To contribute in rural development projects.</li>

                    </ol>
                    <br/>
                    <p>The program  to  be followed will clearly set out the roles and responsibilities  for  the involved personnel and also list out  the  role for third  parties if they  are to be engaged. A schedule of implementation shall be drawn clearly outlining the timelines for the activities involved  therein.</p>
                    <br/>
                    <p class="paragraph-heading">Schedule tor implementation of activities</p>
                    <br/>
                    <table border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td class="no">S. No.</td>
                                    <td class="particular">Particular &nbsp;of activities</td>
                                    <td class="proposed">Proposed Timelines for the<br>
                                    Activity.</td>
                                </tr>
                                <tr>
                                    <td class="no">1.</td>
                                    <td class="particular">&nbsp;</td>
                                    <td class="proposed">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="no" style="width:80px;height:28px;">2.</td>
                                    <td class="particular">&nbsp;</td>
                                    <td class="proposed">&nbsp;</td>
                                </tr>
                            </tbody>
                        </table>
                        <br/>
                        <p class="paragraph-heading">Compliance, monitoring and reporting:</p>
                        <p>Monitoring  and  evaluation   is  an  essential  part  of  any  activity   that   is  undertaken   by  an organization  and that  will  also be followed  for  programs  undertaken  under  the  CSR  Policy.</p>
                        <br/>
                        <p>Programs under  the CSR  Policy will be continuously  monitored on set parameters  by the CSR Committee of the Company.</p><br/>
                        <p>Compliance  will   be  reported to  stakeholders  through   the  Company's  Annual  Report.  CSR Committee   of  the  Company  shall  ensure  that   programs  under   the  CSR   Policy  comprise essentially of CSR activities undertaken  within India.</p><br/>
                        <p>The CSR Policy will be reviewed, unless earlier required  due to change in laws, annually by the Board of lnterGlobe  and updated  if required, to ensure it captures the relevant  current  interest of the stakeholders.</p>
                        <br/>
                        <p>CSR Committee  of the Company will be responsible for ensuring that  the principles  set out in this  CSR  Policy  are  communicated to,  understood  and  observed  by  all  employees  and  for ensuring compliances in letter  and spirit.</p>
                        <br/>
                        <p class="paragraph-heading">Budget:</p>
                        <p>Budget recommended for  a program  by CSR Committee  will  be set out  in the program  to be approved by the Board of the Company. This would also consider the aspect of training element as well. The Board of Directors of lnterGlobe has approved to incur 2% of the average net profits of  the  Company made  during  the  three  immediately  preceding  financial  years for  programs covered under  the CSR  Policy. While  the  allocated amount  would  be 2% of the  average net profits, any income arising from the unutilized sums of such net profits  and surplus arising out of CSR activities undertaken under a program will be the corpus for the program.</p>
                        <br/>
                        <p class="paragraph-heading">Partnerships:</p>
                        <p>It  will  be  endeavor  of  the  Company  to  first  undertake  CSR  initiatives   through   InterGlobe Foundation  to  the  extent  possible. However,  other  collaborative  partnerships  for implementation  of  any  programs  under  the  CSR   Policy  may  be  undertaken   with  certain identified  Non-Government Organizations and other  like-minded stakeholders  including  non­ profit making trusts following the legal requirements  as applicable from time to time. This would help  widen  the  Company's  reach  and  leverage  upon  the  collective expertise,  wisdom  and experience that these partnerships bring to the table.</p>
                        <br/>
                        <p class="paragraph-heading">Management Commitment:</p>
                        <p>InterGlobe  subscribes to the philosophy  of compassionate care. It believes and acts on ethos of generosity  and compassion, characterized  by a  willingness  to  build  a society  that  works  for everyone. This is the cornerstone  of this CSR policy.</p>
                        <br/>
                        <p>It shall always be ensured by us that the surplus arising out of the CSR activities  does not form part  of  business profits   and  is utilized for  the  purpose  it is meant.  Surplus  funds  may  be contributed  to   InterGlobe   Foundation   towards   its  corpus.  In  extreme  circumstance,  the Company may consider contributing to  outside channels for the objectives  that  it perceives it should be part of but is unable to pursue in house.
                            </p><br/>
                            <p class="paragraph-heading">Communication:</p>
                            <p>The CSR Committee  of the Company may also lay out the manner, subject to the relevant legal requirements, in which information on a program being implemented by the Company will be shared. The programs under implementation would be hoisted on the websites of the Company i.e. www.interglobe.com and other group companies.</p>
                        <br/>        
                        <p>This CSR Policy conforms to the Companies {Corporate Social responsibility  Policy) Rules, 2014, spelt out by the Ministry of Corporate Affairs, Government of India.</p>
                            <br/>
                    </div>
    </div>

    <div class="popup-div">

        <div class="modal fade" id="aga-khan" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <!-- <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Modal Header</h4>
                </div> -->
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!-- <p class="path"><a href="home.html">Home</a> / <a href="aboutUs.html">About Us</a> / <a href="">ITQ
                                CSR
                            </a> / <a href="">CSR Activities</a> / <a class="current-page">InterGlobe Foundation and
                                the Aga Khan </a></p>
 -->
                        <div class="">
                            <div class="csr-activities clear-fix">
                                <h5>InterGlobe Foundation and the Aga Khan</h5><br/>
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 f-left">
                                        <img alt="Foundation" class="f-left" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-1.jpg';?>">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 f-right">
                                        <p>InterGlobe Foundation and the Aga Khan Trust for Culture to collaborate on
                                            conservation of Suﬁ poet Abdul Rahim Khan I Khanan's Tomb.India's heritage
                                            lives through its rich architecture, culture and crafts ? but these are
                                            fading
                                            with time. InterGlobe Foundation, the philanthropic arm of InterGlobe and
                                            the Aga Khan Trust for
                                            Culture (AKTC) announced their collaboration to conserve and restore Abdul
                                            Rahim
                                            Khan I Khanan's tomb, a monument owned and protected by the Archaeological
                                            Survey
                                            of India. This is one of the few conservation initiatives that will be
                                            undertaken in collaboration with a corporate at any of India's nationally
                                            protected
                                            monuments.</p>
                                    </div>

                                </div><br />
                                <div class="row">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 f-left">
                                        <p>The tomb sits prominently along the Mathura Road, formerly the Mughul Grand
                                            Trunk
                                            Road, and is in close proximity to the Dargah of Hazrat Nizammudin Auliya
                                            in South
                                            Delhi.Popularly known as Rahim and immortalized through his dohas's or
                                            couplets, Rahim was
                                            amongst the most important ministers in Akbar's court. He was one of the
                                            Navratnas and
                                            continued to serve Salim after his accession to the throne as Emperor
                                            Jahangir.
                                            Besides being a strong administrator and military commander, Rahim was a
                                            great
                                            scholar and poet. He wrote verses in Turkish, Arabic, Sanskrit and Persian,
                                            translated the Baburnama in Persian and authored several prose works,
                                            including
                                            two books on astrology. The mausoleum was built by Rahim for his wife,
                                            making this
                                            the ﬁrst ever Mughal tomb built for a woman. Rahim was buried here in AD
                                            1627.</p>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 f-right">
                                        <img alt="Foundation" class="f-right" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-2.jpg';?>">
                                    </div>
                                </div><br />
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 f-left">
                                        <img alt="Foundation" class="f-left" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-3.jpg';?>">
                                    </div>
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 f-right">
                                        <p>The project, spread over three years, under the guidance and control of the
                                            Archaeological Survey of India, is
                                            proposed to include not only the conservation of the monument but also a
                                            compilation of Rahim's literary works. It will also enhance an
                                            understanding of
                                            the tomb structure and its setting thus allowing scholars to interpret its
                                            inﬂuence
                                            on the construction of the Taj Mahal as well as justify its inclusion
                                            within the
                                            extended Humayun's Tomb World Heritage Site. An important part of the
                                            project
                                            will be the craft-based approach ensuring a revival of craft skills and
                                            creating
                                            employment for craftsmen. Stone carvers, masons, plasterers, will work
                                            together
                                            to halt the deterioration process and ensure long-term preservation.</p>
                                    </div>

                                </div><br />
                                <div class="row">
                                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 f-left">
                                        <p>It is expected that atleast 250,000 man days of work ? directly and
                                            indirectly
                                            ? will be required to complete this assignment. The conservation works will
                                            be part of the Nizamuddin Urban
                                            Renewal initiative undertaken by the Aga Khan Trust for Culture in
                                            partnership
                                            with the Archaeological Survey of India and other government agencies. All
                                            conservation works will be undertaken very carefully by matching the
                                            original material/
                                            details in terms of form, colour and speciﬁcation preferably through the
                                            use of the
                                            same material and employing traditional skills as used in the original
                                            structure.
                                            All such works will be carefully recorded and documented. Elements of the
                                            project
                                            will also include employee volunteering opportunities which will give our
                                            people an
                                            opportunity to develop their creative skills and extend themselves beyond
                                            their
                                            familiar realm of work. InterGlobe Foundation and the Aga Khan Trust for
                                            Culture to collaborate on conservation of Suﬁ poet Abdul Rahim Khan I Khan</p>
                                    </div>
                                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 f-right">
                                        <img alt="Foundation" class="f-right" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-4.jpg';?>">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-5.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-6.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-7.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-8.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-9.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-10.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/Foundation-11.jpg';?>">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div> -->
                </div>
            </div>
        </div>

        <div class="modal fade" id="serve-nation" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">

                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <!-- <p class="path"><a href="home.html">Home</a> / <a href="aboutUs.html">About Us</a> / <a href="">ITQ
                                CSR </a> / <a href="">CSR Activities</a> / <a class="current-page">I Serve National
                                Event </a></p>
 -->

                        <div class="">
                            <div class="csr-activities clear-fix">
                                <h5>I Serve National Event</h5><br/>
                                <p>This Independence day InterGlobe Technology Quotient alongwith its travel partners
                                    undertook a very unique initiative. In their endeavour to reach and extend support
                                    to the underprivileged section of the society the first ever i Serve National Event
                                    for clothes collection was celebrated with great vigour and dedication. As part of
                                    this campaign boxes to collect used clothes were placed at their branch offices as
                                    well as partners' office locations across the country. This was a month long
                                    campaign to collect clothes at various locations which were subsequently donated to
                                    the affiliated NGO's at the grand NATIONAL event held on 15th August at various
                                    locations all across the country. This effort was entirely a result of team members
                                    of ITQ and their travel partners who participated in this campaign very actively
                                    and managed to donate generously for the cause.</p>
                                <br />
                                <p>Commenting on this initiative Mr. Anil Parashar, President and CEO, ITQ said " It
                                    was a great privilege for me to be a part of this activity along with our travel
                                    partners in achieving this task of contributing something back to the society.
                                    These initiatives are important aspects of being a good corporate citizen"</p>
                                <br />
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-1.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-4.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-5.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-6.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-7.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-8.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-9.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-10.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-11.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-12.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-2.jpg';?>">
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 foundation">
                                        <img alt="Foundation" class="" src="<?php echo get_template_directory_uri().'/assets/about-us/csr/National-Event-3.jpg';?>">
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="serve-east" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
    
                        <div class="modal-body">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <!-- <p class="path"><a href="home.html">Home</a> / <a href="aboutUs.html">About Us</a> / <a href="">ITQ
                                    CSR </a> / <a href="">CSR Activities</a> / <a class="current-page">I Serve @ ITQ West 13-14</a></p>-->
                            <div class="">
                                <div class="csr-activities clear-fix">
                                    <h5>I Serve @ ITQ West 13-14</h5><br/>
                                    <p>At 'SMILE' we along with some of the TAFI agents went and spent time with the children out there and interacted with
                                        them through quiz sessions, general discussion, got some of them participated in different activities.</p>
                                    <br />
                                    <p>AT ALL BENGAL WOMEN'S ASSOCIATION</p>
                                    <p>SOME OF US SPENT ONE HALF OF THE DAY WITH THE OLD PEOPLE FROM THE OLD AGE HOME LOCATED AT ALL BENGAL WOMEN'S
                                        ASSOCIATION PREMISES.</p>
                                    <p>SOME OF THEM ALSO VOLUNTARILY CAME FORWARD & PARTCIPATED IN CULTURAL ACTIVITIES LIKE SINGING, RECITATION ETC ALONG
                                        WITH COLLEAGUES FROM ..</p>
                                    <br />
                                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-east-big.jpg';?>" />
                                    <br /><br/>
                                    <h5>AT SMILE FOUNDATION</h5>
                                    <p>At 'SMILE' we along with some of the TAFI agents went and spent time with the children out there and interacted with
                                        them through quiz sessions, general discussion, got some of them participated in different activities.</p>
                                    <br />
                                    <p>This was followed with a magic show which was sponsored by TAFI and we also distributed food packets, old clothes,
                                        toys etc. which were collected over a period of one month from different travel agents, airlines and our colleagues
                                        here who had contributed for a cause.</p>
                                    <br />
                                    <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-east-img.jpg';?>" />
    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="blood-camp" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
        
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <!-- <p class="path"><a href="home.html">Home</a> / <a href="aboutUs.html">About Us</a> / <a href="">ITQ
                                        CSR </a> / <a href="">CSR Activities</a> / <a class="current-page">Blood Donation Camp</a></p> -->
        
        
                                <div class="">
                                    <div class="csr-activities clear-fix">
                                        <h5>Blood Donation Camp</h5><br/>
                                        <ul>
                                            <li>Blood Donation Camp was organised jointly with InterGlobe Technology Quotient and TAFI East chapter</li>
                                            <li>Around 80 participants had come for the camp from the travel fraternity including travel agents, airlines, leisure companies</li>
                                            <li>More than 60 people donated blood</li>
                                            <li>Healthy breakfast was organised post blood donation</li>

                                        </ul>
                                        <br />
                                        <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/blood-donation-big.jpg';?>" />
        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="serve-west" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
            
                                <div class="modal-body">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <!-- <p class="path"><a href="home.html">Home</a> / <a href="aboutUs.html">About Us</a> / <a href="">ITQ
                                            CSR </a> / <a href="">CSR Activities</a> / <a class="current-page">I Serve vat ITQ West 13 14</a></p>
            -->
                                    <div class="">
                                        <div class="csr-activities clear-fix">
                                            <h5>I Serve vat ITQ West 13 14</h5><br/>
                                            <ul>
                                                <li>Collectively 42 days of service</li>
                                                <li>Activities</li>
                                                <li>Blood Donation Camp</li>
                                                <li>Don Bosco Yuva Sanstha</li>
                                                <li>Nirmalya Trust</li>
                                                <li>Helpers of Mary</li>
                                                <li>Environment- Cleaning of Sanjay Gandhi National Park</li>
                                            </ul>
                                            <br />
                                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-west-big.jpg';?>" width="100%" />
                                            <p>&nbsp;</p>
                                            <ul>
                                                <li>Camp on the 6th of Sep 2013</li>
                                                <li>Collected 75 sachets of separated blood</li>
                                                <li>Yearly event we are having since 2012</li>

                                            </ul><br />
                                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-west-big1.jpg';?>"  width="100%" />
                                            <p>&nbsp;</p>
                                            <h5>Don Bosco Yuva Sanstha ? Karjat</h5>
                                            <ul>
                                                <li>Pioneered by the Vision of Fr. Xavier Devadas</li>
                                                <li>Is a ray of hope to many, with having successfully placed more than 600 underprivileged students in industries across Mumbai after training deserving candidates in welding, electrical and a/c mechanics</li>
                                                <li>We interacted with the boys and conducted a training on communication skills and basic etiquettes, to assist during interviews, shared clothes & sweets and raised funds internally for their education.</li>

                                            </ul><br />
                                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-west-big2.jpg';?>"  width="100%" />
                                            <p>&nbsp;</p>
                                            <h5>Nirmalya Trust Pune</h5>
                                            <ul>
                                                <li>Helping hand for differently able with a primary focus to assist in generating employment</li>
                                                <li>The Trust Co-operates with and seek support to undertake vocational training and create on job training opportunities</li>
                                                <li>We contributed by assisting in painting & cleaning the required area , gardening, ironing clothes and preparing paper bags</li>
                                                <li>Collected within the Team (needed for manufacturing paper bags & floor mats) old newspapers, clothes and funds for procuring paper punching machine, stationary , paints & other chemicals</li>
                                            </ul><br />
                                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-west-big3.jpg';?>"  width="100%" />
                                            <p>&nbsp;</p>
                                            <h5>Helpers of Mary Mumbai</h5>
                                            <ul>
                                                <li>The society founded by the German nun Anna Huberta Roggendorf during the 2nd World War. The society of the Helpers of Mary is now active in more than 55 stations in India with a vision to reach out to those who are unreached, in order to enrich, enhance and empower and to promote life at all levels with competence and compassion.</li>
                                                <li>We visited the home in two groups, and shared joy with the elderly by listening to their stories </li>
                                                <li>Engaged in board games and had a Housie session with prizes also shared biscuits, & basic toiletries</li>

                                            </ul> <br />
                                            <img src="<?php echo get_template_directory_uri().'/assets/about-us/csr/itq-west-big4.jpg';?>"  width="100%" />
                                            <p>&nbsp;</p>
                                            <h5>EnvironmentSanjay Gandhi National Park Mumbai</h5>
                                            <ul>
                                                <li>Green oasis in the center of urban sprawl, sustaining sizable population of big cats in a serene and tranquil atmosphere of pleasing verdant wilderness with rich and diverse forest holds which one thousand species of plants, 40 species of mammals, 251 of birds, covering migratory, land and water birds, 38 species of reptiles, 9 species of amphibians besides a large variety of fishes.</li>
                                                <li>Contributed by cleaning the park area and planting trees</li>
                                            </ul>
                                            <br />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
    </div>
</div>
</div>
</div>
</div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>