<?php
/**
 * Template Name: events
 */

get_header(); ?>

    <!--<div id="header"></div>	-->	

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet';?>">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>" type="text/javascript"></script>
    <!--<script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>    
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>-->
		
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="bold">About Us</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="about  company-profile"><a href="<?php echo get_home_url(); ?>/company-profile/">Company Profile</a></li>
                        <li class="leadershipTeam"><a class="about " href="<?php echo get_home_url(); ?>/leadershipteam/">Leadership Team</a></li>
                        <li class="itqJourney"><a class="about" href="<?php echo get_home_url(); ?>/itqjourney/">ITQ Journey </a></li>
                        <li class="awards"><a class="about " href="<?php echo get_home_url(); ?>/awards">Awards</a></li>
                        <li class="csr"><a class="about " href="<?php echo get_home_url(); ?>/csr/">CSR</a></li>
                        <li class="blogs"><a class="about " href="">Blogs</a></li>
                        <li class="active events"><a class="about " href="<?php echo get_home_url(); ?>/events/">Events</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="company-profile" class="tab-pane fade in active">
                <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/company-profile/">About Us</a> / <a class="current-page">Events</a></p>
                <div class="profile">
                <h3>Events</h3><br/>
                
                <ul class="container">
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="../wp-content/uploads/2019/04/ITQ-participates-in-SATTE-2019-events-1.pdf" target="_blank">InterGlobe Technology Quotient’s participates in biggest travel trade engagement show ‘SATTE 2019’</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/ITQ-participates-in-SATTE-2018.pdf';?>" target="_blank">InterGlobe Technology Quotient’s participates in biggest travel trade engagement show ‘SATTE 2018’ </a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe-Technology-Quotient-participates-in-Global-Panorama-Showcase-2017.pdf';?>" target="_blank">InterGlobe Technology Quotient participates in Global Panorama Showcase 2017.</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/ITQ&Travelport-invites-you-to-celebrate-our-partnership-Dinner-with-partners.pdf';?>" target="_blank">ITQ & Travelport invites you to celebrate our partnership Dinner with partners</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe-Technology-Quotient-organizes-learning-session-on-Booking-LCC.pdf';?>" target="_blank">InterGlobe Technology Quotient organizes learning session on Booking LCC (6E) on Travelport</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/ITQ organizes Student of the Year 2017-Grand Finale.pdf';?>" target="_blank">ITQ organizes Student of the Year 2017-Grand Finale</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/ITQ participates in SATTE 2017.pdf';?>" target="_blank">ITQ participates in SATTE 2017</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/ITQ organizes Travel Tech - Knowledge Point.pdf';?>" target="_blank">ITQ organizes Travel Tech - Knowledge Point</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa.pdf';?>" target="_blank">InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe Technology Quotient organizes Student of the Year 2016 Contest.pdf';?>" target="_blank">InterGlobe Technology Quotient organizes Student of the Year 2016 Contest</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe Technology Quotient participates in OTOAI 2016.pdf';?>" target="_blank">InterGlobe Technology Quotient participates in OTOAI 2016</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe Technology Quotient participates in SATTE 2016.pdf';?>" target="_blank">InterGlobe Technology Quotient participates in SATTE 2016</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/InterGlobe Technology Quotient participates in India International Travel and Tourism.pdf';?>" target="_blank">InterGlobe Technology Quotient participates in India International Travel & Tourism</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Calicut.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Calicut</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Cochin.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Cochin</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Trivandrum.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Trivandrum</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Networking Session with Trade Partners at Lucknow on 30th November 2015.pdf';?>" target="_blank">Networking Session with Trade Partners at Lucknow on 30th November 2015</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Ahmedabad.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Ahmedabad</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Baroda.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Baroda</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Lucknow.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Lucknow</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Guwahati.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Guwahati</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Soft Skills Training for TAAI Agents at Bhubaneshwar.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Bhubaneshwar</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/SoftSkillsTrainingNagpur.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Nagpur</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/SoftSkillstrainingIndore.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Indore</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/SoftSkills trainingforTAAIagentsatRaipur-events.pdf';?>" target="_blank">Soft Skills Training for TAAI Agents at Raipur</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Networking Session with Trade Partners at Pune.pdf';?>" target="_blank">Networking Session with Trade Partners at Pune</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Networking Session With Travel Partners In Hyderabad.pdf';?>" target="_blank">Networking Session With Travel Partners In Hyderabad</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Networking Session with Trade Partners at Vishakhapatnam.pdf';?>" target="_blank">Networking Session with Trade Partners at Vishakhapatnam</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/ATM1.pdf';?>" target="_blank">InterGlobe Technology Quotientat ArabianTravelMart(ATM)2015</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Student of the Year_.pdf';?>" target="_blank">Striving for excellence, Travelport’s Galileo introduces ‘Student of the Year’ contest to encourage GDS students</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/IMG_3172_hi.jpg';?>" target="_blank">InterGlobe Technology Quotient networking with Travel Partners in Travel Technology in Bhopal.</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Annual-Sales Conf.pdf';?>" target="_blank">InterGlobe Technology Quotient Annual Sales Conference 2015</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/websitr_TAAI.pdf';?>" target="_blank">ITQ and TAAI soft skills development</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/SATTE-2015- South-Asia- Largest-Travel-Show.pdf';?>" target="_blank">SATTE 2015, South Asia’s Largest Travel Show and now in its 22nd edition, commenced today at Pragati Maidan.</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Bhubaneswar.pdf';?>" target="_blank">InterGlobe Technology Quotient showcases its expertise in Travel Technology in Bhubaneswar</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Jaipur.pdf';?>" target="_blank">InterGlobe Technology Quotient showcases its expertise in Travel Technology in Jaipur </a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/surat-ahmedabad-road-show.pdf';?>" target="_blank">InterGlobe Technology Quotient showcases its expertise in Travel Technology in Surat and Ahmedabad</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/guwahati.pdf';?>" target="_blank">InterGlobe Technology Quotient showcases its expertise in Travel Technology in Guwahati</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Golf_Tournament.pdf';?>" target="_blank">Travelport Galileo by Travelport organized Golf Tournament.</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/CricketEvent.pdf';?>" target="_blank">Travelport Galileo by Travelport sponsored Cricket match.</a>
                    </li>
                    <li class="remove-list-style">
                        <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/events/Galileo_Globe Forex.doc';?>" target="_blank">Travelport Galileo Globe Forex.</a>
                    </li>
                    
                </ul>
        
                </div>
            </div>
        </div>
    </div>

    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>