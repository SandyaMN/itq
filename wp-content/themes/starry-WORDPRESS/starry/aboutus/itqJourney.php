<?php
/**
 * Template Name: itqjourney
 */

get_header(); ?>
    <!--<div id="header"></div> --> 

<link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet';?>">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>" type="text/javascript"></script>
    <!--<script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>    
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>-->
        
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="bold">About Us</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="about company-profile"><a href="<?php echo get_home_url(); ?>/company-profile/">Company Profile</a></li>
                        <li class="leadershipTeam"><a class="about " href="<?php echo get_home_url(); ?>/leadershipteam/">Leadership Team</a></li>
                        <li class="active itqJourney"><a class="about" href="<?php echo get_home_url(); ?>/itqjourney/">ITQ Journey </a></li>
                        <li class="awards"><a class="about " href="<?php echo get_home_url(); ?>/awards">Awards</a></li>
                        <li class="csr"><a class="about " href="<?php echo get_home_url(); ?>/csr/">CSR</a></li>
                        <li class="blogs"><a class="about " href="">Blogs</a></li>
                        <li class="events"><a class="about " href="<?php echo get_home_url(); ?>/events/">Events</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="itqJourney" class="tab-pane fade in active">
<p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/company-profile/">About Us</a> / <a class="current-page">ITQ
        Journey </a></p>
<div class="profile">
    <h3>ITQ Journey </h3>
    <div class="about clear-fix">
        <ul class="journey clear-fix">

                <li>
                        <h3 class="year">2019</h3>
                        <div class="hr-line"></div>
                        <div class="desc">
                            <p>Travelport and Jet Airways sign a new long-term supplier agreement commencing April 2019</p>
                        </div>
                    </li>

                <div class="vr-line"></div>  
                <li>
                    <h3 class="year">2018</h3>
                    <div class="hr-line"></div>
                    <div class="desc">
                        <p>Travelport wins tender for sole distribution supplier to Air India </p>
                    </div>
                </li>

                <div class="vr-line"></div>  
            <li>
                <h3 class="year">2017</h3>
                <div class="hr-line"></div>
                <div class="desc">
                    <p>Capital Group invested in ITQ</p>
                </div>
            </li>
            
            <div class="vr-line"></div>
            <li>
                <h3 class="year">2016</h3>
                <div class="hr-line"></div>
                <div class="desc">
                    <p>IndiGo signs exclusive agreement with Travelport to distribute
                        all of its fares and ancillary products to Travelport-connected
                        customers worldwide.
                    </p>
                </div>
            </li>
            <div class="vr-line"></div>
            <li>
                <h3 class="year">2010</h3>
                <div class="hr-line"></div>
                <div class="desc">
                    <p>ITQ streamlined its Worldspan operations to India, Sri Lanka,
                        Nepal, Maldives, and Mauritius and withdrew from AUS, NZL,
                        PAK, and BGD
                    </p>
                </div>
            </li>
            <div class="vr-line top-line"></div>
            <li class="multiple">
                <h3 class="year">2009</h3>
                <div class="hr-line"></div>
                <div class="vr-line extra-vr"></div>
                <div class="desc">
                    <p>InterGlobe Technology Quotient acquired 100% stake
                        in Calleo Distribution Technologies Private Limited (CDT)
                        that had distribution rights for Worldspan in South Asia,
                        Australia and New Zealand.
                    </p><br/>
                    <p>InterGlobe Technology Quotient appointed as Galileo’s distributor
                        for Sri Lanka and started operating in the country same year
                    </p>
                </div>
            </li>
            <div class="vr-line bottom-line"></div>
            <li>
                <h3 class="year">2008</h3>
                <div class="hr-line"></div>
                <div class="desc">
                    <p>Standard Chartered, DBS & Credit Suisse invested in ITQ
                    </p>
                </div>
            </li>
            <div class="vr-line"></div>
            <li>
                <h3 class="year">2005</h3>
                <div class="hr-line"></div>
                <div class="desc">
                    <p>
                        InterGlobe Technology Quotient was launched
                    </p>
                </div>
            </li>
            <div class="vr-line"></div>
            <li>
                <h3 class="year">1994</h3>
                <div class="hr-line"></div>
                <div class="desc">
                    <p>
                        InterGlobe is named nationwide agent for Galileo<br />
                        International’s Global Distribution Systems
                    </p>
                </div>
            </li>
        </ul>
    </div>
</div>
</div>
</div>
</div>
<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>