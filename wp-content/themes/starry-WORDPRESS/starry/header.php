<?php
/**
 * The Header of STARRY
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <!-- MAKE IT RESPONSIVE -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- <link rel="profile" href="http://gmpg.org/xfn/11"> -->
    <!-- <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"> -->
    <?php // GET FAVICON
    $favicon = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('favicon') : '';
    if( !empty( $favicon ) ) : ?>
        <!-- FAVICON -->
        <link rel="shortcut icon" type="image/png" href="<?php echo esc_url($favicon['url']) ?>">
    <?php endif ?>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
      <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
    <![endif]-->
    <!--[if IE]> 
        <style> 
        .with-separation-top:before{top: 0px!important;} 
        .with-separation-bottom:after{bottom: -30px!important;} 
        </style> 
    <![endif]-->

    <meta charset="utf-8">
    <title><?php bloginfo( 'name' ); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css'; ?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css'; ?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css'; ?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/home.css'; ?>" />

     <!-- Add the slick-theme.css if you want default styling -->
     <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
    <!-- Add the slick-theme.css if you want default styling -->
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css" />
    <link href="https://unpkg.com/gijgo@1.9.11/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js'; ?>" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js'; ?>" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js'; ?>" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri().'/js/home.js'; ?>" type="text/javascript"></script>
    <script src="<?php echo get_template_directory_uri().'/js/footer.js'; ?>" type="text/javascript"></script>
    <script src="https://unpkg.com/gijgo@1.9.11/js/gijgo.min.js" type="text/javascript"></script>

<!-- Loading Party Modules - Start -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js">
</script>
<!-- Loading Party Modules - End -->
    <?php wp_head(); ?>
  </head>

  <!-- START BODY -->
  <?php 
  // GET BODY LAYOUT : BOXED OR WIDE ?
  $body_style = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('layout') : ''; 
  ?>
  <body <?php body_class($body_style); ?>>
  <div id="search-modal" class="modal fade container" role="dialog">
                <div class="modal-dialog container">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <?php echo do_shortcode('[wpdreams_ajaxsearchlite]'); ?>
                        </div>
                    </div>
                </div>
            </div>
    <nav class="navbar navbar-inverse navbar-static-top main">
    <div class="container">
        <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                      </button>
                      
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>"><img src="<?php echo get_template_directory_uri().'/assets/itq-logo.jpg';?>" /></a>
        </div>
        
        <div id="navbar" class="navbar-collapse collapse">
            <!-- <div class="search-div">
                <input type="text" autofocus class="form-control search-input" placeholder="Search..." name="search">
                <i class="fa fa-times"></i>
            </div> -->
            <form class="navbar-form navbar-right" action="">
                <div class="search-icon">
                    <a class="" type="submit" data-toggle="modal" data-target="#search-modal">
                        <i class="fa fa-search"></i>
                    </a>

                </div>
            </form>
            <!-- <?php echo do_shortcode('[wpbsearch]'); ?>    -->
            
            
            <ul class="nav navbar-nav navbar-right main-menu">
                <li class="active"><a href="<?php echo get_home_url(); ?>">HOME</a></li>
                <!-- <li><a href="#"></a></li> -->
                <!-- <li><a href="#">PRODUCTS</a></li> -->
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">ABOUT
                        US
                        <span class="fa fa-angle-down"></span></a>
                    <div class="container dropdown-menu sub-menu" role="menu">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/company-profile/">COMPANY
                                    PROFILE</a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/itqjourney/">ITQ
                                    Journey
                                </a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/leadershipteam/">Leadership
                                    Team
                                </a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/awards/">Awards</a></div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/csr/">CSR</a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="">BLOG</a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/events/">EVENTS</a></div>
                        </div>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">PRODUCTS
                        <span class="fa fa-angle-down"></span></a>
                    <div class="container dropdown-menu sub-menu" role="menu">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/product-travelport/">TRAVELPORT SMARTPOINT (GALILEO)</a>
                                <ul class="header-inner-menu">
                                    <li><a href="<?php echo get_home_url(); ?>/travelport-ticketing/">TICKETING</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/travelport-fares/">FARES</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/travelport-non-air/">NON-AIR</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/travelport-office-management/">OFFICE MANAGEMENT</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/travelport-value-adds/">VALUE ADDS</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/travelport-galileo-desktop/">GALILEO DESKTOP</a></li>
                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/airlines/">AIRLINES</a>
                                <ul class="header-inner-menu">
                                    <li><a href="<?php echo get_home_url(); ?>/airlines/">JET AIRWAYS BOOKING PROCEDURES</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/airlines/">AIR INDIA BOOKING PROCEDURES</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/airlines/">INDIGO BOOKING PROCEDURES</a></li>

                                </ul>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/online-solutions/">ONLINE SOLUTIONS</a>
                                <ul class="header-inner-menu">
                                    <li><a href="<?php echo get_home_url(); ?>/online-solutions-booking/">CORPORATE BOOKING TOOL</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/online-solutions-booking/">SELF BOOKING TOOL</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/online-solutions-booking/">GALSTAR (B2B / B2C WHITE LABEL)</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/online-solutions-booking/">B2B INTERNET BOOKING ENGINE</a></li>
                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/worldspan/">WORLDSPAN</a>
                                <ul class="header-inner-menu">
                                    <li><a href="<?php echo get_home_url(); ?>/worldspan/">TRAVEL AGENCY</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/worldspan/">TRAVEL SUPPLIER</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/worldspan/">ONLINE SOLUTION</a></li>

                                </ul>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/ndc/">NDC</a>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/ecommerce-api/">ECOMMERCE APIs</a>
                                <ul class="header-inner-menu">
                                    <li><a href="<?php echo get_home_url(); ?>/epricing/">UNIVERSAL API</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/epricing/">GALILEO WEB SERVICES</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/epricing/">E-PRICING</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/epricing/">XML API DESKTOP</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/epricing/">MINI RULE SERVICE</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/epricing/">TICKET XCHANGE API - VR3</a></li>
                                </ul>

                            </div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div">
                                <a href="<?php echo get_home_url(); ?>/enhanced-utilities/">ENHANCED UTILITIES</a>
                                <ul class="header-inner-menu">
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">ADVANCED HMPR</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">E-TICKET APPLICATION</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">GALILEO FLIGHT INFO (PNR SMS)</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">TRAVEL INSURANCE</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">ITQ FINANCIAL</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">LCC INTEGRATOR</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">VR3</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">BSP QUOTA TOOL</a></li>
                                    <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">LOW FARE FINDER</a></li>
                                </ul>
                            </div>

                        </div>
                    </div>
 
                </li>
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">PRESS
                        ROOM
                        <span class="fa fa-angle-down"></span></a>
                    <div class="container dropdown-menu sub-menu" role="menu">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/press-release/">Press Releases</a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/advertisements/">Advertisements</a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/articles/">Articles</a></div>

                        </div>
                    </div>

                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">HUMAN
                        CAPITAL
                        <span class="fa fa-angle-down"></span></a>
                    <div class="container dropdown-menu sub-menu" role="menu">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm 4 submenu-div"><a href="<?php echo get_home_url(); ?>/hr-vision/">HR Vision & Values</a></div>
                            <div class="col-lg-4 col-md-4 col-sm 4 submenu-div"><a href="<?php echo get_home_url(); ?>/employee-value/">Employee
                                    Value Proposition & Total Rewards
                                </a></div>
                            <div class="col-lg-4 col-md-4 col-sm 4 submenu-div"><a href="<?php echo get_home_url(); ?>/learning-development/">Learning and Development
                                </a></div>
                            <div class="col-lg-4 col-md-4 col-sm 4 submenu-div"><a href="<?php echo get_home_url(); ?>/diversity-inclusion/">Diversity and Inclusion
                                </a></div>
                            <div class="col-lg-4 col-md-4 col-sm 4 submenu-div"><a href="<?php echo get_home_url(); ?>/carrer/">Careers</a></div>

                        </div>
                    </div>

                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">TRAINING
                        <span class="fa fa-angle-down"></span></a>
                    <div class="container dropdown-menu sub-menu" role="menu">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/our-vision/">Our
                                    Vision</a></div>
                            <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="<?php echo get_home_url(); ?>/travel-education/">Travel
                                    Education
                                </a></div>
                            <!-- <div class="col-lg-3 col-md-3 col-sm 3 submenu-div"><a href="#">Link to new portal</a></div> -->
                        </div>
                    </div>
                </li>

            </ul>

        </div>
        <!--/.nav-collapse -->
    </div>
    <!--/.container-fluid -->
</nav>


<script>
    $(document).ready(function () {
    $('.search-div').hide();
    $('.search a').click(function () {
        $('.search-div').toggle();
        $( ".search-input" ).focus();
    });
});
</script>

    