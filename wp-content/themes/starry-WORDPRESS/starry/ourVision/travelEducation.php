<?php
/**
 * Template Name: travel-education
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/common.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/ourVision.css';?>">

    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us bottom">
            <div class="container">
                <h3 class="">Travel Education</h3>
            </div>
        </div>
    </div>
    <div class="aboutus-menu container m-t-25">
        <div class="row">
            <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a class="current-page" href="<?php echo get_home_url(); ?>/travel-education/">Travel Education</a>
            </p>
        </div>
        <h4 class=" row press-room-header">Travel Education</h4>
        <div class="row  ">
            <h6 class="sub-heading bold">Travel Education Programme<h6>
            <p class="m-t-10-px">Travel Education prepares the foundation for growth and success of students who want to
                pursue a career in the travel industry. With the travel industry on the upswing, travelers are becoming more
                educated about their travel, hence there is a requirement of well qualified professionals. Being pioneer
                in the industry, InterGlobe Technology Quotient recognizes the importance of their need and aims at providing
                the requisite skilled manpower requirements in the industry.</p>
        </div>
        <div class="row m-t-10-px">
        <h6 class="sub-heading bold">Our Vision</h6>
            <p class="m-t-10-px">To be the most preferred travel education provider in the industry. Our aim is to reach
                training academics across India and to create a modern and dynamic structure for the workforce of future.</p>
        </div>
        <div class="row m-t-10-px">
        <h6 class="sub-heading bold">Milestone to Destination<h6>
            <p class="m-t-10-px">At Travelport Galileo, we were the first to recognize the emerging need in the travel industry
                and pioneered the travel education program in 2004. From a handful institutes way back in 2004 to over 80
                academies today has indeed come a long way.</p>
        </div>
        <div class="row m-t-10-px">
        <h6 class="sub-heading bold">Course Curriculum<h6>
            <p class="m-t-10-px">These institutes are using theory based as well as practical oriented training methods to
                their students so that the students are well aware of the industry requirements and the learning becomes
                more examples and practiced based. The Students are provided</p>
        </div>

        <div class="row course">
            <div class="col-lg-8 col-md-8 col-sm-8 login-content">
                <div class="course-login">
                    <h1>If already Registered, Login here!!!</h1>
                    <form>
                        <div class="form-group">
                            <input placeholder="Username" />
                            <input placeholder="Password" />
                            <button class="btn">Login</button>
                        </div>
                    </form><br />
                    <h1>New User?</h1>
                    <a>Register Online</a>
                </div>
                <div class="login-image">
                    <img src="<?php echo get_template_directory_uri().'/assets/login.png';?>" />
                </div>
            </div>
        </div>
    </div>
   
<script src="<?php echo get_template_directory_uri().'/js/press-room.js';?>"></script>
 <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>