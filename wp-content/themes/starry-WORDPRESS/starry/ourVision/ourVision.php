<?php
/**
 * Template Name: our-vision
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/common.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/ourVision.css';?>">

    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us bottom">
            <div class="container">
                <h3 class="">Our Vision</h3>
            </div>
        </div>
    </div>
    <div class="aboutus-tabs container m-t-25">
        <div class="row">
            <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a class="current-page" href="<?php echo get_home_url(); ?>/our-vision/">Our Vision</a>
            </p>
        </div>
        <div class="row">
            <h4 class=" row press-room-header">Our Vision</h4>
        </div>

        <div class="row">
            To be the most preferred travel education provider in the industry. Our aim is to reach training academics across India and
            to create a modern and dynamic structure for the workforce of future.
        </div>

        <div class="row sub-heading m-t-30-px bold">
            About Travel Education Programme:
        </div>
        <ul class="product-list  m-t-10-px">
                    <li> Focused on providing skilled professional to the travel industry</li>
                    <li> Well recognized by training academies in India</li>
                    <li> Tie-ups with more than 80 travel institutes</li>
                    <li> Train more than 5000 students per year</li>
                    <li> Offers Basic training on Air, Hotel and Car reservation</li>
                    <li> Advanced training on ticketing that includes Ticketing, e-tickets,
                refunds, reissue etc</li>

                </ul>
       
    </div>

<script src="<?php echo get_template_directory_uri().'/js/press-room.js';?>"></script>
 <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>