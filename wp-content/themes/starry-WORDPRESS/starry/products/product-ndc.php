<?php
/**
 * Template Name: ndc
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-ndc.css';?>" rel="stylesheet">

    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="product-title">Products</h3>
                <div class="aboutus-menu">
                        <ul class="nav nav-tabs">
                                <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class="airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                            </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">

        <!-- <p class="path m-t-60-px"><a href="home.html">Home</a> / <a href="./product-ndc.html">Products</a> / <a class="current-page" href="./product-ndc.html">NDC
            </a></p> -->
            <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">NDC</a></p>
        <div class="heading profile">
            <h3 class="">
                NDC - New Distribution Celebration
            </h3>

        </div>

        <!-- video -->

        <div class="m-t-20-px">
            <a href="https://www.workcast.com/ControlUsher.aspx?cpak=6880797554132343&pak=4007275794544634" target="_blank">
                <img src="<?php echo get_template_directory_uri().'/assets/ndc_img.png';?>" alt="" class="ndc-top-img">
            </a>
            <!-- <video width="400" controls>
                <source src="<?php echo get_template_directory_uri().'/assets/product-travelport.mp4" type="video/mp4';?>">

            </video> -->
        </div>

        <!-- text -->
        <div class="feature-text m-t-50 ">
            <p class="introduction-text blue-text m-b-25px">
                We’ll manage IATA’s NDC so you don’t have to.<br>
            </p>
            <p>In today’s exciting world of evolving distribution strategies, we don’t believe the conversation should
                be about any single source of content, whether that’s NDC (New Distribution Capability), API
                (Application program interface), classic ATPCo connectivity – or whatever new technology comes next.
                Instead, as the first NDC Level 3 Certified aggregator and first GDS operator to manage the live
                booking of flights using these new standards, we believe that all of us in the industry should be
                around the tech table, talking about ways to ensure that we continue to make the experience of managing
                and buying travel continually better for everyone.<br>


            </p>
        </div>

        <!-- cards -->
        <!-- card-1 -->
        <div class="card-container row m-t-50">
            <div class="card ">

                <div class="card-body card-6-6">
                    <div class="card-left">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-1.jpg';?>" class="ndc-image">
                    </div>
                    <div class="card-right">
                        <p class="blue-text text-heading">Travelport shares New Distribution Capability (NDC)
                            insights</p>
                        <p class="card-text m-t-20-px m-b-36">On 23rd October 2018, we announced that our NDC
                            solution was
                            live with a
                            UK-based travel agency, Meon Valley Travel, and that we had successfully become the first
                            GDS operator to manage the commercial booking of flights using the International Air
                            Transport Association’s (IATA) NDC technical standard. <br><br>

                            Find out more about our five key insights from our first few weeks operating a live NDC
                            booking solution.</p>
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href = 'https://www.travelport.com/blog/travelport-shares-new-distribution-capability-ndc-insights'>Read Now</a>
                    </div>
                </div>

            </div>

        </div>

        <!-- card-2 -->
        <div class="card-container row ">
            <div class="col-md-6  p-r-10 p-l-0 m-t-20-px">


                <div class="card ht-600">
                    <div class="card-header card-text-heading blue-text p-l-25">NDC is not just a standard – it’s a
                        signal to the industry</div>
                    <div class="card-6-6-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-2.jpg';?>" class="card-6-6-image">
                    </div>
                    <div class="card-body p-l-25 p-t-35px">

                        <p>On 23rd October 2018, we announced that our NDC solution was live with a UK-based travel
                            agency, Meon Valley Travel, and that we had successfully become the first GDS operator to
                            manage the commercial booking of flights using the International Air Transport
                            Association’s (IATA) NDC technical standard.

                            <br><br> Find out more about our five key insights from our first few weeks operating a
                            live NDC booking solution.</p>
                    </div>
                    <div class="card-footer ">
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/NDC_is_not_just_a_standard_Feb2019_Online.pdf';?>">Read Now</a>

                    </div>
                </div>

            </div>

            <div class="col-md-6 p-l-10 p-r-0 m-t-20-px">


                <div class="card ht-600">
                    <div class="card-header card-text-heading blue-text p-l-25">Delivering NDC our 2019 roadmap</div>
                    <div class="card-6-6-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-3.jpg';?>" class="card-6-6-image">
                    </div>
                    <div class="card-body p-l-25 p-t-35px">

                        <p>View our roadmap to see the milestones we’re looking forward to reaching
                            during 2019.</p>
                    </div>
                    <div class="card-footer ">
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://www.travelport.com/blog/delivering-ndc-our-2019-roadmap">Read Now</a>
                    </div>
                </div>

            </div>
        </div>
        <!-- card-3 -->
        <div class="card-container row ">
            <div class="card ht-410 m-t-20-px">
                <div class="card-header card-text-heading blue-text p-l-25 p-t-20px">Watch our videos and delve deeper
                    into NDC with us</div>
                <div class="col-md-3 ht-410  p-r-10 p-l-0 ">


                    <div class="card ht-300 border-0">
                        <!-- <div class="card-header card-text-heading blue-text p-l-25">NDC is not just a standard – it’s a
                        signal to the industry</div> -->
                        <div class="card-3-3-image-div m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-4.jpg';?>" class="card-3-3-image">
                        </div>
                        <div class="card-body p-l-25 p-t-35px">

                            <p>In conversation with Adrian Parkes,
                                CEO, GTMC</p>
                        </div>
                        <div class="card-footer ">
                            <!-- <button class="btn btn-default read-now-button ">Watch Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://play.buto.tv/7V6TW">Watch Now</a>
                        </div>
                    </div>

                </div>
                <div class="col-md-3  ht-410 p-r-10 p-l-0">


                    <div class="card  ht-300 border-0 ">
                        <!-- <div class="card-header card-text-heading blue-text p-l-25">NDC is not just a standard – it’s a
                        signal to the industry</div> -->
                        <div class="card-3-3-image-div m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-5.jpg';?>" class="card-3-3-image">
                        </div>
                        <div class="card-body p-l-25 p-t-35px">

                            <p>In conversation with Mervyn Williamson, Managing Director, T&T Statesman</p>
                        </div>
                        <div class="card-footer p-t-10p">
                            <!-- <button class="btn btn-default read-now-button ">Watch Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://play.buto.tv/k2b3f">Watch Now</a>

                        </div>
                    </div>

                </div>
                <div class="col-md-3 ht-410 p-r-10 p-l-0">


                    <div class="card ht-300 border-0">
                        <!-- <div class="card-header card-text-heading blue-text p-l-25">NDC is not just a standard – it’s a
                        signal to the industry</div> -->
                        <div class="card-3-3-image-div m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-6.jpg';?>" class="card-3-3-image">
                        </div>
                        <div class="card-body p-l-25 p-t-35px">

                            <p>In conversation with our Online Travel Agency customers</p>
                        </div>
                        <div class="card-footer p-t-10p">
                            <!-- <button class="btn btn-default read-now-button ">Watch Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://play.buto.tv/l4Gxq">Watch Now</a>

                        </div>
                    </div>

                </div>
                <div class="col-md-3 ht-410 p-r-10 p-l-0">


                    <div class="card ht-300 border-0">
                        <!-- <div class="card-header card-text-heading blue-text p-l-25">NDC is not just a standard – it’s a
                        signal to the industry</div> -->
                        <div class="card-3-3-image-div m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-7.jpg';?>" class="card-3-3-image">
                        </div>
                        <div class="card-body p-l-25 p-t-35px">

                            <p>In conversation with David Rutnam,
                                IATA NDC Regional Implementation Manager</p>
                        </div>
                        <div class="card-footer p-t-10p">
                            <!-- <button class="btn btn-default read-now-button ">Watch Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://play.buto.tv/Vth59">Watch Now</a>

                        </div>
                    </div>

                </div>

            </div>
        </div>

        <!-- card-4 -->
        <div class="card-container row ">
            <div class="col-md-6  ht-500 p-r-10 p-l-0 ">


                <div class="card ht-500">
                    <div class="card-header card-text-heading blue-text p-l-25">Travelport answers the 10 most common
                        NDC questions</div>
                    <div class="card-6-6-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-8.jpg';?>" class="card-6-6-image ht-340">
                    </div>

                    <div class="card-footer p-l-25">
                        <!-- <button class="btn btn-default read-now-button">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/Travelport_NDC_Q_A_Nov2018_Online.pdf';?>">Read Now</a>
                    </div>
                </div>

            </div>

            <div class="col-md-6 ht-500 p-l-10 p-r-0 ">


                <div class="card ht-500">
                    <div class="card-header card-text-heading blue-text p-l-25">The future of airline retailing depends
                        on industry collaboration</div>
                    <div class="card-6-6-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-9.jpg';?>" class="card-6-6-image ht-340">
                    </div>

                    <div class="card-footer p-l-25">
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/Future_of_Airline_Retailing_Dec2018_Online.pdf';?>">Read Now</a>

                    </div>
                </div>

            </div>
        </div>

        <!-- card-5 -->
        <div class="card-container row m-t-20-px">
            <div class="card ht-100 blue-background">

                <div class="card-body card-6-6">
                    <div class="card-left p-2p ">
                        <p class="card-text white-text"> Receive our NDC information and updates by email. </p>
                    </div>
                    <div class="card-right">

                        <!-- <button class="btn btn-default read-now-button f-r">Sign Up!</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12 f-r" href ="https://marketing.cloud.travelport.com/ndc-gated">Read Now</a>
                    </div>
                </div>

            </div>
        </div>

        <!-- card-6 -->
        <div class="card-container row m-t-20-px">
            <div class="col-md-4  ht-515 p-r-10 p-l-0 m-t-20-px">


                <div class="card ht-515 border-0 bg-white">
                    <div class="p-l-10 card-text-heading blue-text ">New Distribution Celebration</div>
                    <div class="card-4-4-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-8.jpg';?>" class="card-4-4-image">
                    </div>

                    <div class="card-body p-t-35px">

                        <p>In our latest development, Travelport becomes the first GDS operator to offer NDC content.
                            The first transaction was made by a UK-based travel agent for a short-haul low-cost flight...
                        </p>
                    </div>
                    <div class="card-footer">
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://www.travelport.com/company/media-center/press-releases/2018-10-23/travelport-becomes-first-gds-operator-offer-ndc">Read Now</a>
                    </div>
                </div>

            </div>

            <div class="col-md-4  ht-515 p-r-10 p-l-0 m-t-20-px">


                <div class="card ht-515 border-0 bg-white">
                    <div class="p-l-10 card-text-heading blue-text ">Come behind the scenes as we get NDC ready</div>
                    <div class="card-4-4-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-8.jpg';?>" class="card-4-4-image">
                    </div>
                    <div class="card-body p-t-35px">

                        <p>New technologies allow us to do things differently, more efficiently and in a personalized,
                            customer-centric way. NDC is part of this change. .</p>
                    </div>
                    <div class="card-footer">
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://www.travelport.com/blog/come-behind-scenes-we-get-ndc-ready">Read Now</a>
                    </div>
                </div>

            </div>

            <div class="col-md-4  ht-515 p-r-10 p-l-0 m-t-20-px">


                <div class="card ht-515 border-0 bg-white">
                    <div class="p-l-10 card-text-heading blue-text ">Four ways APIs are enabling the
                        transformation</div>
                    <div class="card-4-4-image-div m-t-25px">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-8.jpg';?>" class="card-4-4-image">
                    </div>
                    <div class="card-body p-t-35px">

                        <p>Travelport examines the different ways that APIs transform travel shopping into personal,
                            effortless eCommerce-like experiences.</p>
                    </div>
                    <div class="card-footer">
                        <!-- <button class="btn btn-default read-now-button ">Read Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/APIs_enabling_transformation_Oct18.pdf';?>">Read Now</a>
                    </div>
                </div>

            </div>
        </div>



        <!-- card-7 -->
        <div class="card-container row m-t-60-px">
            <div class="col-md-12">
                <div class="card ht-500 bg-beige">

                    <div class="card-body p-l-0 p-t-0 card-6-6 ht-500">
                        <div class="card-left bg-beige">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-10.png';?>" class="ndc-image-book">
                        </div>
                        <div class="card-right bg-beige  ht-435px p-t-6p">
                            <p class="blue-text">Learn more about NDC and airline distribution strategies </p>
                            <p class="card-text m-t-20-px">Download our eBook and join the conversation.</p><br>
                            <ul class="product-list">    
                                <li> The Airline, Agencies, Corporation and Travel Management Company perspective.</li>
                                <li> IATA NDC Level 3: see what Travelport is doing to get you where you want to be.</li>
                                <li> NDC, API, classic connectivity and branded content can all interconnect to help your business grow.</li>
                                <li> 1000s of direct relationships. Connected via Travelport.</li>
                                <li> Ensuring the future’s not fragmented.</li>
                            </ul>
                            <div class="card-footer">
                                <!-- <a target="_blank" class="btn btn-default read-now-button p-t-12" href = ''>Read Now</a> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/New_Distribution_Conversations_eBook_2018.pdf';?>">Read Now</a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <!-- card-8 -->


        <div class="card-container m-t-20-px">
            <div class="card ht-550 m-t-20-px">
                <div class="col-md-4  ht-515  m-t-20-px">
                    <div class="card ht-515 border-0 p-r-10 p-l-0">
                        <div class=" card-text-heading blue-text ">NDC, its certifications and what
                            they mean for you</div><br>
                        <div class="card-4-4-image-div m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-11.jpg';?>" width="100%">
                        </div>

                        <div class="card-body">

                            <p>As the first GDS operator to have been awarded IATA New Distribution Capability Level 3
                                Certification as an Aggregator and Distributor, Travelport explains the various NDC
                                certification levels.</p>
                        </div>
                        <div class="card-footer ">
                            <!-- <button class="btn btn-default read-now-button ">Read More</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://www.travelport.com/blog/ndc-its-certifications-and-what-they-mean-you">Read Now</a>

                        </div>
                    </div>
                </div>


                <div class="col-md-4  ht-515  m-t-20-px">
                    <div class="card ht-515 border-0 p-r-10 p-l-0">
                        <div class=" card-text-heading blue-text ">NDC is changing the way the travel
                            industry is thinking
                        </div><br>
                        <div class="card-4-4-image-div  m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-12.png';?>" width="100%">
                        </div>
                        <div class="card-body">

                            <p>Ian Heywood, Global Head of New Distribution at Travelport discusses how IATA's NDC is
                                starting to shape the industry, how it works and where the benefits lie
                                for all.</p>
                        </div>
                        <div class="card-footer ">
                            <!-- <button class="btn btn-default read-now-button ">Watch Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://www.youtube.com/watch?v=3L4Gjl6M9jI&feature=youtu.be">Watch Now</a>
                        </div>
                    </div>

                </div>

                <div class="col-md-4  ht-515  m-t-20-px">
                    <div class="card ht-515 border-0 p-r-10 p-l-0">
                        <div class=" card-text-heading blue-text ">NDC - What, Why, How and the Journey to
                            2020</div><br>
                        <div class="card-4-4-image-div  m-t-25px">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc-13.png';?>" width="100%">
                        </div>
                        <div class="card-body">

                            <p>Hear from Travelport at the recent CAPA Australia Pacific Aviation & Corporate Travel
                                Summit in Sydney, listen to the latest updates on this new era of travel retail and how
                                it impacts on all players.</p>
                        </div>
                        <div class="card-footer ">
                            <!-- <button class="btn btn-default read-now-button ">Watch Now</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="https://www.youtube.com/watch?v=LpCwA9f0PUI&feature=youtu.be">Watch Now</a>

                        </div>
                    </div>

                </div>
            </div>








        </div>

        <!-- card-9 -->

        <div class="card ht-430 m-t-20-px">
            <!-- <div class="card-header card-text-heading blue-text p-l-25 p-t-20px">Watch our videos and delve deeper
                    into NDC with us</div> -->
            <div class="col-md-3 ht-410  p-r-10 ">


                <div class="card border-0">
                    <div class="card-header text-heading blue-text p-l-25">Airlines </div>
                    <div class="card-3-3-image-div ">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-14.png';?>" class="card-3-3-img">
                    </div>
                    <div class="card-body p-t-35px">

                        <p>Travelport will help you take your airline forward in this new NDC era. Find out about
                            the next evolution in air retailing for tomorrow's world.</p>

                           
                        <!-- <button class="btn btn-default read-now-button ">Read More</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/TRP_2611_OP_Airlines_v2_EN.pdf';?>">Read Now</a>

                    </div>
                </div>

            </div>
            <div class="col-md-3  ht-410 p-r-10 p-l-0">


                <div class="card  border-0 ">
                    <div class="card-header text-heading blue-text p-l-25">Agencies </div>
                    <div class="card-3-3-image-div ">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-15.png';?>" class="card-3-3-img">
                    </div>
                    <div class="card-body p-t-35px">

                        <p>Whether your business is online or offline, Travelport will help you to continue to have
                            access to the broadest choice of content.</p>

                        
                        <!-- <button class="btn btn-default read-now-button">Read More</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/TRP_2611_OP_agency_a_EN.pdf';?>">Read Now</a>
                        </div>
                    
                </div>

            </div>
            <div class="col-md-3 ht-410 p-r-10 p-l-0">


                <div class="card  border-0">
                    <div class="card-header text-heading blue-text p-l-25">Corporations </div>
                    <div class="card-3-3-image-div ">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-16.png';?>" class="card-3-3-img">
                    </div>
                    <div class="card-body p-t-35px">

                        <p>Bringing a new perspective to corporate travel and enhance traveler experience in the
                            new NDC era of buying business travel.</p>

                        
                        <!-- <button class="btn btn-default read-now-button ">Read More</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/TRP_2611_OP_Corporate_v2_EN.pdf';?>">Read Now</a>
                        </div>
                   
                </div>

            </div>
            <div class="col-md-3 ht-410 p-r-10 p-l-0">


                <div class="card  border-0">
                    <div class="card-header text-heading blue-text p-l-25">TMCs </div>
                    <div class="card-3-3-image-div">
                        <img src="<?php echo get_template_directory_uri().'/assets/ndc-17.png';?>" class="card-3-3-img">
                    </div>
                    <div class="card-body p-t-35px">

                        <p>TMCs have to transform in light of NDC. Travelport will help you to continue to deliver
                            the service your clients have become accustomed to.</p>

                            
                        <!-- <button class="btn btn-default read-now-button ">Read More</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12" href ="<?php echo get_template_directory_uri().'/assets/TP_16721_OP_TMC_v3_EN.pdf';?>">Read Now</a>
                    </div>
                </div>

            </div>

        </div>


        <!-- card-10 -->
        <div class="card-container  m-t-20-px">
            <div class="card ht-100 blue-background ">

                <div class="card-body card-6-6">
                    <div class="card-left p-2p ">
                        <p class="card-text white-text">Contact us today to find out how we can help you get NDC ready.
                        </p>
                    </div>
                    <div class="card-right">
                    
                        <!-- <button class="btn btn-default blue-text read-now-button f-r">Get in touch!</button> -->
                        <a target="_blank" class="btn btn-default read-more p-t-12 f-r" href ="https://marketing.cloud.travelport.com/NDC-contactus">Get in touch!</a>

                    </div>
                </div>

            </div>
        </div>


    </div>

    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>