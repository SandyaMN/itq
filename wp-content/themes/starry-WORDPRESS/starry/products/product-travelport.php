<?php
/**
 * Template Name: product-travelport
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <!-- <script src="js/product.js"></script> -->
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">

    <div class="banner ">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                        <ul class="nav nav-tabs">
                                <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                            </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="container content">
            <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page" >Travelport Smartpoint</a></p>
            <div class="heading m-t-20-px profile">
                <h3>Travelport Smartpoint (Galileo)</h3>
            </div>
            <!-- video -->
    
            <div class="product-video clearfix">
                <video autoplay muted>
                    <source src="<?php echo get_template_directory_uri().'/assets/homebanner.mp4';?>" type="video/mp4">
    
                </video>
            </div>
    
            <!-- text -->
            <div class="feature-text m-t-25px">
                <p>
                    Travelport Smartpoint is the ultimate travel technology for today’s connected digital age. It puts the
                    world’s travel content at your fingertips and is packed with features <br>that power better performance
                    and enable you to create the perfect personalized service for your customers.<br>
                </p>
                <p>The new version has the following features:<p>
                <ul class="product-list">
                    <li> Improved productivity with a new email option for the EMD Manager</li>
                    <li> More flexibility with a new Interactive Car Index screen</li>
                    <li> Enhanced fare results as you can select up to 5 carriers on the Air Availability and Flight Shop Search forms</li>
                    <li> Better hotel results as you can select up to 8 multi-level rate codes on the Hotel Availability Search form</li>
                    <li> Easier booking with enhancements to both branded fares and Trip Quote.</li>
                    <li> With many more enhancements to make booking easier for you and the experience for your customers better.</li>
                </ul>
    
    
                </p>
            </div>
    
            <!-- cards -->
            <div class="card-container m-t-25px">
    
    
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card card-default b-r-0 b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header"></div>
                            <div class="card-body card-6-6">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="card-img-responsive">
                                </div>
                                <div class="card-img-white">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="card-img-responsive">
                                </div>
                                <div class="card-center">
    
                                    <div class="col-md-3 img-text"></div>
                                    <div class="col-md-6 img-text">Ticketing</div>
                                    <div class="col-md-3 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                </div>
                            </div>
    
                        </div>
                    </div>
    
                    <div class="col-sm-4">
                        <div class="card card-default b-r-0 b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header"></div>
                            <div class="card-body card-6-6">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-img-white">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-center">
    
    
    
                                    <div class="col-md-3 img-text"></div>
                                    <div class="col-md-6 img-text">Fares</div>
                                    <div class="col-md-3 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                </div>
                            </div>
    
                        </div>
                    </div>
    
                    <div class="col-sm-4">
                        <div class="card card-default b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header"></div>
                            <div class="card-body card-6-6">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-img-white">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-center">
    
                                    <div class="col-md-3 img-text"></div>
                                    <div class="col-md-6 img-text">Non Air</div>
                                    <div class="col-md-3 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                </div>
                            </div>
    
                        </div>
                    </div>
    
    
    
    
    
                </div>
    
                <div class="row ">
                    <div class="col-sm-4">
                        <div class="card card-default b-r-0" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header"></div>
                            <div class="card-body card-6-6">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-img-white">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-center">
                                    <div class="col-md-3 img-text"></div>
                                    <div class="col-md-6 img-text">Office Management</div>
                                    <div class="col-md-3 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                    <!-- <p>Office Management</p> -->
                                </div>
                            </div>
    
                        </div>
                    </div>
    
                    <div class="col-sm-4">
                        <div class="card card-default b-r-0" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header"></div>
                            <div class="card-body card-6-6">
    
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-img-white">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="card-img-responsive">
                                </div>
                                <div class="card-center">
    
                                    <div class="col-md-3 img-text"></div>
                                    <div class="col-md-6 img-text">Value Adds</div>
                                    <div class="col-md-3 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                </div>
                            </div>
    
                        </div>
                    </div>
    
                    <div class="col-sm-4">
                        <div class="card card-default" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header"></div>
                            <div class="card-body card-6-6">
                                <div class="card-img">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="card-img-responsive">
                                </div>
    
                                <div class="card-img-white">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="card-img-responsive">
                                </div>
                                <div class="card-center">
                                    <div class="col-md-3 img-text"></div>
                                    <div class="col-md-6 img-text">Galileo Desktop</div>
                                    <div class="col-md-3 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                </div>
                            </div>
    
                        </div>
                    </div>
    
    
    
    
    
                </div>
            </div>
    
            <!-- link    -->
            <div class="travelport-link m-t-25px">
                <p class="link-text-color">Link to Travelport: <a class="link-text-color link-color" href="https://www.travelport.com">https://www.travelport.com</a>
                </p>
            </div>
        </div>

   <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>