<?php
/**
 * Template Name: online-solutions
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-online-solutions.css';?>" rel="stylesheet">


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/product_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="product-title">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class=" airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="contents container">
        <p class="path m-t-60-px"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Online Solutions </a></p>
        <div class="heading m-t-20-px">
            <h4>
                Online Solutions
            </h4>

        </div>


        <div class="card-container m-t-25px">
            <div class="row">
                <div class="col-sm-6">
                    <div class="card card-default b-r-0 b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/online-solutions-booking/';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/b2cinternet-booking.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/b2cinternet-booking_white.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-center">
                                <div class="img-text">B2B Internet Booking Engine</div>
                                <!-- <div class="col-md-5 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div> -->
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card card-default b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/online-solutions-booking/';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/corporate_booking_tool_blue.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/corporate_booking_tool.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-center">

                                <div class="img-text">Corporate Booking Tool</div>
                                <!-- <div class="col-md-5 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row ">
                <div class="col-sm-6">
                    <div class="card card-default b-r-0" onclick="location.href='<?php echo get_home_url(); ?>/online-solutions-booking/';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/galstar.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/galstar_white.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-center">

                                <div class="img-text">Galstar</div>
                                <!-- <div class="col-md-5 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div> -->
                                <!-- <p>Office Management</p> -->
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-6">
                    <div class="card card-default " onclick="location.href='<?php echo get_home_url(); ?>/online-solutions-booking/';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">

                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/self_booking_tool.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/self_booking_tool_white.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-center">


                                <div class="img-text">Self Booking Tool</div>
                                <!-- <div class="col-md-5 arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div> -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>

<script src="<?php echo get_template_directory_uri().'/js/online-solutions.js';?>"></script>