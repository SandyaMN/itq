<?php
/**
 * Template Name: travelport-galileo-desktop
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">
        <!-- breadcrumbs -->
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Travelport Smartpoint</a></p>
        <!-- heading -->

        <!-- content-text -->
        <div class="content-row row">
            <div class="feature-text col-md-9">
                <div class="ticketing-heading profile">
                    <h3>
                        Galileo Desktop
                    </h3>

                </div>

                <p>Galileo Desktop™ is a sophisticated global reservation, business management and productivity system. It provides access to
                    advanced tools for shopping and booking all air, hotel, car rental and other travel-related products
                    available through the Galileo GDS. </p> 
                    
                    <ul class="product-list">
                    <li> Option to work in a graphical, point-and-click
                    environment using Viewpoint™ or  through the traditional command window structure using
                    Focalpoint™.</li>
                    <li> Completes transactions faster, to higher levels of customer satisfaction with shortcuts,
                    links and customizable workflow links that mirror your business processes, so training
                    is minimal.</li>
                    <li> Available in 23 languages, Galileo Desktop provides choice and flexibility with the
                    ability to send customer itineraries via e-mail in a local language of choice.</li>
                    <li> Low-fare shopping tools feature availability searches by booking class and return fare-validated flights.</li>
                    
                </ul>
                    
                <div class="airways-top  m-t-40">
                    <h5> Relay 3.3</h5>
                </div>


                <p>Relay provides a suite of products which makes Travelport Galileo work faster and easier to use, resulting
                    in faster, more accurate bookings.</p><br>
                <p class="ticketing-subheading-black"> Feature</p>
                <ul class="product-list">
                    <li> Response Capture - Captures host response screens to a file or other destination device, for example
                    a fax machine.</li>
                    <li> Queue Manager - provides a graphical interface for managing and moving your queues.</li>
                    <li> Booking File to Client File - Creates a client file record using the information from a booking
                    file. </li>
                    
                </ul>
               
                <p class="ticketing-subheading-black"> Benefit</p>
                <ul class="product-list">
                    <li> Ensures efficient information flow to customers.</li>
                    <li> Manages queues effectively.</li>
                    <li> Creates Client Files automatically </li>
                    

                </ul>                           


                <p class="ticketing-subheading-black"> Advantage</p>
                <ul class="product-list">
                    <li> Enhances customer service.</li>
                    <li> Enhances efficiency, and saves time and money.</li>
                    <li> Cuts time required
                    for creating a client file.</li>
                    
                </ul>
               

                <div class="airways-top  m-t-40">
                    <h5> Smartpoint </h5>
                </div>

                <p>Travelport Smartpoint helps you achieve immediate productivity gains, reduce costs and improve your customer
                    service. It makes the lives of your agents easier and strengthens your most valuable assets – your existing
                    people and technology. <br> </p>
                <p> For product demo videos, case studies, customer facing materials and more visit:
                    <b>www.travelportsmartpoint.com</b> <br> </p>

                <p> <b>Customer Profile:</b> Travel agencies who operate a fully managed travel model or a mix of managed
                    and un-managed travel. <br> </p>

                <p> <b>Current Version:</b> Apollo and Galileo v3.0 and Worldspan v5.3. <br> </p>

                
                <p> Travelport Smartpoint evolves the GDS experience with new and interactive technology that dramatically
                    improves travel agents productivity and reduces training, saving agencies time and money.
                    <br> It is designed to support agencies looking to: </p>
                    <ul class="product-list">
                    <li> Achieve incremental productivity gains.</li>
                    <li> Reduce staff training costs from new hire and/or when converting from another GDS.</li>
                    <li> Take advantage of new functionality, content and technologies as they become available.</li>
                    
                </ul>
                   
                <p class="ticketing-subheading-black"> It's Fast</p>

                <ul class="product-list">
                    <li> Searching and booking is easier – find relevant options with a couple of clicks and filter results so you can more quickly
                    see the options that best meet your customer needs. </li>
                    <li> Double the information
                    is returned within a single request.</li>
                    <li> Interactive, color coded displays give immediate access to
                    key information from the same screen.</li>
                    <li> Day to day processes such as queues are automatic – no need to work harder just smarter!.</li>
                    <li> Integration with eNett Virtual Account Numbers (VANs) for safe and seamless payments within your GDS
                    booking flow. Visit <b>www.enett.com/travelport</b> for more information. </li>
                    <li> Interactive
                    maps showing flight routes, connection times & terminal information and hotels – no need to launch a
                    separate site to launch a map.</li>
                   


                </ul>
                
                <p class="ticketing-subheading-black"> It's Flexible</p>
                <ul class="product-list">
                    <li> Choose which GDS language you wish to work in – no need to completely learn a new system.</li>
                    <li> New
                    agents can search and book using graphical windows – getting up to speed is easy!</li>
                    <li> Agents can work
                    the way they want to – the workspace can be organised, theme and local language can be chosen too!</li>
                    <li> Builds on how you work today so you’ll be up and running in minutes – no lengthy training.</li>
                    <li>  A modern
                    agency experience that supports your agents desktop needs today and tomorrow.</li>

                </ul>
               
                <p class="ticketing-subheading-black"> It's Familiar</p>

                <ul class="product-list">
                    <li> Nothing has been taken away, we’ve just added more time saving features and new tools.</li>
                    <li> All your
                    scripts and macros can be used as you do today – and new ones too!</li>
                    <li> Experienced users can choose to
                    work how they work today – no need to learn anything new!</li>
                    

                </ul>
               

                <div class="airways-top  m-t-40">
                    <h5> Galileo Desktop Email </h5>
                </div>

                <p> Send email from your Galileo Desktop either via typed entry or the graphical display with a text version
                    of your customer's itinerary. </p>
                    
                    <ul class="product-list">
                    <li> Free service exclusive to Travelport Galileo-connected agencies.</li>
                    <li> No Internet connection needed .</li>
                    <li> No software to install or maintain.</li>
                    <li> Email is sent
                    direct from your Galileo Desktop without need to switch to another software application.</li>
                    <li> Email
                    attachment includes a customised link to ViewTrip.com.</li>

                </ul>
                   

                <p class="ticketing-subheading-black"> Benefits of Galileo Desktop Email</p>
                <ul class="product-list">
                    <li> No need to load additional software or internet access dependency.</li>
                    <li> Simple and easy to use.</li>
                    <li> Quick and efficient means of generating emails without having to toggle between applications.</li>
                   

                </ul>
                
            </div>

            <div class="link-col col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 " onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 bg-blue" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header ht-60">

                                <!-- <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div> -->
                                <div class="card-img-white-small img-display">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small white-text m-t-25-px">Galileo Desktop</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>