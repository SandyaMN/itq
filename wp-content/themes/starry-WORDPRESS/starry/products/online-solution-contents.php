<?php
/**
 * Template Name: online-solutions-booking
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-online-solutions.css';?>" rel="stylesheet">



    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/product_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="product-title">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class=" airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>
                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>





    <!-- contents -->
    <div class="contents container">
         <p class="path m-t-60-px"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page" >Online Solutions </a></p>
        <div class="aboutus-tabs m-t-0">
            <!-- <h4>Enhanced Utilities</h4> -->
            <div class="clear-fix row enhanced-utilities">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 enhanced-utilities-tabs">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-stacked tabs-right">
                            <li><a href="#internet-booking" data-toggle="tab">
                                    <img class="card-img" src="<?php echo get_template_directory_uri().'/assets/b2cinternet-booking.png';?>" />
                                    <img class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/b2cinternet-booking_white.png';?>" /> B2B Internet
                                    Booking Engine</a></li>
                            <li><a href="#corporate-booking" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/corporate_booking_tool_blue.png';?>"
                                    /><img class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/corporate_booking_tool.png';?>" />Corporate
                                    Booking Tool</a></li>
                            <li><a href="#galstar" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/galstar.png';?>" /><img
                                        class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/galstar_white.png';?>" /> Galstar
                                </a></li>
                            <li><a href="#self-booking" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/self_booking_tool.png';?>"
                                    /><img class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/self_booking_tool_white.png';?>" />Self Booking
                                    Tool</a></li>


                        </ul>
                    </div>
                </div>
                <div class="tab-content w-60">
                    <div class="tab-pane active" id="internet-booking">
                        <h4>B2B Internet Booking Engine</h4>
                        <p>An internet booking engine developed for B2B sector to provide online booking tool for their customers.
                            In addition, it helps in consolidation of Air / Non Air content purchased from various service
                            providers such as FSC, LCC, Hotel API etc.</p>
                    </div>
                    <div class="tab-pane" id="corporate-booking">
                        <h4>Corporate Booking Tool</h4>
                        <p><p> Corporate Booking Tool (CBT) and is an intuitive and user-friendly interface designed to automate the travel booking process efficiently. It automates the process of issuing travel services like air, hotels, Visa etc., in the Corporate Travel Management Company's work environment.</p>
                        <br/>
                        <p class="ticketing-subheading">Features: </p>
                        <ul class="product-list">
                            <li> Offers seamless, unified workflow and faster search response time thus, time-saving.</li>
                            <li> Overspending cost elimination due to out of policy bookings.</li>
                            <li> Customize account set-up and user control to reflect your company travel policy.</li>
                            <li> Multiple workflows for approval.</li>
                            <li> Offers advanced user profiles, advanced travel policy, automated policy validation and approval chain.</li>
                            <li> Ease of self generating group level reporting and training people.</li>
                            <li> Remarks configuration compatible with GDSs and mid back office system</li>
                            <li> Reduced code and software maintenance as it's a single application.</li>
                            <li> Central database for booking and customers allows for big data analysis for greater cost savings and revenue management.</li>
                            <li> Lower cost of operations.</li>
                            <li> Offers Customized and real-time corporate MIS reports.</li>
                        </ul>                    
                       
                    </div>
                    <div class="tab-pane" id="galstar">
                        <h4>Galstar</h4>
                        <p class="bold">Galstar is a plug & play white label solution that makes your business come online.
                            User-friendly and easy -to-use, this online (B2C & B2B) Booking Engine offers integrated Galileo
                            content thereby helping your customers make their booking within few minutes.<br>&nbsp<br> </p>

                        <p class="ticketing-subheading">Features: </p>
                        <!-- <b>Features:</b>&nbsp<br> -->

                        <ul class="product-list">
                            <li> Allows self registration by user, sub agency or walk-in client.</li>
                            <li> Comprehensive & user friendly admin tool for controlling day-to-day operations.</li>
                            <li> Value add-ons- Queue Management, Comprehensive Accounting System, MIS Report, Option to Hold or Issue ticket.</li>
                            <li> Admin Owned Galileo PCC & LCC used for bookings, thereby no loss of segment generation.</li>
                            <li> Displays real time fares.</li>
                            <li> Facilitates smooth adoption for users owing to the user-friendly and multi-lingual platform.</li>
                            <li> Offers synchronized reports.</li>
                        </ul>
                        
                    </div>
                    <div class="tab-pane" id="self-booking">
                        <h4> Self Booking Tool</h4>
                        <p>
                            Self Booking Tool (SBT) are intuitive and user-friendly interface designed to automate the travel booking process efficiently.
                            SBT caters to the companies that manage their trips on their own. With easy self-planning & self-booking
                            nature of these tools, corporate/travel desk can maintain their traveller profile, which keeps
                            data secured within the corporate control.<br><br>
                            <p class="ticketing-subheading">Features: </p>
                            <ul class="product-list">
                                <li> Overspending cost elimination due to out of policy bookings.</li>
                                <li> Customize account set-up and user control to reflect your company travel policy.</li>
                                <li> Multiple workflows for approval.</li>
                                <li> Offers advanced user profiles, advanced travel policy, automated policy validation and approval chain.</li>
                                <li> Ease of self generating group level reporting and training people.</li>
                                <li> Remarks configuration compatible with GDSs and mid back office system.</li>
                                <li> Reduced code and software maintenance as it's a single application.</li>
                                <li> Central database for booking and customers allows for big data analysis for greater cost savings and revenue management.</li>
                                <li> Lower cost of operations.</li>
                                <li> Offers Customized and real-time corporate MIS reports.</li>
                            </ul>
                            
                    </div>



                </div>
            </div>
        </div>
    </div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>

<script src="<?php echo get_template_directory_uri().'/js/online-solutions.js';?>"></script>