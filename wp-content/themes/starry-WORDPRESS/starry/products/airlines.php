<?php
/**
 * Template Name: airlines
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <!-- <script src="<?php echo get_template_directory_uri().'/js/product.js';?>"></script> -->
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">

    <div class="banner ">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                        <ul class="nav nav-tabs">
                                <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class="active airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                            </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>
    <div class="container contents m-t-50-px aboutus-tabs">
<p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Airlines</a></p>
<div class="">
    <h4>Airlines</h4>
    <div class="clear-fix airways-top">
        <h5>JET Airways Booking Procedures on Galileo</h5>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 airways">
            <div class="downloads">
                <a href="">
                    <img src="<?php echo get_template_directory_uri().'/assets/products/stepbystepguide.png';?>" class="icon" />
                    <img src="<?php echo get_template_directory_uri().'/assets/products/stepbystepguide_white.png';?>" class="icon-white">
                    <p>Step by step guide</p>
                    <img class="download-icon" src="<?php echo get_template_directory_uri().'/assets/products/download.png';?>" />
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 airways">
            <div class="downloads">
                <a href="">
                    <img src="<?php echo get_template_directory_uri().'/assets/products/faq.png';?>" class="icon" />
                    <img src="<?php echo get_template_directory_uri().'/assets/products/faq_white.png';?>" class="icon-white">
                    <p>FAQ</p>
                    <img class="download-icon" src="<?php echo get_template_directory_uri().'/assets/products/download.png';?>" />
                </a>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <div class="container clear-fix airways-top">
        <h5>Air India Booking Procedures on Galileo</h5>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 airways">
            <div class="downloads">
                <a href="<?php echo get_template_directory_uri().'/assets/products/airlines/air-india/AI 1G-Step-by-Step-Guide_11-18.pdf';?>" target="_blank">
                    <img src="<?php echo get_template_directory_uri().'/assets/products/stepbystepguide.png';?>" class="icon" />
                    <img src="<?php echo get_template_directory_uri().'/assets/products/stepbystepguide_white.png';?>" class="icon-white">
                    <p>Step by step guide</p>
                    <img class="download-icon" src="<?php echo get_template_directory_uri().'/assets/products/download.png';?>" />
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 airways">
            <div class="downloads">
                <a href="<?php echo get_template_directory_uri().'/assets/products/airlines/air-india/FAQs-AI_11-18.pdf';?>" target="_blank">
                    <img src="<?php echo get_template_directory_uri().'/assets/products/faq.png';?>" class="icon" />
                    <img src="<?php echo get_template_directory_uri().'/assets/products/faq_white.png';?>" class="icon-white">
                    <p>FAQ</p>
                    <img class="download-icon" src="<?php echo get_template_directory_uri().'/assets/products/download.png';?>" />
                </a>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row air-india-video training-module clear-fix">
            <p class="module">Training Module</p>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="video-link">
                    <a href="https://www.youtube.com/watch?v=QBCbChGzzDg" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/products/play.png';?>" />
                        <p>English Version</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="video-link">
                    <a href="https://www.youtube.com/watch?v=hHKMfyupHg0" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/products/play.png';?>" />
                        <p>Hindi Version</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="video-link">
                    <a href="https://www.youtube.com/watch?v=curDMwaFh6Q" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/products/play.png';?>" />
                        <p>Punjabi Version</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="video-link">
                    <a href="https://www.youtube.com/watch?v=diRkmvDfaCI" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/products/play.png';?>" />
                        <p>Bengali Version</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="video-link">
                    <a href="https://www.youtube.com/watch?v=JXUn7bf_1dE" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/products/play.png';?>" />
                        <p>Gujrati Version</p>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-4">
                <div class="video-link">
                    <a href="https://www.youtube.com/watch?v=PlKkLSCei9Q" target="_blank">
                        <img src="<?php echo get_template_directory_uri().'/assets/products/play.png';?>" />
                        <p>Malayalam Version</p>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    <div class="container clear-fix airways-top">
        <h5>Indigo Booking Procedures on Galileo</h5>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 airways">
            <div class="downloads">
                <a href="<?php echo get_template_directory_uri().'/assets/products/airlines/indigo/Step-by-Step guide for 6E booking on Galileo.docx';?>" target="_blank">
                    <img src="<?php echo get_template_directory_uri().'/assets/products/stepbystepguide.png';?>" class="icon" />
                    <img src="<?php echo get_template_directory_uri().'/assets/products/stepbystepguide_white.png';?>" class="icon-white">
                    <p>Step by step guide</p>
                    <img class="download-icon" src="<?php echo get_template_directory_uri().'/assets/products/download.png';?>" />
                </a>
            </div>
        </div>

    </div>
    <!-- <p>&nbsp;</p>
    <p>&nbsp;</p> -->
</div>
</div>

    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>