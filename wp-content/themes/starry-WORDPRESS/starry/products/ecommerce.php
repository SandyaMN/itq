<?php
/**
 * Template Name: ecommerce-api
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/eCommerce.js';?>"></script>


    <div class="banner ">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class=" airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- contents -->
    <div class="container content">
        <p class="path">
            <a class="static_links" href="<?php echo get_home_url(); ?>">Home / </a>
            <a class="static_links" href="<?php echo get_home_url(); ?>/product-travelport/">Products / </a>
            <a class="dynamic-link" href="">E-Commerce </a>
        </p>
        <!-- cards -->
        <div class="heading profile">
            <h3 class="m-b-0">E-Commerce</h3>
        </div>
        <div class="card-container m-t-25px m-b-30-px">
            <div class="">
                <div class="col-sm-4">
                    <div class="card card-default b-r-0 b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/epricing';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/epricing.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/epricing_white.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-center">

                                <div class="img-text">E-Pricing</div>
                                <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="card card-default b-r-0 b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/epricing';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/webservice.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/webservice_white.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-center">
                                <div class="img-text">Galileo Web Services</div>
                                <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="card card-default b-b-0" onclick="location.href='<?php echo get_home_url(); ?>/epricing';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/minirule_service.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/minirule_service_white.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-center">

                                <div class="img-text">Mini Rule Service</div>
                                <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="">
                <div class="col-sm-4">
                    <div class="card card-default b-r-0" onclick="location.href='<?php echo get_home_url(); ?>/epricing';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/ticket_exchange.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/ticket_exchange_white.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-center">
                                <div class="img-text">Ticket Xchange API- VR3</div>
                                <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                <!-- <p>Office Management</p> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="card card-default b-r-0" onclick="location.href='<?php echo get_home_url(); ?>/epricing';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">

                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/universal-api-white.png';?>" class="card-img-responsive">
                            </div>

                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/universal-api.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-center">

                                <div class="img-text">UNIVERSAL API (uAPI)</div>
                                <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="card card-default" onclick="location.href='<?php echo get_home_url(); ?>/epricing';">
                        <div class="card-header"></div>
                        <div class="card-body card-6-6">
                            <div class="card-img">
                                <img src="<?php echo get_template_directory_uri().'/assets/xml_api.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-img-white">
                                <img src="<?php echo get_template_directory_uri().'/assets/xml_api_white.png';?>" class="card-img-responsive">
                            </div>
                            <div class="card-center">
                                <div class="img-text">XML API Desktop</div>
                                <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>