<?php
/**
 * Template Name: enhanced-utilities
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <!-- <script src="<?php echo get_template_directory_uri().'/js/product.js';?>"></script> -->

    <div class="banner ">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                        <ul class="nav nav-tabs">
                                <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class=" airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                            </ul>
                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>
    <div class="container contents m-t-50-px ">
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page" href="">Enhanced Utilities</a></p>
        <div class="aboutus-tabs m-t-0">
            <!-- <h4>Enhanced Utilities</h4> -->
            <div class="clear-fix row enhanced-utilities">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 enhanced-utilities-tabs">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-stacked tabs-right">
                            <li class="active"><a href="#advanced-hmps" data-toggle="tab">
                                    <!--<img src="assets/products/stepbystepguide.png';?> />-->Advanced HMPR</a></li>
                            <li><a href="#e-ticket" data-toggle="tab">E-Ticket Application</a></li>
                            <li><a href="#flight-info" data-toggle="tab">Galileo flight info(PNR SMS)</a></li>
                            <li><a href="#travel-insurance" data-toggle="tab">Travel Insurance</a></li>
                            <li><a href="#itq-financial" data-toggle="tab">ITQ Financial</a></li>
                            <li><a href="#llc-integrator" data-toggle="tab">LCC Integrator</a></li>
                            <li><a href="#vr3" data-toggle="tab">VR3</a></li>
                            <li><a href="#bsp-quota" data-toggle="tab">BSP Quota tool</a></li>
                            <li><a href="<?php echo get_template_directory_uri().'/assets/products/Low-Fare-Finder.pdf';?>" target="_blank">Low fare finder</a></li>

                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="advanced-hmps">
                        <h4>Advanced HMPR</h4>
                        <p>An application that allows the user to generate ticket issuance reports in detail. It can
                            also be configured to work on a central machine in the agency which would then generate
                            reports automatically at a specific time and send the reports over email.</p>
                    </div>
                    <div class="tab-pane" id="e-ticket">
                        <h4>E ticket Application</h4>
                        <p>Electronic ticket as per the standards specified by the Director General of Civil Aviation.
                            Bookings once issued gets automatically processed in the specific format. User can add
                            service fee if applicable, and generate the e ticket for the passenger. If required agency
                            can add his remarks in the e-ticket for the information of the passenger. User has the
                            capability of adding the tickets into his mail client directly from the site and send it in
                            one go to the customer.</p>
                    </div>
                    <div class="tab-pane" id="flight-info">
                        <h4>Galileo Flight Info (PNR SMS)</h4>
                        <p>FLIFO allows the user to check the status of the flights and the details of departure and
                            arrival for the participant carriers. Data gets picked directly from the participant
                            carriers database and shown at the user end.</p>
                    </div>
                    <div class="tab-pane" id="travel-insurance">
                        <h4>Travel Insurance</h4>
                        <p>Travel Insurance allows the user to issue insurance policies to their travelers on the
                            participant vendors and get them recorded in the same PNR. It uses the same credentials as
                            the user would use on the vendor site. Insurance data once integrated with the PNR ,
                            becomes a single record and this would get generated to the back office through the MIR
                            files.</p>
                    </div>
                    <div class="tab-pane" id="itq-financial">
                        <h4>ITQ Financial</h4>
                        <p>ITQ Financial, a web interface integrated with Travelport Smartpoint (Galileo) and powered
                            by TallyTM is an online back-office travel accounting software that automates the
                            accounting processes for Travel Management Companies (TMCs), Travel Agencies and Online
                            Travel Agencies (OTAs) seamlessly.</p>
                        <br />
                        <p class="bold ticketing-subheading">Features:</p>
                        <ul class="product-list">
                            <li>Consolidated report generation from global to branch level accounts management</li>
                            <li>Dynamic invoicing template to add required charges</li>
                            <li>Effective Business Reports – Revenue, Credit Control and MIS</li>
                            <li>Lively Dashboard with intelligent commercial data statistics</li>
                            <li>Online support module</li>
                            <li>Single interface auto invoicing- Air, Hotel, Car, Rail, Visa, Insurance, Forex</li>
                            <li>Tally Integration for Statutory reports and financial audits</li>
                            <li>Various auto reconciliations- BSP, HMPR, Bank and Vendor </li>
                            <li>Web based PCI-DSS certified, GST & Ind-AS compliant</li>
                        </ul>
                    </div>
                    <div class="tab-pane" id="llc-integrator">
                        <h4>LCC integrator </h4>
                        <p>An application that helps the user to integrate the LCC segments to the Galileo system. The
                            PNR as created in the Indian LCCs like IndiGo, SpiceJet and GoAir gets integrated into a
                            new PNR if there are not live data available, else it would get integrated with an existing
                            PNR if the names match. Post integration the MIR images are generated by the application
                            automatically.</p>
                    </div>
                    <div class="tab-pane" id="vr3">
                        <h4>VR3</h4>
                        <p>VR3 is an application and a consolidated solution for Reissue, Refund, Revalidation and
                            Void. A user can launch this application and continue with the desired functionality within
                            the workflow on GDS due to interface capability available on Travelport.</p>
                        <br />
                        <p class="bold ticketing-subheading">Features:</p>
                        <ul class="product-list">
                            <li>Only product with a capability of reading CAT 16</li>
                            <li>Provides Fare Build mask</li>
                            <li>Reduces time consumption in manual reissue</li>
                            <li>Simplified subsequent reissues with auto re-calculation of taxes</li>
                            <li>Auto population of account codes and ticket modifiers in case of reissue of private
                                fares</li>
                        </ul>
                    </div>
                    <div class="tab-pane" id="bsp-quota">
                        <h4>BSP Quota Tool</h4>
                        <p>A web based tool used by Travel Agencies and Airlines to manage and provision tickets
                            quota. This application also provides capping request through SMS.</p>
                    </div>
                    <div class="tab-pane" id="fare-finder">
                        <h4></h4>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>