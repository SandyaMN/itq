<?php
/**
 * Template Name: worldspan
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    
    <!-- <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"> -->
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" -->
    <!-- crossorigin="anonymous"> -->
    <!-- <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet"> -->

    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>

    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>

    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script> -->
    <script src="<?php echo get_template_directory_uri().'/js/worldspan.js';?>"></script>
    <!-- <script src="//code.jquery.com/jquery-1.11.1.min.js"></script> -->
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/worldspan.css';?>" rel="stylesheet">


    <div class="banner ">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class=" airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li ><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <!-- contents -->
    <div class="container content">

        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page" href="">Worldspan</a></p>
        <!-- cards -->

        <div class="heading profile ">
            <h3 class="m-t-0-px">Worldspan</h3>
        </div>
        <!-- <div class="container-accordion"> -->
        <div class="subheading-us">
            <div class="container">
                <div class="subheading-menu m-t-0">
                    <ul class="nav sub nav-tabs">
                        <li class="active"><a data-toggle="tab" href="#travelagency">Travel Agency
                            </a>
                        </li>
                        <li><a data-toggle="tab" href="#travelSupplier">Travel Supplier</a></li>
                        <li><a data-toggle="tab" href="#OnlineSolution">Online Solution</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container subheading-tabs">
            <div class="tab-content">
                <div id="travelagency" class="tab-pane fade active in">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> Desktop</span>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 accordion-tab" href="#worldspan">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">WORLDSPAN GO
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#script">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">SCRIPTS</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#worldspango">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">WORLDSPAN GO ® EXPRESS</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default accordion-tab" href="#">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> Ticketing</span>
                                    </a>
                                </h4>

                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 accordion-tab" href="#eTicketing">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text"> E-TICKETING</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 accordion-tab" href="#elecronicTicket">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">ELECTRONIC TICKET TRACKER</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#conslidated">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text"> CONSOLIDATED PNR
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 accordion-tab" href="#auto">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text"> AUTOMATED REFUNDS
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> Fares</span>
                                    </a>
                                </h4>

                            </div>
                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4 accordion-tab" href="#worldspanEpricing">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text"> WORLDSPAN E-PRICING
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 accordion-tab" href="#worldspanasecure">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text">WORLDSPAN SECURATE AIR NET</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 accordion-tab" href="#worldspanFareSource">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text"> WORLDSPAN FARE SOURCE</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">


                                        <div class="col-md-6 accordion-tab" href="#power">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text"> POWER SHOPPER
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 accordion-tab" href="#securateAir">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text"> SECURATE AIR PLUS</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> NON-AIR
                                        </span>
                                    </a>
                                </h4>

                            </div>
                            <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4 accordion-tab" href="#hotelsolutions">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            WORLDSPAN HOTEL SOLUTIONS</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 accordion-tab" href="#carselected">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">

                                                        <div class="
                                                                 img-text">CAR
                                                            SELECT
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 accordion-tab" href="#worldspanroom">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            WORLDSPAN ROOM & MORE</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 accordion-tab" href="#queues">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            QUEUES</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 accordion-tab" href="#customitineary">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            CUSTOM ITINERARY
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 accordion-tab" href="#mytripandmore">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            MY TRIP AND MORE</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> VALUE ADDS
                                        </span>
                                    </a>
                                </h4>

                            </div>
                            <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-4 accordion-tab" href="#worldfile">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">


                                                        <div class="
                                                                 img-text">
                                                            WORLD FILE
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 accordion-tab" href="#defaultrecord">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">

                                                        <div class="
                                                                 img-text">DEFAULT
                                                            RECORD INDEX</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-4 accordion-tab" href="#remarks">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            EMAIL REMARKS
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 accordion-tab" href="#qualitycheck">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            QUALITY CHECK
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 accordion-tab" href="#worldspanaccounting">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            WORLDSPAN ACCOUNTING INTERFACE</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 accordion-tab" href="#scriptPro">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="
                                                                 img-text">
                                                            SCRIPT PRO
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="tab-pane fade" id="travelSupplier">
                    <div class="panel-group" id="accordionOne" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordionOne" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> AIR</span>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseEight" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 accordion-tab" href="#securate-air-plus">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text">SECURATE AIR PLUS
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#span-go">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">WORLDSPAN GO</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#epricing">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">E-PRICING</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordionOne" href="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> NON-AIR</span>
                                    </a>
                                </h4>

                            </div>
                            <div id="collapseseven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 accordion-tab" href="#hotelsolutions">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text"> WORLDSPAN HOTEL SOLUTIONS</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2 accordion-tab" href="#carselected">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text"> CAR SELECT</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#worldspanroom">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text"> WORLDSPAN ROOMS AND MORE
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="OnlineSolution">
                    <div class="panel-group" id="accordionOne" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordionOne" href="#collapseEight" aria-expanded="true" aria-controls="collapseEight">
                                        <span> <img class="sub-heading-accordion" src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" alt=""></span>
                                        <span class="sub-heading-accordion"> ONLINE SOLUTION</span>
                                    </a>
                                </h4>
                            </div>

                            <div id="collapseEight" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-3 accordion-tab" href="#xml-pro">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-center">
                                                        <div class="img-text">WORLDSPAN XML PRO
                                                        </div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#pricing">
                                            <div class="card card-default b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">E-PRICING</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3 accordion-tab" href="#galstar">
                                            <div class="card card-defaultn b-r-0 b-b-0 card-accordion-clicked">
                                                <div class="card-header"></div>
                                                <div class="card-body card-6-6">
                                                    <div class="card-img">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book.png';?>" class="card-img-responsive">
                                                    </div>
                                                    <div class="card-img-white">
                                                        <img src="<?php echo get_template_directory_uri().'/assets/book_blue.png';?>" class="card-img-responsive">
                                                    </div>

                                                    <div class="card-center">
                                                        <div class="img-text">GALSTAR</div>
                                                        <div class="arrow-block"> <img src="<?php echo get_template_directory_uri().'/assets/arrow.png';?>" alt="" class="arrow"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- tabs -->
        <div class="container container-side-tabs">

            <div class="">
                <div class="col-lg-3 col-md-3 col-sm-3 worlspan-menu">
                    <!-- menu -->
                    <div id="main-menu">
                        <div class="list-group panel">
                            <a href="#inner-menu1" onclick="changeicon(this)" class="list-group-item list-group-item-success main-menu-a" data-toggle="collapse"
                                data-parent="#main-menu">Travel Agency
                            </a>
                            <div class="collapse sub-menu" id="inner-menu1">
                                <a href="#first-menu" class="list-group-item sub-menu-a" onclick="submenu(this)" data-toggle="collapse" data-parent="#inner-menu1"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>"> Desktop
                                </a>
                                <div class="collapse list-group-submenu" id="first-menu">
                                    <a href="#worldspan" onclick="tabselect(this)" class="list-group-item go inner-menu-a" data-parent="#first-menu">Worldspan
                                        Go
                                    </a>
                                    <a href="#script" onclick="tabselect(this)" class="list-group-item script inner-menu-a" data-parent="#first-menu">Scripts</a>
                                    <a href="#worldspango" onclick="tabselect(this)" class="list-group-item go-express inner-menu-a" data-parent="#first-menu">Worldspan
                                        Go @Express</a>
                                </div>
                                <a href="#first-menu1" class="list-group-item sub-menu-a" data-toggle="collapse" onclick="submenu(this)" data-parent="#inner-menu1"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>"> Ticketing
                                </a>
                                <div class="collapse list-group-submenu" id="first-menu1">
                                    <a href="#eTicketing" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu1">E-Ticketing</a>
                                    <a href="#elecronicTicket" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu1">Electronic
                                        Ticket Tracker
                                    </a>
                                    <a href="#conslidated" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu1">Consolidated
                                        PNR
                                    </a>
                                    <a href="#auto" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu1">Automated
                                        Refunds
                                    </a>
                                </div>
                                <a href="#first-menu2" class="list-group-item sub-menu-a" data-toggle="collapse" onclick="submenu(this)" data-parent="#inner-menu1"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>"> Fares
                                </a>
                                <div class="collapse list-group-submenu" id="first-menu2">
                                    <a href="#worldspanEpricing" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu2">Worldspan
                                        E-Pricing
                                    </a>
                                    <a href="#worldspanasecure" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu2">Worldspan
                                        Securate Air Net
                                    </a>
                                    <a href="#worldspanFareSource" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu2">Worldspan
                                        Fare Source</a>
                                    <a href="#power" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu2">Power
                                        Shopper
                                    </a>
                                    <a href="#securateAir" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu2">Securate
                                        Air Plus</a>
                                </div>
                                <a href="#first-menu3" class="list-group-item sub-menu-a" data-toggle="collapse" onclick="submenu(this)" data-parent="#inner-menu1"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>"> Non-Air
                                </a>
                                <div class="collapse list-group-submenu" id="first-menu3">
                                    <a href="#hotelsolutions" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu3">Worldspan
                                        Hotel Solutions
                                    </a>
                                    <a href="#carselected" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu3">Car
                                        Select
                                    </a>
                                    <a href="#worldspanroom" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu3">Worldspan
                                        Room & More</a>
                                    <a href="#queues" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu3">Queues</a>
                                    <a href="#customitineary" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu3">Custom
                                        Itinerary
                                    </a>
                                    <a href="#mytripandmore" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu3">My
                                        Trip and More
                                    </a>
                                </div>
                                <a href="#first-menu4" class="list-group-item sub-menu-a" data-toggle="collapse" onclick="submenu(this)" data-parent="#inner-menu1"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>"> Value Adds
                                </a>
                                <div class="collapse list-group-submenu" id="first-menu4">
                                    <a href="#worldfile" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu4">World
                                        File
                                    </a>
                                    <a href="#defaultrecord" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu4">Default
                                        Record Index
                                    </a>
                                    <a href="#remarks" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu4">Email
                                        Remarks
                                    </a>
                                    <a href="#qualitycheck" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu4">Quality
                                        Check
                                    </a>
                                    <a href="#worldspanaccounting" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu4">Worldspan
                                        Accounting Interface</a>
                                    <a href="#scriptPro" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#first-menu4">Script
                                        Pro
                                    </a>
                                </div>
                            </div>
                            <a href="#inner-menu2" onclick="changeicon(this)" class="list-group-item list-group-item-success main-menu-a" data-toggle="collapse"
                                data-parent="#main-menu">Travel Supplier
                            </a>
                            <div class="collapse sub-menu" id="inner-menu2">
                                <a href="#second-menu" class="list-group-item sub-menu-a" data-toggle="collapse" onclick="submenu(this)" data-parent="#inner-menu2"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>"> Air</a>
                                <div class="collapse list-group-submenu" id="second-menu">
                                    <a href="#securate-air-plus" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#second-menu">Securate
                                        Air Plus</a>
                                    <a href="#span-go" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#second-menu">Worldspan
                                        Go
                                    </a>
                                    <a href="#epricing" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#second-menu">E-Pricing</a>
                                </div>
                                <a href="#second-menu2" class="list-group-item sub-menu-a" data-toggle="collapse" onclick="submenu(this)" data-parent="#inner-menu3"><img
                                        class="img-responsive-small" src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>"> Non-Air</a>
                                <div class="collapse list-group-submenu" id="second-menu2">
                                    <a href="#hotelsolutions" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#second-menu2">Worldspan
                                        Hotel Solutions</a>
                                    <a href="#carselected" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#second-menu2">Car
                                        Select
                                    </a>
                                    <a href="#worldspanroom" onclick="tabselect(this)" class="list-group-item inner-menu-a" data-parent="#second-menu2">Worldspan
                                        Room & More</a>
                                </div>
                            </div>
                            <a href="#inner-menu3" onclick="changeicon(this)" class="list-group-item list-group-item-success main-menu-a" data-toggle="collapse"
                                data-parent="#main-menu">Online Solutions
                            </a>
                            <div class="collapse sub-menu" id="inner-menu3">
                                <a href="#xml-pro" class="list-group-item sub-menu-a" onclick="submenu(this);tabselect(this)" data-toggle="collapse" data-parent="#inner-menu3">Worldspan
                                    XML Pro</a>
                                <a href="#pricing" class="list-group-item sub-menu-a" onclick="submenu(this);tabselect(this)" data-toggle="collapse" data-parent="#inner-menu3">E-Pricing</a>
                                <a href="#galstar" class="list-group-item sub-menu-a" onclick="submenu(this);tabselect(this)" data-toggle="collapse" data-parent="#inner-menu3">Galstar</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-6 content-worldspan contents">
                    <div class="worldspan-div" id="worldspan">
                        <h4>Worldspan Go!</h4>

                        <p>Worldspan Go! brings a spectrum of travel planning and management solutions to the travel agent's
                            desktop. Using browser-based travel technologies and the power of the Travelport Worldspan global
                            distribution system, your agency has access to a global marketplace of travel and related information.
                        </p>

                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Access to a world of travel resources through a customised reservation screen and up
                                    to 10 windows in split or full screen.</li>
                                <li>Accessible wherever the Internet is available, but can be restricted if needed for security.
                                </li>
                                <li>Secure login and password provide access to an intuitive suite of productivity tools,
                                    administration tools and online help.</li>
                                <li>Easy installation via a URL and automatic updates for immediate access to the latest
                                    functionality.
                                </li>
                                <li>Agencies can employ user-owned hardware and choose from several connectivity options.
                                </li>

                                <li>Easy access to Travelport Worldspan's world-class Global Learning Centre, featuring virtual
                                    classrooms, self-paced tutorials and more.</li>
                                <li>Integrates front- and back-office systems, and also integrates with agency Intranets
                                    and Extranets.
                                </li>
                                <li>Incorporates the GUI interface and the Cryptic command page in one display</li>
                                <li>Carries with it a bouquet of set defaults including over 75 scripts and Custom links
                                    page to simplify agency operations</li>
                            </ul>
                        </div>

                        <div class="airways-top">
                            <h5>Scripts</h5>
                            <p>Worldspan Scripts have the power to automate complex reservation functions, reduce keystrokes,
                                increase accuracy and ensure consistent levels of service.</p>
                        </div>

                        <div class="airways-top">
                            <h5>HIGHLIGHTS</h5>
                            <p>Worldspan Go! Res has two types of scripts: Go! and ScriptPro. They each provide the same
                                benefits but have a different look and feel.</p>
                        </div>

                        <div class="airways-top">
                            <h5>WORKFLOW</h5>
                            <p>Worldspan scripts are accessed from the Index Page in Worldspan Go! Res. The file names of
                                all standard ScriptPro and Go! scripts can be accessed from the All Scriptsdrop down alphabetical
                                list. User-friendly script names are grouped on the applicable index tab. The list of scripts
                                on the Custom Tab are dynamically created from the list of custom scripts currently on the
                                local fileserver.
                            </p>
                        </div>

                    </div>

                    <div class="worldspan-div" id="script">
                        <h4>Scripts</h4>

                        <p>Worldspan Scripts have the power to automate complex reservation functions, reduce keystrokes,
                            increase accuracy and ensure consistent levels of service. Worldspan Scripts perform screen-scraping
                            capabilities, execute complex cryptic entries, tedious tasks or call up URL sites and write data
                            to files or to the Go! window. </p>

                        <p class="bold">There are three levels of scripts:</p>

                        <ul class="product-list">
                            <li>Standard Scripts is a suite of 90 Standard Scripts available including topics such as air,
                                car, hotel and Power Pricing entries.</li>
                            <li>Value-Added Scripts are low-cost productivity tools provide scripting for a range of capabilities
                                including an E-Ticket Tracker, PNR Quality Control, Travelport Worldspan SecuRate Air Assistance
                                and more.</li>
                            <li>Custom Scripts are created by Travelport Worldspan or an agency's scriptwriter to meet a
                                customer's unique needs.</li>
                        </ul>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Scripts provide a powerful method for automating repetitive tasks.</li>
                                <li>User friendly options help new agents become productive immediately.</li>
                                <li>Reduces learning curve.</li>
                                <li>Customizable for new procedures or to accommodate unique / changing business needs.</li>
                                <li>Easy to enforce travel-booking policies for corporate clients.</li>
                                <li>Saves time thus increasing efficiency and productivity.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="worldspango">
                        <h4>Worldspan Go!® Express</h4>

                        <p>Worldspan Go!® Express provides an easy to use and understandable tool for your agency. This tool
                            is context-sensitive for Air, Car, Hotel and Fares⁄Pricing, which reads your input and displays
                            clickable options for each step. </p>

                        <p class="bold">There are three levels of scripts:</p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Tool can be toggled on and off as desired.</li>
                                <li>Right frame changes based on the user input in Go! Res and provides applicable continuation
                                    entries.
                                </li>
                                <li>Right frame includes buttons for quick entry selection.</li>
                                <li>Interactive area includes color-coded Res information that provides information and⁄or
                                    processes functionality.
                                </li>
                                <li>Provides access to applicable script.</li>
                                <li>Provides a Format Assistant button which accesses Agent Assistant for help with the specific
                                    formats entered.</li>
                                <li>Includes Air, Car, Hotel and Fares⁄Pricing.</li>
                                <li>The Go! Res! Navigation Bar includes Working Frame and My Script. These can be useful
                                    when using Go! Express. The Go! Express displays reside in the right frame, this is the
                                    Working Frame. When a user desires to launch a Go! Script, select My Script and essentially
                                    have another Working Frame. Select Working Frame to return to the where you left off
                                    in the tool.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="eTicketing">
                        <h4>E-Ticketing</h4>

                        <p>Electronic Ticketing enables ticket less travel for customers who prefer to travel without traditional
                            paper tickets. It is convenient for travelers, and it helps reduce costs associated with ticket
                            distribution. Additionally, travel agents can instantly handle refunds, exchanges, and cancellations,
                            since customers don't need to return tickets for processing. Currently there are over 40 airlines
                            in more than 65 countries participating in Travelport Worldspan Electronic Ticketing.</p>
                    </div>

                    <div class="worldspan-div" id="elecronicTicket">
                        <h4>Electronic Ticket Tracker</h4>

                        <p>Processes the pastdate "OPEN FOR USE" electronic ticket coupons based on the defaults entered
                            in the script's setup utility.</p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Default settings in the setup utility, including the past date default and the option
                                    to automatically print the OPEN FOR USE past date coupons, which are required elements
                                    and are in place to provide customization to the script's functionality and process.</li>
                                <li>With each Add and Update, the script re-evaluates the status of all previously filed
                                    ETRs and updates the files according to the coupon and past date status and default settings,
                                    thereby maintaining current report data.
                                </li>
                                <li>The last coupon date in the ETR is used, in addition to the past date default entered
                                    in the setup utility, when processing an ETR as past date and OPEN FOR USE.</li>
                                <li>The Setup Utility also enables PNR documenting and queuing options and includes the option
                                    to gather ETR sort fields, which is required for the sorting and customization of reports.
                                </li>
                                <li>Users may select the option to automatically print past date OPEN FOR USE coupons as
                                    they are displayed, but may also choose to file, and then print all past date open for
                                    use coupons from the main menu at a later time. The coupons of single or multiple ETRs
                                    may also be selected, and then printed from a previewed report in the report generator.</li>
                                <li>Report generator options allow users to print and⁄or remove coupons from reports which
                                    you can preview, print, backup and sort by various sort fields. The "Capture Sort Fields"
                                    option must be checked in the setup utility for the sort option to work.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="conslidated">
                        <h4>Consolidated PNR</h4>

                        <p>The consolidator pnr allows a Travelport Worldspan customer (referred to as the retail agent)
                            to selectively release individual pnrs to another Travelport Worldspan customer (referred to
                            as the consolidator) for servicing and⁄or ticketing without having a bridge relationship.</p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>A retail agent can permit different pnr fields and pnr functions for different consolidators
                                    depending on the nature of the business relationship. The servicing ⁄ticketing⁄ consolidator
                                    can never assume pnr ownership.</li>
                                <li>In order to release individual pnrs each retail agent must first establish a relationship
                                    with each consolidator by creating a consolidator template. The retail agent enters in
                                    the template the subscriber*s id (sid) of the consolidator and selects from among various
                                    pnr fields options viewable to the consolidator and pnr functions permitted by the consolidator
                                    on each released pnr.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="auto">
                        <h4>Automated Refunds</h4>

                        <p>Travelport Worldspan provides functionality to allow automated refund reporting for paper and
                            electronic tickets.
                        </p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Travelport Worldspan provides the functionality to fully automate the refund process
                                    for paper documents by auto filling the required data needed to issue the refund from
                                    the ticket database. The ticket database contains 13 months of ticket information and
                                    is created specifically for the purpose of processing an automated refund.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="worldspanEpricing">
                        <h4>Worldspan E-Pricing</h4>

                        <p>The global standard in low-fare shopping technology, Travelport e-Pricing has revolutionized the
                            worldwide travel shopping experience through highly scalable tools that meet market demand for
                            more fare choices today and in the future. Travelport e-Pricing is the global standard in low-fare
                            shopping technology.
                        </p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Travel agencies, corporate travel buyers and online shoppers are able to shop, price
                                    and book low fares with unprecedented speed and accuracy. Worldwide searches are completed
                                    in seconds - with one powerful request.</li>
                                <li>Travel agencies of any size or business orientation are empowered to compete head-on
                                    in today's evolving travel marketplace.</li>
                                <li>Travelport e-Pricing eliminates the inefficiency of logging into different systems to
                                    find and book optimal choices for travelers.</li>
                                <li>Built on the GDS industry's first multi-server-based pricing technology, e-Pricing is
                                    capable of searching millions of fares and hundreds of thousands of itinerary options
                                    to find the lowest available fares.</li>
                                <li>Each e-Pricing solution is designed to orchestrate and expedite a unique type of search:
                                </li>
                                <li>Power Shopper® searches for low fares prior to booking.</li>
                                <li>Power Pricing® searches for the lowest available fare (worldwide) for an existing itinerary.
                                </li>
                                <li>TPower Flight Search investigates multiple dates with a specified price.</li>
                                <li>Flex Shopping Options find low fares based on flexible travel dates or destinations.</li>
                            </ul>
                        </div>
                    </div>


                    <div class="worldspan-div" id="worldspanasecure">
                        <h4>Worldspan Securate Air Net</h4>

                        <p>The global standard in low-fare shopping technology, Travelport e-Pricing has revolutionized the
                            worldwide travel shopping experience through highly scalable tools that meet market demand for
                            more fare choices today and in the future. Travelport e-Pricing is the global standard in low-fare
                            shopping technology.
                        </p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Flexible technology integrates with Airline Tariff Publishing Company (ATPCO) to allow
                                    you to take advantage of carrier-filed automated fares and rules</li>
                                <li>You can simultaneously employ Worldspan SecuRate® Air Plus, a Web tool that gives you
                                    the internal ability to create and control your own negotiated fares databases.</li>
                                <li>Reflects all fares, including private, published and Web fares, in one integrated display
                                    - within seconds.</li>
                                <li>Integrates with Travelport Worldspan Automated Rules to provide up-to-the minute validation.
                                </li>
                                <li>Interfaces with Travelport Worldspan's tax package to ensure up-to-date taxes are applied.
                                </li>
                                <li>Manages fares through multiple ATPCO categories, including: Category 31 (Voluntary Changes),
                                    Category 35 (Net Fares), Category 15 (Private Fares) and Category 25 (Fare by Rule).</li>
                                <li>Places security controls on view, sell, ticketing and redistribution functions.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="worldspanFareSource">
                        <h4>Worldspan Fare Source</h4>

                        <p>FareSource is the new name for Travelport Worldspan's Fares and Pricing subscription service.
                            Subscribers get access to hosted and participating carriers fares, and as a service to carriers
                            hosted in other systems. It has:
                        </p>

                        <ul class="product-list">
                            <li>Comprehensive range of Fares Displays.</li>
                            <li>Carrier Specific, MoneySaver, Integrated.</li>
                            <li>Reflects all fares, including private, published and Web fares, in one integrated display
                                - within seconds.
                            </li>
                            <li>Vast Fares database.</li>
                            <li>ATPCO and SITA</li>
                            <li>Integrates Private and Public fare displays.</li>
                            <li>Display fares by Fare type, Passenger type and more qualifiers.</li>
                            <li>Can price multiple passenger types and fare types.</li>
                            <li>Converts currencies based on up to date ROE.</li>
                            <li>Travelport Worldspan features a mini text rules display from fare display which highlights
                                the most accessed rule information in short format.</li>
                            <li>Low fare finder verifies or rebooks an itinerary at the lowest applicable fare based on booking
                                class availability and fare qualification rules.</li>
                            <li>Industry leading 13 month historical fare database.</li>
                        </ul>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Enhanced customer servicing as better options can be accessed.</li>
                                <li>Provides availability from fare display and vice versa.</li>
                                <li>Never pressed for information as have all options accessible.</li>
                                <li>Travelport Worldspan guarantees all auto priced itineraries.</li>
                                <li>SecuRate Air provides access and distribution rights of own fares to the agency.</li>
                                <li>Improves efficiency and enhances company image.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="power">
                        <h4>Power Shopper</h4>

                        <p>Power Shopper is applicable to subscribers of power pricing. Power Shopper will serach for low
                            fare alternates for a domestic and ⁄or international trip from a streamlined entry.
                        </p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Flexibility to chose response levels that pre-determine the maximum number of itinerary
                                    options returned to consumers.</li>
                                <li>Ability to apply numerous qualifiers to searches, including non-stop or direct flights,
                                    specific or preferred carriers, multiple airports, and more.</li>
                                <li>Integrating e-Pricing into the travel technology mix helps elevate Traditional agency
                                    and Web site revenues and enhance traveler satisfaction by delivering more options for
                                    fares.
                                </li>
                                <li>Offers Web sites affordable technology that competes head-on with travel retailers of
                                    any size
                                </li>
                                <li>Select from a value-added suite of solutions and a subset of e-Pricing by finding low
                                    fares based on flexible travel dates or origins⁄destinations.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="securateAir">
                        <h4>Securate Air Plus</h4>

                        <p>The Web-based SecuRate Air Plus is a leader in negotiated fares management for airlines. SecuRate
                            Air is a key component of Travelport Worldspan by Travelport?'s industry-leading Fares and Pricing
                            suite.Flexible technology integrates with Airline Tariff Publishing Company (ATPCO) to allow
                            you to take advantage of carrier-filed automated fares and rules.You can simultaneously employ
                            Worldspan SecuRate® Air Plus, a Web tool that gives you the internal ability to create and control
                            your own negotiated fares databases.
                        </p>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Reflects all fares, including private, published and Web fares, in one integrated display
                                    - within seconds.</li>
                                <li>Integrates with Travelport Worldspan Automated Rules to provide up-to-the minute validation..
                                </li>
                                <li>Interfaces with Travelport Worldspan's tax package to ensure up-to-date taxes are applied.
                                </li>
                                <li>Manages fares through multiple ATPCO categories, including: Category 31 (Voluntary Changes),
                                    Category 35 (Net Fares), Category 15 (Private Fares) and Category 25 (Fare by Rule).
                                </li>
                                <li>Places security controls on view, sell, ticketing and redistribution functions.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="hotelsolutions">
                        <h4>Worldspan Hotel Solutions</h4>

                        <p>Travelport Worldspan's dynamic technologies for the global hotel market serve hotel companies,
                            travel agencies, travel Web sites, tour operators and corporate travel buyers on nearly every
                            continent. Through the GDS-based Travelport Worldspan Hotel Select™ system and advanced, integrated
                            Web-based tools, Travelport Worldspan's commanding suite of hotel distribution and booking technologies
                            enable you to expand your reach in today's market and optimise your revenue opportunities.
                        </p>

                        <ul class="product-list">
                            <li> Travelport Worldspan Hotel Source is a participation level which allows users to access
                                a hotel supplier's internal reservation system for rates, availability, and rate rule information.</li>
                            <li>Travelport Worldspan Hotel AccessPlus is a participation level that provides users with availability
                                and rules stored in the Travelport Worldspan system.</li>
                            <li>Travelport Worldspan SecuRate Hotel allows rates negotiated with a hotel chain or a specific
                                hotel property to be securely shopped and booked by the appropriate customer.</li>
                        </ul>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Travelport Worldspan Hotel Select provides travel agencies with live connections to hotel
                                    companies worldwide, including real-time rate range, rate rules, availability status
                                    for tens of thousands of properties and instant booking confirmations.</li>
                                <li>Through Travelport Worldspan's industry-first hotel mapping and map-top booking capabilities,
                                    you can view, shop, compare and book hotel properties based on their proximity to landmarks
                                    or points of interest, and also use property images, hotel videos, 360° virtual tours
                                    and more to make the best selections for your travellers.</li>
                                <li>You have the ability to save hotels during a search and also save "My Favorite" hotel
                                    search for subsequent shopping requests.</li>
                                <li>Hotel Default Record, customisable by your SID and agent sine, promotes the use of your
                                    preferred suppliers and puts the office manager in control of policies.
                                </li>
                                <li>You can book hotel reservations in seconds using the Worldspan Go!® Hotel Booking Tool,
                                    which is simple to use and conveniently accessed right from your Go! desktop.</li>
                                <li>Intuitive point-and-click access to global hotel content and booking functionality helps
                                    travel buyers who are unfamiliar with traditional GDS formats.</li>
                                <li>Interactive functionality automatically populates reservation fields with traveller data
                                    from existing PNRs.</li>
                                <li>You can add your corporate or negotiated rates with one easy click.</li>
                            </ul>
                        </div>

                        <div class="airways-top">
                            <h5>Hotel Source</h5>
                            <p>Travelport Worldspan's dynamic technologies for the global hotel market serve hotel companies,
                                travel agencies, travel Web sites, tour operators and corporate travel buyers on nearly every
                                continent. Through the GDS-based Travelport Worldspan Hotel Select™ system and advanced,
                                integrated Web-based tools, Travelport Worldspan's commanding suite of hotel distribution
                                and booking technologies enable you to expand your reach in today's market and optimise your
                                revenue opportunities.</p>
                        </div>

                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Travelport Worldspan Hotel Select provides travel agencies with live connections to hotel
                                    companies worldwide, including real-time rate range, rate rules, availability status
                                    for tens of thousands of properties and instant booking confirmations.</li>
                                <li>Through Travelport Worldspan's industry-first hotel mapping and map-top booking capabilities,
                                    you can view, shop, compare and book hotel properties based on their proximity to landmarks
                                    or points of interest, and also use property images, hotel videos, 360° virtual tours
                                    and more to make the best selections for your travellers.</li>
                                <li>You have the ability to save hotels during a search and also save "My Favorite" hotel
                                    search for subsequent shopping requests.</li>
                                <li>Hotel Default Record, customisable by your SID and agent sine, promotes the use of your
                                    preferred suppliers and puts the office manager in control of policies.
                                </li>
                                <li>You can book hotel reservations in seconds using the Worldspan Go!® Hotel Booking Tool,
                                    which is simple to use and conveniently accessed right from your Go! desktop..</li>
                                <li>Intuitive point-and-click access to global hotel content and booking functionality helps
                                    travel buyers who are unfamiliar with traditional GDS formats.</li>
                                <li>Interactive functionality automatically populates reservation fields with traveller data
                                    from existing PNRs.</li>
                                <li>You can add your corporate or negotiated rates with one easy click.</li>
                            </ul>
                        </div>

                        <div class="airways-top">
                            <h5>Hotel Integrated</h5>
                            <p>Integrated Hotel Source participants provide True Availability directly from the associates'
                                internal reservation systems. When you include dates and number of people in your Hotel List
                                request, the following status codes appear to the left of the chain code in the response.</p>
                        </div>
                    </div>

                    <div class="worldspan-div" id="carselected">
                        <h4>Car Select</h4>

                        <p class="bold">Customer Profile: Agencies, corporations and internet travel retailers around the
                            globe that access the Worldspan GDS System.
                        </p>

                        <p>The Worldspan Car Select™ System delivers a commanding suite of car rental distribution and booking
                            technologies to the global car rental market, serving car rental suppliers, travel agencies,
                            travel web sites, tour operators and corporate travel buyers on nearly every continent.
                        </p>
                        <p>It maximizes visibility and bookings by providing 24/7 access to availability, pricing, images
                            and policies, along with advanced booking tools and instant confirmations. There are a number
                            of levels of participation & functionality which include;</p>
                        <p>Worldspan Car Source® is the highest participation level in the Worldspan GDS, providing you a
                            direct link for distributing real-time availability, rates and vehicle and location data.</p>
                        <p>Worldspan Car Source™ Complete Pricing transmits the total estimated rental cost within the availability
                            display and at the time of sell. Estimates include the cost of a rental period, taxes, fees and
                            all mandatory charges. This reduces rate discrepancies and unexpected charges for customers at
                            the rental counter.</p>
                        <p>Car Default Record is a fill-in template where travel retailers can define criteria to availability
                            searches, allowing them to designate certain options when searching e.g. unlimited mileage.
                        </p>
                        <p>Car UpSell™ is a valuable merchandising tool that automatically offers upgrades whilst travel
                            agents are booking vehicles in the Worldspan GDS.</p>
                        <p>eVouchers are enabled by participating car rental suppliers, providing Worldspan travel buyers
                            with an efficient, electronic and paperless system for prepaying reservations.</p>
                        <p>Car Rental Delivery and Collection, also supported by participating suppliers, lets buyers send
                            addresses for vehicle delivery and pick-up to suppliers via the GDS.</p>
                    </div>

                    <div class="worldspan-div" id="worldspanroom">
                        <h4>Worldspan Rooms And More</h4>
                        <p class="bold">Customer Profile: Leisure and corporate/leisure mixed travel agencies</p>
                        <p>Travelport Rooms and More is an innovative product that removes all the hassle of finding the
                            perfect hotel accommodation. By simply entering a few key pieces of information on the easy-to-use,
                            intuitive landing page, Travelport Rooms and More searches multiple sources on behalf of the
                            user and returns an easy-to-read list of accommodation choices that optimizes content choice
                            and potential commission revenue earned.</p>
                        <ul class="product-list">
                            <li>Access Anytime, anywhere, whether an existing customer of Travelport or not.</li>
                            <li>Breadth An abundance of rates and inventory, which allows for greater choice. In most instances
                                you will find three providers offering any one property.</li>
                            <li>Content Integrated with all Travelport global distribution systems (GDSs), with a single
                                sign on, no need to chase commission payments, content is available quickly from one place.</li>
                        </ul>
                    </div>

                    <div class="worldspan-div" id="queues">
                        <h4>Queues</h4>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>The Travelport Worldspan Queuing system is designed to provide maximum flexibility in
                                    organizing and prioritizing PNRs. To optimize productivity, queues can be established
                                    by category and date range. Additional efficiencies can be achieved by using tools such
                                    as PNR Search, a method for gathering PNRs that contain specified characteristics, and
                                    End, Work and Retrieve (EWR), a format that allows the user to process schedule changes
                                    and confirmations using a single keyboard entry.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="customitineary">
                        <h4>Custom Itinerary</h4>
                        <p>Enables agents to print an itinerary document to give, fax with their third party software, or
                            e-mail to their clients. Agents may customize the itinerary by adding a logo, address and generic
                            comments. Agents may also select ticketing information on the itinerary provided by the document
                            history.</p>
                    </div>

                    <div class="worldspan-div" id="mytripandmore">
                        <h4>My Trip And More</h4>
                        <p>Travelport ViewTrip is a secure Web site accessed by travellers who hold a Travelport Worldspan
                            by Travelport™ booking record. ViewTrip is your single source online authority for traveller
                            itinerary and electronic ticketing needs. Travellers can access personalized itineraries and
                            electronic tickets anytime, from any Internet-enabled location, free of charge. </p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Generates an e-mail to the traveller at the booking agent's request, once an itinerary
                                    has been booked.
                                </li>
                                <li>Travellers have 24⁄7 online access to personal itineraries, electronic ticket records
                                    and electronic expense receipts, including the ability to retrieve, review, print and
                                    e-mail data.</li>
                                <li>Customers of Travelport Worldspan users can choose among 10 language preferences.</li>
                                <li>Complete PNR segment and destination information can be accessed via ViewTrip, including
                                    flight confirmation numbers, departure and arrival times, electronic ticket information,
                                    flight duration and mileage, on-time flight status, meal information, hotels, car rentals,
                                    destination maps, weather, driving directions and more.</li>
                                <li>Integrated Online Check-In from participating airlines is available to Travelport Worldspan
                                    customers, currently from British Airways and Midwest Airlines, with more carriers planned.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="worldfile">
                        <h4>World File</h4>
                        <p>World files are created to store travel information for companies and individuals. Once the world
                            file is created information can be copied into individual pnrs to eliminate the need to manually
                            enter this information each.</p>
                    </div>

                    <div class="worldspan-div" id="defaultrecord">
                        <h4>Default Record Index</h4>
                        <p>System default records are fill-in templates that store frequently used options for selecting
                            and customizing displays- thereby shortening entries and standardizing requests. Travelport Worldspan
                            also offers associate default records. For information on associate ⁄hotel and car⁄default records</p>
                    </div>

                    <div class="worldspan-div" id="remarks">
                        <h4>Email Remarks</h4>
                        <p>Ability to store an email address in a PNR</p>
                    </div>


                    <div class="worldspan-div" id="qualitycheck">
                        <h4>Quality Check</h4>
                        <p>This script facilitates the quality control process by ensuring required fields and⁄or accurate
                            information has been included in a PNR.</p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>A feature to build customized quality control databases is also available for added flexibility.
                                    This feature populates databases with PNR search settings for all records and/or specific
                                    client account numbers. A onetime query occurs on the initial launching of the script
                                    to determine the agency location.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="worldspanaccounting">
                        <h4>Worldspan Accounting Interface</h4>
                        <p>Interface is an electronic, one-way communication line between two computer systems. This line
                            is called the Data Transmission (DT) Line, or the Interface Line. </p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Information can be transmitted over the DT line from Travelport Worldspan® to the interface
                                    accounting system via an Interface Message. This interface message is also referred to
                                    as a Travel Accounting Interface Record (TAIR). The interface message contains information
                                    from the PNR⁄the entry that was used to transmit the message.
                                </li>
                                <li>
                                    An agent may also refer to their interface system as a back office system, an accounting system, or a third-party system.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="scriptPro">
                        <h4>Script PRO</h4>
                        <p>ScriptPro scripts are created in ScriptPro editor, which is a Travelport Worldspan proprietary
                            application using pop-up dialog boxes with a gray background. The programming language is similar
                            to Visual and Pascal. File names begin with Z4 in the All Scripts drop down list.</p>
                    </div>

                    <div class="worldspan-div" id="securate-air-plus">
                        <h4>Securate Air Plus</h4>
                        <p>Worldspan Go! brings a spectrum of travel planning and management solutions to the travel agent's
                            desktop. Using browser-based travel technologies and the power of the Travelport Worldspan global
                            distribution system, your agency has access to a global marketplace of travel and related information.
                            </p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Reflects all fares, including private, published and Web fares, in one integrated display
                                    - within seconds.</li>
                                <li>Integrates with Travelport Worldspan Automated Rules to provide up-to-the minute validation.
                                </li>
                                <li>Interfaces with Travelport Worldspan's tax package to ensure up-to-date taxes are applied.
                                </li>
                                <li>Manages fares through multiple ATPCO categories, including: Category 31 (Voluntary Changes),
                                    Category 35 (Net Fares), Category 15 (Private Fares) and Category 25 (Fare by Rule).</li>
                                <li>Places security controls on view, sell, ticketing and redistribution functions.
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="span-go">
                        <h4>Worldspan Go</h4>
                        <p>The Web-based SecuRate Air Plus is a leader in negotiated fares management for airlines. SecuRate
                            Air is a key component of Travelport Worldspan by Travelport™'s industry-leading Fares and Pricing
                            suite.Flexible technology integrates with Airline Tariff Publishing Company (ATPCO) to allow
                            you to take advantage of carrier-filed automated fares and rules.You can simultaneously employ
                            Worldspan SecuRate® Air Plus, a Web tool that gives you the internal ability to create and control
                            your own negotiated fares databases. </p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Access to a world of travel resources through a customised reservation screen and up
                                    to 10 windows in split or full screen.</li>
                                <li>Accessible wherever the Internet is available, but can be restricted if needed for security.
                                </li>
                                <li>Secure login and password provide access to an intuitive suite of productivity tools,
                                    administration tools and online help.</li>
                                <li>Easy installation via a URL and automatic updates for immediate access to the latest
                                    functionality.
                                </li>
                                <li>Agencies can employ user-owned hardware and choose from several connectivity options.
                                </li>
                                <li>Easy access to Travelport Worldspan's world-class Global Learning Centre, featuring virtual
                                    classrooms, self-paced tutorials and more.</li>
                                <li>Integrates front- and back-office systems, and also integrates with agency Intranets
                                    and Extranets.</li>
                                <li>Incorporates the GUI interface and the Cryptic command page in one display</li>
                                <li>Carries with it a bouquet of set defaults including over 75 scripts and Custom links
                                    page to simplify agency operations</li>

                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="epricing">
                        <h4>Worldspan E-Pricing</h4>
                        <p>The global standard in low-fare shopping technology, Travelport e-Pricing has revolutionized the
                            worldwide travel shopping experience through highly scalable tools that meet market demand for
                            more fare choices today and in the future. Travelport e-Pricing is the global standard in low-fare
                            shopping technology. </p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Travel agencies, corporate travel buyers and online shoppers are able to shop, price
                                    and book low fares with unprecedented speed and accuracy. Worldwide searches are completed
                                    in seconds - with one powerful request.</li>
                                <li>Travel agencies of any size or business orientation are empowered to compete head-on
                                    in today's evolving travel marketplace.</li>
                                <li>Travelport e-Pricing eliminates the inefficiency of logging into different systems to
                                    find and book optimal choices for travelers.</li>
                                <li>Built on the GDS industry's first multi-server-based pricing technology, e-Pricing is
                                    capable of searching millions of fares and hundreds of thousands of itinerary options
                                    to find the lowest available fares.</li>
                                <li>Each e-Pricing solution is designed to orchestrate and expedite a unique type of search:
                                </li>
                                <li>Power Shopper® searches for low fares prior to booking.</li>
                                <li>Power Pricing® searches for the lowest available fare (worldwide) for an existing itinerary.
                                </li>
                                <li>Power Flight Search investigates multiple dates with a specified price.</li>
                                <li>Flex Shopping Options find low fares based on flexible travel dates or destinations.</li>

                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="hotel-solution">
                        <h4>Worldspan Hotel Solutions</h4>
                        <p>Travelport Worldspan's dynamic technologies for the global hotel market serve hotel companies,
                            travel agencies, travel Web sites, tour operators and corporate travel buyers on nearly every
                            continent. Through the GDS-based Travelport Worldspan Hotel Select™ system and advanced, integrated
                            Web-based tools, Travelport Worldspan's commanding suite of hotel distribution and booking technologies
                            enable you to expand your reach in today's market and optimise your revenue opportunities. </p>
                        <p>1. Travelport Worldspan Hotel Source is a participation level which allows users to access a hotel
                            supplier's internal reservation system for rates, availability, and rate rule information.</p>
                        <p>2. Travelport Worldspan Hotel AccessPlus is a participation level that provides users with availability
                            and rules stored in the Travelport Worldspan system.
                        </p>
                        <p>3. Travelport Worldspan SecuRate Hotel allows rates negotiated with a hotel chain or a specific
                            hotel property to be securely shopped and booked by the appropriate customer.</p>
                        <div class="airways-top">
                            <h5>Benefits</h5>

                            <ul class="product-list">
                                <li>Travelport Worldspan Hotel Select provides travel agencies with live connections to hotel
                                    companies worldwide, including real-time rate range, rate rules, availability status
                                    for tens of thousands of properties and instant booking confirmations.</li>
                                <li>Through Travelport Worldspan's industry-first hotel mapping and map-top booking capabilities,
                                    you can view, shop, compare and book hotel properties based on their proximity to landmarks
                                    or points of interest, and also use property images, hotel videos, 360° virtual tours
                                    and more to make the best selections for your travellers.</li>
                                <li>You have the ability to save hotels during a search and also save "My Favorite" hotel
                                    search for subsequent shopping requests.</li>
                                <li>Hotel Default Record, customisable by your SID and agent sine, promotes the use of your
                                    preferred suppliers and puts the office manager in control of policies.</li>
                                <li>You can book hotel reservations in seconds using the Worldspan Go!® Hotel Booking Tool,
                                    which is simple to use and conveniently accessed right from your Go! desktop.
                                </li>
                                <li>Intuitive point-and-click access to global hotel content and booking functionality helps
                                    travel buyers who are unfamiliar with traditional GDS formats.</li>
                                <li>Interactive functionality automatically populates reservation fields with traveller data
                                    from existing PNRs.</li>
                                <li>You can add your corporate or negotiated rates with one easy click.</li>
                            </ul>
                        </div>
                        <div class="airways-top">
                            <h5>Hotel Source</h5>
                            <p>Travelport Worldspan's dynamic technologies for the global hotel market serve hotel companies,
                                travel agencies, travel Web sites, tour operators and corporate travel buyers on nearly every
                                continent. Through the GDS-based Travelport Worldspan Hotel Select™ system and advanced,
                                integrated Web-based tools, Travelport Worldspan's commanding suite of hotel distribution
                                and booking technologies enable you to expand your reach in today's market and optimise your
                                revenue opportunities.</p>
                        </div>
                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Travelport Worldspan Hotel Select provides travel agencies with live connections to hotel
                                    companies worldwide, including real-time rate range, rate rules, availability status
                                    for tens of thousands of properties and instant booking confirmations.</li>
                                <li>Through Travelport Worldspan's industry-first hotel mapping and map-top booking capabilities,
                                    you can view, shop, compare and book hotel properties based on their proximity to landmarks
                                    or points of interest, and also use property images, hotel videos, 360° virtual tours
                                    and more to make the best selections for your travellers.</li>
                                <li>You have the ability to save hotels during a search and also save "My Favorite" hotel
                                    search for subsequent shopping requests.</li>
                                <li>Hotel Default Record, customisable by your SID and agent sine, promotes the use of your
                                    preferred suppliers and puts the office manager in control of policies.</li>
                                <li>You can book hotel reservations in seconds using the Worldspan Go!® Hotel Booking Tool,
                                    which is simple to use and conveniently accessed right from your Go! desktop.</li>
                                <li>Intuitive point-and-click access to global hotel content and booking functionality helps
                                    travel buyers who are unfamiliar with traditional GDS formats.</li>
                                <li>Interactive functionality automatically populates reservation fields with traveller data
                                    from existing PNRs.</li>
                                <li>You can add your corporate or negotiated rates with one easy click.</li>
                            </ul>ul>
                        </div>
                        <div class="airways-top">
                            <h5>Hotel Integrated</h5>
                            <p>Integrated Hotel Source participants provide True Availability directly from the associates'
                                internal reservation systems. When you include dates and number of people in your Hotel List
                                request, the following status codes appear to the left of the chain code in the response.</p>
                        </div>
                    </div>

                    <div class="worldspan-div" id="car-select">
                        <h4>Car Select</h4>
                        <p>Travelport's pacesetting technologies for the global car rental market serve Travelport Worldspan
                            travel agencies, travel Web sites, tour operators and corporate travel buyers on nearly every
                            continent. Through the GDS-based Travelport Worldspan Car SelectSM system and advanced, integrated
                            Web-based tools, Travelport Worldspan's commanding suite of car rental distribution and booking
                            technologies enable you to expand your business in today's market, optimise revenue opportunities,
                            lower operating costs and increase traveller satisfaction.
                        </p>
                    </div>

                    <div class="worldspan-div" id="read-more">
                        <h4>Travelport Rooms And More</h4>
                        <div class="airways-top">
                            <h5>Customer Profile: Leisure and corporate/leisure mixed travel agencies</h5>
                            <p>Travelport Rooms and More is an innovative product that removes all the hassle of finding
                                the perfect hotel accommodation. By simply entering a few key pieces of information on the
                                easy-to-use, intuitive landing page, Travelport Rooms and More searches multiple sources
                                on behalf of the user and returns an easy-to-read list of accommodation choices that optimizes
                                content choice and potential commission revenue earned.</p>
                            <ul class="product-list">
                                <li>Access Anytime, anywhere, whether an existing customer of Travelport or not.</li>
                                <li>Breadth An abundance of rates and inventory, which allows for greater choice. In most
                                    instances you will find three providers offering any one property.</li>
                                <li>Content Integrated with all Travelport global distribution systems (GDSs), with a single
                                    sign on, no need to chase commission payments, content is available quickly from one
                                    place.</li>

                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="xml-pro">
                        <h4>Worldspan XML Pro</h4>
                        <p>Worldspan XML Pro is the key, strategic messaging platform for travel companies wanting to interface
                            their unique travel applications to the Travelport Worldspan by Travelport™ GDS. Customers with
                            widely varying business models and connectivity needs are able to utilise the Internet to implement
                            this low-cost, quick connection between the GDS and third-party applications, Internet booking
                            engines (IBE), back-office systems and more. XML Pro is a sophisticated blend of Travelport Worldspan
                            technology, eXtensible Markup Language (XML) and OpenTravel Alliance specifications. XML Pro
                            is the leading global data exchange tool for building fully functional travel applications and
                            establishing GDS direct connects via the Internet.
                        </p>
                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>XML Pro lowers your total GDS interface development, infrastructure and maintenance costs
                                    (Web Services).</li>
                                <li>XML Pro helps you create superior e-Commerce opportunities, boost travel distribution
                                    and transaction processing capabilities, and expand travel revenue sources.</li>
                                <li>XML Pro offers complete host functionality and is geared around a service-oriented architecture
                                    and Web Services, enabling efficient scaling of the product as enhancements are rolled
                                    out on the GDS.</li>
                                <li>While XML Pro offers unlimited flexibility with the broadest range of content and industry
                                    leading functionality available, Travelport GDS continuously expands global content access
                                    and adds innovative functionality to meet the needs of growing distribution channels.</li>
                                <li>The XML Pro online software development kit (SDK) helps your developers learn, customize
                                    and implement the best solutions for your travel applications.</li>
                                <li>Well-refined implementation and support practices ensure you're up and running quickly.</li>
                                <li>Development is further simplified through the use of Simple Object Access Protocol (SOAP)
                                    and the latest data transfer standards for fast deployment of Web Services.</li>
                                <li>Implementation can be done over an existing TCP/IP connection using https protocol -
                                    no dedicated circuits are required.</li>
                                <li>Advanced data compression capabilities provide smaller messages, reduced bandwidth requirements
                                    and faster system response times.</li>
                                <li>Encryption technologies boost end consumer confidence, minimizing security breaches and
                                    business liability in the process.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="pricing">
                        <h4>E - Pricing</h4>
                        <p>The global standard in low-fare shopping technology, Travelport Worldspan e-Pricing has revolutionised
                            the worldwide travel shopping experience through highly scalable tools that meet market demand
                            for more fare choices today and in the future.</p>
                        <div class="airways-top">
                            <h5>Benefits</h5>
                            <ul class="product-list">
                                <li>Travel agencies, corporate travel buyers and online shoppers are able to shop, price
                                    and book low fares with unprecedented speed and accuracy. Worldwide searches are completed
                                    in seconds - with one powerful request.</li>
                                <li>Travel agencies of any size or business orientation are empowered to compete head-on
                                    in today's evolving travel marketplace.</li>
                                <li>Travelport e-Pricing eliminates the inefficiency of logging into different systems to
                                    find and book optimal choices for travelers.</li>
                                <li>Built on the GDS industry's first multi-server-based pricing technology, e-Pricing is
                                    capable of searching millions of fares and hundreds of thousands of itinerary options
                                    to find the lowest available fares.</li>
                                <li>Each e-Pricing solution is designed to orchestrate and expedite a unique type of search:
                                </li>
                                <li>Power Shopper® searches for low fares prior to booking.</li>
                                <li>Power Pricing® searches for the lowest available fare (worldwide) for an existing itinerary.
                                </li>
                                <li>Power Flight Search investigates multiple dates with a specified price.</li>
                                <li>Flex Shopping Options find low fares based on flexible travel dates or destinations.</li>

                            </ul>
                        </div>
                    </div>

                    <div class="worldspan-div" id="galstar">
                        <h4>Galstar</h4>
                        <div class="airways-top">

                            <ul class="product-list">
                                <li>White Label Solution.</li>
                                <li>Travel agencies of any size or business orientation are empowered to compete head-on
                                    in today's evolving travel marketplace.</li>
                                <li>Multi Tier Architecture with Single Connector.</li>
                                <li>Caters both Business 2 Business (B2B) and Business 2 Customer (B2C).</li>
                                <li>Integrated with both Travelport Galileo & Travelport Worldspan.</li>
                                <li>Developed on Java Platform using Struts, Ajax and Web 2.0 technologies.</li>

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    </div>
    
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>