<?php
/**
 * Template Name: travelport-ticketing
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">
        <!-- breadcrumbs -->
         <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Travelport Smartpoint</a></p>
        <!-- heading -->

        <!-- content-text -->
        <div class="content-row row">
            <div class="feature-text col-md-9">
                <div class="ticketing-heading profile">
                    <h3>
                        Ticketing
                    </h3>

                </div>


                <div class="airways-top  m-t-40">
                    <h5>E-Ticket Viewer </h5>
                </div>
                <p>A mobile friendly application provides ticket copy for Travel Agent’s customers and can allocate charges,
                    branding etc. as needed.
                </p>

                <div class="airways-top  m-t-40">
                    <h5>E-Tracker </h5>
                </div>
                <p class="ticketing-subheading-black">Efficient e-Ticket Management</p>

                <p>Are you still tracking electronic-ticket usage manually? Or maybe you aren’t tracking it at all and rely
                    on customers requesting refunds / reissues, which could result in a loss of potential service fee revenue
                    for your agency. IATA statistics show that 4% of e-tickets remain unused. Manually tracking the status
                    of e-tickets can be inefficient and make it difficult to maintain e-ticket inventory. With Galileo e-Tracker’s
                    simple and powerful e-ticket management solution, you can record, monitor and generate reports on the
                    status of all e-tickets issued by your agency. Galileo e-Tracker is a simple to use, web-based tool that
                    enables agents to check all electronic tickets issued via Galileo. With a significant number of e-tickets
                    issued never actually being used, processing customer refunds can provide a potentially significant revenue
                    stream.

                </p>

                <p class="ticketing-subheading-black">
                    Benefits and features at a glance:
                </p>
                <ul class="product-list">
                    <li> Simplifies the ticketing process, improving agent efficiency and reduces paperwork.</li>
                    <li> Greater efficiency delivers cost savings- access the real time status of an individual coupon with a single mouse click and produce status reports with ease.</li>
                    <li> Easy-to-use, browser-based functionality, fully integrated as part of the agency desktop for enhanced speed and efficiency.</li>
                    <li> Available in 7 languages: English, French, German, Spanish, Italian, Czech and Portuguese.</li>
                    
                </ul>                

                <div class="airways-top  m-t-40">
                    <h5>Automated Refunds </h5>
                </div>

                <p>Automated Refunds automates the refund procedure and automatically passes the information onto BSP (Billing
                    Settlement Plan). All related documents, automated or manual, are also included subject to procedural
                    and or input validation requirements.
                </p>

                <p class="ticketing-subheading-black">Feature</p>
                <ul class="product-list">
                    <li>Reports tickets for partial or full refunds electronically to BSP</li>
                </ul>
               
                <p class="ticketing-subheading-black">Benefit</p>
                <ul class="product-list">
                    <li>Enhances efficiency by saving delivery time, since you need not send the refund forms to the concerned airline office</li>
                </ul>
               
               
                <p class="ticketing-subheading-black">Advantage</p>
                <ul class="product-list">
                    <li>Enhances efficiency by saving delivery time, since you need not send the refund forms to the concerned airline office</li>
                </ul>
                               
            </div>

            <div class="link-col col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 bg-blue" onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header ht-60">
                                <!--     
                                    <div class="card-img-small">
                                        <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="img-responsive-small">
                                        <span class="img-text-small m-t-25-px">Ticketing</span>
                                    </div> -->
                                <div class="card-img-white-small img-display">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small white-text m-t-25-px">Ticketing</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

   <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>