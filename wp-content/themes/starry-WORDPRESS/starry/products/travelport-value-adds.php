<?php
/**
 * Template Name: travelport-value-adds
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">
        <!-- breadcrumbs -->
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Travelport Smartpoint</a></p>
        <!-- heading -->

        <!-- content-text -->
        <div class="content-row row">
            <div class="feature-text col-md-9">
                <div class="ticketing-heading profile">
                    <h3>
                        Travelport Document Converter
                    </h3>

                </div>

                <div class="airways-top  m-t-40">
                    <h5> Customer Profile: Galileo ticketing agencies </h5>
                </div>

                <p>Travelport Document Converter is a standalone desktop application designed to convert the generic e-ticket
                    documents printed to a MIR file into a specific file structure using a standard naming convention and
                    allows it to be saved into a specific folder on agent computer. This allows agent to easily access these
                    documents.



                </p>
                <p> The application is able to convert all E-ticket support documents and supports four different output
                    file formats.  A file search tool is built in which enables agent to retrieve a converted file on the
                    basis of search criteria.

                </p>





                <p class="ticketing-subheading-black m-t-40"> Key Features: </p>

                <ul class="product-list">
                    <li> Convert a host generated print text file of e-ticket document into a pre-defined output format file.</li>
                    <li> Rename a host generated MIR file of E-ticket document into a standard structure file name.</li>
                    <li> Save a converted file into a pre-defined monthly subfolder of Document Converter file folders automatically.</li>
                    <li> Search capability to locate a particular file from the converted output file folders & its subfolders.</li>
                    <li> Choice of default location of converted files based upon customer’s needs.</li>
                    <li> Provide options
                    of output file  format  for all documents and choice of print, store or email an itinerary to  a customer.</li>
                    <li> Append the company logo on a passenger receipt & an unconverted document at agent’s choice.</li>


                </ul>
                
                <p class="ticketing-subheading-black m-t-40"> Customer Benefits:</p>
                <ul class="product-list">
                    <li> Supports the paperless working environment.</li>
                    <li> Reduce agency costs and preserves the environment.</li>
                    <li> A Fully automated process to store all E-ticket documents in electronic and in a centre place.</li>
                    <li> Improves travel agent's efficiency</li>

                </ul>
                
            </div>

            <div class="link-col col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 " onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 bg-blue" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header ht-60">

                                <!-- <div class="card-img-small">
                                        <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="img-responsive-small">
                                        <span class="img-text-small m-t-25-px">Value Adds</span>
                                    </div> -->
                                <div class="card-img-white-small img-display">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small white-text m-t-25-px">Value Adds</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>