<?php
/**
 * Template Name: travelport-office-management
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>

    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">
        <!-- breadcrumbs -->
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Travelport Smartpoint</a></p>
        <!-- heading -->

        <!-- content-text -->
        <div class="content-row row">
            <div class="feature-text col-md-9">
                <div class="ticketing-heading profile">
                    <h3>
                        Office Management
                    </h3>

                </div>

                <div class="airways-top  m-t-40">
                    <h5> XML Queue Manager</h5>
                </div>

                <p>Customer Profile: Galileo customers in APAC that require an automated queue sorting system. The XML Queue
                    Manager program runs on Galileo® XML API Desktop technology, which allows continuous queue distribution
                    over a Galileo Desktop terminal.

                </p>
                <ul class="product-list">
                    <li> Improved stability and connectivity than the generic Queue Manager.</li>
                    <li> Continuous PNR queue distribution
                    (or determined by customer). </li>
                    <li> Pre-assigned queue distribution.</li>
                    <li> Distribute PNR with/without
                    notepad added.</li>
                    <li> Automatic Sign on.</li>
                    <li> Multi PCCs distribution.</li>
                    <li> Unsuccessful distribution
                    log on each PNR.</li>
                    <li> Editable settings file.</li>
                    <li> Editable task file.</li>
                    <li> Editable distribution
                    mapping file.</li>
                    <li> Reset past dated TAU.</li>
                    <li> Customer maintenance possible.</li>

                </ul>
               
                <div class="airways-top  m-t-40">
                    <h5> ViewTrip White Label</h5>
                </div>

                <p>Customer Profile: Galileo and Worldspan customers and is used by traditional travel agencies, online agencies
                    and multi-host airlines across the globe.
                </p>
                <p>Travelport ViewTrip™ is a comprehensive online travel resource providing agencies and their customers
                    with direct access to their own personal travel itinerary, electronic ticket records and electronic expense
                    receipts, including the ability to retrieve, review, print and e-mail data from any internet-enabled
                    location. Customers benefit from online access to their itinerary – 24 hours a day, 365 days a year,
                    from anywhere in the world and it is also downloadable to Outlook or Lotus Notes calendars.

                </p><br>
                <p>Content available includes: </p>
                <ul class="product-list">
                    <li> Itinerary updates and the very latest trip status and related information
                    including maps, driving directions, local weather etc.</li>
                    <li> Complete PNR segment and destination information
                    including flight confirmation numbers, departure and arrival times, electronic ticket
                    information, flight duration and mileage, on-time flight status, meal information, hotels, car rentals,
                    destination maps, weather, driving directions and more.</li>
                    <li> re-departure content,
                    including a currency converter, interactive maps, passport/visa information, travel and health advice,
                    events calendar and weather.</li>
                    <li> Adverts for products, tools and services to match the
                    travelers needs e.g. travel medication, electronic bag tags.</li>
                    <li> The convenience of integrated online
                    check-in from participating airlines. For the latest list of Carriers participating in Online &nbsp &nbsp
                    Check-In via Travelport ViewTrip please refer to ASK Travelport, (Answer ID 14546).</li>
                    <li> Customers of Worldspan can choose among 10 language preferences,  Customers of Galileo have the
                    choice of 23 languages.</li>

                </ul>
    <br/>
                <p>Agencies benefit from improved customer service levels:</p> 
                
                <ul class="product-list">
                    <li> Less hassle and improved efficiency
                    – empowering customers means trip related queries are reduced and agencies spend less time 
                    on the phone and less time answering email enquiries.</li>
                    <li> A range of customization options gives flexibility
                    to apply the agency look and feel to the web site to reflect their brand and &nbsp&nbsp&nbsp&nbsppromote
                    their own business.</li>
                    
                </ul>
              
                <div class="airways-top  m-t-40">
                    <h5> ViewTrip Mobile™</h5>
                </div>

                <p>
                    Travelport ViewTrip Mobile™ is a secure, on demand mobile itinerary management and planning tool offered to agencies currently
                    using Travelport ViewTrip™. This powerful mobile service, provides travellers with flexible itinerary
                    management throughout their entire journey, supported by timely ‘pushed’ messages, useful content guides,
                    and pro-active flight alerts. Travellers are equipped to manage, plan, execute and receive pertinent
                    “need to know” information. The itinerary automatically appears in the application on the customer’s
                    mobile device when their itinerary is emailed to them. Mobile is an important part of Travelport's strategy
                    to provide flexible access to content across multiple apps and devices.



                </p>


            </div>

            <div class="link-col col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 " onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 bg-blue" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header ht-60">

                                <!-- <div class="card-img-small">
                                        <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="img-responsive-small">
                                        <span class="img-text-small m-t-25-px">Office Management</span>
                                    </div> -->
                                <div class="card-img-white-small img-display">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small white-text m-t-25-px">Office Management</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>