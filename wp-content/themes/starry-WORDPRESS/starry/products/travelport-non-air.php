<?php
/**
 * Template Name: travelport-non-air
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>

                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">
        <!-- breadcrumbs -->
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Travelport Smartpoint</a></p>
        <!-- heading -->

        <!-- content-text -->
        <div class="content-row row">
            <div class="feature-text col-md-9">
                <div class="ticketing-heading profile">
                    <h3>
                        Galileo RoomMaster™
                    </h3>

                </div>

                <div class="airways-top  m-t-40">
                    <h5> Customer Profile: All Apollo and Galileo connected agencies </h5>
                </div>

                <p>Galileo RoomMaster™ is a complete and accurate hotel shopping and booking system. It provides direct,
                    real-time access to the internal reservations systems of over 400 hotel brands and over 87,000 properties
                    worldwide providing the ability to shop, book and receive instant confirmations. Agencies have access
                    to the sophisticated tools needed to develop a new revenue stream by focusing more on hotel bookings
                    to earn higher commission rates.</p>

                <br>
                <p class="ticketing-subheading-black">It gives you the power of:
                </p>
                <ul class="product-list">
                    <li><b> Lowest Public Rate – </b> The RoomMaster Best Available Rate (BAR) program has been enhanced -
                    chainparticipant agree to provide RoomMaster users with access to their lowest public rates - both restricted
                    and unrestricted rates. </li>
                    <li><b> Best Available Rate™ – </b> Book the best non-restricted rates available online or off for more
                    than 25,000 properties worldwide.</li>
                    <li><b> Inside Availability™ – </b> Connect seamlessly to more than 99 percent of our hotels, providing
                    immediate inventory and rates from the hotel's reservation system.</li>
                    <li><b> Inside Shopper™ – </b> Reach directly into the reservation systems of over 170 hotel chains for
                    real-time pricing and availability, all on the first shopping screen.</li>
                    <li><b> Galileo Vacation Rentals – </b> Search and book high-yield vacation properties. </li>
                    <li><b> Images – </b> Access a photo library of hotel exteriors, rooms, and amenities to make booking even
                    easier.</li>

                </ul>
                
                <div class="airways-top  m-t-40">
                    <h5> Inside Link</h5>
                </div>

                <p>The interactive advantage, allowing subscribers to receive instant confirmation on hotel sells. Inside
                    Link is the first step to a seamless environment. With Inside Link rates, rules and descriptive data
                    is maintained in RoomMaster, with the sell message transmitted seamlessly to your CRS, allowing subscribers
                    to obtain instant confirmation from your system when the sell transaction takes place. By establishing
                    a real-time link to your central reservation system, Inside Link takes your distribution to a new level
                    of sophistication and effectiveness - and provides the ultimate in dynamic inventory control and yield
                    management.
                </p>
                <p>Inside Link allows you to return an instant confirmation or cancellation number, along with your own custom
                    marketing message. The enhanced connectivity solutions of Inside Availability and Inside Shopper provide
                    the very latest 'seamless' technology between travel agents your CRS.</p>


                <div class="airways-top  m-t-40">
                    <h5> Inside Shopper</h5>
                </div>

                <p>Allows agencies direct access to your CRS from the initial availability screen for the most up-todate
                    rates and availability. Inside Shopper is our most advanced connectivity option providing agents the
                    confidence to book your brand and produce dramatic booking results.

                </p>
                <p>Inside Shopper simply interfaces with a hotel's CRS during the shopping process as well as the selling
                    process. So, when an agent requests hotel availability in a given area, Inside Shopper participants send
                    Travelport Galileo live rates and availability directly from their CRS.
                </p>

                <p>Since its introduction in 2002 Inside Shopper has become the preferred method of connectivity to Travelport
                    Galileo with over 180 hotel groups utilising the new technology. </p> <br>

                <p class="ticketing-subheading-black">Benefits of Inside Shopper Participation: </p>
                <p>The same up-to-the-minute accuracy and room availability for travel agents, online travel providers and
                    corporate clients as offered through your own internal reservation system. Reduced rate and availability
                    maintenance in RoomMaster. With Inside Shopper Bypass, multilevel rate maintenance at the property level
                    is practically eliminated. Enhanced effectiveness of your GDS distribution, thus generating more sales.
                    (e.g. A leading hotel chain that implemented Inside Shopper, realised a 32 percent increase in sales
                    volume during the first month of implementation) No more complaints about inaccurate data on rate and
                    rule information as it comes directly from your CRS!
                </p>
                <p>1-2-1 Marketing on Shopping Requests - ability to accept and respond to availability requests specifically
                    from Frequent Guests </p>
                <br>
                <p><b> Why is Inside Shopper producing such dramatic sales results? </b>
                </p>
                <p>It is a simple, straightforward process, without requiring huge investment or implementation time. </p>
                <p>Agents do NOT have to learn new formats.</p>
                <p>Agencies are moving market share by booking Inside Shopper participating hotels More 'requests' are being
                    converted into 'bookings' as a result of this enhanced data accuracy (E.g. In January 2003, 34 percent
                    of room requests resulted in bookings, compared with 19 percent in June 2002, prior to Inside Shopper
                    being implemented). </p>

                <p class="ticketing-subheading-black m-t-40"> Detailed Hotel Company benefits:
                </p>
                <p> <b>True Availability - </b> Inside Shopper provides you with the ability to show "true" availability
                    directly from your CRS when the travel agent is shopping on the initial Hotel Availability (HOA) screen.
                </p>

                <p> <b>Increased Visibility - </b>Inside Shopper allows you to return properties that meet the agent's criteria
                    even if the rate requested is currently not available or if the property is "sold out." While Inside
                    Shopper will alert the agent/user of these conditions, it will also provide you with the opportunity
                    to market other rates, or in a closed out situation, market other properties through Alternate Availability.</p>

                <p> <b> Reduce Database Maintenance - </b> With the rates and availability being sent from your CRS, only
                    a minimal number of rates will need to stay in RoomMaster. These will be used only for backup in case
                    the link is not responding.

                    <p> <b>1-2-1 Marketing on Shopping Request - </b> Knowing an availability request is coming from one
                        of your valued frequent guests, (Frequent Guest Number included with request) allows you to respond
                        with availability that is specifically geared to that guest. </p>

                    <p> <b> Promotional/Short Duration Rates -</b> RoomMaster users can request special promotional/short
                        duration rates from Inside Shopper participants by using an ID code in the shopping request. The
                        competitiveness of the hotel company's product is enhanced as the shopping request can reflect this
                        promotional rate. </p>


                    <p class="ticketing-subheading-black m-t-40"> RoomMaster User Benefits: </p>
                    <ul class="product-list">
                    <li> Increased credibility of availability and rates on the HOA screen.</li>
                    <li> Elimination of confusion
                        associated with HOA and HOA data discrepancies.</li>
                        <li> Enhanced availability display (HOA).</li>
                    <li> More efficient hotel search and booking process, resulting in enhanced productivity.</li>
                    <li> A NEW LOOK HOA SCREENS IN SUPPORT OF INSIDE SHOPPER.</li>
                    
                </ul>
                <i> Please review the pdf document in the 'Resources' guide in conjunction with the following descriptions.
                        </i>
                    
                    <div class="airways-top  m-t-40">
                        <h5>SCREEN DISPLAY CHANGES</h5>
                    </div>


                    <p>Inside Shopper Availability Indicators - Inside Shopper participants will be identified on the HOA
                        screen with an indicator of either A, O, or C. These indicators are defined as: <br> <b>
                            A = "AVAILABLE" -</b>
                        The property (and rate requested - if applicable) is available in the Hotel Company CRS. <br><b>
                            O = "OTHER RATES" -</b> The rate type requested was not available, however, the Hotel Company
                        has "other" rates available. <br><b>
                            C = "CLOSED" -</b> The property is not available for the dates requested, but alternate properties
                        are recommended: <br>
                        <b>Rate Range vs. Rate Value - </b>Rates now appear as a range versus a specific value, improving
                        the credibility of the data. The rate range will reflect only available non-credential rates, unless
                        specific room rate/types have been included in the request. "Non-credential rates" are described
                        as rates for which anyone can qualify. Examples include  Rack (RAC), Weekend (WKD), Corporate (COR),
                        Club (CLB), Special (SPL), and Package (PKG). <br>
                        <b> Removed Blank Line between properties -</b> allows more properties to appear on each screen.<br>
                        <b> Remove Selected Rate⁄Property Indicators - </b> based upon feedback from travel agents, several
                        inaccurate or misleading indicators have been removed from the HOA screen. <br>Removed indicators
                        include: Guarantee⁄Deposit⁄Hold time, Location Code, Transportation Code ( This data will still appear
                        in the appropriate Hotel Rules (HOV) and Hotel Description (HOD) displays). <br> <b> Inside Availability
                            HOC Changes - </b>The HOC screen will allow a fourth line of text in the room/rate description
                        for identification of special rates, promotions, or program offerings when a Frequent Guest number
                        or Promotional rate was requested using the FG/ID fields, respectively
                    </p>

                    <div class="airways-top  m-t-40">
                        <h5>WHO CAN PARTICIPATE?</h5>
                    </div>
                    <p>The following are prerequisites to participation in Inside Shopper:</p>
                    <ul class="product-list">
                    <li> CRS Validation Requirements
                        - It is critical that your CRS is able to validate rate related search qualifiers on an HOA Request
                        and return the applicable rates and indicators as appropriate.</li>
                    <li> While the RoomMaster® database
                        will be interrogated to identify the properties that best match the availability request, without
                        concern for availability of requested rate data - validating search criteria such as distance, transportation,
                        property features/amenities, chain code, and Point of Sale rules, the CRS will then be expected to
                        validate the rate/room type requests and respond appropriately.</li>
                    
                </ul>
                   
                    <div class="airways-top  m-t-40">
                        <h5>Best Available Rates</h5>
                    </div>

                    <p>This program provides all Travelport Galileo-connected travel agents with access to the best available
                        published, non-restricted rates offered by participating hoteliers.  Travelport Galileo agents can
                        now provide a more efficient and enhanced service to their clients while booking participating hotels
                        through RoomMaster with greater confidence and increasing loyalty, knowing that they are booking
                        the same great rates found on a hotel's website or by calling the hotel direct.  Launched in July
                        of 2004, the program has already attracted both large global hotel groups and regional hotel chains
                        alike including: Marriott Hotels & Resorts, Kempinski Hotels & Resorts, Kimpton Hotels, Destination
                        Hotels & Resorts, Small Luxury Hotels, Best Western Hotels, Carlson Hotels Worldwide, Hilton Hotels,
                        Hyatt Hotels and Resorts, Omni Hotels, Outrigger Hotels & Resorts, WestCoast and Red Lion Hotels,
                        Minto Place Suite Hotel, Wyndham Hotels & Resorts, and Boscolo Hotels. "Best Available Rate" or "BAR"
                        rates mean they are the lowest 'Unrestricted Rates' defined as a rate available to the general public
                        that does not require pre-payment and does not impose cancellation or change penalties and⁄or fees,
                        other than those imposed as a result of a hotel property's normal cancellation policy. Group rates,
                        opaque rates, and other rates that require credentials are excluded.
                    </p>

                    <p class="ticketing-subheading-black m-t-40"> Benefits of Participating in the BAR program</p>
                    <ul class="product-list">
                    <li> Increased confidence and loyalty from Travelport Galileo agents worldwide. </li>
                    <li> Increased bookings
                        for participating hotels. </li>
                        <li> Extensive sales and marketing support worldwide to promote participating
                        hotels. </li>
                        <li> Creates consistent rate policy across distribution channels. </li>
                    <li> Control of brand
                        and rate integrity.</li>
                    <li> Increase brand share from non-participating competitors.</li>
                    
                </ul>
                   

                    <p><b>"Travelport Galileo's focus on the best available rate in the GDS fits perfectly into Marriott's
                            approach and our Look No Further Best Rate Guarantee," </b> - Mike Burns, Director GDS at Marriott
                        International

                        <b> "Hyatt is pleased to participate in Travelport Galileo's Best Available Rate program, reaffirming
                            its commitment to the travel agent community -- whom Hyatt has long recognized as an important
                            distribution channel." </b>- Joan Lowell, Vice President, Electronic Distribution, at Hyatt Hotels
                        & Resorts.
                    </p>

                    <div class="airways-top  m-t-40">
                        <h5> CarMaster™</h5>
                    </div>

                    <p>CarMaster connects more than 200,000 subscriber-powered workstations to over 27 car rental companies
                        with 29,000 locations throughout the world. To you, our Partner Participant, it promises a cost-effective
                        means to distribute your product to those travelling the globe. CarMaster offers an unrivaled database
                        for housing all of the information you need to communicate with the travel agency: Detailed corporate
                        policy and location details, such as license requirements, insurance, and frequent traveler programs.
                        A comprehensive variety of rates including weekly, weekend, and monthly rates Standard tariffs, as
                        well as promotional, inclusive, and negotiated rates Individual rate rules corresponding to each
                        rate. CarMaster allows unmatched flexibility in selling your cars through a GDS. Maximise Your Profitability
                        Inside LinkTM- A powerful tool to increase customer confidence and sales by providing the subscriber
                        with instant booking confirmation. CarMaster Inside AvailabilityTM - CarMaster Inside Availability®
                        expands distribution options, shortens time to market, reduces your costs and provides you with a
                        competitive advantage. CarMaster Inside Availability® features real-time rates, rules and availability
                        status, all of which are key to increasing customer confidence.

                    </p>


            </div>

            <div class="link-col col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 " onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Fares</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 bg-blue" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header ht-60">

                                <!-- <div class="card-img-small">
                                        <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="img-responsive-small">
                                        <span class="img-text-small m-t-25-px">Non Air</span>
                                    </div> -->
                                <div class="card-img-white-small img-display">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small white-text m-t-25-px">Non Air</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>