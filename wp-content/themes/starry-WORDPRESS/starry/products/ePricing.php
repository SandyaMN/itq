<?php
/**
 * Template Name: epricing
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css';?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/ecommerce.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/eCommerce.js';?>"></script>

    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li class=""><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li class=" airlines"><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li class="active"><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="contents container">
        <p class="path"><a class="static_links" href="<?php echo get_home_url(); ?>">Home /</a>
            <a class="static_links" href="<?php echo get_home_url(); ?>/product-travelport/">Products </a> / <a class="current-page" href="">Ecommerce APIs
            </a></p>
        <div class="aboutus-tabs m-t-0">
            <div class="clear-fix row enhanced-utilities">
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6 enhanced-utilities-tabs">
                    <div class="tabs">
                        <ul class="nav nav-tabs nav-stacked tabs-right">
                            <li class="active"><a href="#ePricing" data-toggle="tab">
                                    <img class="card-img" src="<?php echo get_template_directory_uri().'/assets/epricing.png';?>" alt=""> <img class="card-img-white"
                                        src="<?php echo get_template_directory_uri().'/assets/epricing_white.png';?>" alt=""> E-Pricing</a></li>
                            <li><a href="#galileo" data-toggle="tab">
                                    <img class="card-img" src="<?php echo get_template_directory_uri().'/assets/webservice.png';?>" alt=""> <img class="card-img-white"
                                        src="<?php echo get_template_directory_uri().'/assets/webservice_white.png';?>" alt=""> Galileo Web Services</a></li>
                            <li><a href="#mini" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/minirule_service.png';?>"
                                        alt=""> <img class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/minirule_service_white.png';?>" alt="">                                    Mini
                                    Rule Service</a></li>
                            <li><a href="#ticket" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/ticket_exchange.png';?>"
                                        alt=""> <img class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/ticket_exchange_white.png';?>" alt="">                                    Ticket
                                    Xchange API- VR3</a></li>
                            <li><a href="#universal" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/universal-api-white.png';?>"
                                        alt=""> <img class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/universal-api.png';?>" alt=""> UNIVERSAL
                                    API (uAPI)</a></li>
                            <li><a href="#xml" data-toggle="tab"><img class="card-img" src="<?php echo get_template_directory_uri().'/assets/xml_api.png';?>" alt="">                                    <img
                                        class="card-img-white" src="<?php echo get_template_directory_uri().'/assets/xml_api_white.png';?>" alt=""> XML API Desktop</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-6 p-0">
                <div class="tab-content">
                    <div class="tab-pane active" id="ePricing">
                        <h4> E-Pricing</h4>
                        <p>Travelport e-Pricing technology has revolutionized the worldwide travel shopping experience. It
                            is built upon the first multi-server based shopping technology in the global distribution system
                            (GDS) industry. It allows travel agencies, corporate travel buyers and online shoppers to shop,
                            price and book low fares faster and more accurately than ever before.</p>

                        <p class="ticketing-subheading m-t-40">Benefits </p>

                        <ul class="product-list">
                            <li> Shop across millions of published, negotiated, web and advertised
                            fares and hundreds of thousands of itinerary options in seconds with just one simple entry. This
                            removes the need to log into different systems or web sites which ensures customers get the information
                            & choice they expect.ever actually being used, processing customer refunds can provide a potentially
                            significant revenue stream.</li>
                            <li> Receive a convenient combined display of ready-to-book
                            itineraries, direct to a desktop with the ability to provide from 25 up to 350 of the best available
                            options.</li>
                            <li> Valuable option to shop for low fares prior to booking an
                            itinerary or shop the lowest available fares worldwide for an existing itinerary.</li>
                            <li>Ensure accuracy on all ticketed low fares, with automatically
                            validated rules on worldwide itinerary pricing, ticketing and accurate taxes.</li>
                        </ul>
                        <!-- <p><i class="fas fa-chevron-right "></i>Shop across millions of published, negotiated, web and advertised
                            fares and hundreds of thousands of itinerary options in seconds with just one simple entry. This
                            removes the need to log into different systems or web sites which ensures customers get the information
                            & choice they expect.ever actually being used, processing customer refunds can provide a potentially
                            significant revenue stream.
                        </p>
                        <p>
                            <i class="fas fa-chevron-right "></i> Receive a convenient combined display of ready-to-book
                            itineraries, direct to a desktop with the ability to provide from 25 up to 350 of the best available
                            options.
                        </p>
                        <p><i class="fas fa-chevron-right "></i>Valuable option to shop for low fares prior to booking an
                            itinerary or shop the lowest available fares worldwide for an existing itinerary.

                        </p>
                        <p>
                            <i class="fas fa-chevron-right "></i> Ensure accuracy on all ticketed low fares, with automatically
                            validated rules on worldwide itinerary pricing, ticketing and accurate taxes.
                        </p> -->
                    </div>
                    <div class="tab-pane" id="galileo">
                        <h4>Galileo Web Services</h4>

                        <div>
                            <span class="side-small-heading">
                                Customer Profile:
                            </span>
                            <span>Corporate and leisure developer customers and large subscribers with in house development
                                teams.
                            </span>
                        </div>

                        <p class="m-t-30-px">Galileo Web Services (GWS) is an application program interface (API) that enables
                            customers to build an interface such as a website, connected to the Galileo GDS. Through GWS,
                            travel companies can easily integrate Galileo travel content with their own systems to create
                            their own website and grow their business. GWS adheres to cross-platform web services industry
                            standards. Client applications can be deployed in any environment that supports HTTP (Hypertext
                            Transfer Protocol), including Windows® and UNIX, without requiring bridges or specialized interfaces.
                            In addition, a wide variety of languages and development developer toolkits can be used to design
                            and deploy client applications for Web Services.
                        </p>

                        <p class="m-t-30-px">It significantly reduces the need for customers to possess travel industry expertise
                            and knowledge of specific transaction calls to build an application compatibility with existing
                            XML Select message formats. GWS will deliver the functionality of the existing XML Select product
                            as a web service and provide 100% compatibility with existing XML Select message formats. GWS
                            transactions provide access to the native Apollo/Galileo functions.
                        </p>
                    </div>
                    <div class="tab-pane" id="mini">
                        <h4>Mini Rule Service</h4>
                        <p class="m-t-30-px">This API provides structured information of change Fee, Cancel Fee and No-show
                            Fee to customers so that they can provide exact amount to their customers for change in existing
                            ticket. An algorithm based rule engine has been developed to read unstructured information from
                            CAT-31 and CAT-16.
                        </p>
                    </div>
                    <div class="tab-pane" id="ticket">
                        <h4>Ticket Xchange API- VR3</h4>
                        <p class="m-t-30-px">This solution provides automated Re-issue , Refund and Revalidation service
                            to online travel agents and helps to customers in reducing development efforts to fill complicated
                            exchange mask, calculate Taxes and change Fee, Cancel Fee and No show Fee.
                        </p>
                    </div>


                    <div class="tab-pane" id="universal">
                        <h4>UNIVERSAL API (uAPI)</h4>
                        <p class="m-t-30-px">Travelport Universal API provides access to the aggregated content, availability,
                            pricing and functionality from multiple sources using a single interface. Equipped with powerful
                            pull and push data feeds, it captures and delivers information from worldwide travel suppliers
                            to provide richer business intelligence.
                        </p>

                        <p class="ticketing-subheading m-t-40">Features: </p>
                        <ul class="product-list">
                            <li> Multiple international low cost airlines under one API</li>
                            <li> Reduce costs of maintaining multiple APIs</li>
                            <li> Rich ancillary services information via Merchandising Hub</li>
                            <li> Improves efficiency and streamlines the travel supply chain</li>
                            <li> Reduces  complexities and escalating costs involved in developing, maintaining and upgrading to multiple API connections</li>
                            <li>Expedites delivery of travel related applications for deployment on the web, desktop solutions or mobile applications</li>

                        </ul>

                        <!-- <p><i class="fas fa-chevron-right "></i>Multiple international low cost airlines under one API </p>
                        <p><i class="fas fa-chevron-right "></i>Reduce costs of maintaining multiple APIs</p>
                        <p><i class="fas fa-chevron-right "></i>Rich ancillary services information via Merchandising Hub
                        </p>
                        <p><i class="fas fa-chevron-right "></i> Improves efficiency and streamlines the travel supply chain</p>
                        <p><i class="fas fa-chevron-right "></i>Reduces  complexities and escalating costs involved in developing,
                            maintaining and upgrading to multiple API connections</p>
                        <p><i class="fas fa-chevron-right "></i> Expedites delivery of travel related applications for deployment
                            on the web, desktop solutions or mobile applications</p> -->
                    </div>

                    <div class="tab-pane" id="xml">
                        <h4>XML API Desktop </h4>
                        <div>
                            <span class="side-small-heading">
                                Customer Profile:
                            </span>
                            <span>Galileo and Apollo Subscribers using Galileo Desktop; Corporate & Leisure, developer customers
                                & regional offices</span>
                        </div>

                        <p class="m-t-30-px">The XML API Desktop solution enables clients to use transactions to modify existing
                            Galileo GDS desktop products or create entirely new desktop based travel applications. All XML
                            transactions available within Galileo Web Services are also available to the user of XML API
                            Desktop and uniquely XML API Desktop allows interaction with the Booking File through XML transactions
                            prior to End Transact. This provides the developer with a range of options and a much greater
                            degree of flexibility to create powerful applications such as quality assurance and policy checks,
                            after sales reporting and statistics and fulfillment and bespoke desktop process flows designed
                            to reduce time and costs.</p>

                        <p class="m-t-30-px">XML API Desktop allows the developer customer to interact with the booking prior
                            to end transact, this is a massive advantage because it offers much more functionality and power
                            to the developer customer. It utilizes common development languages (not a bespoke tool like
                            script writer plus) allowing users to go anywhere for training.</p>

                        <p class="ticketing-subheading-black">XML API Desktop improves support within an agencies business
                            by:
                        </p>

                        <ul class="product-list">
                            <li> Automating frequent or time consuming tasks to increase productivity and quality.</li>
                            <li> Adding agency specific custom feature.</li>
                            <li> Rich ancillary services information via Merchandising Hub</li>
                            <li> "Branding" the display with custom graphics.</li>
                            <li> "Branding" an email that is sent to customers</li>
                        </ul>
                        <!-- <p><i class="fas fa-chevron-right "></i>Automating frequent or time consuming tasks to increase productivity
                            and quality.
                        </p>
                        <p><i class="fas fa-chevron-right "></i>Adding agency specific custom feature.</p>
                        <p><i class="fas fa-chevron-right "></i>Rich ancillary services information via Merchandising Hub
                        </p>
                        <p><i class="fas fa-chevron-right "></i> "Branding" the display with custom graphics.</p>
                        <p><i class="fas fa-chevron-right "></i>"Branding" an email that is sent to customers</p> -->

                        <p class="ticketing-subheading m-t-40">Feature Benefit </p>

                        <ul class="product-list">
                            <li> Strengths Improves support for an agencies business by:</li>
                            <li> Automating frequent or time consuming tasks to increase productivity and quality.</li>
                            <li> Adding agency specific custom features</li>
                            <li> "Branding" the display with custom graphics</li>
                            <li> "Branding" email that is sent to customers</li>
                        </ul>

                        <!-- <p><i class="fas fa-chevron-right "></i>Strengths Improves support for an agencies business by: </p>
                        <p><i class="fas fa-chevron-right "></i>Automating frequent or time consuming tasks to increase productivity
                            and quality.</p>
                        <p><i class="fas fa-chevron-right "></i>Adding agency specific custom features
                        </p>
                        <p><i class="fas fa-chevron-right "></i> "Branding" the display with custom graphics</p>
                        <p><i class="fas fa-chevron-right "></i>"Branding" email that is sent to customers
                        </p> -->

                        <p class="m-t-30-px">
                            Other GDS offerings are not as powerful as Galileo's in that they do not allow interaction with the AAA (Agents Assembly
                            Area) until the booking has been end transacted. XML API Desktop allows the developer customer
                            to interact with the booking prior to end transact, this is a massive advantage because it offers
                            much more functionality and power to the developer customer.
                        </p>

                        <p class="m-t-30-px">
                            Utilizes common development languages (not a bespoke tool like script writer plus) allowing users to go anywhere for training.
                        </p>

                        <p class="m-t-30-px">
                            GDS Access Flexibility XML Select provides access to both Apollo and Galileo host systems.
                        </p>

                        <p class="m-t-30-px">
                            Ability to send and receive transaction data in a structured data format using XML Reduces the need for frequency of updates
                            when minor changes occur at the host.  No need for customer to update their application when
                            XML Transactions are enhanced.  Previous transactions will continue to function with the GDS
                            without modification.
                        </p>

                        <p class="m-t-30-px">
                            Most XML Structured Data transactions are identical for Apollo and Galileo Means the customer only has to develop an application
                            once – a bonus for multinational corporations.
                        </p>
                    </div>
                    <div class="tab-pane" id="fare-finder">
                        <h4></h4>
                        <p></p>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

     <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>