<?php
/**
 * Template Name: travelport-fares
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/home.js';?>"></script>
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/product-travelport.css';?>" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri().'/css/products.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Products</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li  class="active"><a href="<?php echo get_home_url(); ?>/product-travelport/">Travelport Smartpoint
                                    </a>
                                </li>
                                <li><a href="<?php echo get_home_url(); ?>/ndc/">NDC</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/airlines/">Airlines</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/ecommerce-api/">Ecommerce APIs</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/online-solutions/">Online Solutions</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/enhanced-utilities/">Enhanced Utilities</a></li>
                                <li><a href="<?php echo get_home_url(); ?>/worldspan/">Worldspan</a></li>
                    </ul>
                </div>
                <!-- <div class="arrow-down"></div> -->
            </div>
        </div>

    </div>

    <!-- contents -->
    <div class="content container">
        <!-- breadcrumbs -->
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a> / <a class="current-page">Travelport Smartpoint</a></p>
        <!-- heading -->

        <!-- content-text -->
        <div class="content-row row">
            <div class="feature-text col-md-9">
                <div class="ticketing-heading profile">
                    <h3>
                        BRANDED FARES (TMP)
                    </h3>

                </div>


                <div class="airways-top  m-t-40">
                    <h5>Galileo 360° Fares </h5>
                </div>

                <p>Comprehensive database consisting of millions of fares updated several times a day, to ensure the very
                    latest information is at your fingertips.
                </p>

                <ul class="product-list">
                    <li> Full complement of fare types include public, airline private, agency private and web fares on the Galileo Desktop.</li>
                    <li> Guaranteed preferred content from our airline partners.</li>
                    <li> Industry-leading support for automated fares and rules loading system resulting in faster and more accurate faring for more than &nbsp&nbsp&nbsptwo million published fares.</li>
                    <li> Advanced shopping tools find the best itineraries.</li>
                    
                </ul>
                
                <p class="ticketing-subheading-black">Benefits of Galileo 360° Fares
                </p>
                <ul class="product-list">
                    <li> To increase efficiency and accuracy of faring.</li>
                    <li> Enhance efficiency with access to greater range of fares.</li>
                    <li> Improve productivity by accessing fares with tools you already use.</li>
                    <li> Increase customer loyalty and attract new business with better fares and personal service.</li>
                   
                </ul>
                
                <div class="airways-top  m-t-40">
                    <h5>Airlines Private Fare </h5>
                </div>
                <p>A negotiated fares database allowing agencies to load Net, Selling and Net/Selling fares and display them
                    in an integrated tariff display.</p>
                <ul class="product-list">
                    <li> Powerful tool that automates and integrates fare display and quote functionality of negotiated fares with all other Travelport Galileo fares.</li>
                    <li> Host Controlled Database Management: Agency creates, maintains and controls their own database of negotiated fares by entering contract data into the web-based graphical user interface.</li>
                    <li> Secured Data: Agency determines who can display, quote, ticket and distribute private fare data.</li>
                    <li> Integrated Fares: Integrate private and public fares to ensure the lowest fare.</li>
                   

                </ul>
               
                <br>
                <p class="ticketing-subheading-black">Benefits of Agency Private Fares
                </p>
                <p>To increase efficiency and accuracy of fares</p>
                <ul class="product-list">
                    <li> Increase efficiency of storing, quoting and ticketing private fares.</li>
                    <li> 100% rule validation decreases amount of inaccuracies.</li>
                    <li> Eliminate potential inaccuracies when creating individual pricing records.</li>
                    <li> Agents can search and quote negotiate private fare faster with integrated displays.</li>
                    
                </ul>                
                <br>
                <p>To comply with corporate travel programs and supplier agreements.</p>
                <ul class="product-list">
                    <li> Increase policy compliance with automated storing, quoting and ticketing private fares.</li>
                    <li> Ensure sale of a negotiated fare only to the applicable customer.</li>
                    <li> Maintain total control of database with built-in security features.</li>
                   
                </ul>
                
                


                <div class="airways-top  m-t-40">
                    <h5>Airline Public Fares</h5>
                </div>

                <p><b> The Travel Industry's Leading Airline Fares and Rules System.</b>
                </p>
                <p>Today's airlines need the most advanced solutions to distribute, display and enable the selling of every
                    type of fare in any business channel. Travelport 360 Fares is the premier system supporting the distribution
                    and sale of public, private, online, interline and alliance partner fares and rules. Our industry-leading
                    platform serves airlines as well as corporate, leisure and retail points of sale globally, including
                    airline and other travel websites. Travelport 360 Fares provides complete fares and pricing for online
                    and interline itineraries via a comprehensive fares database running on a fully automated server-based
                    open system platform. Travelport 360 Fares reduces operational costs without compromising accuracy or
                    value by delivering airline-filed fares, rules and conditions, as well as complex interline processing,
                    including calculation of taxes and surcharges.

                </p>
                <br>
                <p class="ticketing-subheading-black">Benefits and features at a glance:</p>
                <ul class="product-list">
                    <li> Eliminates need for manual fare quotation.</li>
                    <li> Supports IATA and government mandates.</li>
                    <li> Provides negotiated and net fares management tools.</li>
                    <li> Shares single data source with Travelport e-Pricing.</li>
                    <li> Available across all sales channels.</li>
                    
                </ul>
                <i>This product is available for airlines globally.</i>
                

            </div>
            <div class="link-col col-md-3">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 " onclick="location.href='<?php echo get_home_url(); ?>/travelport-ticketing/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Ticketing_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Ticketing</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15 bg-blue" onclick="location.href='<?php echo get_home_url(); ?>/travelport-fares/';">
                            <div class="card-header ht-60">

                                <!-- <div class="card-img-small">
                                        <img src="<?php echo get_template_directory_uri().'/assets/Fares.png';?>" class="img-responsive-small">
                                        <span class="img-text-small m-t-25-px">Fares</span>
                                    </div> -->
                                <div class="card-img-white-small img-display">
                                    <img src="<?php echo get_template_directory_uri().'/assets/Fares_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small white-text m-t-25-px">Fares</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-non-air/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/non-air_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Non Air</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-office-management/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/office_management_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Office Management</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-value-adds/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/value_add_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Value Adds</span>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-12">
                        <div class="card card-default ht-50 br-5 m-b-15" onclick="location.href='<?php echo get_home_url(); ?>/travelport-galileo-desktop/';">
                            <div class="card-header ht-60">

                                <div class="card-img-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                                <div class="card-img-white-small">
                                    <img src="<?php echo get_template_directory_uri().'/assets/galileo_desktop_white.png';?>" class="img-responsive-small">
                                    <span class="img-text-small m-t-25-px">Galileo Desktop</span>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>
    
    <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>