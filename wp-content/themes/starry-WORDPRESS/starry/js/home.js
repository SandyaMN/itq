$(document).ready(function () {
    // $('#searchform').hide();
    // $('.search-icon a').click(function () {
    //     $('#searchform').toggle();
    //     $( "#searchform input" ).focus();
    // });
    // $('.search-div i').click(function(){
    //     $('.search-div').hide();
    //     // $( ".search-input" ).val('');
    // });
    // $(".slider").diyslider({
    //     width: "1000px", // width of the slider
    //     height: "510px", // height of the slider
    //     display: 3, // number of slides you want it to display at once
    //     loop: false // disable looping on slides
    // }); // this is all you need!
    // // $("input.search-input").blur(function (e) {
    // //     $('.search-div').hide();
    // // });

    // $('#go-left').hide();

    // // use buttons to change slide
    // $("#go-left").bind("click", function (e) {
    //     // Go to the previous slide
    //     e.stopPropagation();
    //     $(".slider").diyslider("move", "back");

    // });
    // $("#go-right").bind("click", function (e) {
    //     // Go to the previous slide
    //     e.stopPropagation();
    //     $(".slider").diyslider("move", "forth");
    //     $('#go-left').show();
    //     $('.product-div').addClass('changed');
    // });

    $('.product-slick-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        infinite: false,
        arrows: true,
        variableWidth: true,
        autoplay: false,
     });

     $('.product-slick-slider').on('afterChange', function (event, slick, currentSlide) {
        if(currentSlide === 4) {
            $('.product-slick-slider .slick-next').addClass('hidden');
        }
        else {
            $('.product-slick-slider .slick-next').removeClass('hidden');
        }

        if(currentSlide === 0) {
            $('.product-slick-slider .slick-prev').addClass('hidden');
        }
        else {
            $('.product-slick-slider .slick-prev').removeClass('hidden');
        }  
    });

    
});