var contentstring = [];
	var regionlocation = [];
	var markers = [];
	var iterator = 0;
	var areaiterator = 0;
	var map;
	var infowindow = [];
	geocoder = new google.maps.Geocoder();
	
	$(document).ready(function () {
		setTimeout(function() { initialize(); }, 400);
	});
	
	function initialize() {           
		infowindow = [];
		markers = [];
		GetValues();
		iterator = 0;
		areaiterator = 0;
		region = new google.maps.LatLng(regionlocation[areaiterator].split(',')[0], regionlocation[areaiterator].split(',')[1]);
		map = new google.maps.Map(document.getElementById("map"), { 
			zoom: 5,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			center: region,
		});
		drop();
	}
	
	function GetValues() {
		//Get the Latitude and Longitude of a Point site : http://itouchmap.com/latlong.html
		contentstring[0] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd,  6th Floor, Tower 6, Candor Tech Space, Tikri, Sector 48, Gurgaon-122018, Haryana, India</p>'+'</div>';
		regionlocation[0] = '28.457523 , 77.026344';
					
		contentstring[1] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, Ground Floor, Thapar House, Central Wing, 124, Janpath, New Delhi - 110 001.</p>'+'</div>';
		regionlocation[1] = "28.653229	, 77.308601";
		
		contentstring[2] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, SCO 66-67,Second Floor, Cabin No-207A, Sector 8-C, Madhya Marg, Chandigarh - 160018</p>'+'</div>';
		regionlocation[2] = "30.741482 , 76.768066";
		
		contentstring[3] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, 106, Jaipur Towers, Opposite All India Radio, M.I. Road, Jaipur - 302 001</p>'+'</div>';
		regionlocation[3] = "26.922070 , 75.778885";
		
		contentstring[4] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd,  1-3, Raj Chambers, 1st Floor, 29/9, R.P.Marg,, Lucknow-226001</p>'+'</div>';
		regionlocation[4] = "26.83928 , 80.92313";
		
		contentstring[5] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, 92-93,1st Floor,Mohinder Chamber , Rajinder Nagar Mkt,Opp Commissioner office, Jalandhar City-144001</p>'+'</div>';
        regionlocation[5] = "31.326015 , 75.576180";
        
        contentstring[6] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd Dirang Arcade, IInd Floor, 2B-2, GNB Road, Chandmari, Guwahati-781003</p>'+'</div>';
        regionlocation[6] = "26.148043 , 91.731377";
        
        contentstring[7] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd,  Dirang Arcade, IInd Floor, 2B-2, GNB Road, Chandmari, Guwahati-781003.</p>'+'</div>';
        regionlocation[7] = "22.572645 , 88.363892";
        
        contentstring[8] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt. Ltd. 5th Floor, Apeejay House,Dinshaw Vacha Road, Mumbai - 400020</p>'+'</div>';
        regionlocation[8] = "19.076090, 72.877426";
        
        contentstring[9] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, H.NO. 3/3/19, COELHO PEREIRA BLDG, OPP JAMO MASJID, PANAJI, GOA - 403001</p>'+'</div>';
        regionlocation[9] = "15.533414 , 73.764954";
        
        contentstring[10] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd,  9-11,Upper Level, Empire Towers C.G. Road, Navrangpura, Ahmedabad - 380006</p>'+'</div>';
        regionlocation[10] = "23.033863 , 72.585022";
        
        contentstring[11] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, 103 A, 103 B, City Tower, Dhole Patil Road, Pune - 411 001</p>'+'</div>';
        regionlocation[11] = "18.516726 , 73.856255";
        
        contentstring[12] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, 1st Floor, Janardhan Corporation No. 133/2, Janardhan Towers, Residency Road, Bangalore - 560025 </p>'+'</div>';
        regionlocation[12] = "12.972442 , 77.580643";
        
        contentstring[13] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, Maalvika Centre, Ground Floor, 144-145, Koddambakkam High Road, Nungambakam, Chennai-600034</p>'+'</div>';
        regionlocation[13] = "13.067439 , 80.237617";
        
        contentstring[14] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, TC 26/699(13) 1st Floor, Krishna Commercial Complex, Bakery Junction, Trivandrum - 695 001</p>'+'</div>';
        regionlocation[14] = "8.524139 , 76.936638";
        
        contentstring[15] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, B-20/21, First Floor,, Noor Complex, Mavoor Road, Calicut – 673 004</p>'+'</div>';
        regionlocation[15] = "11.2610298 , 75.7886018";
        
        contentstring[16] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, NO. 39/3540-43, RUKHIYA BAGH, RAVIPURAM, M.G. RAOD, COCHIN, KERALA - 682016</p>'+'</div>';
        regionlocation[16] = "9.931233 , 76.267303";
        
        contentstring[17] = '<div class="info_content">' + '<p>Interglobe Technology Quotient Pvt Ltd, 5-9-86/1, Ground and 2nd Floor, Chapel Road, Nampally, Hyderabad-500001</p>'+'</div>';
		regionlocation[17] = "17.387140 , 78.491684";
		
	}
		   
	function drop() {
		for (var i = 0; i < contentstring.length; i++) {
			setTimeout(function() {
				addMarker();
			}, 800);
		}
	}
 
	function addMarker() {
		var address = contentstring[areaiterator];
        // var icons = 'http://maps.google.com/mapfiles/ms/icons/red-dot.png';
        var icons ="../assets/placeholder.svg";
		var templat = regionlocation[areaiterator].split(',')[0];
		var templong = regionlocation[areaiterator].split(',')[1];
		var temp_latLng = new google.maps.LatLng(templat, templong);
		markers.push(new google.maps.Marker(
		{
			position: temp_latLng,
			map: map,
			icon: icons,
			draggable: false
		}));            
		iterator++;
		info(iterator);
		areaiterator++;
	}
 
	function info(i) {
		infowindow[i] = new google.maps.InfoWindow({
			content: contentstring[i - 1]
		});
		infowindow[i].content = contentstring[i - 1];
		google.maps.event.addListener(markers[i - 1], 'mouseover', function() {
			for (var j = 1; j < contentstring.length + 1; j++) {
				infowindow[j].close();
			}
			infowindow[i].open(map, markers[i - 1]);
		});
	}