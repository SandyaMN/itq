$(document).ready(function () {
    // Add minus icon for collapse element which is open by default
    $(".collapse.in").each(function () {
        $(this).siblings(".panel-heading").find(".glyphicon").addClass("glyphicon-minus").removeClass("glyphicon-plus");
    });

    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function () {
        $(this).parent().find(".glyphicon").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hide.bs.collapse', function () {
        $(this).parent().find(".glyphicon").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });

    $('.card-accordion-clicked').click(function () {
        $('.subheading-us').hide();
        $('.subheading-tabs').hide();
        $('.container-side-tabs').show();
    });

    $('.content-worldspan .worldspan-div').hide();

    $('.panel-body .accordion-tab').click(function(){
        $('#main-menu a').removeClass('activetab');
        var tab = $(this).attr('href');
        var tabid = tab.substring(1);
        $('#'+tabid).show();
        var currenttab = $("a[href$='"+tabid+"']");
        // console.log($("a[href$='"+tabid+"']"));
        var subparent = $(currenttab).parent();
        if(subparent){
            var parent = $(subparent).parent();
            $(parent).addClass('in');
            $(subparent).addClass('in');
            $(subparent).prev().addClass('submenu-active');
            $(currenttab).addClass('activetab');
            $(parent).prev().addClass('changed-icon');

        }
    });

});
function submenu(ele) {
    $('.list-group-item').removeClass('submenu-active');
    $('.list-group-item').removeClass('active-submenu');
    $('.list-group-submenu').removeClass('in');
    $(ele).addClass('active-submenu');
}
function changeicon(ele){
    $('.list-group-item').removeClass('changed-icon');
    $(ele).addClass('changed-icon');
}
function tabselect(tab) {
    var link = $(tab).attr('href');
    var tablink = link.substring(1);
    $('.content-worldspan .worldspan-div').hide();
    $('#' + tablink).show();
    $('#main-menu a').removeClass('activetab');
    $(tab).addClass('activetab');
}