$(document).ready(function(){
    // $(".new-slider").diyslider({
    //     width: "1200px", // width of the slider
    //     height: "200px", // height of the slider
    //     display: 3, // number of slides you want it to display at once
    //     loop: false // disable looping on slides
    // }); // this is all you need!

    // // use buttons to change slide
    // $("#new-left").bind("click", function (e) {
    //     // Go to the previous slide
    //     e.stopPropagation();
    //     $(".new-slider").diyslider("move", "back");
    // });
    // $("#new-right").bind("click", function (e) {
    //     // Go to the previous slide
    //     e.stopPropagation();
    //     $(".new-slider").diyslider("move", "forth");
    // });
    $(".blog-slider").diyslider({
        width: "1200px", // width of the slider
        height: "260px", // height of the slider
        display: 2, // number of slides you want it to display at once
        loop: false // disable looping on slides
    }); // this is all you need!

    // use buttons to change slide
    $("#blog-left").bind("click", function (e) {
        // Go to the previous slide
        e.stopPropagation();
        $(".blog-slider").diyslider("move", "back");
    });
    $("#blog-right").bind("click", function (e) {
        // Go to the previous slide
        e.stopPropagation();
        $(".blog-slider").diyslider("move", "forth");
    });
    $('#partnercarousel.carousel[data-type="multi"] .item').each(function () {
        var next = $(this).next();
    
        
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < 2; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }

            next.children(':first-child').clone().appendTo($(this));
        }
    });
});