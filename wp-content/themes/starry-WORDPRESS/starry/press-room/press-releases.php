<?php
/**
 * Template Name: press-release
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <!-- <link href="../css/common.css" rel="stylesheet">
    <link href="../css/aboutus.css" rel="stylesheet"> -->
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/press-room.css';?>">

   
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="main-heading">Press Room</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="active press-releases"><a class="release" href="<?php echo get_home_url(); ?>/press-release/">Press Releases</a></li>
                        <li class="advertisements"><a class="release" href="<?php echo get_home_url(); ?>/advertisements/">Advertisements</a></li>
                        <li class="articles"><a class="release" href="<?php echo get_home_url(); ?>/articles/">Articles</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="press-releases" class="tab-pane fade in active">
<div class="row m-t-10-px">
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="">Press Room</a>
                / <a class="current-page">Press Releases
                </a></p>

</div>
<div class="row">
        <h4 class=" row press-room-header">Press Releases</h4>
</div>
<div class="row">
        <div class="col-md-1">
                <ul class="nav flex-column years-tabs">
                        <li class="active year-list"><a data-toggle="tab" class="release " href="#2019">2019</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2018">2018</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2017">2017</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2016">2016</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2015">2015</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2014">2014</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2013">2013</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2012">2012</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2011">2011</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2010">2010</a></li>
                </ul>
        </div>

        <div class="col-md-11">
                <div class="tab-content">
                        <div id="2019" class="tab-pane fade in active ">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="march19hading">
                                                        <h4 class="panel-title panel-title-name"> <a class="remove-link" data-toggle="collapse"
                                                                        data-parent="#accordion" href="#march19" aria-expanded="false"
                                                                        aria-controls="march19">
                                                                        March 2019 </a> </h4>
                                                </div>
                                                <div id="march19" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="march19hading">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 05 Mar 2019
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2019/March/523-Special Olympics Campaign Launch Release_5th March 2019.pdf';?>"
                                                                                        target="blank">Travelport launches global campaign to encourage more support for airline passengers with intellectual disabilities</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 19 Mar 2019
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2019/March/524-Holi-19March2019.pdf';?>"
                                                                                        target="blank">Where are people flying in from to celebrate Holi in India? </a></li>
                                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 22 Mar 2019
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2019/March/525-JAL-Travelport Launch New Joint Venture-22march2019.pdf';?>"
                                                                                        target="blank">Japan Airlines and Travelport Agree to Launch New Joint Venture</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                                                        aria-controls="collapseOne" class="collapsed remove-link">
                                                                        February 2019 </a> </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 15 Feb
                                                                                        2019 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/press-release/2019/Feb/520-Delivering-NDC-FEB2019.pdf';?>"
                                                                                        target="blank">Delivering
                                                                                        NDC- our 2019 roadmap</a>

                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 21 Feb
                                                                                        2019 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/press-release/2019/Feb/521-Travelport-Digital and-21 Feb 2019.pdf';?>"
                                                                                        target="blank">Travelport
                                                                                        Digital and BCD Travel extend partnership
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 28 Feb
                                                                                        2019
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2019/Feb/522-Travelport extension PR-28Feb2019.pdf';?>"
                                                                                        target="blank">Travelport extends contract
                                                                                        with United Airlines to introduce
                                                                                        NDC initiatives</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title panel-title-name"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordion" href="#collapseTwo" aria-expanded="false"
                                                                        aria-controls="collapseTwo">
                                                                        January 2019 </a> </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 14 Jan
                                                                                        2019
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2019/Jan/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        participates in SATTE 2019</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 30 Jan
                                                                                        2019
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2019/Jan/519-Travelport completes early NDC onboarding- 30 Jan 2019.pdf';?>"
                                                                                        target="blank">Travelport completes onboarding
                                                                                        of first wave of New Distribution
                                                                                        Capability (NDC) customers</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <div class="tab-pane fade " id="2018">
                                <div class="panel-group" id="accordionOne" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#dec_2018" aria-expanded="true"
                                                                        aria-controls="dec_2018" class="remove-link">
                                                                        December 2018 </a> </h4>
                                                </div>
                                                <div id="dec_2018" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 07 Dec
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Dec/510-PR_Travelport announces agreement to be acquired by affiliates of Siris & Evergreen_12-18.pdf';?>"
                                                                                        target="blank">Travelport awarded Best GDS
                                                                                        title at India Travel Awards 2018
                                                                                        for North, East & West regions</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 10 Dec
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Dec/511-ITA Awards_2018_Press Release_12-18.pdf';?>"
                                                                                        target="blank">Travelport announces agreement
                                                                                        to be acquired by affiliates of Siris
                                                                                        & Evergreen
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 12 Dec
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Dec/512-Press Release_ITQ_Golden Peacock Award_12-18.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        Pvt Ltd awarded the prestigious Golden
                                                                                        Peacock Award for Risk Management
                                                                                        2018 </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#nov_2018" aria-expanded="false"
                                                                        aria-controls="nov_2018">
                                                                        November 2018 </a> </h4>
                                                </div>
                                                <div id="nov_2018" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingFour">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 20 Nov
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Nov/509-Travelport GDTR India_11-18.pdf';?>"
                                                                                        target="blank">India tops Travelport's global
                                                                                        ranking of digital travelers
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#octo_2018" aria-expanded="false"
                                                                        aria-controls="octo_2018">
                                                                        October 2018 </a> </h4>
                                                </div>
                                                <div id="octo_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Oct
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Oct/504-PR_Travelport wins tender for sole distribution supplier to Air India_10-18.pdf';?>"
                                                                                        target="blank">Travelport wins tender for
                                                                                        sole distribution supplier to Air
                                                                                        India
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 16 Oct
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Oct/506-PR_Jet Airways signs new long-term distribution agreement with Travelport_10-18.pdf';?>"
                                                                                        target="blank">Travelport and Jet Airways
                                                                                        sign a new long-term supplier agreement
                                                                                        commencing April 2019</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 23 Oct
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Oct/507-PR_Travelport becomes first GDS operator to offer NDC content _10-18.pdf';?>"
                                                                                        target="blank">Travelport becomes first GDS
                                                                                        operator to offer NDC content </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#may_2018" aria-expanded="false"
                                                                        aria-controls="may_2018">
                                                                        May 2018 </a> </h4>
                                                </div>
                                                <div id="may_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 08 May
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/May/501-ITQ conferred with Most Professional GDS award for Travelport Smartpoint (Galileo) at Global Star Awards 2018.pdf';?>"
                                                                                        target="blank">ITQ conferred with 'Most Professional
                                                                                        GDS' award for Travelport Smartpoint
                                                                                        (Galileo) at Global Star Awards 2018</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 15 May
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/May/502-Travelport Awarded Best GDS title at South India Travel Awards 2018.pdf';?>"
                                                                                        target="blank">Travelport Awarded 'Best GDS'
                                                                                        title at South India Travel Awards
                                                                                        2018
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 29 May
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/May/503-PR_How Indias millennial business travelers are changing corporate travel_5-18.pdf';?>"
                                                                                        target="blank">How India's millennial business
                                                                                        travelers are changing corporate
                                                                                        travel
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#feb_2018" aria-expanded="false"
                                                                        aria-controls="feb_2018">
                                                                        February 2018 </a> </h4>
                                                </div>
                                                <div id="feb_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 20 Feb
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Feb/500-Travelport announces new products that enable airlines and agents in the NDC era.pdf';?>"
                                                                                        target="blank">Travelport announces new products
                                                                                        that enable airlines and agents in
                                                                                        the NDC era
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingEight">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#jan_2018" aria-expanded="false"
                                                                        aria-controls="jan_2018">
                                                                        January 2018 </a> </h4>
                                                </div>
                                                <div id="jan_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 22 Jan
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Jan/498-Travelport now gives 250 airlines a branded boost as it extends its merchandising lead.pdf';?>"
                                                                                        target="blank">Travelport now gives 250 airlines
                                                                                        a branded boost as it extends its
                                                                                        merchandising lead
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 25 Jan
                                                                                        2018
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2018/Jan/499-InterGlobe Technology Quotient at SATTE 2018.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        at SATTE 2018</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>

                        <div class="tab-pane fade " id="2017">
                                <div class="panel-group" id="accordionThree" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#dec_2017" aria-expanded="true"
                                                                        aria-controls="dec_2017" class="remove-link">
                                                                        December 2017 </a> </h4>
                                                </div>
                                                <div id="dec_2017" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 Dec
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Dec/494-Travelport launches PCI DSS Certification Wizard Tool for Agency Customers.pdf';?>"
                                                                                        target="blank">Travelport launches PCI DSS
                                                                                        Certification Wizard Tool for Agency
                                                                                        Customers
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 13 Dec
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Dec/496-Travelport achieves ground-breaking IATA NDC Level 3 certification.pdf';?>"
                                                                                        target="blank">Travelport achieves ground-breaking
                                                                                        IATA NDC Level 3 certification </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 22 Dec
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Dec/497-Travelport wins Best GDS title at North-India Travel Awards 2017.pdf';?>"
                                                                                        target="blank">Travelport wins Best GDS title
                                                                                        at North-India Travel Awards 2017
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#nov_2017" aria-expanded="false"
                                                                        aria-controls="nov_2017">
                                                                        November 2017 </a> </h4>
                                                </div>
                                                <div id="nov_2017" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingFour">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 09 Nov
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Nov/493-Travelport global survey puts India top of digital traveler rankings.pdf';?>"
                                                                                        target="blank">Travelport global survey puts
                                                                                        India top of digital traveler rankings</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#aug_2017" aria-expanded="false"
                                                                        aria-controls="aug_2017">
                                                                        August 2017 </a> </h4>
                                                </div>
                                                <div id="aug_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 09 Aug
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/August/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf';?>"
                                                                                        target="blank">ITQ appoints Sandeep Dwivedi
                                                                                        as its Chief Operating Officer
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 23 Aug
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/August/490-MAKEMYTRIP TO USE THE TRAVELPORT PLATFORM.pdf';?>"
                                                                                        target="blank">MAKEMYTRIP TO USE THE TRAVELPORT
                                                                                        PLATFORM
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 31 Aug
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/August/491-Travelport announces a new multiyear full content agreement with Ethiopian Airlines.pdf';?>"
                                                                                        target="blank">Travelport announces a new
                                                                                        multiyear full content agreement
                                                                                        with Ethiopian Airlines
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#jul_2017" aria-expanded="false"
                                                                        aria-controls="jul_2017">
                                                                        July 2017 </a> </h4>
                                                </div>
                                                <div id="jul_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 06 Jul
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/July/488-Travelport awarded Best GDS at the Arabian Travel Awards.pdf';?>"
                                                                                        target="blank">Travelport awarded "Best GDS"
                                                                                        at the Arabian Travel Awards</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#jun_2017" aria-expanded="false"
                                                                        aria-controls="jun_2017">
                                                                        June 2017 </a> </h4>
                                                </div>
                                                <div id="jun_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 07 Jun
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/June/486-ITQ to conduct road shows on booking IndiGo 6E through Travelport.pdf';?>"
                                                                                        target="blank">ITQ to conduct road shows
                                                                                        on booking IndiGo (6E) through Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 30 Jun
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/June/487-Travelport announces new hotel content deal with Treebo Hotels.pdf';?>"
                                                                                        target="blank">Travelport announces new hotel
                                                                                        content deal with Treebo Hotels</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#may_2017" aria-expanded="false"
                                                                        aria-controls="may_2017">
                                                                        May 2017 </a> </h4>
                                                </div>
                                                <div id="may_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 08 May
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/May/482-ITQ announces Winners of Student Of The Year 2017 Contest0517.pdf';?>"
                                                                                        target="blank">ITQ announces Winners of -Student
                                                                                        Of The Year 2017 Contest
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 15 May
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/May/483-ITQ Wins Two Prestigious Industry Awards This Year.pdf';?>"
                                                                                        target="blank">ITQ Wins Two Prestigious Industry
                                                                                        Awards This Year</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 15 May
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/May/484-Treebo Hotels Partners With Travelport to Strengthen Its Corporate Travelers Portfolio.pdf';?>"
                                                                                        target="blank">Treebo Hotels Partners With
                                                                                        Travelport to Strengthen Its Corporate
                                                                                        Travelers' Portfolio
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 17 May
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/May/485-Travelport Celebrates Over 220 Airlines Using Rich Content and Branding Solution.pdf';?>"
                                                                                        target="blank">Travelport Celebrates Over
                                                                                        220 Airlines Using 'Rich Content
                                                                                        and Branding' Solution
                                                                                </a></li>

                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#apr_2017" aria-expanded="false"
                                                                        aria-controls="apr_2017">
                                                                        April 2017 </a> </h4>
                                                </div>
                                                <div id="apr_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 24 Apr
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/April/477-Travelport and Onur Air announce long term agreement.pdf';?>"
                                                                                        target="blank">Travelport and Onur Air announce
                                                                                        long term agreement</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 26 Apr
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/April/479-Travelport and Nesma Airlines announce new long term agreement.pdf';?>"
                                                                                        target="blank">Travelport and Nesma Airlines
                                                                                        announce new, long term agreement</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 27 Apr
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/April/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf';?>"
                                                                                        target="blank">Business travel sector to
                                                                                        grow 3.7% annually to 2027 as new
                                                                                        tech fuels takeup
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#feb_2017" aria-expanded="false"
                                                                        aria-controls="feb_2017">
                                                                        February 2017 </a> </h4>
                                                </div>
                                                <div id="feb_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 14 Feb
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Feb/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf';?>"
                                                                                        target="blank">Long-time partners Travelport
                                                                                        and Air Canada announce new full
                                                                                        content agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 16 Feb
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Feb/474-Travelport and Emirates extend global partnership through the enhancement of Emirates Branded Fares with advanced seat selection.pdf';?>"
                                                                                        target="blank">Travelport and Emirates extend
                                                                                        global partnership through the enhancement
                                                                                        of Emirates' Branded Fares with advanced
                                                                                        seat selection</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 20 Feb
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Feb/475-Alitalia Adopts Travelport Rapid Reprice to Simplify Ticket Exchanges.pdf';?>"
                                                                                        target="blank">Alitalia Adopts Travelport
                                                                                        Rapid Reprice to Simplify Ticket
                                                                                        Exchanges
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 28 Feb
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Feb/476-Travelport is first GDS to receive NDC Aggregator certification from IATA.pdf';?>"
                                                                                        target="blank">Travelport is first GDS to
                                                                                        receive NDC Aggregator certification
                                                                                        from IATA</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingEight">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#jan_2017" aria-expanded="false"
                                                                        aria-controls="jan_2017">
                                                                        January 2017 </a> </h4>
                                                </div>
                                                <div id="jan_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 27 Jan
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2017/Jan/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        all set for SATTE 2017
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>

                        <div class="tab-pane fade " id="2016">
                                <div class="panel-group" id="accordionFour" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#dec_2016" aria-expanded="true"
                                                                        aria-controls="dec_2016" class="remove-link">
                                                                        December 2016 </a> </h4>
                                                </div>
                                                <div id="dec_2016" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 02 Dec
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/December/468-Vueling signs up to Travelport Rich Content and Branding.pdf';?>"
                                                                                        target="blank">Vueling signs up to Travelport
                                                                                        Rich Content and Branding
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Dec
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/December/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf';?>"
                                                                                        target="blank">Travelport and ARC partner
                                                                                        to simplify and increase ancillary
                                                                                        sales for airlines
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 15 Dec
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/December/470-InterGlobe Technology Quotient launches 4th edition of Student of the Year Contest.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        launches 4th edition of Student of
                                                                                        the Year Contest
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 21 Dec
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/December/471-UTair Aviation signs a full content agreement with Travelport.pdf';?>"
                                                                                        target="blank">UTair Aviation signs a full
                                                                                        content agreement with Travelport
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#nov_2016" aria-expanded="false"
                                                                        aria-controls="nov_2016">
                                                                        November 2016 </a> </h4>
                                                </div>
                                                <div id="nov_2016" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 02 Nov
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/November/464-Bulgaria Air renews its full content agreement with Travelport.pdf';?>"
                                                                                        target="blank">Bulgaria Air renews its full
                                                                                        content agreement with Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 15 Nov
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/November/466-OTA starts selling airline ancillaries with Travelport.pdf';?>"
                                                                                        target="blank">OTA starts selling airline
                                                                                        ancillaries with Travelport </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 25 Nov
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/November/467-Travel industry leaders celebrate the launch of IndiGos fares and ancillaries with Travelport in India.pdf';?>"
                                                                                        target="blank">Travel industry leaders celebrate
                                                                                        the launch of IndiGo's fares and
                                                                                        ancillaries with Travelport in India
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#octo_2016" aria-expanded="false"
                                                                        aria-controls="octo_2016">
                                                                        October 2016 </a> </h4>
                                                </div>
                                                <div id="octo_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 04 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf';?>"
                                                                                        target="blank">Travelport comes out on top
                                                                                        at two prestigious industry awards
                                                                                        in the Asia-Pacific region
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 11 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/459-Travelport Business Insights transforms travel data.pdf';?>"
                                                                                        target="blank">Travelport Business Insights
                                                                                        transforms travel data
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 19 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/465-Travelport voted Best Innovative GDS in Asia.pdf';?>"
                                                                                        target="blank">Travelport voted Best Innovative
                                                                                        GDS in Asia </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 20 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/460-Travelport signs renewal agreement with Air Astana.pdf';?>"
                                                                                        target="blank">Travelport signs renewal agreement
                                                                                        with Air Astana</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 20 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/461-Visit Finland utilises Travelports travel commerce platform to boost Asian stopover traffic.pdf';?>"
                                                                                        target="blank">Visit Finland utilises Travelport's
                                                                                        travel commerce platform to boost
                                                                                        Asian stopover traffic
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 27 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/458-PRTravelport once again win Best GDS title at India Travel Awards 2016  North10 16v2.pdf';?>"
                                                                                        target="blank">Travelport once again wins
                                                                                        Best GDS title at India Travel Awards
                                                                                        2016 - North
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 28 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/462-PR Travelport continues its industry leadership in air merchandising 1116.pdf';?>"
                                                                                        target="blank">Travelport continues its industry
                                                                                        leadership in air merchandising</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 31 Oct
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/October/463-IndiGo signs with Travelport which is now in 18 languages.pdf';?>"
                                                                                        target="blank">IndiGo signs with Travelport
                                                                                        which is now in 18 languages </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#sep_2016" aria-expanded="false"
                                                                        aria-controls="sep_2016">
                                                                        Septemeber 2016 </a> </h4>
                                                </div>
                                                <div id="sep_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 15 Sep
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/September/452-IndiGo signs its first ever GDS agreement with Travelport.pdf';?>"
                                                                                        target="blank">IndiGo signs its first ever
                                                                                        GDS agreement with Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 19 Sep
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/September/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf';?>"
                                                                                        target="blank">Travelport launches "Mobile
                                                                                        Payment Solution" offering M-Pesa
                                                                                        and Airtel Money transfer services
                                                                                        in Kenya
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 23 Sep
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/September/454-Lots of Hotels content now available in Travelport Rooms and More.pdf';?>"
                                                                                        target="blank">Lots of Hotels content now
                                                                                        available in Travelport Rooms and
                                                                                        More </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 26 Sep
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/September/455-South African Airways releases ancillary services with Travelport.pdf';?>"
                                                                                        target="blank">South African Airways' releases
                                                                                        ancillary services with Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 26 Sep
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/September/456-Travelport and Mastercard maximize customer insights to drive new partnership strategies for airlines.pdf';?>"
                                                                                        target="blank">Travelport and Mastercard
                                                                                        maximize customer insights to drive
                                                                                        new partnership strategies for airlines
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#aug_2016" aria-expanded="false"
                                                                        aria-controls="aug_2016">
                                                                        August 2016 </a> </h4>
                                                </div>
                                                <div id="aug_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 01 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/441-Travelport expands its European partnership with Expedia.pdf';?>"
                                                                                        target="blank">Travelport expands its European
                                                                                        partnership with Expedia</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 03 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf';?>"
                                                                                        target="blank">Travelport announces renewal
                                                                                        of long-term Emirates partnership
                                                                                        and roll-out of merchandising technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 04 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/443-ITQ wins Todays Traveller Award 2016 for Best Technology Solution Provider.pdf';?>"
                                                                                        target="blank">ITQ wins Today's Traveller
                                                                                        Award 2016 for Best Technology Solution
                                                                                        Provider
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 04 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/444-Three additional airlines in the Pacific region live with Travelports air merchandising technology.pdf';?>"
                                                                                        target="blank">Three additional airlines
                                                                                        in the Pacific region live with Travelport's
                                                                                        air merchandising technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 05 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/447-Airtrain to distribute via Travelport Travel Commerce Platform.pdf';?>"
                                                                                        target="blank">Airtrain to distribute via
                                                                                        Travelport Travel Commerce Platform
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 05 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/449-Travelport bags India Travel Awards West 2016 for the Best GDS 2016.pdf';?>"
                                                                                        target="blank">Travelport bags India Travel
                                                                                        Awards West 2016 for the Best GDS
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/445-Travelport and Rehlatcom announce new agreement.pdf';?>"
                                                                                        target="blank">Travelport and Rehlat announce
                                                                                        new agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Aug
                                                                                        2016:
                                                                                </p>
                                                                                <a class="list-details remove-link li-a-hover" href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/446-Collinson Group selects MTT to support its digital strategy for airport lounge access.pdf';?>"
                                                                                        target="blank">Collinson
                                                                                        Group selects MTT to support its
                                                                                        digital strategy for airport lounge
                                                                                        access
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Aug
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/August/450-ITQ helps children of Blind Relief Association this Independence Day 2016.pdf';?>"
                                                                                        target="blank">ITQ helps children of Blind
                                                                                        Relief Association this Independence
                                                                                        Day 2016
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#jul_2016" aria-expanded="false"
                                                                        aria-controls="jul_2016">
                                                                        July 2016 </a> </h4>
                                                </div>
                                                <div id="jul_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/430-UAE travel agents at forefront of technology in meeting traveller trends and growing the emirates travel industry.pdf';?>"
                                                                                        target="blank">UAE travel agents at forefront
                                                                                        of technology in meeting traveller
                                                                                        trends and growing the emirates'
                                                                                        travel industry</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 05 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/429-Mauritania Airlines International Selects Travelport Technology in Distribution Partnership.pdf';?>"
                                                                                        target="blank">Mauritania Airlines International
                                                                                        Selects Travelport Technology in
                                                                                        Distribution Partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/431-Copa Airlines launches sophisticated new apps with MTT.pdf';?>"
                                                                                        target="blank">Copa Airlines launches sophisticated
                                                                                        new apps with MTT
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 13 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/433-Travelport signs renewal deal with long standing customer Piloot Koen.pdf';?>"
                                                                                        target="blank">Travelport signs renewal deal
                                                                                        with long-standing customer Piloot
                                                                                        Koen
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/432-Travelport and OYO announce new hotel content deal.pdf';?>"
                                                                                        target="blank">Travelport and OYO announce
                                                                                        new hotel content deal
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/434-India Travel Awards East  2016.pdf';?>"
                                                                                        target="blank">Travelport scoops 'Best GDS'
                                                                                        title at India Travel Awards- East
                                                                                        2016
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/436-Travelport and Groupize launch new small group booking tool.pdf';?>"
                                                                                        target="blank">Travelport and Groupize launch
                                                                                        new small group booking tool </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 18 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/435-Travelport signs renewal agreement with Aeroflot.pdf';?>"
                                                                                        target="blank">Travelport signs renewal agreement
                                                                                        with Aeroflot</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 25 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/437-Travix adopts Travelports technology to drive accelerated growth.pdf';?>"
                                                                                        target="blank">Travix adopts Travelport's
                                                                                        technology to drive accelerated growth
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 26 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/438-Hong Kongs Edmond Travel grows with Travelport.pdf';?>"
                                                                                        target="blank">Hong Kong's Edmond Travel
                                                                                        grows with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 26 Jul
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/July/439-Travelport signs long term agreement with Roibek Travel in Saudi Arabia.pdf';?>"
                                                                                        target="blank">Travelport signs long term
                                                                                        agreement with Roibek Travel in Saudi
                                                                                        Arabia
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#jun_2016" aria-expanded="false"
                                                                        aria-controls="jun_2016">
                                                                        June 2016 </a> </h4>
                                                </div>
                                                <div id="jun_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 02 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf';?>"
                                                                                        target="blank">Air Namibia makes the most
                                                                                        of Travelport's industry leading
                                                                                        airline merchandising solution
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf';?>"
                                                                                        target="blank">Travelport celebrates 20 years
                                                                                        of growth and innovation in the Romanian
                                                                                        travel industry
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/421-Travelport Group scoops up four awards for innovation.pdf';?>"
                                                                                        target="blank">Travelport Group scoops up
                                                                                        four awards for innovation</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 07 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/422-Travelport wraps up successful Asia Pacific customer conference.pdf';?>"
                                                                                        target="blank">Travelport wraps up successful
                                                                                        Asia-Pacific customer conference
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 09 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/423-DataArt Joins the Travelport Developer Network.pdf';?>"
                                                                                        target="blank">DataArt Joins the Travelport
                                                                                        Developer Network
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 12 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/426-Travelports Africa Middle East  South Asia legal team named Middle Eastern Legal Team of the Year.pdf';?>"
                                                                                        target="blank">Travelport's Africa, Middle
                                                                                        East & South Asia (AFMESA) legal
                                                                                        team named Middle Eastern Legal Team
                                                                                        of the Year</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 15 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/425-Travelport names Carlos Quijano to head Latin America Air Commerce.pdf';?>"
                                                                                        target="blank">Travelport names Carlos Quijano
                                                                                        to head Latin America Air Commerce</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 16 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/424-Travelport adds Kelly Kolb as vice president of government affairs.pdf';?>"
                                                                                        target="blank">Travelport adds Kelly Kolb
                                                                                        as vice president of government affairs</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 30 Jun
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/June/428-Fiji Airways improves GDS displays with Travelport.pdf';?>"
                                                                                        target="blank">Fiji Airways improves GDS
                                                                                        displays with Travelport</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#may_2016" aria-expanded="false"
                                                                        aria-controls="may_2016">
                                                                        May 2016 </a> </h4>
                                                </div>
                                                <div id="may_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 06 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf';?>"
                                                                                        target="blank">Air France KLM signs up to
                                                                                        Travelport's industry leading airline
                                                                                        merchandising technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 11 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf';?>"
                                                                                        target="blank">UTair signs up to Travelport's
                                                                                        industry-leading airline merchandising
                                                                                        technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 11 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/410-Travelport and Marriott International renew distribution agreement.pdf';?>"
                                                                                        target="blank">Travelport and Marriott International
                                                                                        renew distribution agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 13 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/411-Travelport encourages its Hungarian customers to upsell ancillary services.pdf';?>"
                                                                                        target="blank">Travelport encourages its
                                                                                        Hungarian customers to upsell ancillary
                                                                                        services
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 16 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/412-PR InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa0516v4.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        organizes Annual Sales Conference
                                                                                        2016 in Lavasa
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 18 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/413-Travelport extends its suite of virtual payment solutions.pdf';?>"
                                                                                        target="blank">Travelport extends its suite
                                                                                        of virtual payment solutions
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 19 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/414-Travelport and Mango Sign New MultiYear Global Full Content Agreement.pdf';?>"
                                                                                        target="blank">Travelport and Mango Sign
                                                                                        New Multi-Year Global Full Content
                                                                                        Agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 23 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/417-The Low Fare Study Gimmick 2.pdf';?>"
                                                                                        target="blank">The Low Fare Study Gimmick
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 25 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/415-Travelport launches new XML solution with Movida to enhance car rental offering.pdf';?>"
                                                                                        target="blank">Travelport launches new XML
                                                                                        solution with Movida to enhance car
                                                                                        rental offering
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 25 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/416-Aeromexico and Travelport agree on extended partnership including new digital services with MTT.pdf';?>"
                                                                                        target="blank">Aeromexico and Travelport
                                                                                        agree on extended partnership including
                                                                                        new digital services with MTT</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 31 May
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/May/418-Travelport showcases its latest technology to the Russian travel industry.pdf';?>"
                                                                                        target="blank">Travelport showcases its latest
                                                                                        technology to the Russian travel
                                                                                        industry
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#apr_2016" aria-expanded="false"
                                                                        aria-controls="apr_2016">
                                                                        April 2016 </a> </h4>
                                                </div>
                                                <div id="apr_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/394-easyJet and Travelport announce new long term agreement.pdf';?>"
                                                                                        target="blank">easyJet and Travelport announce
                                                                                        new long-term agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 06 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/395-Travelport acquires its third party distributor in Japan.pdf';?>"
                                                                                        target="blank">Travelport acquires its third
                                                                                        party distributor in Japan </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 07 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/396-Travelport embraces collaborative approach to help customers launch new travel services.pdf';?>"
                                                                                        target="blank">Travelport embraces collaborative
                                                                                        approach to help customers launch
                                                                                        new travel services
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 13 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/397-Travelport wins Best GDS Title at India Travel Awards South 2016.pdf';?>"
                                                                                        target="blank">Travelport wins 'Best GDS'
                                                                                        Title at India Travel Awards South-2016
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 13 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/398-Macau Government Tourism Office partners with Travelport to boost traffic.pdf';?>"
                                                                                        target="blank">Macau Government Tourism Office
                                                                                        partners with Travelport to boost
                                                                                        traffic
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 18 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/399-Travelport Outlines How Technology Supports the Travel Industrys Growth in the Face of Sector Challenges.pdf';?>"
                                                                                        target="blank">Travelport Outlines How Technology
                                                                                        Supports the Travel Industry's Growth
                                                                                        in the Face of Sector Challenges</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 20 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/400-InterGlobe Technology Quotient organizes Student of the Year 2016 Contest.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        organizes 'Student of the Year 2016'
                                                                                        Contest
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 20 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/401-China Southern becomes latest major Chinese airline to adopt Travelport Rich Content and Branding.pdf';?>"
                                                                                        target="blank">China Southern becomes latest
                                                                                        major Chinese airline to adopt Travelport
                                                                                        Rich Content and Branding </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 21 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/402-Travelport demonstrates ongoing commitment to the travel industry in Namibia.pdf';?>"
                                                                                        target="blank">Travelport demonstrates ongoing
                                                                                        commitment to the travel industry
                                                                                        in Namibia</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 26 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/403-Overseas Travel Bureau Renews Agreement with Travelport.pdf';?>"
                                                                                        target="blank">Overseas Travel Bureau Renews
                                                                                        Agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 27 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/404-Al Tayyar Travel Group extends their agreement with Travelport to expand their global business.pdf';?>"
                                                                                        target="blank">Al Tayyar Travel Group extends
                                                                                        their agreement with Travelport to
                                                                                        expand their global business </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 27 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/405-Travelport and TTS Launches Mobile Agent for Windows Phones.pdf';?>"
                                                                                        target="blank">Travelport and TTS Launches
                                                                                        Mobile Agent for Windows Phones</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 29 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/406-South Africas newest airline Fly Blue Crane signs up with Travelport for the distribution of content globally.pdf';?>"
                                                                                        target="blank">South Africa's newest airline,
                                                                                        Fly Blue Crane signs up with Travelport
                                                                                        for the distribution of content globally
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 29 Apr
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/April/407-Germania Flug AG goes live with Travelports unrivalled merchandising technology.pdf';?>"
                                                                                        target="blank">Germania Flug AG goes live
                                                                                        with Travelport's unrivalled merchandising
                                                                                        technology
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#mar_2016" aria-expanded="false"
                                                                        aria-controls="mar_2016">
                                                                        March 2016 </a> </h4>
                                                </div>
                                                <div id="mar_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 02 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/386-Scandinavian Airlines SAS signs up to Travelports industry leading merchandising functionality.pdf';?>"
                                                                                        target="blank">Scandinavian Airlines (SAS)
                                                                                        signs up to Travelport's industry-leading
                                                                                        merchandising functionality
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 08 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/387-Travel Technology Leaders LIVE Spotlight on Innovation Roadshow kicks off in March across the Kingdom.pdf';?>"
                                                                                        target="blank">Travel Technology Leader's
                                                                                        LIVE "Spotlight on Innovation" Roadshow
                                                                                        kicks off in March across the Kingdom</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 10 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/388-Travelport continues to drive industry momentum with its pioneering airline merchandising technology.pdf';?>"
                                                                                        target="blank">Travelport continues to drive
                                                                                        industry momentum with its pioneering
                                                                                        airline merchandising technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 12 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/389-Travelport and Gray Dawes sign multiyear renewal agreement.pdf';?>"
                                                                                        target="blank">Travelport and Gray Dawes
                                                                                        sign multi-year renewal agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 14 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/390-CTS Systems becomes a preferred supplier of THOR.pdf';?>"
                                                                                        target="blank">CTS Systems becomes a preferred
                                                                                        supplier of THOR</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 16 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/392-Colombian TMC Aviatur selects Travelport technology.pdf';?>"
                                                                                        target="blank">Colombian TMC Aviatur selects
                                                                                        Travelport technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 17 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/391-Travelport and Marine Tours announce new multiyear renewal agreement.pdf';?>"
                                                                                        target="blank">Travelport and Marine Tours
                                                                                        announce new multiyear renewal agreement</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 31 Mar
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/March/393-Bangkok Airways signs multiyear agreement with Travelport and utilises industry leading merchandising technology.pdf';?>"
                                                                                        target="blank">Bangkok Airways signs multi-year
                                                                                        agreement with Travelport and utilises
                                                                                        industry-leading merchandising technology</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#feb_2016" aria-expanded="false"
                                                                        aria-controls="feb_2016">
                                                                        February 2016 </a> </h4>
                                                </div>
                                                <div id="feb_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 02 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/374-Travelport Wins The Most Innovative Travel Technology Partner Award 2016.pdf';?>"
                                                                                        target="blank">Travelport Wins The Most Innovative
                                                                                        Travel Technology Partner Award 2016</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 05 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/375-Travelport signs new deal with NG Travel Group Beach Tours and Dubai Tours move to Travelport.pdf';?>"
                                                                                        target="blank">Travelport signs new deal
                                                                                        with NG Travel Group Beach Tours
                                                                                        and Dubai Tours move to Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 08 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/376-Fly540 Signs Global MultiYear Content Agreement with Travelport.pdf';?>"
                                                                                        target="blank">Fly540 Signs Global Multi-Year
                                                                                        Content Agreement with Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 10 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/377-InterGlobe Technology Quotient and CAP Foundation launch skill development initiative.pdf';?>"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        and CAP Foundation launch skill development
                                                                                        initiative
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/378-GOL branded fares merchandising launched in Travelport Smartpoint and Travelport Universal API.pdf';?>"
                                                                                        target="blank">GOL branded fares merchandising
                                                                                        launched in Travelport Smartpoint
                                                                                        and Travelport Universal API
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 13 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/382-NG Travel launches two OTAs on Travelport Platform.pdf';?>"
                                                                                        target="blank">NG Travel launches two OTAs
                                                                                        on Travelport Platform
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 15 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/381-East African Air Express Signs Global Multi Year Full Content Agreement with Travelport.pdf';?>"
                                                                                        target="blank">East African Air Express Signs
                                                                                        Global Multi Year Full Content Agreement
                                                                                        with Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 16 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/379-Travelport and Mobacar announce global agreement.pdf';?>"
                                                                                        target="blank">Travelport and Mobacar announce
                                                                                        global agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 16 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/380-Travelport announces new multiyear distribution and merchandising agreement with Cathay Pacific and Dragonair.pdf';?>"
                                                                                        target="blank">Travelport announces new multi-year
                                                                                        distribution and merchandising agreement
                                                                                        with Cathay Pacific and Dragonair</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 17 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/383-New Chief Executive Officer Appointed for MTT.pdf';?>"
                                                                                        target="blank">New Chief Executive Officer
                                                                                        Appointed for MTT</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/384-British Airways and Iberia select Travelports industry leading airline merchandising technology.pdf';?>"
                                                                                        target="blank">British Airways and Iberia
                                                                                        select Travelport's industry leading
                                                                                        airline merchandising technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 23 Feb
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/Feb/385-The marine travel company is first travel agency recognised in Travelport certification program.pdf';?>"
                                                                                        target="blank">The marine travel company
                                                                                        is first travel agency recognised
                                                                                        in Travelport certification program
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#jan_2016" aria-expanded="false"
                                                                        aria-controls="jan_2016">
                                                                        January 2016 </a> </h4>
                                                </div>
                                                <div id="jan_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 01 Jan
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/January/368-Al Fanar Travel upgrades Travelport agreement.pdf';?>"
                                                                                        target="blank">Al Fanar Travel upgrades Travelport
                                                                                        agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 08 Jan
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/January/369-Malmo Aviation signs deal for Travelport Rich Content and Branding.pdf';?>"
                                                                                        target="blank">Malmö Aviation signs deal
                                                                                        for Travelport Rich Content and Branding</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 11 Jan
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/January/370-Travelport and CEE Travel Systems announce a new agreement with Student Agency.pdf';?>"
                                                                                        target="blank">Travelport and CEE Travel
                                                                                        Systems announce a new agreement
                                                                                        with Student Agency
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 Jan
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/January/371-Travelport announces new agreement with Frontier Airlines.pdf';?>"
                                                                                        target="blank">Travelport announces new agreement
                                                                                        with Frontier Airlines </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 26 Jan
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/January/372-Travelport launches state of the art customer Helpdesk in Kenya.pdf';?>"
                                                                                        target="blank">Travelport launches state
                                                                                        of the art customer Helpdesk in Kenya
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 27 Jan
                                                                                        2016
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="<?php echo get_template_directory_uri().'/assets/press-release/2016/January/373-Travelport and Jambojet sign a new multiyear global full content agreement.pdf';?>"
                                                                                        target="blank">Travelport and Jambojet sign
                                                                                        a new multi-year global full content
                                                                                        agreement
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <div class="tab-pane fade " id="2015">
                                <div class="panel-group" id="accordionFive" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#dec_2015" aria-expanded="true"
                                                                        aria-controls="dec_2015" class="remove-link">
                                                                        December 2015 </a> </h4>
                                                </div>
                                                <div id="dec_2015" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 02 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank">Travelport signs new agreement
                                                                                        with North-West Transport Agency
                                                                                        in Russia</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 04 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">euroAtlantic airways signs
                                                                                        Travelport Rich Content and Branding
                                                                                        agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 08 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/470-InterGlobe Technology Quotient launches 4th edition of Student of the Year Contest.pdf"
                                                                                        target="blank">Travelport renews deal with
                                                                                        QAS holidays
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 09 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/471-UTair Aviation signs a full content agreement with Travelport.pdf"
                                                                                        target="blank">Travelport voted 'Best GDS'
                                                                                        for North at India Travel Awards
                                                                                        on 8 December 2015
                                                                                </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 13 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank">Travelport and Kenya Airways
                                                                                        extend multi-year agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 14 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport Appoints Bernard
                                                                                        Bot as Chief Financial Officer </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 14 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/470-InterGlobe Technology Quotient launches 4th edition of Student of the Year Contest.pdf"
                                                                                        target="blank">Equatorial Congo Airlines
                                                                                        and Travelport enter global full
                                                                                        content agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 15 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/471-UTair Aviation signs a full content agreement with Travelport.pdf"
                                                                                        target="blank">Aviacenter, top Russian consolidator
                                                                                        signs new deal with Travelport </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 16 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank">Etihad Airways Boosts Travel
                                                                                        Agents Travellers Understanding of
                                                                                        New Fare Structure with Innovative
                                                                                        Technology Roll Out</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 17 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Enhancing the selling experience
                                                                                        for travel agents-Travelport Smartpoint
                                                                                        6.5 now available
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 17 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/470-InterGlobe Technology Quotient launches 4th edition of Student of the Year Contest.pdf"
                                                                                        target="blank">Travelport offers new WestJet
                                                                                        ancillary merchandising opportunity
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 18 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/471-UTair Aviation signs a full content agreement with Travelport.pdf"
                                                                                        target="blank">Res2 and Travelport sign new
                                                                                        agreement with WOW air for iPort
                                                                                        DCS </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 21 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport Sweeps India's
                                                                                        Travel Awards Industry with Seven
                                                                                        Accolades in 2015
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 21 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/470-InterGlobe Technology Quotient launches 4th edition of Student of the Year Contest.pdf"
                                                                                        target="blank">HSMAI To Honor Thor, Inc.
                                                                                        With A Gold Award In 59th Annual
                                                                                        Adrian Awards Competition
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 30 Dec
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/471-UTair Aviation signs a full content agreement with Travelport.pdf"
                                                                                        target="blank">Interglobe Technology Quotient
                                                                                        launches Trip38 on Galileo </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#nov_2015" aria-expanded="false"
                                                                        aria-controls="nov_2015">
                                                                        November 2015 </a> </h4>
                                                </div>
                                                <div id="nov_2015" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 03 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">China Eastern Airlines and
                                                                                        subsidiary Shanghai Airlines extend
                                                                                        multi-year commitment to Travelport's
                                                                                        Rich Content & Branding </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 04 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/466-OTA starts selling airline ancillaries with Travelport.pdf"
                                                                                        target="blank">Air China signs multi-year
                                                                                        content agreement with Travelport
                                                                                        with enhancement of Rich Content
                                                                                        & Branding </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 12 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/467-Travel industry leaders celebrate the launch of IndiGos fares and ancillaries with Travelport in India.pdf"
                                                                                        target="blank">Travelport and Sindbad announce
                                                                                        new agreement </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 13 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">EVA Air turns to Travelport
                                                                                        for its business intelligence solution
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 14 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/466-OTA starts selling airline ancillaries with Travelport.pdf"
                                                                                        target="blank">Travelport voted Best Technology
                                                                                        Provider by Scottish travel agents
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 17 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/467-Travel industry leaders celebrate the launch of IndiGos fares and ancillaries with Travelport in India.pdf"
                                                                                        target="blank">Travelport Universal API Demo
                                                                                        Site Launches New Functionality </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 19 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Travelport scoops Innovation
                                                                                        Award at Travel Magazine's Travel
                                                                                        Awards Belgium
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 19 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/467-Travel industry leaders celebrate the launch of IndiGos fares and ancillaries with Travelport in India.pdf"
                                                                                        target="blank">Tigerair Taiwan and Travelport
                                                                                        announce new global agreement </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 20 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Agents can now earn commission
                                                                                        on Heathrow Express tickets booked
                                                                                        through Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 21 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/466-OTA starts selling airline ancillaries with Travelport.pdf"
                                                                                        target="blank">Travelport voted Best GDS
                                                                                        for East at India Travel Awards on
                                                                                        19 November 2015
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 24 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/467-Travel industry leaders celebrate the launch of IndiGos fares and ancillaries with Travelport in India.pdf"
                                                                                        target="blank">Air New Zealand becomes 100th
                                                                                        carrier to launch Rich Content and
                                                                                        Branding on Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 25 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/467-Travel industry leaders celebrate the launch of IndiGos fares and ancillaries with Travelport in India.pdf"
                                                                                        target="blank">Travelport launches new seat
                                                                                        map application for Eurostar </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 26 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Luxair signs multi-year full
                                                                                        content agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 30 Nov
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/466-OTA starts selling airline ancillaries with Travelport.pdf"
                                                                                        target="blank">Travelport takes majority
                                                                                        share of Aussie start-up, Locomote
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#octo_2015" aria-expanded="false"
                                                                        aria-controls="octo_2015">
                                                                        October 2015 </a> </h4>
                                                </div>
                                                <div id="octo_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 01 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport and SNCF Announce
                                                                                        a Renewed Distribution Agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 05 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/459-Travelport Business Insights transforms travel data.pdf"
                                                                                        target="blank">Travelport acquires its distributor
                                                                                        in Austria</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 08 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/465-Travelport voted Best Innovative GDS in Asia.pdf"
                                                                                        target="blank">Travelport Hotelzon launches
                                                                                        mobile app for business travellers
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 15 Oct
                                                                                        2015:
                                                                                </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/460-Travelport signs renewal agreement with Air Astana.pdf"
                                                                                        target="blank">TDiversity
                                                                                        Travel moves to Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 16 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/461-Visit Finland utilises Travelports travel commerce platform to boost Asian stopover traffic.pdf"
                                                                                        target="blank">Middle East Travel Industry
                                                                                        Leaders Convene at Technology Summit</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 19 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/458-PRTravelport once again win Best GDS title at India Travel Awards 2016  North10 16v2.pdf"
                                                                                        target="blank">Travelport picks up Innovation
                                                                                        Award at the Dutch Travel Industry
                                                                                        Congress
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 21 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/462-PR Travelport continues its industry leadership in air merchandising 1116.pdf"
                                                                                        target="blank">Travelport Rich Content and
                                                                                        Branding reaches first anniversary
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 22 Oct
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/463-IndiGo signs with Travelport which is now in 18 languages.pdf"
                                                                                        target="blank">Travelport and Tripsta announce
                                                                                        new agreement </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#sep_2015" aria-expanded="false"
                                                                        aria-controls="sep_2015">
                                                                        Septemeber 2015 </a> </h4>
                                                </div>
                                                <div id="sep_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 09 Sep
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/452-IndiGo signs its first ever GDS agreement with Travelport.pdf"
                                                                                        target="blank">Travelport and OneTwoTrip
                                                                                        to sign long-term agreement</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 16 Sep
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">Travelport Hotelzon's expansion
                                                                                        continues into the Netherlands</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 23 Sep
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">First Car Rental & Travelport
                                                                                        Announce Partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 28 Sep
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">AXESS and Travelport to deliver
                                                                                        enhanced agency desktop
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 28 Sep
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/456-Travelport and Mastercard maximize customer insights to drive new partnership strategies for airlines.pdf"
                                                                                        target="blank">Hahn Air Systems offers air
                                                                                        partners rich content solutions through
                                                                                        Travelport
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#aug_2015" aria-expanded="false"
                                                                        aria-controls="aug_2015">
                                                                        August 2015 </a> </h4>
                                                </div>
                                                <div id="aug_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 04 Aug
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">Travellers in India Prefer
                                                                                        Packages
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 12 Aug
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank">Air China turns to Travelport
                                                                                        for its business intelligence solution</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#jul_2015" aria-expanded="false"
                                                                        aria-controls="jul_2015">
                                                                        July 2015 </a> </h4>
                                                </div>
                                                <div id="jul_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/430-UAE travel agents at forefront of technology in meeting traveller trends and growing the emirates travel industry.pdf"
                                                                                        target="blank">Travel agents gain new level
                                                                                        of airline fares information with
                                                                                        Travelport Smartpoint 6.0 -</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 02 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/429-Mauritania Airlines International Selects Travelport Technology in Distribution Partnership.pdf"
                                                                                        target="blank">Travelport Hotelzon announces
                                                                                        entry into Italy and a new appointment
                                                                                        to its management team -</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 07 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/431-Copa Airlines launches sophisticated new apps with MTT.pdf"
                                                                                        target="blank">Travelport Acquires Mobile
                                                                                        Travel Technologies Ltd (MTT) </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 18 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/433-Travelport signs renewal deal with long standing customer Piloot Koen.pdf"
                                                                                        target="blank">SunExpress goes live with
                                                                                        Travelport Rich Content and Branding
                                                                                        - </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 27 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/432-Travelport and OYO announce new hotel content deal.pdf"
                                                                                        target="blank">Cox & Kings renews its alliance
                                                                                        with InterGlobe Technology Quotient</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#jun_2015" aria-expanded="false"
                                                                        aria-controls="jun_2015">
                                                                        June 2015 </a> </h4>
                                                </div>
                                                <div id="jun_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 08 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Jeju Air and Travelport announce
                                                                                        new global agreement</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 12 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Portman wins the Travelport
                                                                                        GTMC Innovation Award
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 12 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank">Travelport UK sales named
                                                                                        ?Business Development Team of the
                                                                                        Year? at The People Awards
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 19 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/422-Travelport wraps up successful Asia Pacific customer conference.pdf"
                                                                                        target="blank">Travelport wins 'Best Air
                                                                                        Rail Distribution Solution' at The
                                                                                        GARA Awards
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 22 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/423-DataArt Joins the Travelport Developer Network.pdf"
                                                                                        target="blank">Travelport Strengthens Relationship
                                                                                        with IATI in Turkey </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 22 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/426-Travelports Africa Middle East  South Asia legal team named Middle Eastern Legal Team of the Year.pdf"
                                                                                        target="blank">Kenya Airways Signs Up for
                                                                                        Travelports Rich Content and Branding
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 25 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/425-Travelport names Carlos Quijano to head Latin America Air Commerce.pdf"
                                                                                        target="blank">Travelport Hotelzon integrates
                                                                                        taxi and airport transfer booking
                                                                                        solutions in partnership with Cabforce
                                                                                        -
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 26 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/424-Travelport adds Kelly Kolb as vice president of government affairs.pdf"
                                                                                        target="blank">SWISS upgrades to Travelport
                                                                                        Rich Content and Branding - </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 29 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/428-Fiji Airways improves GDS displays with Travelport.pdf"
                                                                                        target="blank">Travelport announces access
                                                                                        to Cuba air travel for U.S. travel
                                                                                        agents -
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 30 Jun
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/428-Fiji Airways improves GDS displays with Travelport.pdf"
                                                                                        target="blank">Hajj Pilgrims Urged to Book
                                                                                        Trips Securely </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#may_2015" aria-expanded="false"
                                                                        aria-controls="may_2015">
                                                                        May 2015 </a> </h4>
                                                </div>
                                                <div id="may_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 01 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf"
                                                                                        target="blank">Hotelzon announces expansion
                                                                                        into Spain and further new management
                                                                                        appointments in Europe </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf"
                                                                                        target="blank">Travelport Reveals Middle
                                                                                        East's $72 Billion Travel Market
                                                                                        Poised for More Growth
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 05 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/410-Travelport and Marriott International renew distribution agreement.pdf"
                                                                                        target="blank">Travelport and HRG extend
                                                                                        partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 06 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/411-Travelport encourages its Hungarian customers to upsell ancillary services.pdf"
                                                                                        target="blank">Travelport and Nile Air announce
                                                                                        new global full content agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 07 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/412-PR InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa0516v4.pdf"
                                                                                        target="blank">Travelport and Air Go Egypt
                                                                                        sign a new multi-year global full
                                                                                        content agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/413-Travelport extends its suite of virtual payment solutions.pdf"
                                                                                        target="blank">Travelport and Etihad Airways
                                                                                        renew full content agreement & include
                                                                                        Rich Content & Branding</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/414-Travelport and Mango Sign New MultiYear Global Full Content Agreement.pdf"
                                                                                        target="blank">AirAsia signs up for Travelpotr
                                                                                        Rich Content
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 25 May
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/417-The Low Fare Study Gimmick 2.pdf"
                                                                                        target="blank"> Striving for excellence,
                                                                                        Travelport Galileo introduces Student
                                                                                        of the Year? contest to encourage
                                                                                        GDS students</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#apr_2015" aria-expanded="false"
                                                                        aria-controls="apr_2015">
                                                                        April 2015 </a> </h4>
                                                </div>
                                                <div id="apr_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport renews long term
                                                                                        agreement with East Sea Travel Group
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 09 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport Embarks on Successful
                                                                                        UAE Solutions Roadshow </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 13 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">Travelport expands hotel content
                                                                                        with the introduction of HRS properties</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 15 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Helloworld Limited welcomes
                                                                                        partnership with Travelport </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 16 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">TAP Portugal and Travelport
                                                                                        have signed a new multi-year global
                                                                                        full content agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 22 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">UNIGLOBE Travel Partners-Atlanta
                                                                                        Agency Converts to Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 24 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport Worldwide Limited
                                                                                        Announces Date and Time for First
                                                                                        Quarter 2015 Earnings Conference
                                                                                        Call
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 27 Apr
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport Appoints Former
                                                                                        Visa Europe CIO, Steve Chambers,
                                                                                        as a Non-Executive Director to Its
                                                                                        Board </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#mar_2015" aria-expanded="false"
                                                                        aria-controls="mar_2015">
                                                                        March 2015 </a> </h4>
                                                </div>
                                                <div id="mar_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 03 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport becomes the first
                                                                                        to offer a new industry-leading solution
                                                                                        for airport express services
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 13 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport renews partnership
                                                                                        with Accor Hotels</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 16 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">Jet Asia Airways signs up
                                                                                        for Travelport's Rich Content and
                                                                                        Branding solution to reposition itself
                                                                                        as a full service airline </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 17 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Biman Bangladesh Airlines
                                                                                        and Travelport Expand Partnership
                                                                                        with Full Content Agreement</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 24 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport in Poland signs
                                                                                        long-term agreement with First Class
                                                                                        -
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 25 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">100 airlines already signed
                                                                                        up for Travelport's Rich Content
                                                                                        and Branding merchandising technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 26 Mar
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport announces new agreement
                                                                                        with Greek carrier Ellinair - </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#feb_2015" aria-expanded="false"
                                                                        aria-controls="feb_2015">
                                                                        February 2015 </a> </h4>
                                                </div>
                                                <div id="feb_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 08 Feb
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travel Agents Association
                                                                                        of India extends partnership with
                                                                                        InterGlobe Technology Quotient in
                                                                                        India </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#jan_2015" aria-expanded="false"
                                                                        aria-controls="jan_2015">
                                                                        January 2015 </a> </h4>
                                                </div>
                                                <div id="jan_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 12 Jan
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport Announces Agent
                                                                                        of the Future for 2014 t
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Jan
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport and Vistara announce
                                                                                        long term distribution and merchandising
                                                                                        agreement
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <div class="tab-pane fade " id="2014">
                                <div class="panel-group" id="accordionSix" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#dec_2014" aria-expanded="true"
                                                                        aria-controls="dec_2014" class="remove-link">
                                                                        December 2014 </a> </h4>
                                                </div>
                                                <div id="dec_2014" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Dec
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank">Travelport enhances the visual
                                                                                        hotel content in its Travel Commerce
                                                                                        Platform
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 19 Dec
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport Smartpoint continues
                                                                                        to innovate with further enhanced
                                                                                        features to grow revenues for travel
                                                                                        agencies </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#nov_2014" aria-expanded="false"
                                                                        aria-controls="nov_2014">
                                                                        November 2014 </a> </h4>
                                                </div>
                                                <div id="nov_2014" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 04 Nov
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Travelport and AirAsia India
                                                                                        signs distribution agreement </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#octo_2014" aria-expanded="false"
                                                                        aria-controls="octo_2014">
                                                                        October 2014 </a> </h4>
                                                </div>
                                                <div id="octo_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport Universal API Launches
                                                                                        In India </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/459-Travelport Business Insights transforms travel data.pdf"
                                                                                        target="blank">Travelport Announces the Appointment
                                                                                        of Three Additional Non-Executive
                                                                                        Directors to its Board</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 02 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/465-Travelport voted Best Innovative GDS in Asia.pdf"
                                                                                        target="blank">Travelport Universal API launches
                                                                                        in India </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 02 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/460-Travelport signs renewal agreement with Air Astana.pdf"
                                                                                        target="blank">Travelport named Best GDS
                                                                                        in the Asia-Pacific for the sixth
                                                                                        consecutive year
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/461-Visit Finland utilises Travelports travel commerce platform to boost Asian stopover traffic.pdf"
                                                                                        target="blank">Aeroflot ? Russian Airlines
                                                                                        first in Russia to sign up to Travelport
                                                                                        Rich Content and Branding</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 08 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/458-PRTravelport once again win Best GDS title at India Travel Awards 2016  North10 16v2.pdf"
                                                                                        target="blank">eNett and Cornerstone partner
                                                                                        to deliver innovative virtual card
                                                                                        payment solution for Travel Management
                                                                                        Companies
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 15 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/462-PR Travelport continues its industry leadership in air merchandising 1116.pdf"
                                                                                        target="blank">Travelport Worldwide Limited
                                                                                        Announces Date and Time for Third
                                                                                        Quarter Earnings Conference Call
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 15 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/463-IndiGo signs with Travelport which is now in 18 languages.pdf"
                                                                                        target="blank">Travelport Appoints Walter
                                                                                        Di Luca General Manager of Southern
                                                                                        Cone Region
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 16 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/458-PRTravelport once again win Best GDS title at India Travel Awards 2016  North10 16v2.pdf"
                                                                                        target="blank">Travelport announces new full
                                                                                        content agreement with CemAir </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 17 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/462-PR Travelport continues its industry leadership in air merchandising 1116.pdf"
                                                                                        target="blank">Hotelzon announces expansion
                                                                                        into France and Poland </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 20 Oct
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/463-IndiGo signs with Travelport which is now in 18 languages.pdf"
                                                                                        target="blank">WestJet and Travelport to
                                                                                        Provide Customized Brand Content
                                                                                        to Travel Agents
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#sep_2014" aria-expanded="false"
                                                                        aria-controls="sep_2014">
                                                                        Septemeber 2014 </a> </h4>
                                                </div>
                                                <div id="sep_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/452-IndiGo signs its first ever GDS agreement with Travelport.pdf"
                                                                                        target="blank">European OTA expands deal
                                                                                        with Travelport</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">Travelport signs up South
                                                                                        African Airways
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 22 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">Travelport announces new long
                                                                                        term agreement with Carlson Wagonlit
                                                                                        Travel
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 24 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">Travelport, a distribution
                                                                                        services and e-commerce provider
                                                                                        for the global travel industry, announces
                                                                                        the launch of its ground-breaking
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 24 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/456-Travelport and Mastercard maximize customer insights to drive new partnership strategies for airlines.pdf"
                                                                                        target="blank">Travelport and BCD Travel
                                                                                        Sign New Multi-Year Global Agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 24 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">Travelport Worldwide Limited
                                                                                        Announces Pricing of Its Initial
                                                                                        Public Offering of Common Shares</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 25 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">Travelport Universal API to
                                                                                        help build next gen travel app </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 25 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">Travelport's Universal API
                                                                                        now available in India</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 26 Sep
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/456-Travelport and Mastercard maximize customer insights to drive new partnership strategies for airlines.pdf"
                                                                                        target="blank">TODAY IN APIS: TRAVELPORT
                                                                                        EXPANDS UNIVERSAL API AVAILABLILITY
                                                                                        TO INDIA
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#aug_2014" aria-expanded="false"
                                                                        aria-controls="aug_2014">
                                                                        August 2014 </a> </h4>
                                                </div>
                                                <div id="aug_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 05 Aug
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">Travelport strengthens UK
                                                                                        management team
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 19 Aug
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank">Travelport Awards dnata with
                                                                                        "Top Producing Agency" Award</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#jul_2014" aria-expanded="false"
                                                                        aria-controls="jul_2014">
                                                                        July 2014 </a> </h4>
                                                </div>
                                                <div id="jul_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 29 Jul
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/430-UAE travel agents at forefront of technology in meeting traveller trends and growing the emirates travel industry.pdf"
                                                                                        target="blank">ITQ to undertake comprehensive
                                                                                        mapping of travel agencies across
                                                                                        the country
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 02 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/429-Mauritania Airlines International Selects Travelport Technology in Distribution Partnership.pdf"
                                                                                        target="blank">Travelport Hotelzon announces
                                                                                        entry into Italy and a new appointment
                                                                                        to its management team -</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 07 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/431-Copa Airlines launches sophisticated new apps with MTT.pdf"
                                                                                        target="blank">Travelport Acquires Mobile
                                                                                        Travel Technologies Ltd (MTT) </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 18 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/433-Travelport signs renewal deal with long standing customer Piloot Koen.pdf"
                                                                                        target="blank">SunExpress goes live with
                                                                                        Travelport Rich Content and Branding
                                                                                        - </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 27 Jul
                                                                                        2015
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/432-Travelport and OYO announce new hotel content deal.pdf"
                                                                                        target="blank">Cox & Kings renews its alliance
                                                                                        with InterGlobe Technology Quotient</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#jun_2014" aria-expanded="false"
                                                                        aria-controls="jun_2014">
                                                                        June 2014 </a> </h4>
                                                </div>
                                                <div id="jun_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 03 Jun
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Hainan Airlines and Travelport
                                                                                        sign new merchandising agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 12 Jun
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travelport acquires Hotelzon
                                                                                        to spearhead further hotel growth</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 26 Jun
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank">Travelport teams up with Expedia
                                                                                        Affiliate Network and Majid Al Futtaim
                                                                                        Finance to launch UAE's largest online
                                                                                        travel reward scheme</a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#may_2014" aria-expanded="false"
                                                                        aria-controls="may_2014">
                                                                        May 2014 </a> </h4>
                                                </div>
                                                <div id="may_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 01 May
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf"
                                                                                        target="blank">Travelport renews Oman operator
                                                                                        agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 May
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf"
                                                                                        target="blank">Hangzhou Tourism Commission
                                                                                        and Travelport ink destination marketing
                                                                                        cooperation
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 14 May
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/410-Travelport and Marriott International renew distribution agreement.pdf"
                                                                                        target="blank">HangzHotelAir renews and expands
                                                                                        with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 15 May
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/411-Travelport encourages its Hungarian customers to upsell ancillary services.pdf"
                                                                                        target="blank">Leading online travel agency
                                                                                        Ezfly renews long - term agreement
                                                                                        with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 17 May
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/412-PR InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa0516v4.pdf"
                                                                                        target="blank">Travelport Rooms & More 4
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 20 May
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/413-Travelport extends its suite of virtual payment solutions.pdf"
                                                                                        target="blank">Travelport and CityJet sign
                                                                                        distribution agreement
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#apr_2014" aria-expanded="false"
                                                                        aria-controls="apr_2014">
                                                                        April 2014 </a> </h4>
                                                </div>
                                                <div id="apr_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 04 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport Launches Smartpoint
                                                                                        Apps in India
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 04 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport and AirAsia sign
                                                                                        important distribution agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 10 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">Travelport Celebrates the
                                                                                        One Year Anniversary of its Merchandising
                                                                                        Platform
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 14 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport renews long-term
                                                                                        distributor agreement in Iraq </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 16 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Comair renews its global full
                                                                                        content agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 22 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">Al Tayyar Travel Group and
                                                                                        Travelport enhance partnership with
                                                                                        new agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 24 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Air China signs up for Travelport
                                                                                        Rich Content and Branding technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 28 Apr
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Virgin Atlantic renews full
                                                                                        content agreement with Travelport
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#mar_2014" aria-expanded="false"
                                                                        aria-controls="mar_2014">
                                                                        March 2014 </a> </h4>
                                                </div>
                                                <div id="mar_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 20 Mar
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport Announces New
                                                                                        Managing Director for Asia Pacific
                                                                                        and New Leadership Team for Africa
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#feb_2014" aria-expanded="false"
                                                                        aria-controls="feb_2014">
                                                                        February 2014 </a> </h4>
                                                </div>
                                                <div id="feb_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 05 Feb
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">EasyJet renews its deal with
                                                                                        Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 10 Feb
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Orbitz Worldwide and Travelport
                                                                                        Sign New Long Term GDS Agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 25 Feb
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport announces new global
                                                                                        full content agreement with Middle
                                                                                        East Airlines
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>


                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#jan_2014" aria-expanded="false"
                                                                        aria-controls="jan_2014">
                                                                        January 2014 </a> </h4>
                                                </div>
                                                <div id="jan_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">ITQ awarded "Best GDS of the
                                                                                        year"
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 03 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Pinpoint Travel Group chooses
                                                                                        Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 07 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Short's Travel Corporate Booking
                                                                                        Tool Leverages Travelport's Universal
                                                                                        API "</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 14 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport cements Kuwait
                                                                                        operator partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 21 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> ITQ AWARDED "BEST GDS OF
                                                                                        THE YEAR" FOR THIRD CONSECUTIVE YEAR
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 22 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport renews Pakistan
                                                                                        operator agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 27 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport cements Kuwait
                                                                                        operator partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 27 Jan
                                                                                        2014
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">AIR CANADA AND TRAVELPORT
                                                                                        AGREE TO MULTI-YEAR EXTENSION
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>

                        <div class="tab-pane fade " id="2013">
                                <div class="panel-group" id="accordionSeven" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#dec_2013" aria-expanded="true"
                                                                        aria-controls="dec_2013" class="remove-link">
                                                                        December 2013 </a> </h4>
                                                </div>
                                                <div id="dec_2013" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Thu, 05 Dec 2013 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank"> Travelport
                                                                                        appoints Head of OTA to drive online
                                                                                        business across MEA and India
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 10 Dec
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport and Hertz team
                                                                                        up in Saudi Arabia </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Thu, 12 Dec 2013 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank"> eNett
                                                                                        gains traction by simplifying payments
                                                                                        within agency booking flows
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 16 Dec
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport Signs Multi-Year
                                                                                        Agreement with FlightCar
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Mon, 16 Dec 2013 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank"> 250
                                                                                        industry leaders gather in Dubai
                                                                                        for Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 23 Dec
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">ITQ awarded "Best GDS of the
                                                                                        year" for third consecutive year
                                                                                        at Annual TAAI Travel Awards | APN
                                                                                        News </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#nov_2013" aria-expanded="false"
                                                                        aria-controls="nov_2013">
                                                                        November 2013 </a> </h4>
                                                </div>
                                                <div id="nov_2013" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 19 Nov
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        renews GDS agreement with Via </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 19 Nov
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        renews GDS agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 28 Nov
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Travelport marks 15 years
                                                                                        in Tanzania with new BCD Winglink
                                                                                        Travel deal
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 28 Nov
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> Travelport and GTMC join
                                                                                        forces to recognise innovation </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 29 Nov
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> East Air makes GDS debut
                                                                                        with Travelport
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#octo_2013" aria-expanded="false"
                                                                        aria-controls="octo_2013">
                                                                        October 2013 </a> </h4>
                                                </div>
                                                <div id="octo_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 04 Oct
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport named Best GDS
                                                                                        in the Asia Pacific region for the
                                                                                        fifth consecutive year at the Annual
                                                                                        TTG Travel Awards </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#sep_2013" aria-expanded="false"
                                                                        aria-controls="sep_2013">
                                                                        Septemeber 2014 </a> </h4>
                                                </div>
                                                <div id="sep_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 10 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/452-IndiGo signs its first ever GDS agreement with Travelport.pdf"
                                                                                        target="blank">Travelport Galileo Celebrates
                                                                                        15 years in India.
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 11 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">Jet Airways and Travelport
                                                                                        renew content agreement </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 12 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">AXESS and Travelport deliver
                                                                                        new Japanese GDS
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 17 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">Transavia.com Signs Up For
                                                                                        The Travelport Merchandising Platform
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 17 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/456-Travelport and Mastercard maximize customer insights to drive new partnership strategies for airlines.pdf"
                                                                                        target="blank">Satena Designates Travelport
                                                                                        Strategic Technology Partner </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 18 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">Visas, Vaccinations and Weather
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 19 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">Booking Vacations Gets Easier
                                                                                        for Canadian Travel Agents </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 26 Sep
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">Travelport and Maritime Travel
                                                                                        Renew Partnership
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#aug_2013" aria-expanded="false"
                                                                        aria-controls="aug_2013">
                                                                        August 2013 </a> </h4>
                                                </div>
                                                <div id="aug_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 01 Aug
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">Airline Yakutia enters into
                                                                                        its first GDS agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 05 Aug
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank">Air Mediterranee successfully
                                                                                        implemented in Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Aug
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">Travelport cements partnership
                                                                                        with Uniglobe Lets Go Travel in Kenya
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Aug
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank">Protea Hospitality Group Upgrades
                                                                                        to Travelports latest hotel booking
                                                                                        technology
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 20 Aug
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank"> Travelport seals global full
                                                                                        content agreement with Finnair </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#jul_2013" aria-expanded="false"
                                                                        aria-controls="jul_2013">
                                                                        July 2013 </a> </h4>
                                                </div>
                                                <div id="jul_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 09 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/430-UAE travel agents at forefront of technology in meeting traveller trends and growing the emirates travel industry.pdf"
                                                                                        target="blank">Top Aviation to Deploy iPort
                                                                                        Departure Control System Across Egypt
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 10 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/429-Mauritania Airlines International Selects Travelport Technology in Distribution Partnership.pdf"
                                                                                        target="blank">Travelport Unveils East Africa
                                                                                        Investment Plans
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/431-Copa Airlines launches sophisticated new apps with MTT.pdf"
                                                                                        target="blank">Travelport appoints new Head
                                                                                        of Online Travel Agencies (OTA) for
                                                                                        Western Europe Jul 11, </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/433-Travelport signs renewal deal with long standing customer Piloot Koen.pdf"
                                                                                        target="blank">Travelport Unveils East Africa
                                                                                        Investment Plans
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 15 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/432-Travelport and OYO announce new hotel content deal.pdf"
                                                                                        target="blank">Travelport Mobile Agen Now
                                                                                        Launched in US and Canada </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 16 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/430-UAE travel agents at forefront of technology in meeting traveller trends and growing the emirates travel industry.pdf"
                                                                                        target="blank">Maxims Travel and Travelport
                                                                                        renew partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 17 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/429-Mauritania Airlines International Selects Travelport Technology in Distribution Partnership.pdf"
                                                                                        target="blank">Travelport inks global full
                                                                                        content agreement with EL AL </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 17 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/431-Copa Airlines launches sophisticated new apps with MTT.pdf"
                                                                                        target="blank">AZUL GROWS PRESENCE WITH TRAVELPORT
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 19 Jul
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/433-Travelport signs renewal deal with long standing customer Piloot Koen.pdf"
                                                                                        target="blank">Travelport invests in new
                                                                                        office in Hong Kong
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#jun_2013" aria-expanded="false"
                                                                        aria-controls="jun_2013">
                                                                        June 2013 </a> </h4>
                                                </div>
                                                <div id="jun_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 01 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Travelport Rooms & More 9
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 01 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travelport Rooms & More 7
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 04 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank"> Travelport unveils investment
                                                                                        plan to boost Egypts travel industry
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 10 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">WestJet and Travelport sign
                                                                                        long-term agreement June 10, 2013.pdf
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 10 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travelport Awards celebrate
                                                                                        industry talent in Afghanistan </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 12 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank"> Travelport launches new product
                                                                                        to benefit MEA travel agents June
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 12 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Travelport Mobile Agent now
                                                                                        launched in Asia
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 13 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travelport hotel content successfully
                                                                                        integrated with TravelSky </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 16 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank"> Travelport Rooms & More 5
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 17 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Travelport Rooms & More 6
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 19 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travel industry losing out
                                                                                        by failing to understand the true
                                                                                        costs of payments
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 20 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank">Trending of Emirates 'Inspire
                                                                                        Me' Application Engineered by Travelport
                                                                                        Confirms Leisure Traveler Desire
                                                                                        for Inspirational Shopping Tools
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 16 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank"> Travelport Rooms & More 5
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 21 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank"> Transaero Airlines signs
                                                                                        full content agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 25 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travelport renews long-term
                                                                                        agreement with Galileo Vietnam </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 27 Jun
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank"> Travelport cements partnership
                                                                                        with Uniglobe Skylink Travel & Tours
                                                                                        in Tanzania
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#may_2013" aria-expanded="false"
                                                                        aria-controls="may_2013">
                                                                        May 2013 </a> </h4>
                                                </div>
                                                <div id="may_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf"
                                                                                        target="blank">Webjet and Travelport extend
                                                                                        partnership with Zuji acquisition
                                                                                        New agreement will see Zuji transitioning
                                                                                        to Travelport GDS
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf"
                                                                                        target="blank">Douglas M. Steenland appointed
                                                                                        Chairman of Travelport Limited May
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 02 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/410-Travelport and Marriott International renew distribution agreement.pdf"
                                                                                        target="blank"> Porter Airlines and Travelport
                                                                                        Extend Full Content Agreement May
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 05 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/411-Travelport encourages its Hungarian customers to upsell ancillary services.pdf"
                                                                                        target="blank">Enhanced version of Travelport
                                                                                        Rooms and More launched globally
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 05 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/412-PR InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa0516v4.pdf"
                                                                                        target="blank">Al-Futtaim Travel reaffirms
                                                                                        commitment to Travelport with online
                                                                                        expansion
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 09 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/413-Travelport extends its suite of virtual payment solutions.pdf"
                                                                                        target="blank">Travel industry exposed by
                                                                                        inefficient payment systems May </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 13 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf"
                                                                                        target="blank"> Air Tickets to implement
                                                                                        Travelport Net Fare Manager May 13,
                                                                                        2013.pdf
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 14 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf"
                                                                                        target="blank">Travelport and Rail Europe
                                                                                        unveil enriched ticketing solution
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 18 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/410-Travelport and Marriott International renew distribution agreement.pdf"
                                                                                        target="blank"> Travelport Rooms & More 8
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 18 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/411-Travelport encourages its Hungarian customers to upsell ancillary services.pdf"
                                                                                        target="blank">Travelport Rooms & More 3
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sat, 18 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/412-PR InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa0516v4.pdf"
                                                                                        target="blank">InterGlobe Tech launches hotel
                                                                                        booking engine for travel agents_AboutIndia.com_17
                                                                                        May 2013.pdf </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Sun, 19 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/413-Travelport extends its suite of virtual payment solutions.pdf"
                                                                                        target="blank">Travelport Rooms & More 1
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 20 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf"
                                                                                        target="blank">Travelport Rooms & More 2
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 22 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf"
                                                                                        target="blank"> Jet2.com and Norwegian join
                                                                                        EasyJet in the recently launched
                                                                                        Travelport Merchandising Platform
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 24 May
                                                                                        2013:
                                                                                </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/410-Travelport and Marriott International renew distribution agreement.pdf"
                                                                                        target="blank"> Significant
                                                                                        win for Travelport in Hungary
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 27 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/411-Travelport encourages its Hungarian customers to upsell ancillary services.pdf"
                                                                                        target="blank">Shams Abu Dhabi Travels reinforces
                                                                                        commitment to Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 31 May
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/412-PR InterGlobe Technology Quotient organizes Annual Sales Conference 2016 in Lavasa0516v4.pdf"
                                                                                        target="blank">Air France, KLM and Travelport
                                                                                        sign global full content agreement
                                                                                        May
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#apr_2013" aria-expanded="false"
                                                                        aria-controls="apr_2013">
                                                                        April 2013 </a> </h4>
                                                </div>
                                                <div id="apr_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 02 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">On the Beach signs with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 05 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport establishes direct
                                                                                        operation in Kenya
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 10 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">Travelport Mobile Agent launches
                                                                                        in the Pacific
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 10 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport brings next-gen
                                                                                        travel booking to companies in Poland
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">Travelport Unveils Travelport
                                                                                        Merchandising Platform </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 15 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank">Quickbeds.com joins Travelports
                                                                                        Rooms and MoreApril
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 16 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport Limited Completes
                                                                                        Comprehensive Refinancing Plan
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 18 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport chosen as The
                                                                                        Travel Network Groups preferred GDS
                                                                                        for twelfth year running April </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 23 Apr
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport set to Inspire
                                                                                        at Arabian Travel Market 2013 </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#mar_2013" aria-expanded="false"
                                                                        aria-controls="mar_2013">
                                                                        March 2013 </a> </h4>
                                                </div>
                                                <div id="mar_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport Limited Launches
                                                                                        Comprehensive Refinancing
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport Driving Forward
                                                                                        Momentum March </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport extends its reach
                                                                                        to Tunisia
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Brooker Travel Group re-signs
                                                                                        with Travelport, extending long-standing
                                                                                        partnership March </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 15 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport and Alitalia complete
                                                                                        merchandising deployment March </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 15 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Gray Dawes keeps Travelport
                                                                                        as technology provider March </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 19 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport announces content
                                                                                        agreement with China Eastern Airlines
                                                                                        and Shanghai Airlines March </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 20 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport and Aegean Airlines
                                                                                        sign merchandising agreement,March
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 21 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Travelport Smartpoint App
                                                                                        Launches for Worldp Go Desktop
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 21 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> TTravelport cements operator
                                                                                        partnership in Yemen
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 25 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Saudi Arabias travel agencies
                                                                                        pledge commitment to Travelport </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 26 Mar
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> Holiday Genie and Travelport
                                                                                        seal long-standing partnership </a></li>

                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#feb_2013" aria-expanded="false"
                                                                        aria-controls="feb_2013">
                                                                        February 2013 </a> </h4>
                                                </div>
                                                <div id="feb_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 01 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport starts 2013 with
                                                                                        a win in Poland
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport launches new fare
                                                                                        management tool
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport and Ethiopian Airlines
                                                                                        renew partnership agreement </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 05 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport Announces New Managing
                                                                                        Director for China </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 08 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Developers in Russia inspired
                                                                                        by Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 11 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport and Rickshaw Travels
                                                                                        announce continued partnership </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 13 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Aero Club selects Travelport
                                                                                        in Russia
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 18 Feb
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport renews contract
                                                                                        with UAE travel agency SNTTA </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>


                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#jan_2013" aria-expanded="false"
                                                                        aria-controls="jan_2013">
                                                                        January 2013 </a> </h4>
                                                </div>
                                                <div id="jan_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 10 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport Announces New
                                                                                        U.S. Commercial Leader
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 17 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport Rooms and More
                                                                                        Adds Huntington Travel's Hotel Consolidator
                                                                                        Content to Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 17 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">ATC Travel Management Designates
                                                                                        Travelport as Primary Technology
                                                                                        Partner
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 22 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport launches business
                                                                                        intelligence tools for MEA agents
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 24 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport announces full
                                                                                        content agreement with Virgin Australia
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 25 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Content From Carlson Rezidor
                                                                                        Hotel Group Travels a More Cost-Efficient
                                                                                        Path to Travelport's GDS </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 28 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport signs new agreement
                                                                                        with Scoot </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Wed, 30 Jan 2013 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport
                                                                                        enhances its partnership with Yemenia
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 30 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport and Al Tayyar Travel
                                                                                        Group join forces to drive developments
                                                                                        in Saudi travel industry
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 30 Jan
                                                                                        2013
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport Enhances Mid Back
                                                                                        Office Solution
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Wed, 30 Jan 2013 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">Travelport
                                                                                        unveils latest innovations from Travelport
                                                                                        Developer Network at Travel Technology
                                                                                        Europe (TTE) exhibition
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="tab-pane fade " id="2012">
                                <div class="panel-group" id="accordionEight" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#dec_2012" aria-expanded="true"
                                                                        aria-controls="dec_2012" class="remove-link">
                                                                        December 2012 </a> </h4>
                                                </div>
                                                <div id="dec_2012" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Mon, 03 Dec 2012 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank"> PIA
                                                                                        and Travelport Sign Full Content
                                                                                        Agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 03 Dec
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport Continues Ongoing
                                                                                        Program of Investment and Enhancement
                                                                                        of Technology with New IBM Agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Thu, 06 Dec 2012 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank">Travelport
                                                                                        Servicing the Digital Leisure Traveller
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 11 Dec
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport launches new merchandising
                                                                                        capability
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Fri, 14 Dec 2012 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/468-Vueling signs up to Travelport Rich Content and Branding.pdf"
                                                                                        target="blank"> Travelport
                                                                                        and Sharaf Aviation Services Academy
                                                                                        announce a pioneering new partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 17 Dec
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport appoints new distributor
                                                                                        in Morocco
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 17 Dec
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/469-Travelport and ARC partner to simplify and increase ancillary sales for airlines.pdf"
                                                                                        target="blank">Travelport Agent of the Future
                                                                                        Announced
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#nov_2012" aria-expanded="false"
                                                                        aria-controls="nov_2012">
                                                                        November 2012 </a> </h4>
                                                </div>
                                                <div id="nov_2012" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 01 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">BidTravel Group and Travelport
                                                                                        reach new strategic partnership agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 02 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> Travelport Limited Third
                                                                                        Quarter 2012 Results
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 06 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Travelport and Alitalia agree
                                                                                        merchandising distributionlaunches
                                                                                        mobile app for MEA agents
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 07 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> Travelport launches mobile
                                                                                        app for MEA agents
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 12 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> First UAE leads the Middle
                                                                                        East online travel raceof Travelport
                                                                                        Developer Network
                                                                                </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 12 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">First Anniversary of Travelport
                                                                                        Developer Network
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> Copa Hays Travel switches
                                                                                        to TravelportChooses Travelport Rapid
                                                                                        Reprice Technology to Simplify and
                                                                                        Accelerate Ticket </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Copa Airlines Chooses Travelport
                                                                                        Rapid Reprice Technology to Simplify
                                                                                        and Accelerate Ticket </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 13 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> Copa Airlines Chooses Travelport
                                                                                        Rapid Reprice Technology to Simplify
                                                                                        and Accelerate Ticket Exchanges
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 19 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Grupa Travel chooses Travelport
                                                                                </a></li>

                                                                        <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                        <p class="list-details-date">Wed, 21 Nov 2012
                                                                        </p>
                                                                        <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                target="blank"> GOL
                                                                                Ups Performance Ability and International
                                                                                Expansion via Travelport's Global Inter-Airline
                                                                        </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 27 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Alaska Airlines Signs Full
                                                                                        Content Agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 28 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank"> Top Russian consolidator
                                                                                        signs new deal with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 29 Nov
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/464-Bulgaria Air renews its full content agreement with Travelport.pdf"
                                                                                        target="blank">Travelport's e-volve Summit
                                                                                        explores the future of MEA travel
                                                                                        industry
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#octo_2012" aria-expanded="false"
                                                                        aria-controls="octo_2012">
                                                                        October 2012 </a> </h4>
                                                </div>
                                                <div id="octo_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 09 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport adds to UK & Ireland
                                                                                        team
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 11 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Statesman migrates latest
                                                                                        travel management acquisition to
                                                                                        Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 12 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank"> Travelport Appoints Industry
                                                                                        Leader Damian Hickey as Vice President
                                                                                        of Distribution Sales and </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 15 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport Appoints Industry
                                                                                        Leader Damian Hickey as Vice President
                                                                                        of Distribution Sales and </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 16 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Aeromexico Signs Full Content
                                                                                        Agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 17 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport Announces Release
                                                                                        of GalileoTerminal in Asia Pacific
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 17 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Executive Travel Designates
                                                                                        Travelport Primary Technology Partner
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">
                                                                                        Thu, 18 Oct 2012 </p>
                                                                                <a class="list-details remove-link li-a-hover" href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">National
                                                                                        Travel Systems Designates Travelport
                                                                                        Primary Technology Partner
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 23 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport Comments on IATA's
                                                                                        NDC
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 23 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank"> Online travel to experience
                                                                                        solid growth in the Middle East according
                                                                                        to new studyTAAI Travel Awards honours
                                                                                        Travelport with the Best GDS </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 25 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank"> Annual TAAI Travel Awards
                                                                                        honours Travelport with the Best
                                                                                        GDS </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 29 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport Appoints New Vice
                                                                                        President and Regional Managing Director
                                                                                        for Asia-Pacific
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 30 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Custom Travelport signs long
                                                                                        term agreement with eTravelSolutions
                                                                                        Designates Travelport as Primary
                                                                                        Technology Partner
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 31 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank"> Custom Travel Solutions Designates
                                                                                        Travelport as Primary Technology
                                                                                        Partner
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 31 Oct
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/457-Travelport comes out on top at two prestigious industry awards in the AsiaPacific region.pdf"
                                                                                        target="blank">Travelport strikes deal with
                                                                                        Virgin Atlantic FlightstoreGroup
                                                                                        and Travelport reach new strategic
                                                                                        partnership agreement
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#sep_2012" aria-expanded="false"
                                                                        aria-controls="sep_2012">
                                                                        Septemeber 2012 </a> </h4>
                                                </div>
                                                <div id="sep_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 06 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/452-IndiGo signs its first ever GDS agreement with Travelport.pdf"
                                                                                        target="blank">Travelport Joins Global Travel
                                                                                        & Tourism Partnership Supporting
                                                                                        Global Education In Travel & </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 06 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">transavia.com makes GDS debut
                                                                                        with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 07 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">Travelport industry first
                                                                                        97 percent compliance in new streamlining
                                                                                        process for hotel negotiated
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 07 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">Travelport completes KLM Economy
                                                                                        Comfort rollout for Galileo agents
                                                                                        in Benelux
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 11 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/456-Travelport and Mastercard maximize customer insights to drive new partnership strategies for airlines.pdf"
                                                                                        target="blank">Travelport and RAK Airways
                                                                                        sign first GDS participation agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 12 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/451-Travelport launches Mobile Payment Solution offering MPesa and Airtel Money transfer services in Kenya.pdf"
                                                                                        target="blank">TUIfly.com Poised to Expand
                                                                                        Flight Offering, Service and Partnerships
                                                                                        through Travelport Interline e </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 18 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">Pullman and Sofitel join Travelport's
                                                                                        Lowest Public Rate' Hotel Programme
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 24 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">Travelport and Gulf Air renew
                                                                                        agreement to provide full content
                                                                                        for travel agents
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 26 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/454-Lots of Hotels content now available in Travelport Rooms and More.pdf"
                                                                                        target="blank">Travelport brings next-gen
                                                                                        travel booking to Corporations in
                                                                                        Russia </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 26 Sep
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/455-South African Airways releases ancillary services with Travelport.pdf"
                                                                                        target="blank">nasair enters into new GDS
                                                                                        partnership with TravelportSep </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#aug_2012" aria-expanded="false"
                                                                        aria-controls="aug_2012">
                                                                        August 2012 </a> </h4>
                                                </div>
                                                <div id="aug_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 01 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">Travelport signs new agreement
                                                                                        with China Southern Airlines </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 03 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank">Avior Airlines Signs Full
                                                                                        Content Agreement with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">Travelport Triples its Hotel
                                                                                        Content
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank"> Travelport signs full content
                                                                                        agreement with Air China </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank"> Travelport Second Quarter
                                                                                        2012 Results
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 09 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/489-ITQ appoints Sandeep Dwivedi as its Chief Operating Officer.pdf"
                                                                                        target="blank">LOT signs full content agreement
                                                                                        with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 24 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank"> Travelport renews partnership
                                                                                        with Digital Travel </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 28 Aug
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/442-Travelport announces renewal of longterm Emirates partnership and rollout of merchandising technology.pdf"
                                                                                        target="blank"> Airport pickups are a breeze
                                                                                        with Travelport Drive MeJoins Global
                                                                                        Travel & Tourism Partnership Supporting
                                                                                        Global Education In Travel & </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#jul_2012" aria-expanded="false"
                                                                        aria-controls="jul_2012">
                                                                        July 2012 </a> </h4>
                                                </div>
                                                <div id="jul_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 18 Jul
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/430-UAE travel agents at forefront of technology in meeting traveller trends and growing the emirates travel industry.pdf"
                                                                                        target="blank">Travelport and Air Canada
                                                                                        Unveil Enhanced Version of Travelport
                                                                                        Agencia
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 23 Jul
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/429-Mauritania Airlines International Selects Travelport Technology in Distribution Partnership.pdf"
                                                                                        target="blank"> TIX.nl signs long term agreement
                                                                                        with Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 26 Jul
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/431-Copa Airlines launches sophisticated new apps with MTT.pdf"
                                                                                        target="blank">Bulgaria Air Signs Full Content
                                                                                        Agreement with Travelport </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#jun_2012" aria-expanded="false"
                                                                        aria-controls="jun_2012">
                                                                        June 2012 </a> </h4>
                                                </div>
                                                <div id="jun_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 04 Jun
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Delta to Sell Economy Comfort
                                                                                        through Travelport Systems </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 Jun
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">Travelport and Egypt Air reach
                                                                                        new content agreement
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 14 Jun
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/421-Travelport Group scoops up four awards for innovation.pdf"
                                                                                        target="blank"> Travelport signs full content
                                                                                        agreement with kulula.com </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 18 Jun
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/419-Air Namibia makes the most of Travelports industry leading airline merchandising solution.pdf"
                                                                                        target="blank">Travelport and Nakhal and
                                                                                        Cie reaffirm partnership
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 26 Jun
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/420-Travelport celebrates 20 years of growth and innovation in the Romanian travel industry.pdf"
                                                                                        target="blank">FCm becomes first Irish travel
                                                                                        agent to sign up for Travelport Universal
                                                                                        Desktop
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#may_2012" aria-expanded="false"
                                                                        aria-controls="may_2012">
                                                                        May 2012 </a> </h4>
                                                </div>
                                                <div id="may_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 03 May
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/408-Air France KLM signs up to Travelports industry leading airline merchandising technology.pdf"
                                                                                        target="blank">Kuoni renews and extends multi-national
                                                                                        agreement with Travelport </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 08 May
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/409-UTair signs up to Travelports industryleading airline merchandising technology.pdf"
                                                                                        target="blank">Thetrainline.com connects
                                                                                        with Travelport Universal API
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 22 May
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/410-Travelport and Marriott International renew distribution agreement.pdf"
                                                                                        target="blank"> Avianca and TACA Airlines
                                                                                        Expand Partnership with Travelport
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#apr_2012" aria-expanded="false"
                                                                        aria-controls="apr_2012">
                                                                        April 2012 </a> </h4>
                                                </div>
                                                <div id="apr_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 03 Apr
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank">Travelport achieves significant
                                                                                        merchandising milestone with KLM
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 18 Apr
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/479-Travelport and Nesma Airlines announce new long term agreement.pdf"
                                                                                        target="blank">easyJet grows presence with
                                                                                        Travelport
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 18 Apr
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/480-Business travel sector to grow 3.7percent annually to 2027 as new tech fuels takeup.pdf"
                                                                                        target="blank"> Dnata Signs Up for Travelports
                                                                                        IndustryLeading Universal APIComfort
                                                                                        through Travelport Systems
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 23 Apr
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/477-Travelport and Onur Air announce long term agreement.pdf"
                                                                                        target="blank"> AXESS International Network
                                                                                        to Adopt Travelport Technology </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#feb_2012" aria-expanded="false"
                                                                        aria-controls="feb_2012">
                                                                        February 2012 </a> </h4>
                                                </div>
                                                <div id="feb_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 03 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport Rooms and More
                                                                                        signs up Bookingcom
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 06 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank"> Travelport Travelport launches
                                                                                        free mobile travel itinerary app
                                                                                        for iPhone and Androidits partnership
                                                                                        with Yemenia </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 09 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank"> Bangkok Airways signs full
                                                                                        content deal with Travelport
                                                                                </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 16 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Emirates Airline Enhances
                                                                                        Customer Online Shopping Experience
                                                                                        with Launch of Travelport </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 23 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport Signs New Full
                                                                                        Content Agreement with Lufthansa
                                                                                </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Thu, 23 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">Travelport and SWISS Sign
                                                                                        New Full Content Agreement
                                                                                </a></li>

                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 29 Feb
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank">TravelSky and Travelport ink
                                                                                        hotel cooperation </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#jan_2012" aria-expanded="false"
                                                                        aria-controls="jan_2012">
                                                                        January 2012 </a> </h4>
                                                </div>
                                                <div id="jan_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 18 Jan
                                                                                        2012
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank"> Travelport and TAP Portugal
                                                                                        sign full content deal </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="tab-pane fade " id="2011">
                                <div class="panel-group" id="accordionNine" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionNine" href="#octo_2011" aria-expanded="true"
                                                                        aria-controls="octo_2011" class="remove-link"> October 2011
                                                                </a> </h4>
                                                </div>
                                                <div id="octo_2011" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 07 Oct
                                                                                        2011
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/520-Delivering-NDC-FEB2019.pdf"
                                                                                        target="blank">Travelport voted Asia-Pacific's
                                                                                        Best GDS </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionNine" href="#aug_2011" aria-expanded="false"
                                                                        aria-controls="aug_2011">
                                                                        August 2011 </a> </h4>
                                                </div>
                                                <div id="aug_2011" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 08 Aug
                                                                                        2011
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        and Taj Hotels announce exciting
                                                                                        joint promotion.
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>

                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionNine" href="#feb_2011" aria-expanded="false"
                                                                        aria-controls="feb_2011">
                                                                        February 2011 </a> </h4>
                                                </div>
                                                <div id="feb_2011" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 21 Feb
                                                                                        2011
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf"
                                                                                        target="blank">Travelport and Air Astana
                                                                                        sign first ever content agreement.
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>

                                        </div>
                                </div>
                        </div>

                        <div class="tab-pane fade " id="2010">
                                <div class="panel-group" id="accordionTen" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#octo_2010" aria-expanded="true"
                                                                        aria-controls="octo_2010" class="remove-link"> October 2010
                                                                </a> </h4>
                                                </div>
                                                <div id="octo_2010" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 12 Oct
                                                                                        2010
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/520-Delivering-NDC-FEB2019.pdf"
                                                                                        target="blank">Travelport named Best GDS
                                                                                        in the Asia Pacific in 2010 TTG Asia
                                                                                        Travel Awards.
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#aug_2010" aria-expanded="false"
                                                                        aria-controls="aug_2010">
                                                                        August 2010 </a> </h4>
                                                </div>
                                                <div id="aug_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 02 Aug
                                                                                        2010
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf"
                                                                                        target="blank">Kuoni signs multi-national
                                                                                        agreement with Travelport.
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>

                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#jul_2010" aria-expanded="false"
                                                                        aria-controls="jul_2010">
                                                                        July 2010 </a> </h4>
                                                </div>
                                                <div id="jul_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Mon, 26 Jul
                                                                                        2010
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf"
                                                                                        target="blank">Travelport Galileo GDS Announces
                                                                                        Agreement with Hermes i-Ticket to
                                                                                        Provide Indian Rail Content. </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#apr_2010" aria-expanded="false"
                                                                        aria-controls="apr_2010">
                                                                        April 2010 </a> </h4>
                                                </div>
                                                <div id="apr_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Wed, 14 Apr
                                                                                        2010
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf"
                                                                                        target="blank"> Onestopshop selects Travelport
                                                                                        Galileo as its preferred GDS partner.
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#apr_2010" aria-expanded="false"
                                                                        aria-controls="apr_2010">
                                                                        February 2010 </a> </h4>
                                                </div>
                                                <div id="apr_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 09 Feb
                                                                                        2010 s </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/518-PR_InterGlobe Technology Quotient participates in SATTE 2019_01-19.pdf"
                                                                                        target="blank"> CWT selects travelport as
                                                                                        its new GDS partner in India. </a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Tue, 09 Feb
                                                                                        2010
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/473-Long-time partners Travelport and Air Canada announce new full content agreement.pdf"
                                                                                        target="blank"> Cleartrip selects Travelport
                                                                                        Galileo as its preferred technology
                                                                                        partner.
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>
</div>
</div>
</div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>