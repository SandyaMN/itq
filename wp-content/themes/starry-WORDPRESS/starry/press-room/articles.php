<?php
/**
 * Template Name: articles
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <!-- <link href="../css/common.css" rel="stylesheet">
    <link href="../css/aboutus.css" rel="stylesheet"> -->
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/press-room.css';?>">

   
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="main-heading">Press Room</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class=" press-releases"><a class="release" href="<?php echo get_home_url(); ?>/press-release/">Press Releases</a></li>
                        <li class="advertisements"><a class="release" href="<?php echo get_home_url(); ?>/advertisements/">Advertisements</a></li>
                        <li class="active articles"><a class="release" href="<?php echo get_home_url(); ?>/articles/">Articles</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="press-releases" class="tab-pane fade in active">
<div class="row m-t-10-px">
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="">Press Room</a>
                / <a class="current-page">Articles
                </a></p>

</div>
<div class="row">
        <h4 class=" row press-room-header">Articles</h4>
</div>
<div class="row">
        <div class="col-md-1">
                <ul class="nav flex-column years-tabs">
                        <li class="active year-list"><a data-toggle="tab" class="release " href="#2019">2019</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2018">2018</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2017">2017</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2016">2016</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2015">2015</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2014">2014</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2013">2013</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2012">2012</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2011">2011</a></li>
                        <li class="year-list"><a data-toggle="tab" class="release " href="#2010">2010</a></li>
                </ul>
        </div>

        <div class="col-md-11">
                <div class="tab-content">
                        <div id="2019" class="tab-pane fade in active ">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordion" href="#collapseOne" aria-expanded="true"
                                                                        aria-controls="collapseOne" class="remove-link">
                                                                        March 2019 </a> </h4>
                                                </div>
                                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                
                                                                                <a class="list-details remove-link li-a-hover" href=""
                                                                                        target="blank">TravelBiz Monitor: Travelport, United Airlines extend contract to include NDC content</a>

                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                
                                                                                <a class="list-details remove-link li-a-hover" href=""
                                                                                        target="blank">Travel Trends Today: Travelport Launches Campaign To Support Passengers With Intellectual Disabilities
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TTravel Trends Today: India Set To Become Top Market For Travelport
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravelBiz Monitor: Travelport launches global campaign for more support from airline passengers with intellectual disabilities
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Heights: Re-rooted with BCD travel, Travelport extends contract with united airlines to introduce NDC initiatives
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Whereabouts: Travelport reveals where are people flying in from to celebrate Holi in India
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Trends Today: Where Are People Flying In From To Celebrate Holi In India?
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TTJ: Where are people flying in from to celebrate Holi in India?
                                                                                </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordion" href="#collapseTwo" aria-expanded="true"
                                                                        aria-controls="collapseTwo" class="collapsed remove-link">
                                                                        February 2019 </a> </h4>
                                                </div>
                                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                
                                                                                <a class="list-details remove-link li-a-hover" href=""
                                                                                        target="blank">Travel Weekly: Travelport takeover vote scheduled for March</a>

                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                
                                                                                <a class="list-details remove-link li-a-hover" href=""
                                                                                        target="blank">Whereabouts: Travelport completes on-boarding of first wave of NDC customers
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravelScapes: Anil Parashar shares his views on Corporate Travel Trends in 2019 and beyond
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravelBiz Monitor: Our investments in India have started delivering results: Chris Ramm, VP- APAC, Travelport
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Heights: Travelport completed onboarding of first wave of new distribution capability (NDC) customers
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravelScapes: ITQ awarded the "Most Preferred GDS" title for Travelport at Versatile Excellence Travel Awards (VETA), 2019
                                                                                </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Whereabouts: Travelport Digital and BCD Travel extend partnership
                                                                                </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordion" href="#collapseThree" aria-expanded="false"
                                                                        aria-controls="collapseThree">
                                                                        January 2019 </a> </h4>
                                                </div>
                                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravTalk: Travelport awarded Best GDS title at India Travel Awards 2018 for North, East &amp; West Regions</a></li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">
                                                                                            Travel Trends Today: ITQ wins the 'Golden Peacock Award' 2018 for Risk Management
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">
                                                                                            Travel Trends Today: Sandeep Dwivedi contributes to the cover story 'Adapting Digital Transformation'
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">
                                                                                            Anil Parashar expresses his views to Financial Express on How companies can enable hassle-free business travel for employees
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">
                                                                                            Hospitality Net: Travelport Completes Onboarding Of First Wave Of New Distribution Capability (NDC) Customers
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <div class="tab-pane fade " id="2018">
                                <div class="panel-group" id="accordionOne" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#dec_2018" aria-expanded="true"
                                                                        aria-controls="dec_2018" class="remove-link">
                                                                        December 2018 </a> </h4>
                                                </div>
                                                <div id="dec_2018" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Safari India: India Tops Travelport's Global Ranking of Digital Travelers
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Sandeep Dwivedi speaking to TravelScapes Correspondent about the aviation trends expected to take over the Indian skies in not-so-distant future
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel and Hospitality: Sandeep Dwivedi in an exclusive interview speaks about how 2018 has been a game changer for ITQ and the travel industry
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Heights: Sandeep Dwivedi speaks on air connectivity and disruptive technology driving tourism in an exclusive interview
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">InterGlobe Technology Quotient wins Stars of the Industry Awards, 2018 published on ITQ LinkedIn page
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Heights: Siris and Evergreen acquires Travelport for $4.4 billion
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Whereabout: India tops Travelport's global ranking of digital travellers
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#nov_2018" aria-expanded="false"
                                                                        aria-controls="nov_2018">
                                                                        November 2018 </a> </h4>
                                                </div>
                                                <div id="nov_2018" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingFour">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Heights: Sandeep Dwivedi shares his views on technology in travel and tourism
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TTJ: Travelport and Jet Airways sign a new long-term supplier agreement commencing April 2019
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Whereabouts: Jet Airways to deploy Travelport Rich Content and Branding
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravelScapes: Travelport wins Air India contract
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#octo_2018" aria-expanded="false"
                                                                        aria-controls="octo_2018">
                                                                        October 2018 </a> </h4>
                                                </div>
                                                <div id="octo_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravelBiz Monitor: Travelport bags domestic distribution rights of Air India
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Indian Aviation - a game changer in itself
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport to offer Rich Content &amp; Branding to Air India, becomes sole distribution supplier
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Sandeep Dwivedi sharing his opinion with T3 correspondent on: Big Data Transforming Travel
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#sep_2018" aria-expanded="false"
                                                                        aria-controls="sep_2018">
                                                                        September 2018 </a> </h4>
                                                </div>
                                                <div id="sep_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">TravTalk: Anil Parashar shares his views on global trends in travel &amp; tourism
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Sandeep Dwivedi speaks to Whereabouts on Automation driving easy operations, transcending traditional barriers
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#aug_2018" aria-expanded="false"
                                                                        aria-controls="aug_2018">
                                                                        Agust 2018 </a> </h4>
                                                </div>
                                                <div id="aug_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Anil Parashar contributes to Travelscapes Cover Story: Joining the missing pieces in India's Aviation Puzzle
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Sandeep Dwivedi speaks at ICONIC 2018 on  How Technology has challenged existing biz models leading to disruption
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#july_2018" aria-expanded="false"
                                                                        aria-controls="july_2018">
                                                                        July 2018 </a> </h4>
                                                </div>
                                                <div id="july_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport Smartpoint 8.1- A smarter way to the top: Travel and Hospitality
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport Smartpoint 8.1: A Smarter Way to The Top - Whereabouts
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#june_2018" aria-expanded="false"
                                                                        aria-controls="june_2018">
                                                                        June 2018 </a> </h4>
                                                </div>
                                                <div id="june_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Biz Monitor: India's millennial business travellers are changing corporate travel, says Travelport
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Technology Transcending the Barriers of Front Office, Mid Office and Back Office: Namastaai
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">India's millennial business travelers are changing corporate travel-Travel and Hospitality report
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#may_2018" aria-expanded="false"
                                                                        aria-controls="may_2018">
                                                                        May 2018 </a> </h4>
                                                </div>
                                                <div id="may_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Sandeep Dwivedi speaks to Travel Trends Today on Corporate Travel Changing Dynamics
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travel Trends Today: India's Millennial Business Travelers Are Changing Corporate Travel, Says Travelport
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <!--<div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#apr_2018" aria-expanded="false"
                                                                        aria-controls="feb_2018">
                                                                        April 2018 </a> </h4>
                                                </div>
                                                <div id="apr_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>-->
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#march_2018" aria-expanded="false"
                                                                        aria-controls="feb_2018">
                                                                        March 2018 </a> </h4>
                                                </div>
                                                <div id="march_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Check you quickness with booking commands- Travelport Making life simpler: Namastaai
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Anil Parashar in conversation with TravelBizMonitor on ITQ's future-centric approach on data security solutions &amp; digital travel technology
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport announces new products that enable airlines and agents in NDC era- Whereabouts
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#feb_2018" aria-expanded="false"
                                                                        aria-controls="feb_2018">
                                                                        February 2018 </a> </h4>
                                                </div>
                                                <div id="feb_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Sandeep Dwivedi in conversation with Travtalk- Travel trade looks forward to Budget
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Union Budget 2018: A hit or a miss? Anil Parashar expresses his views to Wonderlust
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport to offer products for airlines &amp; agents integrating with NDC content- Travel Biz Monitor
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport will now bring NDC to the existing customer base- Travel Biz Monitor in coversation with Anil Parashar
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Travelport's New Products Enable Agents In The NDC Era- Travel Trends Today
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingEight">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionOne" href="#jan_2018" aria-expanded="false"
                                                                        aria-controls="jan_2018">
                                                                        January 2018 </a> </h4>
                                                </div>
                                                <div id="jan_2018" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Indians rank first by combining the main indicators of digital use-Travel Trade Journal
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">New certification tool for agents: Sandeep Dwivedi in conversation with Travtalk
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Technology Driving Hotel's performance-Travel Trends Today
                                                                                        </a>
                                                                        </li>
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <a class="list-details remove-link li-a-hover"
                                                                                        href=""
                                                                                        target="blank">Looking Back Looking Ahead: Anil Parashar in conversation with Travelscapes
                                                                                        </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>

                        <!--<div class="tab-pane fade " id="2017">
                                <div class="panel-group" id="accordionThree" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingThree">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#dec_2017" aria-expanded="true"
                                                                        aria-controls="dec_2017" class="remove-link">
                                                                        December 2017 </a> </h4>
                                                </div>
                                                <div id="dec_2017" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingThree">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFour">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#nov_2017" aria-expanded="false"
                                                                        aria-controls="nov_2017">
                                                                        November 2017 </a> </h4>
                                                </div>
                                                <div id="nov_2017" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingFour">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingFive">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#aug_2017" aria-expanded="false"
                                                                        aria-controls="aug_2017">
                                                                        August 2017 </a> </h4>
                                                </div>
                                                <div id="aug_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#jul_2017" aria-expanded="false"
                                                                        aria-controls="jul_2017">
                                                                        July 2017 </a> </h4>
                                                </div>
                                                <div id="jul_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#jun_2017" aria-expanded="false"
                                                                        aria-controls="jun_2017">
                                                                        June 2017 </a> </h4>
                                                </div>
                                                <div id="jun_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSix">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#may_2017" aria-expanded="false"
                                                                        aria-controls="may_2017">
                                                                        May 2017 </a> </h4>
                                                </div>
                                                <div id="may_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        

                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#apr_2017" aria-expanded="false"
                                                                        aria-controls="apr_2017">
                                                                        April 2017 </a> </h4>
                                                </div>
                                                <div id="apr_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingSeven">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#feb_2017" aria-expanded="false"
                                                                        aria-controls="feb_2017">
                                                                        February 2017 </a> </h4>
                                                </div>
                                                <div id="feb_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingEight">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionThree" href="#jan_2017" aria-expanded="false"
                                                                        aria-controls="jan_2017">
                                                                        January 2017 </a> </h4>
                                                </div>
                                                <div id="jan_2017" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        <li class="remove-list-style"> <i class="fas fa-chevron-right m-r-10-px"></i>
                                                                                <p class="list-details-date">Fri, 27 Jan
                                                                                        2017
                                                                                </p> <a class="list-details remove-link li-a-hover"
                                                                                        href="http://www.itq.in/cms_uploads/pressReleases/472-InterGlobe Technology Quotient all set for SATTE 2017.pdf"
                                                                                        target="blank">InterGlobe Technology Quotient
                                                                                        all set for SATTE 2017
                                                                                </a></li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="tab-pane fade " id="2016">
                                <div class="panel-group" id="accordionFour" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#dec_2016" aria-expanded="true"
                                                                        aria-controls="dec_2016" class="remove-link">
                                                                        December 2016 </a> </h4>
                                                </div>
                                                <div id="dec_2016" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#nov_2016" aria-expanded="false"
                                                                        aria-controls="nov_2016">
                                                                        November 2016 </a> </h4>
                                                </div>
                                                <div id="nov_2016" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#octo_2016" aria-expanded="false"
                                                                        aria-controls="octo_2016">
                                                                        October 2016 </a> </h4>
                                                </div>
                                                <div id="octo_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#sep_2016" aria-expanded="false"
                                                                        aria-controls="sep_2016">
                                                                        Septemeber 2016 </a> </h4>
                                                </div>
                                                <div id="sep_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#aug_2016" aria-expanded="false"
                                                                        aria-controls="aug_2016">
                                                                        August 2016 </a> </h4>
                                                </div>
                                                <div id="aug_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#jul_2016" aria-expanded="false"
                                                                        aria-controls="jul_2016">
                                                                        July 2016 </a> </h4>
                                                </div>
                                                <div id="jul_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#jun_2016" aria-expanded="false"
                                                                        aria-controls="jun_2016">
                                                                        June 2016 </a> </h4>
                                                </div>
                                                <div id="jun_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#may_2016" aria-expanded="false"
                                                                        aria-controls="may_2016">
                                                                        May 2016 </a> </h4>
                                                </div>
                                                <div id="may_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#apr_2016" aria-expanded="false"
                                                                        aria-controls="apr_2016">
                                                                        April 2016 </a> </h4>
                                                </div>
                                                <div id="apr_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#mar_2016" aria-expanded="false"
                                                                        aria-controls="mar_2016">
                                                                        March 2016 </a> </h4>
                                                </div>
                                                <div id="mar_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#feb_2016" aria-expanded="false"
                                                                        aria-controls="feb_2016">
                                                                        February 2016 </a> </h4>
                                                </div>
                                                <div id="feb_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFour" href="#jan_2016" aria-expanded="false"
                                                                        aria-controls="jan_2016">
                                                                        January 2016 </a> </h4>
                                                </div>
                                                <div id="jan_2016" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <div class="tab-pane fade " id="2015">
                                <div class="panel-group" id="accordionFive" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#dec_2015" aria-expanded="true"
                                                                        aria-controls="dec_2015" class="remove-link">
                                                                        December 2015 </a> </h4>
                                                </div>
                                                <div id="dec_2015" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#nov_2015" aria-expanded="false"
                                                                        aria-controls="nov_2015">
                                                                        November 2015 </a> </h4>
                                                </div>
                                                <div id="nov_2015" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#octo_2015" aria-expanded="false"
                                                                        aria-controls="octo_2015">
                                                                        October 2015 </a> </h4>
                                                </div>
                                                <div id="octo_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#sep_2015" aria-expanded="false"
                                                                        aria-controls="sep_2015">
                                                                        Septemeber 2015 </a> </h4>
                                                </div>
                                                <div id="sep_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#aug_2015" aria-expanded="false"
                                                                        aria-controls="aug_2015">
                                                                        August 2015 </a> </h4>
                                                </div>
                                                <div id="aug_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#jul_2015" aria-expanded="false"
                                                                        aria-controls="jul_2015">
                                                                        July 2015 </a> </h4>
                                                </div>
                                                <div id="jul_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#jun_2015" aria-expanded="false"
                                                                        aria-controls="jun_2015">
                                                                        June 2015 </a> </h4>
                                                </div>
                                                <div id="jun_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#may_2015" aria-expanded="false"
                                                                        aria-controls="may_2015">
                                                                        May 2015 </a> </h4>
                                                </div>
                                                <div id="may_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#apr_2015" aria-expanded="false"
                                                                        aria-controls="apr_2015">
                                                                        April 2015 </a> </h4>
                                                </div>
                                                <div id="apr_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#mar_2015" aria-expanded="false"
                                                                        aria-controls="mar_2015">
                                                                        March 2015 </a> </h4>
                                                </div>
                                                <div id="mar_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#feb_2015" aria-expanded="false"
                                                                        aria-controls="feb_2015">
                                                                        February 2015 </a> </h4>
                                                </div>
                                                <div id="feb_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionFive" href="#jan_2015" aria-expanded="false"
                                                                        aria-controls="jan_2015">
                                                                        January 2015 </a> </h4>
                                                </div>
                                                <div id="jan_2015" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>


                        <div class="tab-pane fade " id="2014">
                                <div class="panel-group" id="accordionSix" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#dec_2014" aria-expanded="true"
                                                                        aria-controls="dec_2014" class="remove-link">
                                                                        December 2014 </a> </h4>
                                                </div>
                                                <div id="dec_2014" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#nov_2014" aria-expanded="false"
                                                                        aria-controls="nov_2014">
                                                                        November 2014 </a> </h4>
                                                </div>
                                                <div id="nov_2014" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#octo_2014" aria-expanded="false"
                                                                        aria-controls="octo_2014">
                                                                        October 2014 </a> </h4>
                                                </div>
                                                <div id="octo_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#sep_2014" aria-expanded="false"
                                                                        aria-controls="sep_2014">
                                                                        Septemeber 2014 </a> </h4>
                                                </div>
                                                <div id="sep_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#aug_2014" aria-expanded="false"
                                                                        aria-controls="aug_2014">
                                                                        August 2014 </a> </h4>
                                                </div>
                                                <div id="aug_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#jul_2014" aria-expanded="false"
                                                                        aria-controls="jul_2014">
                                                                        July 2014 </a> </h4>
                                                </div>
                                                <div id="jul_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#jun_2014" aria-expanded="false"
                                                                        aria-controls="jun_2014">
                                                                        June 2014 </a> </h4>
                                                </div>
                                                <div id="jun_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#may_2014" aria-expanded="false"
                                                                        aria-controls="may_2014">
                                                                        May 2014 </a> </h4>
                                                </div>
                                                <div id="may_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                       
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#apr_2014" aria-expanded="false"
                                                                        aria-controls="apr_2014">
                                                                        April 2014 </a> </h4>
                                                </div>
                                                <div id="apr_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#mar_2014" aria-expanded="false"
                                                                        aria-controls="mar_2014">
                                                                        March 2014 </a> </h4>
                                                </div>
                                                <div id="mar_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#feb_2014" aria-expanded="false"
                                                                        aria-controls="feb_2014">
                                                                        February 2014 </a> </h4>
                                                </div>
                                                <div id="feb_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>


                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSix" href="#jan_2014" aria-expanded="false"
                                                                        aria-controls="jan_2014">
                                                                        January 2014 </a> </h4>
                                                </div>
                                                <div id="jan_2014" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>

                        <div class="tab-pane fade " id="2013">
                                <div class="panel-group" id="accordionSeven" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#dec_2013" aria-expanded="true"
                                                                        aria-controls="dec_2013" class="remove-link">
                                                                        December 2013 </a> </h4>
                                                </div>
                                                <div id="dec_2013" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#nov_2013" aria-expanded="false"
                                                                        aria-controls="nov_2013">
                                                                        November 2013 </a> </h4>
                                                </div>
                                                <div id="nov_2013" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#octo_2013" aria-expanded="false"
                                                                        aria-controls="octo_2013">
                                                                        October 2013 </a> </h4>
                                                </div>
                                                <div id="octo_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#sep_2013" aria-expanded="false"
                                                                        aria-controls="sep_2013">
                                                                        Septemeber 2014 </a> </h4>
                                                </div>
                                                <div id="sep_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#aug_2013" aria-expanded="false"
                                                                        aria-controls="aug_2013">
                                                                        August 2013 </a> </h4>
                                                </div>
                                                <div id="aug_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#jul_2013" aria-expanded="false"
                                                                        aria-controls="jul_2013">
                                                                        July 2013 </a> </h4>
                                                </div>
                                                <div id="jul_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#jun_2013" aria-expanded="false"
                                                                        aria-controls="jun_2013">
                                                                        June 2013 </a> </h4>
                                                </div>
                                                <div id="jun_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#may_2013" aria-expanded="false"
                                                                        aria-controls="may_2013">
                                                                        May 2013 </a> </h4>
                                                </div>
                                                <div id="may_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#apr_2013" aria-expanded="false"
                                                                        aria-controls="apr_2013">
                                                                        April 2013 </a> </h4>
                                                </div>
                                                <div id="apr_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading18">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#mar_2013" aria-expanded="false"
                                                                        aria-controls="mar_2013">
                                                                        March 2013 </a> </h4>
                                                </div>
                                                <div id="mar_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading18">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        

                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#feb_2013" aria-expanded="false"
                                                                        aria-controls="feb_2013">
                                                                        February 2013 </a> </h4>
                                                </div>
                                                <div id="feb_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>


                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionSeven" href="#jan_2013" aria-expanded="false"
                                                                        aria-controls="jan_2013">
                                                                        January 2013 </a> </h4>
                                                </div>
                                                <div id="jan_2013" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="tab-pane fade " id="2012">
                                <div class="panel-group" id="accordionEight" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingnine">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#dec_2012" aria-expanded="true"
                                                                        aria-controls="dec_2012" class="remove-link">
                                                                        December 2012 </a> </h4>
                                                </div>
                                                <div id="dec_2012" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingnine">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingten">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#nov_2012" aria-expanded="false"
                                                                        aria-controls="nov_2012">
                                                                        November 2012 </a> </h4>
                                                </div>
                                                <div id="nov_2012" class="panel-collapse collapse   " role="tabpanel" aria-labelledby="headingten">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading11">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#octo_2012" aria-expanded="false"
                                                                        aria-controls="octo_2012">
                                                                        October 2012 </a> </h4>
                                                </div>
                                                <div id="octo_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading12">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#sep_2012" aria-expanded="false"
                                                                        aria-controls="sep_2012">
                                                                        Septemeber 2012 </a> </h4>
                                                </div>
                                                <div id="sep_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading13">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#aug_2012" aria-expanded="false"
                                                                        aria-controls="aug_2012">
                                                                        August 2012 </a> </h4>
                                                </div>
                                                <div id="aug_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading14">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#jul_2012" aria-expanded="false"
                                                                        aria-controls="jul_2012">
                                                                        July 2012 </a> </h4>
                                                </div>
                                                <div id="jul_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading15">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#jun_2012" aria-expanded="false"
                                                                        aria-controls="jun_2012">
                                                                        June 2012 </a> </h4>
                                                </div>
                                                <div id="jun_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading16">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#may_2012" aria-expanded="false"
                                                                        aria-controls="may_2012">
                                                                        May 2012 </a> </h4>
                                                </div>
                                                <div id="may_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading16">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading17">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#apr_2012" aria-expanded="false"
                                                                        aria-controls="apr_2012">
                                                                        April 2012 </a> </h4>
                                                </div>
                                                <div id="apr_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading17">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading19">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#feb_2012" aria-expanded="false"
                                                                        aria-controls="feb_2012">
                                                                        February 2012 </a> </h4>
                                                </div>
                                                <div id="feb_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading19">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="heading20">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionEight" href="#jan_2012" aria-expanded="false"
                                                                        aria-controls="jan_2012">
                                                                        January 2012 </a> </h4>
                                                </div>
                                                <div id="jan_2012" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading20">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="tab-pane fade " id="2011">
                                <div class="panel-group" id="accordionNine" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionNine" href="#octo_2011" aria-expanded="true"
                                                                        aria-controls="octo_2011" class="remove-link"> October 2011
                                                                </a> </h4>
                                                </div>
                                                <div id="octo_2011" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionNine" href="#aug_2011" aria-expanded="false"
                                                                        aria-controls="aug_2011">
                                                                        August 2011 </a> </h4>
                                                </div>
                                                <div id="aug_2011" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>

                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionNine" href="#feb_2011" aria-expanded="false"
                                                                        aria-controls="feb_2011">
                                                                        February 2011 </a> </h4>
                                                </div>
                                                <div id="feb_2011" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>

                                        </div>
                                </div>
                        </div>

                        <div class="tab-pane fade " id="2010">
                                <div class="panel-group" id="accordionTen" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingOne">
                                                        <h4 class="panel-title panel-title-name"> <a data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#octo_2010" aria-expanded="true"
                                                                        aria-controls="octo_2010" class="remove-link"> October 2010
                                                                </a> </h4>
                                                </div>
                                                <div id="octo_2010" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#aug_2010" aria-expanded="false"
                                                                        aria-controls="aug_2010">
                                                                        August 2010 </a> </h4>
                                                </div>
                                                <div id="aug_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>

                                        </div>
                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#jul_2010" aria-expanded="false"
                                                                        aria-controls="jul_2010">
                                                                        July 2010 </a> </h4>
                                                </div>
                                                <div id="jul_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#apr_2010" aria-expanded="false"
                                                                        aria-controls="apr_2010">
                                                                        April 2010 </a> </h4>
                                                </div>
                                                <div id="apr_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>

                                        <div class="panel panel-default">
                                                <div class="panel-heading" role="tab" id="headingTwo">
                                                        <h4 class="panel-title"> <a class="collapsed remove-link" data-toggle="collapse"
                                                                        data-parent="#accordionTen" href="#apr_2010" aria-expanded="false"
                                                                        aria-controls="apr_2010">
                                                                        February 2010 </a> </h4>
                                                </div>
                                                <div id="apr_2010" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                        <div class="panel-body">
                                                                <ul class="container">
                                                                        
                                                                </ul>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>-->
                </div>
        </div>
</div>
</div>
</div>
</div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>