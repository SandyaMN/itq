<?php
/**
 * Template Name: advertisements
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <!-- <link href="../css/common.css" rel="stylesheet">
    <link href="../css/aboutus.css" rel="stylesheet"> -->
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/press-room.css';?>">

   
    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="main-heading">Press Room</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class=" press-releases"><a class="release" href="<?php echo get_home_url(); ?>/press-release/">Press Releases</a></li>
                        <li class="active advertisements"><a class="release" href="<?php echo get_home_url(); ?>/advertisements/">Advertisements</a></li>
                        <li class=" articles"><a class="release" href="<?php echo get_home_url(); ?>/articles/">Articles</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="press-releases" class="tab-pane fade in active">
<div class="row m-t-10-px">
        <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="">Press Room</a>
                / <a class="current-page">Advertisements
                </a></p>

</div>
<div class="row">
        <h4 class=" row press-room-header">Advertisements</h4>
</div>
<div class="gallery">
    <?php echo do_shortcode('[foogallery id="407"]'); ?>   

</div>
</div>
</div>
</div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>