<?php
/**
 * Template Name: learning-development
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Human Capital</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="hr-vision"><a class="release" href="<?php echo get_home_url(); ?>/hr-vision/">HR Vision & Values </a></li>
                        <li class="employee-value"><a class="release" href="<?php echo get_home_url(); ?>/employee-value/">Employee Value Proposition </a></li>
                        <li class="learning-development"><a class="release" href="<?php echo get_home_url(); ?>/learning-development/">Learning and Development</a></li>
                        <li class="active diversity-inclusion"><a class="release" href="<?php echo get_home_url(); ?>/diversity-inclusion/">Diversity and Inclusion </a></li>
                        <li class="carrer"><a class="release" href="<?php echo get_home_url(); ?>/carrer/">Careers</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="diversity-inclusion" class="tab-pane fade in active">
<div class="row m-t-10-px">
    <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/hr-vision/">Human Capital</a> / <a
            class="current-page" href="./human-capital.html?tab=diversity-inclusion">Diversity and Inclusion</a></p>
</div>
<div class="row">
    <h4 class=" row press-room-header">Diversity and Inclusion</h4>
</div>

<div class="row">
    <div class="row sub-heading bold">
        What is D & I?
    </div>
    <div class="row m-t-20-px">
        Diversity is the range of human differences, including but not limited to race, ethnicity, gender, gender identity, sexual
        orientation, age, social class, physical ability or attributes, religious or ethical values system, national origin,
        and political beliefs.
    </div>
</div>

<div class="row m-t-30-px">
    <div class="row sub-heading bold">
        Mission and Vision of D & I
    </div>
    <div class="row m-t-20-px">
        <em>
            "InterGlobe is committed to a culture that promotes diversity to create a competitive environment. Our Diversity and Inclusion
            efforts are designed to attract, nurture and advance the lives of our people and customers irrespective of their
            differently abled status, gender, lifestyle, sexual orientation, religion and nationality."
        </em>
    </div>
</div>

<div class="row m-t-30-px">
    <div class="row sub-heading bold">
        The role of D & I in InterGlobe Technology Quotient
    </div>
    <div class="row m-t-20-px">
        At InterGlobe Technology Quotient, we are committed towards D&I, be it aligning ourselves Gender Diversity or Regional, Generation,
        differently-abled, sexual orientation, religion etc.
    </div>

    <div class="row m-t-20-px">
        The D&I committee is formed by the employees of the organisation and the programme is lead by one of our leaders, Neena Gupta,
        Group General Counsel, InterGlobe Enterprises.
    </div>

    <div class="row m-t-20-px">
        The council members brainstorm with employees from various regions, departments etc to formulate new practices, policies
        and celebrate different functions e.g. Father's Day, World Women's Day etc.
    </div>
</div>

<div class="row m-t-30-px">
    <div class="row sub-heading bold">
        D&I policy and HR initiatives
    </div>
    <div class="row m-t-20-px">
        Under D&I Committee, we have formulated various policies to support our focus.
    </div>
</div>

<div class="video_border m-t-20-px h-173-px p-10">
    <div class="sub-list-heading m-l-3-px m-t-15-px">
        The company also has various processes which supports Diversity
    </div>
    <div class="row p-10-px m-l-3-px ">
        <span class="side-small-heading">Customized Employee Referral:</span>
        <span>
            Promoting a balance in the work force in terms of Gender, Regional etc.
        </span>
    </div>
    <div class="row p-10-px m-l-3-px ">
        <span class="side-small-heading">Conscious Balance of all Generations</span>
    </div>
    <div class="row p-10-px m-l-3-px ">
        <span class="side-small-heading">Special Focus of Minority Groups –DIVA Connect (Women's Forum) etc.</span>
    </div>
</div>
</div>
</div>
</div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>