<?php
/**
 * Template Name: carrer
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Human Capital</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="hr-vision"><a class="release" href="<?php echo get_home_url(); ?>/hr-vision/">HR Vision & Values </a></li>
                        <li class="employee-value"><a class="release" href="<?php echo get_home_url(); ?>/employee-value/">Employee Value Proposition </a></li>
                        <li class="learning-development"><a class="release" href="<?php echo get_home_url(); ?>/learning-development/">Learning and Development</a></li>
                        <li class="diversity-inclusion"><a class="release" href="<?php echo get_home_url(); ?>/diversity-inclusion/">Diversity and Inclusion </a></li>
                        <li class="active carrer"><a class="release" href="<?php echo get_home_url(); ?>/carrer/">Careers</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="carrer" class="tab-pane fade in active">
<div class="row m-t-10-px"><p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/hr-vision/">Human Capital</a> / <a class="current-page">Careers</a></p>
</div>
<div class="row">
    <h4 class=" row press-room-header">Career</h4>
</div>

<div class="row">
    <div class="row sub-heading bold">
        Welcome to the our online careers centre
    </div>
    <div class="m-t-10-px">

        InterGlobe Technology Quotient is a broad-based business services provider catering to a large segment of the travel industry
        in distributing Travelport GDS. As one of the world's largest travel content aggregators and distributors, and a
        leading provider of critical transaction processing solutions, Travelport powers the travel industry on a global
        scale by connecting buyers and sellers through agency, online and corporate travel channels. ITQ is a progressive
        employer offering a diverse and innovative culture, competitive rewards and, quite possibly, the career of your lifetime.
    </div>
</div>

<div class="row m-t-30-px">
    <div class="row sub-heading bold">
        Positions for every career path
    </div>
    <div class="m-t-10-px">
        Our business is multi-faceted, offering opportunities in account sales and service, product and technology development, customer
        training and support, human resources, marketing and communications, accounting, legal, finance and many other fields
        of endeavour. Prior experience in the travel industry is not always critical, but a commitment to the customer, integrity,
        and an innovative and driven personality are highly desirable traits for our team. We?re always looking for passionate,
        talented new people to join our team. Learn how you can be a part of the travel industry − the largest industry in
        the world ? and explore the exciting opportunities here at InterGlobe Technology Quotient.
    </div>
</div>


<div class="row m-t-30-px">
    <div class="row sub-heading bold">
        Current opportunities
    </div>
</div>

<div class="row m-t-20-px">
    <div class="video_border img-centre careers-video">
        <!-- <span class="m-r-10-px m-r-8-px p-10-px">
            <iframe width="360" height="200" src="https://www.youtube.com/embed/kjQc6nzMG64">
            </iframe>
        </span>
        <span class="m-r-10-px m-r-8-px">
            <iframe class="" width="360" height="200" src="https://www.youtube.com/embed/eaIoirl8v4s">
            </iframe>            
        </span>
        <span class="m-r-10-px m-r-8-px">
            <iframe width="360" height="200" src="https://www.youtube.com/embed/N92SF8ew3sI">
            </iframe>           
        </span> -->
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <!-- <iframe width="360" height="200" src="https://www.youtube.com/embed/kjQc6nzMG64">
            </iframe> -->
            
            <!-- <?php echo do_shortcode('[video_lightbox_youtube video_id="kjQc6nzMG64" width="640" height="480" anchor="click here"]'); ?>    -->
            <a href="#" class="wp-video-popup"><img src="<?php echo get_template_directory_uri().'/assets/careers-1.png';?>"></a>
            <?php echo do_shortcode('[wp-video-popup video="https://www.youtube.com/embed/kjQc6nzMG64"]');?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
            <!-- <iframe class="" width="360" height="200" src="https://www.youtube.com/embed/eaIoirl8v4s">
            </iframe> -->
            <a href="#" class="wp-video-popup"><img src="<?php echo get_template_directory_uri().'/assets/careers-2.png';?>"></a>
            <?php echo do_shortcode('[wp-video-popup video="https://www.youtube.com/embed/eaIoirl8v4s"]');?>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <!-- <iframe width="360" height="200" src="https://www.youtube.com/embed/N92SF8ew3sI">
            </iframe> -->
            <a href="#" class="wp-video-popup"><img src="<?php echo get_template_directory_uri().'/assets/careers-3.png';?>"></a>
            <?php echo do_shortcode('[wp-video-popup video="https://www.youtube.com/embed/N92SF8ew3sI"]');?>
        </div>
    </div>
</div>


<div class="row m-t-30-px">
    We are looking for bright minds and sparks to be part of our setup across the country.
</div>

<div class="row m-t-20-px">
    <div class="row m-t-20-px">
        <span>
            Position Title: </span>
        <span><u>Relationship Manager</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Reporting To: </span>
        <span><u>Business Manager</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Function: </span>
        <span><u>Relationship Management</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Company: </span>
        <span><u>InterGlobe Technology Quotient</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Location: </span>
        <span><u>Pan India</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Band: </span>
        <span><u>1</u></span>
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Educational Qualification / Experience:
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Graduate with 1- 4 years of work experience in Travel or FMCG-Sales Domain, Graduate in any discipline.</li>
        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> Graduate with 1- 4 years of work experience in Travel or FMCG-Sales Domain,
            Graduate in any discipline.
        </p> -->
    </div>
    <div class="row  m-t-20-px">
        <span class="side-small-heading">
            Purpose:
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Responsible for all sales activities in assigned accounts or regions.</li>
            <li> Manage quality and consistency of product and service delivery.</li>
            <li> To work in a team to maximize revenue segments & promote Galileo as a product to agents</li>

        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> Responsible for all sales activities in assigned accounts or regions.
        </p>
        <p>
            <i class="fas fa-chevron-right "></i>
            Manage quality and consistency of product and service delivery.
        </p>
        <p>
            <i class="fas fa-chevron-right "></i>
            To work in a team to maximize revenue segments & promote Galileo as a product to agents
        </p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Primary Responsibilities:
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Maintain and develop existing and new customers through planned individual account support, and liaison with internal services team.</li>
            <li> Increase market share in the accounts assigned.</li>
            <li> Follow up on new leads and referrals resulting from field activity.</li>
            <li> Identify sales prospects and contact these and other accounts as assigned.</li>
            <li> Establish and maintain current client and potential client relationships.</li>
            <li> Prepare presentations, proposals and sales contracts.</li>
            <li> Negotiating the terms of an agreement and closing sales</li>
            <li> Maintain and develop a computerized customer and prospect database.</li>
            <li> Responsible for generation of the sales and market information reports on a regular basis.</li>

        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> Maintain and develop existing and new customers through planned individual
            account support, and liaison with internal services team.
        </p>
        <p> <i class="fas fa-chevron-right "></i> Increase market share in the accounts assigned.</p>
        <p> <i class="fas fa-chevron-right "></i>Follow up on new leads and referrals resulting from field activity.</p>
        <p> <i class="fas fa-chevron-right "></i>Identify sales prospects and contact these and other accounts as assigned.
        </p>
        <p> <i class="fas fa-chevron-right "></i>Establish and maintain current client and potential client relationships.</p>
        <p> <i class="fas fa-chevron-right "></i>Prepare presentations, proposals and sales contracts.</p>
        <p> <i class="fas fa-chevron-right "></i>Negotiating the terms of an agreement and closing sales</p>
        <p> <i class="fas fa-chevron-right "></i>Maintain and develop a computerized customer and prospect database.</p>
        <p> <i class="fas fa-chevron-right "></i>Responsible for generation of the sales and market information reports on
            a regular basis.</p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Relationship Management
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li>A Relationship Manager is responsible for all sales related activities in assigned accounts or regions. Key objective of
            this profile is to hunt for new accounts i.e. Travel Agencies and maximize business from the present set of accounts,
            manage quality and consistency of product and service delivery thereby maximizing revenue & promote Galileo as
            a product to travel agents.</li>
        </ul>
        <!-- <p class="m-t-10-px"><i class="fas fa-chevron-right "></i>
            A Relationship Manager is responsible for all sales related activities in assigned accounts or regions. Key objective of
            this profile is to hunt for new accounts i.e. Travel Agencies and maximize business from the present set of accounts,
            manage quality and consistency of product and service delivery thereby maximizing revenue & promote Galileo as
            a product to travel agents.</p> -->
    </div>
</div>


<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Attributes
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Ability to interact with people</li>
            <li> Good Communication and Negotiation Skills</li>
            <li> Good Understanding on high quality transactions and business profit.</li>
            <li> Keen for new experience, responsibility and accountability.</li>
            <li> Any knowledge of GDS, Travel Trade, Travel Sales would be a plus</li>
           
        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> Ability to interact with people
        </p>
        <p> <i class="fas fa-chevron-right "></i> Good Communication and Negotiation Skills</p>
        <p> <i class="fas fa-chevron-right "></i>Good Understanding on high quality transactions and business profit. </p>
        <p> <i class="fas fa-chevron-right "></i>Keen for new experience, responsibility and accountability.
        </p>
        <p> <i class="fas fa-chevron-right "></i>Any knowledge of GDS, Travel Trade, Travel Sales would be a plus</p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Experience and Qualification
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> 1 -3 year of proven experience in Sales domain</li>
            <li> Graduate in any discipline</li>
        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> 1 -3 year of proven experience in Sales domain
        </p>
        <p> <i class="fas fa-chevron-right "></i> Graduate in any discipline</p> -->
    </div>
</div>

<div class="dottedborderbottom">

</div>

<div class="row m-t-20-px">
    <div class="row m-t-20-px">
        <span>
            Position Title: </span>
        <span><u>Trainer</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Reporting To: </span>
        <span><u>Business Manager</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Function: </span>
        <span><u>GDS Training and Development</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Company: </span>
        <span><u>InterGlobe Technology Quotient</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Location: </span>
        <span><u>Pan India</u></span>
    </div>
    <div class="row m-t-20-px">
        <span>
            Band: </span>
        <span><u>1</u></span>
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Educational Qualification / Experience:
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li>3- 4 yrs of hands on experience on any GDS/CRS system. Graduate in any discipline, IATA / Diploma in Travel & Tourism, Should
            have worked for a travel agency either at an executive or supervisory level & sound knowledge of operations of
            a travel agency, Candidate with relevant training background will be preferred.</li>
        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i>
            3- 4 yrs of hands on experience on any GDS/CRS system. Graduate in any discipline, IATA / Diploma in Travel & Tourism, Should
            have worked for a travel agency either at an executive or supervisory level & sound knowledge of operations of
            a travel agency, Candidate with relevant training background will be preferred.
        </p> -->
    </div>
    <div class="row m-t-20-px">
        <span class="side-small-heading">
            Purpose:
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Responsible for imparting class room trainings, on site trainings on all Galileo and Worldspan products for Customer (Internal
            +External) and NDC staff.</li>
            <li> Design training courses, programs, course material.</li>
            <li> Carry out routine checks to assess the knowledge of NDC staff by conducting quarterly evaluations & undertake regular analysis
            of areas of improvement for NDC staff.</li>

        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i>
            Responsible for imparting class room trainings, on site trainings on all Galileo and Worldspan products for Customer (Internal
            +External) and NDC staff.
        </p>
        <p>
            <i class="fas fa-chevron-right "></i>
            Design training courses, programs, course material.
        </p>
        <p>
            <i class="fas fa-chevron-right "></i>

            Carry out routine checks to assess the knowledge of NDC staff by conducting quarterly evaluations & undertake regular analysis
            of areas of improvement for NDC staff.
        </p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Primary Responsibilities:
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Retention & Growth of Business-Provide support the current trainer ,
            mainly on handling customer queries , service support , trouble shooting , handling agency queries and develop
            into a future trainer; Updating the market on all new products launched.</li>
            <li> Revenue Generation- Tie up with Non-ITQ customers like colleges and Travel
            institutes and generate revenue for the company.</li>
            <li> Enhancing Customer Experience- New product testing & Update agents about
            new ITQ products, commands etc.; process Audits for customers</li>

        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> Ø Retention & Growth of Business-Provide support the current trainer ,
            mainly on handling customer queries , service support , trouble shooting , handling agency queries and develop
            into a future trainer; Updating the market on all new products launched.
        </p>
        <p> <i class="fas fa-chevron-right "></i> Ø Revenue Generation- Tie up with Non-ITQ customers like colleges and Travel
            institutes and generate revenue for the company.</p>
        <p> <i class="fas fa-chevron-right "></i>Ø Enhancing Customer Experience- New product testing & Update agents about
            new ITQ products, commands etc.; process Audits for customers</p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            GDS Training & Development
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> A GDS Trainer is responsible for imparting class room trainings, on site trainings on all Galileo and Worldspan products
            for Customer (Internal +External) and NDC staff. Design training courses, programs, course material. Carry out
            routine checks to assess the knowledge of NDC staff by conducting quarterly evaluations & undertake regular analysis
            of areas of improvement for NDC staff.</li>
        </ul>
        <!-- <p class="m-t-10-px"><i class="fas fa-chevron-right "></i>

            A GDS Trainer is responsible for imparting class room trainings, on site trainings on all Galileo and Worldspan products
            for Customer (Internal +External) and NDC staff. Design training courses, programs, course material. Carry out
            routine checks to assess the knowledge of NDC staff by conducting quarterly evaluations & undertake regular analysis
            of areas of improvement for NDC staff.</p> -->
    </div>
</div>


<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Attributes
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> Strong leadership, team building, and organizational skills</li>
            <li> Outstanding communication and interpersonal skills</li>

        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> Strong leadership, team building, and organizational skills
        </p>
        <p> <i class="fas fa-chevron-right "></i> Outstanding communication and interpersonal skills</p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">
            Experience and Qualification
        </span><br/>
        <ul class="product-list m-t-10-px">
            <li> 1 -3 year of proven experience in Sales domain</li>
            <li> Graduate in any discipline</li>

        </ul>
        <!-- <p class="m-t-10-px">
            <i class="fas fa-chevron-right "></i> 1 -3 year of proven experience in Sales domain
        </p>
        <p> <i class="fas fa-chevron-right "></i> Graduate in any discipline</p> -->
    </div>
</div>

<div class="row m-t-20-px">
    <span>
        Please send your resume to </span>
    <span class="mail">careers@galileo.co.in</span>
</div>
</div>
</div>
</div>
<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>