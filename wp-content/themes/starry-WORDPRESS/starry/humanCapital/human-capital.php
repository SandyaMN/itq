<?php


get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css';?>" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Human Capital</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="active hr-vision"><a class="release" href="<?php echo get_home_url(); ?>/hr-vision/">HR Vision & Values </a></li>
                        <li class="employee-value"><a class="release" href="<?php echo get_home_url(); ?>/employee-value/">Employee Value Proposition </a></li>
                        <li class="learning-development"><a class="release" href="<?php echo get_home_url(); ?>/learning-development/">Learning and Development</a></li>
                        <li class="diversity-inclusion"><a class="release" href="<?php echo get_home_url(); ?>/diversity-inclusion/">Diversity and Inclusion </a></li>
                        <li class="carrer"><a class="release" href="<?php echo get_home_url(); ?>/carrer/">Careers</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="hr-vision" class="tab-pane fade in active">
                <div class="row m-t-10-px">
    <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/hr-vision/">Human Capital</a> / <a
            class="current-page">HR Vision & Values</a></p>
</div>
<div class="row">
    <h4 class=" row press-room-header">HR Vision</h4>
</div>
<div class="row">
    We have come a long way from becoming a value driven partner to being a strategic partner to the business.
    <div class="row">
        Our HR vision perfectly captures what we believe in:
    </div>
</div>

<div class="row m-t-20-px">
    <span class="side-small-heading">
        HR Vision:
    </span>
    <span>
        We will be a Strategic Partner to the business in creating a focused, performance-driven learning Organization.
    </span>
</div>

<div class="row m-t-50-px ">
    <h4 class=" row press-room-header">Values</h4>
</div>

<div class="row">
    Our values are part of our open culture and they are the basic fragment of the way we think and the way we do things.
</div>

<div class="row m-t-20-px">
    <div class="row">
        <span class="side-small-heading">Integrity:</span>
        <span> Is observing financial & intellectual honesty, taking personal responsibility & facing reality, regardless
            of consequences.
        </span>
    </div>

    <div class="row m-t-20-px">
        <span class="side-small-heading">Customer Orientation:</span>
        <span> Means always seeing things from the customer’s perspective. Identifying the customer’s unstated as well as
            emerging needs.
        </span>
    </div>

    <div class="row m-t-20-px">
        <span class="side-small-heading">
            Future Mindedness:
        </span>
        <span>
            Is about staying in touch with new developments consistently. Questioning existing assumptions, and making new ideas work
        </span>
    </div>
    <div class="row m-t-20-px">
        Integrity, Customer Orientation and Future Mindedness, as values continue to be the source of our pride and serve as a moral
        compass for the culture of InterGlobe.
    </div>
</div>
            </div>
            <!--<div class="tab-pane fade" id="employee-value">

            </div>
            <div class="tab-pane fade" id="learning-development">

            </div>
            <div class="tab-pane fade" id="diversity-inclusion">

            </div>
            <div class="tab-pane fade" id="carrer">

            </div>-->
        </div>
    </div>

</body>

</html>

<script src="<?php echo get_template_directory_uri().'/js/human-capital.js';?>"></script>

 <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>