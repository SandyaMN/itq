<?php
/**
 * Template Name: learning-development
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Human Capital</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="hr-vision"><a class="release" href="<?php echo get_home_url(); ?>/hr-vision/">HR Vision & Values </a></li>
                        <li class="employee-value"><a class="release" href="<?php echo get_home_url(); ?>/employee-value/">Employee Value Proposition </a></li>
                        <li class="active learning-development"><a class="release" href="<?php echo get_home_url(); ?>/learning-development/">Learning and Development</a></li>
                        <li class="diversity-inclusion"><a class="release" href="<?php echo get_home_url(); ?>/diversity-inclusion/">Diversity and Inclusion </a></li>
                        <li class="carrer"><a class="release" href="<?php echo get_home_url(); ?>/carrer/">Careers</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="learning-development" class="tab-pane fade in active">
<div class="row m-t-10-px">
    <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/hr-vision/">Human Capital</a> / <a
            class="current-page">Learning and Development</a></p>
</div>
<div class="row">
    <h4 class=" row press-room-header">Learning and Development</h4>
</div>

<div class="row">
    <div class="interglobeLearning">
        <img src="../assets/interglobe-img.jpg" alt="">
    </div>
    <div class="row">
        <span class="side-small-heading">
            Our Goal:
        </span>
        <span>
            Focus on up skilling & employability of all the employees.
        </span>
    </div>
    <div class="row">
        <span class="side-small-heading">
            Our Approach
        </span>
        <span>
            We have recognized employees in 4 clusters based on their level in the organization.
        </span>
    </div>
</div>

<div class="row m-t-50-px">
    <div class="contentBoxHolder border-left">
        <span>
            Frontline Employees
        </span>
        <p class="m-t-10-px">Customer interfacing team</p>
    </div>
    <div class="contentBoxHolder">
        <span>
            Frontline Leaders
        </span>
        <p class="m-t-10-px">Frontline Management Team</p>
    </div>
    <div class="contentBoxHolder">
        <span>
            Mid Level Leaders
        </span>
        <p class="m-t-10-px">Middle Management Team</p>
    </div>
    <div class="contentBoxHolder">
        <span class="external-padding">
            Business & Functional Leaders
        </span>
        <p class="m-t-10-px">Senior Management Team</p>
    </div>
</div>

<div class="m-t-20-px">
    <span>The core of our L&D model comes from our Values and focusses on all the three groups. Our approach towards Learning
        is based on a culture of Self Learning, Performance and Feedback.</span>
    <div class="img-centre">
        <img src="../assets/graph2.png" alt="">
    </div>
</div>

<div class="row m-t-10-px ">
    <h4 class="bold row sub-heading">Mediums of Learning</h4>
</div>

<div class="row">
    <div class="row">
        We believe that only classroom trainings do not lead to a wholesome development/knowledge building for an individual. We
        thus focus on 360 degree learning through e learning modules, Self development programmes, on the Job Projects, case
        studies, coaching and Mentoring Session, live projects and professional trainings.
    </div>
    <div class="img-centre m-t-20-px">
        <img src="<?php echo get_template_directory_uri().'/assets/graphs.png';?>" alt="">
    </div>
</div>

<div class="row">
    <p>InterGlobe Technology Quotient adopts various strategies to encourage employee career ownership.</p>
    <ul class="product-list">
        <li> Position Based career paths</li>
        <li> Internal Job Postings</li>
        <li> Mentorship sessions</li>
        <li> Growth based careers</li>

    </ul>
    <!-- <span>
        InterGlobe Technology Quotient adopts various strategies to encourage employee career ownership.
    </span>

    <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Position Based career paths
    </div>

    <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Internal Job Postings
    </div>
    <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Mentorship sessions
    </div>
    <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Growth based careers
    </div> -->
</div>
</div>
</div>
</div>
 <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>