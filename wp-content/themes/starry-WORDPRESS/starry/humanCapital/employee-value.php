<?php
/**
 * Template Name: employee-value
 */

get_header(); ?>
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>">
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/jquery.diyslider.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css';?>" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr"
        crossorigin="anonymous">
    <link href="<?php echo get_template_directory_uri().'/css/home.css';?>" rel="stylesheet">
    <script src="<?php echo get_template_directory_uri().'/js/aboutus.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/human-capital.css';?>">


    <div class="banner">
        <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
        <div class="about-us">
            <div class="container">
                <h3 class="">Human Capital</h3>
                <div class="aboutus-menu">
                    <ul class="nav nav-tabs">
                        <li class="hr-vision"><a class="release" href="<?php echo get_home_url(); ?>/hr-vision/">HR Vision & Values </a></li>
                        <li class="active employee-value"><a class="release" href="<?php echo get_home_url(); ?>/employee-value/">Employee Value Proposition </a></li>
                        <li class="learning-development"><a class="release" href="<?php echo get_home_url(); ?>/learning-development/">Learning and Development</a></li>
                        <li class="diversity-inclusion"><a class="release" href="<?php echo get_home_url(); ?>/diversity-inclusion/">Diversity and Inclusion </a></li>
                        <li class="carrer"><a class="release" href="<?php echo get_home_url(); ?>/carrer/">Careers</a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="container aboutus-tabs">
        <div class="tab-content">
            <div id="employee-value" class="tab-pane fade in active">
<div class="row m-t-10-px">
    <p class="path"><a href="<?php echo get_home_url(); ?>">Home</a> / <a href="<?php echo get_home_url(); ?>/hr-vision/">Human Capital</a> / <a
            class="current-page">Employee Value Proposition</a></p>
</div>
<div class="row">
    <h4 class=" row press-room-header">Employee Value Proposition</h4>
</div>

<div class="row">
    Our Employee Value Proposition is to provide a challenging, empowered people centric organization based on rewarding, collaboration
    and innovation.
</div>

<div class="row m-t-20-px ">
    <h4 class=" row press-room-header">Total Rewards</h4>
</div>

<div class="row">
    <div class="row">
        <span class="side-small-heading">
            Development & Career Opportunity:
        </span>
        <span>
            We offer our employees many opportunities to develop, grow and to perform optimally. Our goal is to focus on up skilling
            & employability of all the employees. The employee development cycle is driven by values & competencies and results
            in capable leaders, up skilling of employees and increased employability.
        </span>
    </div>

    <div class="row m-t-20-px">
        <span class="side-small-heading">
            Wellness Initiatives:
        </span>
        <span>
            Our goal towards wellness is to focus on the overall wellness needs of the employees to reduce presenteeism.
        </span>
    </div>

    <div class="row m-t-20-px">
        <span class="side-small-heading">
            Work Life Balance & Inclusivity:
        </span>
        <span>
            We aim at maintaining healthy work life balance at workplace along with embracing and valuing diversity to help build engagement
            and foster creativity in the organization.
        </span>
    </div>

    <div class="row m-t-20-px">
        <span class="side-small-heading">
            Pay:
        </span>
        <span>
            We focus on building a culture of rewarding performance. The performance development cycle (PDP) is to build a performance
            enabling culture, to improve leadership potential in employees and to enhance a culture of continued learning.
            It facilitates the effective delivery of strategic and operational goals by measuring performance, calibrating
            a reward system and identifying developmental needs.
        </span>
    </div>

    <div class="row m-t-20-px">
        <span class="side-small-heading">
            Rewards & Recognition:
        </span>
        <span>
            We reward high performance and recognise outstanding achievements with a range of financial and other benefits. Every employee
            is encouraged to grow to his or her full potential. We have a performance based rewards structure in place that
            aims to:
        </span>
    </div><br/>
    <ul class="product-list"> 
                <li> Recognize employees who live the InterGlobe Values and Competencies.</li>
                <li> Recognize employees who are committed to the Company's growth through their sustained high-quality efforts and excellence.</li>
                <li> Reward employees for contributing significantly to organization development.</li>

    </ul>
    <!-- <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Recognize employees who live the InterGlobe Values and Competencies.
    </div>

    <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Recognize employees who are committed to the Company's growth through
        their sustained high-quality efforts and excellence.
    </div>

    <div class="row m-t-10-px">
        <i class="fas fa-chevron-right m-r-10-px"></i> Reward employees for contributing significantly to organization development.
    </div> -->
</div>
</div>
</div>
</div>

<?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>