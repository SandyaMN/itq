<?php if (!defined('FW')) die('Forbidden');

$cfg = array(
	'page_builder' => array(
		'tab'         => __('Starry Elements', 'starry'),
		'title'       => __('Custom Section', 'starry'),
		'description' => __('Add a Section', 'starry'),
		'type'        => 'section' // WARNING: Do not edit this
	)
);