<?php if ( ! defined( 'FW' ) ) {
	die( 'Forbidden' );
}

$manifest = array();

$manifest['id'] = 'starry';

$manifest['name']         = __('Starry', 'starry');
$manifest['uri']          = 'themeforest.net/user/alkaweb';
$manifest['description']  = __('Another awesome wordpress theme', 'starry');
$manifest['version']      = '1.3.2';
$manifest['author']       = '2Fwebd';
$manifest['author_uri']   = '//themeforest.net/user/alkaweb';

$manifest['supported_extensions'] = array(
	'page-builder' => array(),
	'breadcrumbs' => array(),
	'megamenu' => array(),
	'sidebars' => array(),
	'seo' => array(),
	'backup' => array(),
	'analytics' => array(),
	'slider' => array(),
);
