<?php if ( ! defined( 'ABSPATH' ) ) { die( 'Direct access forbidden.' ); }

/**
 * Theme Includes
 */
require_once get_template_directory() .'/inc/init.php';

/**
 * TGM Plugin Activation
 */
{
	require_once dirname( __FILE__ ) . '/inc/class-tgm-plugin-activation.php';

	// INSTALL THE FRAMEWORK UNYSON 
	function _action_theme_register_required_plugins() {
		tgmpa( array(
			array(
				'name'      => 'Unyson',
				'slug'      => 'unyson',
				'force_activation'  => true,
				'required'  => true,
			),
			array(
				'name'      => 'Contact Form 7',
				'slug'      => 'contact-form-7',
				'force_activation'  => false,
				'required'  => false,
			),
			array(
	            'name'                  => 'Starry Custom Post Types & Shortcodes', // The plugin name
	            'slug'                  => 'starry-assets', // The plugin slug (typically the folder name)
	            'source'                => get_stylesheet_directory() . '/inc/starry-assets.zip', 
            	'force_activation'      => true, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            	'force_deactivation'    => true, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
	            'required'              => true, // If false, the plugin is only 'recommended' instead of required
	        )
		) );

	}
	add_action( 'tgmpa_register', '_action_theme_register_required_plugins' );

	//custom menu
	function wpb_custom_new_menu() {
	  register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
	}
	add_action( 'init', 'wpb_custom_new_menu' );

	function wpbsearchform( $form ) {
 
    $form = '<form class="navbar-form navbar-right" role="search" method="get" id="searchform" action="' . home_url( '/' ) . '" >
    <div class="search-element">
    <input class="form-control" placeholder="Search..." type="text" value="' . the_search_query() . '" name="s" id="s" />
    <button class="form-control" type="submit" id="searchsubmit" ><i class="fa fa-search"></i></button>
    </div>
    </form>';
 
    return $form;
}
 
add_shortcode('wpbsearch', 'wpbsearchform');
}
