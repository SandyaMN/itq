<?php
/**
 * The WOOCOMMERCE TEMPLATE
 */

get_header(); 
		//GET THEME HEADER CONTENT
		starry_header(); ?> 		
  	</header>
  	<!-- END HEADER -->

  	<!-- START MAIN CONTAINER -->
	<div id="the-container" class="main-container">
	
		<div class="container">
	  		<?php woocommerce_content(); ?>
		</div>
	
	  	<?php
		//GET EXTRA FOOTER
		starry_extrafooter();
		?>
		
	</div>
	<!-- END MAIN CONTAINER -->
	<?php 
get_footer();
