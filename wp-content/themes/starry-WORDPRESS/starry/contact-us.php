<?php
/**
 * Template Name: contact-us
 */

get_header(); ?>

    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/bootstrap.min.css';?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/font-awesome-4.7.0/css/font-awesome.min.css';?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/common.css';?>" />
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/home.css';?>" />
    <script src="<?php echo get_template_directory_uri().'/js/jquery.min.js';?>"></script>
    <script src="<?php echo get_template_directory_uri().'/js/bootstrap.min.js';?>"></script>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/aboutus.css';?>">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri().'/css/contact-us.css';?>" />
    <script src="<?php echo get_template_directory_uri().'/js/common.js';?>"></script>


    <div class="">
        <div class="banner">
            <img src="<?php echo get_template_directory_uri().'/assets/about-us/small_banner.png';?>" width="100%" />
            <div class="about-us bottom">
                <div class="container">
                    <h3>Contact Us</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        
        <div class="innercontainer">
            <div id="company-profile" class="tab-pane fade in active">
                <p class="path"><a href="home.html">Home</a> / <a class="current-page">Contact Us</a></p>
            </div>
            <h4>Contact</h4>
            <div class="accounts">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 primary-accounts">
                        <h2>Principal Accounts</h2>
                        <p class="blue-text">Toll Free Number</p>
                        <p class="blue-text-big">1800 266 4505 </p><br/>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <p class="blue-text bold">PILOT NUMBER</p>
                                <p>0124 611 1000</p>
                                <p>0446 684 0700</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <p class="blue-text bold">SITE</p>
                                <p>Gurgoan</p>
                                <p>Chennai</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <p class="blue-text bold">REGION</p>
                                <p>North & East</p>
                                <p>South & West </p>
                            </div>
                        </div>
                    </div>
                    <div class="vl"></div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 key-accounts">
                        <h2>Key Accounts</h2>
                        <p class="blue-text">Toll Free Number</p>
                        <p class="blue-text-big">1800 266 4501</p><br/>
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <p class="blue-text bold">PILOT NUMBER</p>
                                <p>0124 661 4999</p>
                                <p>0446 684 0800</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <p class="blue-text bold">SITE</p>
                                <p>Gurgoan</p>
                                <p>Chennai</p>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                <p class="blue-text bold">REGION</p>
                                <p>North & East</p>
                                <p>South & West </p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12 map-div">
                <div id="map">
                </div>
            </div>
            <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 contact-form">
                <form action="submit-query" id="submit-query" method="post">
                    <div class="travelPortcustHolder">
                        <div>
                            <h5>Are you presently a Travelport customer?</h5>
                            <label><input type="radio" name="isCustomer" value="Yes" /> Yes</label> <label><input type="radio"
                                    name="isCustomer" value="No" /> No</label>
                            <span id="isCustomerErr" class="formFieldError1" style="display:none">Please select one
                                option</span>
                        </div>
                        <br />
                        <div class="selectOptionHolder">
                            <ul>
                                <!--<li><label style="width:100%"><strong>Select the option that best describes your business:&nbsp;*</strong></label></li>-->
                                <li class="form-group"><label>Select your business <span class="required">*</span></label>
                                    <select name="business" class="form-control">
                                        <option>Select</option>
                                        <option>Business</option>
                                        <option>Corporate</option>
                                        <option>Global Account-Alliance</option>
                                        <option>Leisure</option>
                                        <option>New Agency</option>
                                        <option>Online - OTA</option>
                                        <option>Travel Management Company - TMC</option>
                                        <option>Airline</option>
                                        <option>Car</option>
                                        <option>Cruise</option>
                                        <option>Ground Handling Agent</option>
                                        <option>Hotel</option>
                                        <option>Limo Ground Transportation</option>
                                        <option>Rail</option>
                                        <option>Tour</option>
                                        <option>Advertising Marketing Solutions</option>
                                        <option>Corporations</option>
                                        <option>Developer</option>
                                    </select></li>
                                <!--<li><label>Select your region: *</label> <select name="region"><option>Select</option><option>East India</option><option>West India</option><option>North India</option><option>West India</option><option>SriLanka</option> </select></li>-->
                            </ul>
                        </div>

                        <ul class="formHolder">
                            <li>
                                <dl>
                                    <dt><label>First name <span class="required">*</span></label> </dt>

                                    <dd><input class="form-control" type="text" name="firstName" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Last Name <span class="required">*</span></label></dt>

                                    <dd> <input class="form-control" type="text" name="lastName" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Title <span class="required">*</span></label> </dt>

                                    <dd><input class="form-control" type="text" name="title" /></dd>
                                </dl>

                                <dl>
                                    <dt><label>Company <span class="required">*</span></label> </dt>
                                    <dd><input class="form-control" type="text" name="company" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Identification number(s) </label></dt>
                                    <dd><input class="form-control" type="text" name="identificationNumber" /></dd>
                                </dl>

                                <dl>
                                    <dt><label>IATA <span class="required">*</span></label></dt>
                                    <dd> <input type="radio" name="iata" value="Yes" /> Yes <input type="radio" name="iata"
                                            value="No" />
                                        No </dd>
                                </dl>
                                <dl>
                                    <dt><label> <span></span></label></dt>
                                    <dd> <input class="form-control" type="text" name="noniata" /></dd>
                                </dl>


                                <dl>
                                    <dt><label>PCC </label></dt>
                                    <dd> <input class="form-control" type="text" name="pcc" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>TIDS </label></dt>
                                    <dd> <input class="form-control" type="text" name="tids" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Address line 1 <span class="required">*</span></label> </dt>
                                    <dd><input class="form-control" type="text" name="addressLine1" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Address line 2 <span class="required">*</span></label> </dt>
                                    <dd><input class="form-control" type="text" name="addressLine2" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>City <span class="required">*</span></label> </dt>
                                    <dd><input class="form-control" type="text" name="city" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>State, Province, District <span class="required">*</span></label></dt>
                                    <dd> <input class="form-control" type="text" name="stateProvince" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Zip/Postal code <span class="required">*</span></label></dt>
                                    <dd> <input class="form-control" type="text" name="zip" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Country <span class="required">*</span></label></dt>
                                    <dd> <input class="form-control" type="text" name="country" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Telephone <span class="required">*</span> <i>(include country or area
                                                code)</i></label></dt>
                                    <dd> <input class="form-control" type="text" name="telephone" /></dd>
                                </dl>



                                <dl>
                                    <dt><label>Email Address <span class="required">*</span></label></dt>
                                    <dd> <input class="form-control" type="text" name="email" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Contact me by </label></dt>
                                    <dd> <select class="form-control" name="contactMeBy">
                                            <option>Select</option>
                                            <option>Email</option>
                                            <option>Phone</option>
                                        </select></dd>
                                </dl>
                                <dl>
                                    <dt><label>Website Address </label></dt>
                                    <dd> <input class="form-control" type="text" name="websiteAddress" /></dd>
                                </dl>
                                <dl>
                                    <dt><label>Comments </label></dt>
                                    <dd><textarea class="form-control" name="comments"></textarea></dd>
                                </dl>
                                <br />
                                <dl class="buttons"><dt class="loading">
                                        <span id="loader">Please wait...</span>
                                    </dt>
                                    <dd>
                                        <input class="form-control" type="hidden" name="captchafield" /><input class="submit btn btn-primary"
                                            id="trkbtn" type="submit" value="Submit">

                                    </dd>
                                </dl>

                            </li>
                        </ul>
                    </div>
                </form>
            </div>

        </div>
    </div>
   

    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri().'/js/map.js';?>"></script>

 <?php //GET EXTRA FOOTER
        starry_extrafooter();
        ?>
        <?php
get_footer(); ?>