<?php
/**
 * The template for displaying the footer
 */
?>
		<!-- START FOOTER -->
	  	<!--<footer id="footer" class="with-separation-top">

	  		<?php
	  		// IF YOU WANT TO DISPLAY WIDGET AREA IN THE FOOTER
			$showfooterwidgets = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('footerwidgets') : '';
			if(  $showfooterwidgets == "yes" ) :
			?>
		  		<?php get_sidebar( 'footer' ); ?>
			<?php endif; ?>

			<?php
			// IF YOU WANT TO DISPALY THE COPYRIGHTS
			$showcopyrights = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('footercopyrightdisplay') : '';
			if(  $showcopyrights == "yes" ) :
			?>
			  	<div id="copyright" class="animate-me fadeInUp">
			  		<div class="container">
			  			<?php
			  			// GET COPYRIGHT CONTENTS
						$copyrightscontent = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('footercopyrightcontent') : '';
						?>
				  		<p><?php echo $copyrightscontent; ?></p>

				  		<?php 
				  		// COPYRIGHT MENU
				  		if ( has_nav_menu('copyright') ) {
					  		$settings_footer = array(
								'theme_location'  => 'copyright',
								'container'       => '',
								'items_wrap'      => '<ul id="footer-navigation" class="%2$s">%3$s</ul>',
							);
							wp_nav_menu( $settings_footer ); 
						}
						?>
			  		</div>
			  	</div>
			<?php endif; ?>
	  	</footer>-->
	  	<!-- END FOOTER -->

	  	<footer>
    <div class="footer-top">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <img class="footer-logo" src="<?php echo get_template_directory_uri().'/assets/interglobe_logo.png';?>" width="50%" />
                <p><br /> InterGlobe Technology Quotient Strategic business unit of InterGlobe Enterprises Which is a leader
                    in aviation, hospitality and travel related services... <br />
                    <a href="<?php echo get_home_url(); ?>/about/">Read More</a>
                </p>
                <div class="social-links">

                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 footer-menu">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li><a href="<?php echo get_home_url(); ?>/company-profile/">About Us</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/product-travelport/">Products</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/press-release/">Press Room</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/hr-vision/">Human Capital</a></li>
                        <li><a href="">Training</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/contact-us/">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <ul>
                        <li><a href="">Privacy Policy</a></li>
                        <li><a href="">Sitemap</a></li>
                        <li><a href="">Terms and condition</a></li>
                        <li><a href="">Disclaimer</a></li>
                        <li><a href="">Interglobe Business</a></li>
                        <li><a href="">Testimonials</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p>Copyright 2019 &copy; InterGlobe Technology Quotient Pvt Ltd. All rights reserved.</p>
    </div>
</footer>
	  	
	  	<?php
	  	// IF YOU WANT TO DISPLAY SCROLL BUTTON
		$scrolltop = ( function_exists( 'fw_get_db_settings_option' ) ) ? fw_get_db_settings_option('scrolltop') : '';
		if(  $scrolltop == "yes" ) :
		?>
			<!-- SCROLL TOP -->
			<a href="#header" id="scroll-top" class="fadeInRight animate-me"><i class="fa fa-angle-double-up"></i></a>
		<?php endif; ?>
  	</div>

    <!-- SCRIPTS -->
    <?php wp_footer(); ?>
  </body>
  <!-- END BODY -->
</html>