<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme and one
 * of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query,
 * e.g., it puts together the home page when no home.php file exists.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 */
get_header(); ?>
		<?php //GET THEME HEADER CONTENT
		starry_header(); ?> 		
  	</header>
  	<!-- END HEADER -->
       
  	    <div class="">
            <section>
            <div class="video clear-fix">
                <video autoplay muted loop>
                    <source src="<?php echo get_template_directory_uri().'/assets/banner-video.mp4" type="video/mp4';?>">
                </video>
            </div>
        </section>
    </div>
    <div class="container-fluid top-div">
        <div class="container">
            <div class="col-lg-3 col-md-3 col-sm-3 list-count">
                <img src="<?php echo get_template_directory_uri().'/assets/hm1.png';?>" width="90" />
                <p> <span> More than <b class="f-s-26"> 50% </b></span><br><span>Market share</span></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 list-count">
                <img src="<?php echo get_template_directory_uri().'/assets/hm2.png';?>" width="90" />
                <p>400 cities<br /><span>Extended network</span></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 list-count">
                <img src="<?php echo get_template_directory_uri().'/assets/hm3.png';?>" width="90" />
                <p>19<br /><span>Dedicated offices</span></p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 list-count">
                <img src="<?php echo get_template_directory_uri().'/assets/hm4.png';?>" width="90" />
                <p>36,000<br /><span>Agency terminals</span></p>
            </div>
        </div>
    </div>

    <div class="container-fluid products">
        <div class="container">
            <h3 class="">Our Offerings</h3><br />
            <div class="product-slider">
                <div class="product-div slider">
                    <div class="theme product-slick-slider">
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/Galileo.png';?>" />
                            <div class="product-bottom">
                                <h4>Travelport Smartpoint (GALILEO)</h4>
                                <p>Travelport Smartpoint is the ultimate travel technology for today’s connected digital age. It puts the world’s travel...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/product-travelport/">Read more...</a>
                            </div>
                        </div>
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/ndc.png';?>" />
                            <div class="product-bottom">
                                <h4>NDC</h4>
                                <p>In today’s exciting world of evolving distribution strategies, we don’t believe the conversation should be about any single source of content,...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/ndc/">Read more...</a>
                            </div>
                        </div>
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/airline.png';?>" />
                            <div class="product-bottom">
                                <h4>Airline</h4>
                                <p>InterGlobe Technology Quotient, is a strategic business unit of InterGlobe Enterprises
                                    which is a leader in aviation, hospitality and travel...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/airlines/">Read more...</a>
                            </div>
                        </div>
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/ecommerce_api.png';?>" />
                            <div class="product-bottom">
                                <h4>Ecommerce API</h4>
                                <p>Travelport e-Pricing technology has revolutionized the worldwide travel shopping experience. It is built upon the first multi-server based ...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/ecommerce-api/">Read more...</a>
                            </div>
                        </div>
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/online_solutions.png';?>" />
                            <div class="product-bottom">
                                <h4>Online Solutions </h4>
                                <p>An internet booking engine developed for B2B sector to provide online booking tool for their customers. In addition, it helps in consolidation...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/online-solutions/">Read more...</a>
                            </div>
                        </div>
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/enhanced-utilities.png';?>" />
                            <div class="product-bottom">
                                <h4>Enhanced Utilities </h4>
                                <p>An application that allows the user to generate ticket issuance reports in detail. It can also be configured to work on a central machine in the agency...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/enhanced-utilities/">Read more...</a>
                            </div>
                        </div>
                        <div class="product">
                            <img src="<?php echo get_template_directory_uri().'/assets/worldspan.png';?>" />
                            <div class="product-bottom">
                                <h4>Worldspan </h4>
                                <p>Worldspan Go! brings a spectrum of travel planning and management solutions to the travel agent's desktop. Using browser-based...</p>
                                <br />
                                <a class="read-more" href="<?php echo get_home_url(); ?>/worldspan/">Read more...</a>
                            </div>
                        </div>
                    </div>
                    <!-- <a id="go-left"><img src="<?php echo get_template_directory_uri().'/assets/arrow-left.png';?>"  /></a>
                    <a id="go-right"><img src="<?php echo get_template_directory_uri().'/assets/arrow-right.png';?>"  /></a> -->
                </div>
            </div>
        </div>
    </div>
    <!-- our business -->
    <!-- end our business -->
    <div class="container-fluid products">
        <div class="container-fluid whats-new">
            <div class="container">
                <h4 class="bold">Whats New</h4>
                <div class="new-slider">
                    <ul class="theme">
                        <li class="new">
                            <a href="<?php echo get_home_url(); ?>/press-release/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/download_icons.png';?>" /></a>
                            <div class="new-right">
                                <h5>March 2019</h5>
                                <p>Travelport launches global campaign to encourage more support for airline passengers with intellectual disabilities
                                </p>
                                <br />
                            </div>
                        </li>
                        <li class="new">
                            <a href="<?php echo get_home_url(); ?>/press-release/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/download_icons.png';?>" /></a>
                            <div class="new-right">
                                <h5>March 2019</h5>
                                <p>Where are people flying in from to celebrate Holi in India?
                                </p>
                                <br />
                            </div>
                        </li>
                        <li class="new">
                            <a href="<?php echo get_home_url(); ?>/press-release/" target="_blank"><img src="<?php echo get_template_directory_uri().'/assets/download_icons.png';?>" /></a>
                            <div class="new-right">
                                <h5>March 2019</h5>
                                <p>Japan Airlines and Travelport Agree to Launch New Joint Venture
                                </p>
                                <br />
                            </div>
                        </li>
                        <!-- <li class="new">
                            <a><img src="<?php echo get_template_directory_uri().'/assets/download_icons.png';?>" /></a>
                            <div class="new-right">
                                <h5>Airline Merchandising Technology</h5>
                                <p>Travelport continues to drive industry momentum with its pioneering airline merchandising
                                    technology
                                </p>
                                <br />
                            </div>
                        </li> -->
                    </ul>
                    <!-- <a id="new-left"><img src="<?php echo get_template_directory_uri().'/assets/arrow-left.png';?>" width="60%" /></a>
                    <a id="new-right"><img src="<?php echo get_template_directory_uri().'/assets/arrow-right.png';?>" width="60%" /></a> -->
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </div>
                <div class="container testimonial">
                    <h4 class="bold">Testimonials</h4>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <div class="item active">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 testimonial-pic">
                                    <img src="<?php echo get_template_directory_uri().'/assets/user-icon.png';?>" alt="" width="100%"/>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-6 saying">
                                    <p><q><i>HRG dnata India surely values its partnership with Travelport. The relationship between
                                        the two organizations is build on mutual trust and confidence...</i></q></p><br />
                                    <p class="name">Ashish Kishore - HRG dnata</p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 testimonial-pic">
                                    <!-- <img src="<?php echo get_template_directory_uri().'/assets/user-blank-icon.png';?>" alt="" width="100%"/> -->
                                    <img src="<?php echo get_template_directory_uri().'/assets/user-icon.png';?>" alt="" width="100%"/>

                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-6 saying">
                                    <p><q><i>We have been using Galileo for a number of years now. The product on its own is excellent.
                                        There is constant innovation to provide us with newer and better technology for us
                                        to use in the travel industry</i></q></p><br />
                                    <p class="name">Anil Punjabi - AR-ES Travels Pvt. Ltd.</p>
                                </div>
                            </div>

                            <div class="item">
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 testimonial-pic">
                                <!-- <img src="<?php echo get_template_directory_uri().'/assets/user-blank-icon.png';?>" alt="" width="100%"/>                                     -->
                                <img src="<?php echo get_template_directory_uri().'/assets/user-icon.png';?>" alt="" width="100%"/>

                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-6 saying">
                                    <p><q><i>The association with InterGlobe Technology Quotient and Galileo has indeed been a
                                        remarkable opportunity in becoming a strong tool to harness optimum potentials with
                                        an innovative application</i></q></p><br />
                                    <p class="name">A. Mahapatra - Twings.net</p>
                                </div>
                            </div>
                        </div>

                        <!-- Left and right controls -->

                    </div>
                </div>
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
        </div>
        <div class="container partners">
            <h4>Our Partners</h4>
            <div class="carousel slide" data-ride="carousel" data-type="multi" data-interval="3000" id="partnercarousel">
                <div class="carousel-inner">
                    <div class="item active">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/ezeego.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/goibibo.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/easemytrip.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/makemytrip.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/travelhouse.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/via.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/jetairways.png';?>" class="img-responsive">
                            </a></li>
                    </div>
                    <div class="item">
                        <li><a href="#">
                                <img src="<?php echo get_template_directory_uri().'/assets/Air_India_Logo.png';?>" class="img-responsive">
                            </a></li>
                    </div>

                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#partnercarousel" data-slide="prev"><i class="fa fa-angle-left"></i></a>
                <a class="right carousel-control" href="#partnercarousel" data-slide="next"><i class="fa fa-angle-right"></i></a>
            </div>
            <!--<div class="blog">
                <h4>Blog</h4>
                <div class="blog-slider">
                    <ul class="blog-theme">
                        <li class="blog-li">
                            <h5>Dummy text HRG dnata India surely </h5>
                            <img src="<?php echo get_template_directory_uri().'/assets/blog1.png';?>" />
                            <div class="blog-right">
                                <p>HRG dnata India surely values its partnership with Travelport. The relationship between
                                    the two organizations is build on mutual trust and confidence</p>
                                <br />
                                <a class="read-more">Read more...</a><br /><br />
                            </div>
                        </li>
                        <li class="blog-li">
                            <h5>Dummy text HRG dnata India surely </h5>
                            <img src="<?php echo get_template_directory_uri().'/assets/blog2.png';?>" />
                            <div class="blog-right">
                                <p>HRG dnata India surely values its partnership with Travelport. The relationship between
                                    the two organizations is build on mutual trust and confidence</p>
                                <br />
                                <a class="read-more">Read more...</a><br /><br />
                            </div>
                        </li>
                        <li class="blog-li">
                            <h5>Dummy text HRG dnata India surely </h5>
                            <img src="<?php echo get_template_directory_uri().'/assets/blog1.png';?>" />
                            <div class="blog-right">
                                <p>HRG dnata India surely values its partnership with Travelport. The relationship between
                                    the two organizations is build on mutual trust and confidence</p>
                                <br />
                                <a class="read-more">Read more...</a><br /><br />
                            </div>
                        </li>
                        <li class="blog-li">
                            <h5>Dummy text HRG dnata India surely </h5>
                            <img src="<?php echo get_template_directory_uri().'/assets/blog2.png';?>" />
                            <div class="blog-right">
                                <p>HRG dnata India surely values its partnership with Travelport. The relationship between
                                    the two organizations is build on mutual trust and confidence</p>
                                <br />
                                <a class="read-more">Read more...</a><br /><br />
                            </div>
                        </li>
                    </ul>
                    <a id="blog-left"><img src="<?php echo get_template_directory_uri().'/assets/prev.png';?>" width="60%" /></a>
                    <a id="blog-right"><img src="<?php echo get_template_directory_uri().'/assets/next.png';?>" width="60%" /></a>
                    <p>&nbsp;</p>
                    <p>&nbsp;</p>
                </div>
            </div>-->
        </div>

        <div class="blog container">
        <h4 >Blog</h4>
            <?php post_carousel_id('250'); ?>
        </div>
    </div>

	
<?php
get_footer();
