<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'itq_new' );

/** MySQL database username */
define( 'DB_USER', 'itqadmin' );

/** MySQL database password */
define( 'DB_PASSWORD', "password" );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'egNzFTGW>:G 5;nBQ^!mv6n/;A#NLh0K ZFID9!1AbN|sfo:R. F%PH*HlB!H5QV' );
define( 'SECURE_AUTH_KEY',  '-W(Q&odnY3i3!FEbvj5#N|cE-IaksL/2oc&-X6GL%#li3,?_o*:o0.1WaKFeMxp~' );
define( 'LOGGED_IN_KEY',    ':>=KRL}|WM./eHD`<]f!^K)StF$V2lUb !pHtai`o9}t_Yn-.j<IJU@k%Z:`?NrD' );
define( 'NONCE_KEY',        'nU1AV31DOY_awuxA*di^(g,yy# #,@F66;=4aAqr`Dbp)+cv47pTEFh[0hsay;qm' );
define( 'AUTH_SALT',        '64n%*nF3Ni[|pG=Qc1:LQ-f>SY&](`5mjd3m=^RdpXl#w$lvA|G4lwI^~elaZeEl' );
define( 'SECURE_AUTH_SALT', '7^3/lo>>zw~Ga{Y_zB@t%p#0yD2L#FK4&m@FW,g][rTMC!C)IZ1sbY@uORt+:G!n' );
define( 'LOGGED_IN_SALT',   '<1jk5TG^:0j<DsYsJwh<Mgf|@*_HW:be&JaXofx9:sg+)/p.0s1d95jw$enr@Pbl' );
define( 'NONCE_SALT',       '3V9W>vg^4T~I-8T6yom?_X|D.38#XjGA$uV)T/;z.5j*#R@tL0_h{Bg-UJ&dO.gB' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

