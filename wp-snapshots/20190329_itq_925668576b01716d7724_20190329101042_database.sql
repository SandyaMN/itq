/* DUPLICATOR-LITE (PHP BUILD MODE) MYSQL SCRIPT CREATED ON : 2019-03-29 10:10:44 */

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) unsigned NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_duplicator_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) NOT NULL,
  `hash` varchar(50) NOT NULL,
  `status` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `owner` varchar(60) NOT NULL,
  `package` mediumblob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

CREATE TABLE `wp_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) unsigned NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=InnoDB AUTO_INCREMENT=924 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=780 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=InnoDB AUTO_INCREMENT=244 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

CREATE TABLE `wp_users` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;


/* INSERT TABLE DATA: wp_comments */
INSERT INTO `wp_comments` VALUES("1", "1", "A WordPress Commenter", "wapuu@wordpress.example", "https://wordpress.org/", "", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "Hi, this is a comment.\nTo get started with moderating, editing, and deleting comments, please visit the Comments screen in the dashboard.\nCommenter avatars come from <a href=\"https://gravatar.com\">Gravatar</a>.", "0", "1", "", "", "0", "0");

/* INSERT TABLE DATA: wp_duplicator_packages */
INSERT INTO `wp_duplicator_packages` VALUES("13", "20190329_itq", "925668576b01716d7724_20190329101042", "21", "2019-03-29 10:10:44", "unknown", "O:11:\"DUP_Package\":25:{s:7:\"Created\";s:19:\"2019-03-29 10:10:42\";s:7:\"Version\";s:6:\"1.3.10\";s:9:\"VersionWP\";s:5:\"5.1.1\";s:9:\"VersionDB\";s:7:\"10.1.38\";s:10:\"VersionPHP\";s:5:\"7.3.3\";s:9:\"VersionOS\";s:5:\"Linux\";s:2:\"ID\";i:13;s:4:\"Name\";s:12:\"20190329_itq\";s:4:\"Hash\";s:35:\"925668576b01716d7724_20190329101042\";s:8:\"NameHash\";s:48:\"20190329_itq_925668576b01716d7724_20190329101042\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:38:\"/opt/lampp/htdocs/ITQ/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://localhost/ITQ/wp-snapshots/\";s:8:\"ScanFile\";s:58:\"20190329_itq_925668576b01716d7724_20190329101042_scan.json\";s:10:\"TimerStart\";i:-1;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";d:21;s:6:\"WPUser\";s:7:\"unknown\";s:7:\"Archive\";O:11:\"DUP_Archive\":21:{s:10:\"FilterDirs\";s:0:\"\";s:11:\"FilterFiles\";s:0:\"\";s:10:\"FilterExts\";s:0:\"\";s:13:\"FilterDirsAll\";a:0:{}s:14:\"FilterFilesAll\";a:0:{}s:13:\"FilterExtsAll\";a:0:{}s:8:\"FilterOn\";i:0;s:12:\"ExportOnlyDB\";i:0;s:4:\"File\";s:60:\"20190329_itq_925668576b01716d7724_20190329101042_archive.zip\";s:6:\"Format\";s:3:\"ZIP\";s:7:\"PackDir\";s:21:\"/opt/lampp/htdocs/ITQ\";s:4:\"Size\";i:0;s:4:\"Dirs\";a:0:{}s:5:\"Files\";a:0:{}s:10:\"FilterInfo\";O:23:\"DUP_Archive_Filter_Info\":8:{s:4:\"Dirs\";O:34:\"DUP_Archive_Filter_Scope_Directory\":5:{s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:6:\"Global\";a:0:{}s:8:\"Instance\";a:0:{}}s:5:\"Files\";O:29:\"DUP_Archive_Filter_Scope_File\":6:{s:4:\"Size\";a:0:{}s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:6:\"Global\";a:0:{}s:8:\"Instance\";a:0:{}}s:4:\"Exts\";O:29:\"DUP_Archive_Filter_Scope_Base\":3:{s:4:\"Core\";a:0:{}s:6:\"Global\";a:0:{}s:8:\"Instance\";a:0:{}}s:9:\"UDirCount\";i:0;s:10:\"UFileCount\";i:0;s:9:\"UExtCount\";i:0;s:8:\"TreeSize\";a:0:{}s:11:\"TreeWarning\";a:0:{}}s:14:\"RecursiveLinks\";a:0:{}s:10:\"file_count\";i:-1;s:10:\"\0*\0Package\";O:11:\"DUP_Package\":25:{s:7:\"Created\";s:19:\"2019-03-29 10:10:42\";s:7:\"Version\";s:6:\"1.3.10\";s:9:\"VersionWP\";s:5:\"5.1.1\";s:9:\"VersionDB\";s:7:\"10.1.38\";s:10:\"VersionPHP\";s:5:\"7.3.3\";s:9:\"VersionOS\";s:5:\"Linux\";s:2:\"ID\";N;s:4:\"Name\";s:12:\"20190329_itq\";s:4:\"Hash\";s:35:\"925668576b01716d7724_20190329101042\";s:8:\"NameHash\";s:48:\"20190329_itq_925668576b01716d7724_20190329101042\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:38:\"/opt/lampp/htdocs/ITQ/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://localhost/ITQ/wp-snapshots/\";s:8:\"ScanFile\";N;s:10:\"TimerStart\";i:-1;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";i:0;s:6:\"WPUser\";N;s:7:\"Archive\";r:23;s:9:\"Installer\";O:13:\"DUP_Installer\":11:{s:4:\"File\";s:62:\"20190329_itq_925668576b01716d7724_20190329101042_installer.php\";s:4:\"Size\";i:0;s:10:\"OptsDBHost\";s:0:\"\";s:10:\"OptsDBPort\";s:0:\"\";s:10:\"OptsDBName\";s:0:\"\";s:10:\"OptsDBUser\";s:0:\"\";s:12:\"OptsSecureOn\";i:0;s:14:\"OptsSecurePass\";s:0:\"\";s:13:\"numFilesAdded\";i:0;s:12:\"numDirsAdded\";i:0;s:10:\"\0*\0Package\";r:63;}s:8:\"Database\";O:12:\"DUP_Database\":15:{s:4:\"Type\";s:5:\"MySQL\";s:4:\"Size\";N;s:4:\"File\";s:61:\"20190329_itq_925668576b01716d7724_20190329101042_database.sql\";s:4:\"Path\";N;s:12:\"FilterTables\";s:0:\"\";s:8:\"FilterOn\";i:0;s:4:\"Name\";N;s:10:\"Compatible\";s:0:\"\";s:8:\"Comments\";s:19:\"Source distribution\";s:4:\"info\";O:16:\"DUP_DatabaseInfo\":15:{s:9:\"buildMode\";N;s:13:\"collationList\";a:0:{}s:17:\"isTablesUpperCase\";N;s:15:\"isNameUpperCase\";N;s:4:\"name\";N;s:15:\"tablesBaseCount\";N;s:16:\"tablesFinalCount\";N;s:14:\"tablesRowCount\";N;s:16:\"tablesSizeOnDisk\";N;s:18:\"varLowerCaseTables\";i:0;s:7:\"version\";N;s:14:\"versionComment\";N;s:18:\"tableWiseRowCounts\";a:1:{s:14:\"wp_commentmeta\";s:1:\"0\";}s:33:\"\0DUP_DatabaseInfo\0intFieldsStruct\";a:0:{}s:42:\"\0DUP_DatabaseInfo\0indexProcessedSchemaSize\";a:0:{}}s:10:\"\0*\0Package\";r:1;s:25:\"\0DUP_Database\0dbStorePath\";s:100:\"/opt/lampp/htdocs/ITQ/wp-snapshots/tmp/20190329_itq_925668576b01716d7724_20190329101042_database.sql\";s:23:\"\0DUP_Database\0EOFMarker\";s:0:\"\";s:26:\"\0DUP_Database\0networkFlush\";b:0;s:19:\"sameNameTableExists\";b:0;}s:13:\"BuildProgress\";O:18:\"DUP_Build_Progress\":12:{s:17:\"thread_start_time\";N;s:11:\"initialized\";b:0;s:15:\"installer_built\";b:0;s:15:\"archive_started\";b:0;s:20:\"archive_has_database\";b:0;s:13:\"archive_built\";b:0;s:21:\"database_script_built\";b:0;s:6:\"failed\";b:0;s:7:\"retries\";i:0;s:14:\"build_failures\";a:0:{}s:19:\"validation_failures\";a:0:{}s:27:\"\0DUP_Build_Progress\0package\";r:63;}}s:29:\"\0DUP_Archive\0tmpFilterDirsAll\";a:0:{}s:24:\"\0DUP_Archive\0wpCorePaths\";a:5:{i:0;s:30:\"/opt/lampp/htdocs/ITQ/wp-admin\";i:1;s:40:\"/opt/lampp/htdocs/ITQ/wp-content/uploads\";i:2;s:42:\"/opt/lampp/htdocs/ITQ/wp-content/languages\";i:3;s:39:\"/opt/lampp/htdocs/ITQ/wp-content/themes\";i:4;s:33:\"/opt/lampp/htdocs/ITQ/wp-includes\";}s:29:\"\0DUP_Archive\0wpCoreExactPaths\";a:2:{i:0;s:21:\"/opt/lampp/htdocs/ITQ\";i:1;s:32:\"/opt/lampp/htdocs/ITQ/wp-content\";}}s:9:\"Installer\";r:86;s:8:\"Database\";r:98;s:13:\"BuildProgress\";r:130;}");

/* INSERT TABLE DATA: wp_options */
INSERT INTO `wp_options` VALUES("1", "siteurl", "http://localhost/ITQ", "yes");
INSERT INTO `wp_options` VALUES("2", "home", "http://localhost/ITQ", "yes");
INSERT INTO `wp_options` VALUES("3", "blogname", "ITQ", "yes");
INSERT INTO `wp_options` VALUES("4", "blogdescription", "Just another WordPress site", "yes");
INSERT INTO `wp_options` VALUES("5", "users_can_register", "0", "yes");
INSERT INTO `wp_options` VALUES("6", "admin_email", "sandya.mn@mobinius.com", "yes");
INSERT INTO `wp_options` VALUES("7", "start_of_week", "1", "yes");
INSERT INTO `wp_options` VALUES("8", "use_balanceTags", "0", "yes");
INSERT INTO `wp_options` VALUES("9", "use_smilies", "1", "yes");
INSERT INTO `wp_options` VALUES("10", "require_name_email", "1", "yes");
INSERT INTO `wp_options` VALUES("11", "comments_notify", "1", "yes");
INSERT INTO `wp_options` VALUES("12", "posts_per_rss", "10", "yes");
INSERT INTO `wp_options` VALUES("13", "rss_use_excerpt", "0", "yes");
INSERT INTO `wp_options` VALUES("14", "mailserver_url", "mail.example.com", "yes");
INSERT INTO `wp_options` VALUES("15", "mailserver_login", "login@example.com", "yes");
INSERT INTO `wp_options` VALUES("16", "mailserver_pass", "password", "yes");
INSERT INTO `wp_options` VALUES("17", "mailserver_port", "110", "yes");
INSERT INTO `wp_options` VALUES("18", "default_category", "1", "yes");
INSERT INTO `wp_options` VALUES("19", "default_comment_status", "open", "yes");
INSERT INTO `wp_options` VALUES("20", "default_ping_status", "open", "yes");
INSERT INTO `wp_options` VALUES("21", "default_pingback_flag", "1", "yes");
INSERT INTO `wp_options` VALUES("22", "posts_per_page", "10", "yes");
INSERT INTO `wp_options` VALUES("23", "date_format", "F j, Y", "yes");
INSERT INTO `wp_options` VALUES("24", "time_format", "g:i a", "yes");
INSERT INTO `wp_options` VALUES("25", "links_updated_date_format", "F j, Y g:i a", "yes");
INSERT INTO `wp_options` VALUES("26", "comment_moderation", "0", "yes");
INSERT INTO `wp_options` VALUES("27", "moderation_notify", "1", "yes");
INSERT INTO `wp_options` VALUES("28", "permalink_structure", "/%postname%/", "yes");
INSERT INTO `wp_options` VALUES("29", "rewrite_rules", "a:86:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}", "yes");
INSERT INTO `wp_options` VALUES("30", "hack_file", "0", "yes");
INSERT INTO `wp_options` VALUES("31", "blog_charset", "UTF-8", "yes");
INSERT INTO `wp_options` VALUES("32", "moderation_keys", "", "no");
INSERT INTO `wp_options` VALUES("33", "active_plugins", "a:1:{i:0;s:25:\"duplicator/duplicator.php\";}", "yes");
INSERT INTO `wp_options` VALUES("34", "category_base", "", "yes");
INSERT INTO `wp_options` VALUES("35", "ping_sites", "http://rpc.pingomatic.com/", "yes");
INSERT INTO `wp_options` VALUES("36", "comment_max_links", "2", "yes");
INSERT INTO `wp_options` VALUES("37", "gmt_offset", "0", "yes");
INSERT INTO `wp_options` VALUES("38", "default_email_category", "1", "yes");
INSERT INTO `wp_options` VALUES("39", "recently_edited", "a:5:{i:0;s:74:\"/opt/lampp/htdocs/ITQ/wp-content/themes/starry-WORDPRESS/starry/header.php\";i:2;s:87:\"/opt/lampp/htdocs/ITQ/wp-content/themes/starry-WORDPRESS/starry/page-templates/blog.php\";i:3;s:74:\"/opt/lampp/htdocs/ITQ/wp-content/themes/starry-WORDPRESS/starry/single.php\";i:4;s:74:\"/opt/lampp/htdocs/ITQ/wp-content/themes/starry-WORDPRESS/starry/footer.php\";i:5;s:73:\"/opt/lampp/htdocs/ITQ/wp-content/themes/starry-WORDPRESS/starry/style.css\";}", "no");
INSERT INTO `wp_options` VALUES("40", "template", "starry-WORDPRESS/starry", "yes");
INSERT INTO `wp_options` VALUES("41", "stylesheet", "starry-WORDPRESS/starry", "yes");
INSERT INTO `wp_options` VALUES("42", "comment_whitelist", "1", "yes");
INSERT INTO `wp_options` VALUES("43", "blacklist_keys", "", "no");
INSERT INTO `wp_options` VALUES("44", "comment_registration", "0", "yes");
INSERT INTO `wp_options` VALUES("45", "html_type", "text/html", "yes");
INSERT INTO `wp_options` VALUES("46", "use_trackback", "0", "yes");
INSERT INTO `wp_options` VALUES("47", "default_role", "subscriber", "yes");
INSERT INTO `wp_options` VALUES("48", "db_version", "44719", "yes");
INSERT INTO `wp_options` VALUES("49", "uploads_use_yearmonth_folders", "1", "yes");
INSERT INTO `wp_options` VALUES("50", "upload_path", "", "yes");
INSERT INTO `wp_options` VALUES("51", "blog_public", "1", "yes");
INSERT INTO `wp_options` VALUES("52", "default_link_category", "2", "yes");
INSERT INTO `wp_options` VALUES("53", "show_on_front", "posts", "yes");
INSERT INTO `wp_options` VALUES("54", "tag_base", "", "yes");
INSERT INTO `wp_options` VALUES("55", "show_avatars", "1", "yes");
INSERT INTO `wp_options` VALUES("56", "avatar_rating", "G", "yes");
INSERT INTO `wp_options` VALUES("57", "upload_url_path", "", "yes");
INSERT INTO `wp_options` VALUES("58", "thumbnail_size_w", "150", "yes");
INSERT INTO `wp_options` VALUES("59", "thumbnail_size_h", "150", "yes");
INSERT INTO `wp_options` VALUES("60", "thumbnail_crop", "1", "yes");
INSERT INTO `wp_options` VALUES("61", "medium_size_w", "300", "yes");
INSERT INTO `wp_options` VALUES("62", "medium_size_h", "300", "yes");
INSERT INTO `wp_options` VALUES("63", "avatar_default", "mystery", "yes");
INSERT INTO `wp_options` VALUES("64", "large_size_w", "1024", "yes");
INSERT INTO `wp_options` VALUES("65", "large_size_h", "1024", "yes");
INSERT INTO `wp_options` VALUES("66", "image_default_link_type", "none", "yes");
INSERT INTO `wp_options` VALUES("67", "image_default_size", "", "yes");
INSERT INTO `wp_options` VALUES("68", "image_default_align", "", "yes");
INSERT INTO `wp_options` VALUES("69", "close_comments_for_old_posts", "0", "yes");
INSERT INTO `wp_options` VALUES("70", "close_comments_days_old", "14", "yes");
INSERT INTO `wp_options` VALUES("71", "thread_comments", "1", "yes");
INSERT INTO `wp_options` VALUES("72", "thread_comments_depth", "5", "yes");
INSERT INTO `wp_options` VALUES("73", "page_comments", "0", "yes");
INSERT INTO `wp_options` VALUES("74", "comments_per_page", "50", "yes");
INSERT INTO `wp_options` VALUES("75", "default_comments_page", "newest", "yes");
INSERT INTO `wp_options` VALUES("76", "comment_order", "asc", "yes");
INSERT INTO `wp_options` VALUES("77", "sticky_posts", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("78", "widget_categories", "a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("79", "widget_text", "a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("80", "widget_rss", "a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("81", "uninstall_plugins", "a:0:{}", "no");
INSERT INTO `wp_options` VALUES("82", "timezone_string", "", "yes");
INSERT INTO `wp_options` VALUES("83", "page_for_posts", "0", "yes");
INSERT INTO `wp_options` VALUES("84", "page_on_front", "0", "yes");
INSERT INTO `wp_options` VALUES("85", "default_post_format", "0", "yes");
INSERT INTO `wp_options` VALUES("86", "link_manager_enabled", "0", "yes");
INSERT INTO `wp_options` VALUES("87", "finished_splitting_shared_terms", "1", "yes");
INSERT INTO `wp_options` VALUES("88", "site_icon", "0", "yes");
INSERT INTO `wp_options` VALUES("89", "medium_large_size_w", "768", "yes");
INSERT INTO `wp_options` VALUES("90", "medium_large_size_h", "0", "yes");
INSERT INTO `wp_options` VALUES("91", "wp_page_for_privacy_policy", "3", "yes");
INSERT INTO `wp_options` VALUES("92", "show_comments_cookies_opt_in", "1", "yes");
INSERT INTO `wp_options` VALUES("93", "initial_db_version", "44719", "yes");
INSERT INTO `wp_options` VALUES("94", "wp_user_roles", "a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:61:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}", "yes");
INSERT INTO `wp_options` VALUES("95", "fresh_site", "0", "yes");
INSERT INTO `wp_options` VALUES("96", "widget_search", "a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("97", "widget_recent-posts", "a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("98", "widget_recent-comments", "a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("99", "widget_archives", "a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("100", "widget_meta", "a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("101", "sidebars_widgets", "a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:7:\"widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}", "yes");
INSERT INTO `wp_options` VALUES("102", "widget_pages", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("103", "widget_calendar", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("104", "widget_media_audio", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("105", "widget_media_image", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("106", "widget_media_gallery", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("107", "widget_media_video", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("108", "widget_tag_cloud", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("109", "widget_nav_menu", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("110", "widget_custom_html", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("111", "cron", "a:5:{i:1553857741;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1553868541;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1553868564;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1553868567;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}", "yes");
INSERT INTO `wp_options` VALUES("112", "theme_mods_twentynineteen", "a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1553177435;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}", "yes");
INSERT INTO `wp_options` VALUES("125", "can_compress_scripts", "0", "no");
INSERT INTO `wp_options` VALUES("130", "current_theme", "Starry", "yes");
INSERT INTO `wp_options` VALUES("131", "theme_mods_starry-WORDPRESS/starry", "a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:14:\"my-custom-menu\";i:3;}s:18:\"custom_css_post_id\";i:6;}", "yes");
INSERT INTO `wp_options` VALUES("132", "theme_switched", "", "yes");
INSERT INTO `wp_options` VALUES("133", "widget_widget_about", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("134", "widget_contact", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("135", "widget_flickr", "a:1:{s:12:\"_multiwidget\";i:1;}", "yes");
INSERT INTO `wp_options` VALUES("525", "nav_menu_options", "a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}", "yes");
INSERT INTO `wp_options` VALUES("759", "category_children", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("776", "recently_activated", "a:0:{}", "yes");
INSERT INTO `wp_options` VALUES("779", "ftp_credentials", "a:3:{s:8:\"hostname\";s:19:\"localhost/wordpress\";s:8:\"username\";s:5:\"admin\";s:15:\"connection_type\";s:3:\"ftp\";}", "yes");
INSERT INTO `wp_options` VALUES("867", "duplicator_settings", "a:15:{s:7:\"version\";s:6:\"1.3.10\";s:18:\"uninstall_settings\";b:1;s:15:\"uninstall_files\";b:1;s:16:\"uninstall_tables\";b:1;s:13:\"package_debug\";b:0;s:17:\"package_mysqldump\";s:1:\"0\";s:22:\"package_mysqldump_path\";s:0:\"\";s:24:\"package_phpdump_qrylimit\";s:3:\"100\";s:17:\"package_zip_flush\";s:1:\"0\";s:20:\"storage_htaccess_off\";b:0;s:18:\"archive_build_mode\";s:1:\"2\";s:17:\"skip_archive_scan\";b:0;s:17:\"active_package_id\";i:13;s:12:\"last_updated\";s:19:\"2019-03-29-10-10-32\";s:18:\"package_ui_created\";s:1:\"1\";}", "yes");
INSERT INTO `wp_options` VALUES("868", "duplicator_version_plugin", "1.3.10", "yes");
INSERT INTO `wp_options` VALUES("869", "duplicator_ui_view_state", "a:6:{s:22:\"dup-pack-storage-panel\";s:1:\"0\";s:22:\"dup-pack-archive-panel\";s:1:\"1\";s:24:\"dup-pack-installer-panel\";s:1:\"1\";s:19:\"dup-pack-build-try1\";s:1:\"1\";s:19:\"dup-pack-build-try2\";s:1:\"0\";s:19:\"dup-pack-build-try3\";s:1:\"1\";}", "yes");
INSERT INTO `wp_options` VALUES("871", "duplicator_package_active", "O:11:\"DUP_Package\":25:{s:7:\"Created\";s:19:\"2019-03-29 10:10:42\";s:7:\"Version\";s:6:\"1.3.10\";s:9:\"VersionWP\";s:5:\"5.1.1\";s:9:\"VersionDB\";s:7:\"10.1.38\";s:10:\"VersionPHP\";s:5:\"7.3.3\";s:9:\"VersionOS\";s:5:\"Linux\";s:2:\"ID\";N;s:4:\"Name\";s:12:\"20190329_itq\";s:4:\"Hash\";s:35:\"925668576b01716d7724_20190329101042\";s:8:\"NameHash\";s:48:\"20190329_itq_925668576b01716d7724_20190329101042\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:38:\"/opt/lampp/htdocs/ITQ/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://localhost/ITQ/wp-snapshots/\";s:8:\"ScanFile\";s:58:\"20190329_itq_925668576b01716d7724_20190329101042_scan.json\";s:10:\"TimerStart\";i:-1;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";i:0;s:6:\"WPUser\";N;s:7:\"Archive\";O:11:\"DUP_Archive\":21:{s:10:\"FilterDirs\";s:0:\"\";s:11:\"FilterFiles\";s:0:\"\";s:10:\"FilterExts\";s:0:\"\";s:13:\"FilterDirsAll\";a:0:{}s:14:\"FilterFilesAll\";a:0:{}s:13:\"FilterExtsAll\";a:0:{}s:8:\"FilterOn\";i:0;s:12:\"ExportOnlyDB\";i:0;s:4:\"File\";N;s:6:\"Format\";s:3:\"ZIP\";s:7:\"PackDir\";s:21:\"/opt/lampp/htdocs/ITQ\";s:4:\"Size\";i:0;s:4:\"Dirs\";a:0:{}s:5:\"Files\";a:0:{}s:10:\"FilterInfo\";O:23:\"DUP_Archive_Filter_Info\":8:{s:4:\"Dirs\";O:34:\"DUP_Archive_Filter_Scope_Directory\":5:{s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:6:\"Global\";a:0:{}s:8:\"Instance\";a:0:{}}s:5:\"Files\";O:29:\"DUP_Archive_Filter_Scope_File\":6:{s:4:\"Size\";a:0:{}s:7:\"Warning\";a:0:{}s:10:\"Unreadable\";a:0:{}s:4:\"Core\";a:0:{}s:6:\"Global\";a:0:{}s:8:\"Instance\";a:0:{}}s:4:\"Exts\";O:29:\"DUP_Archive_Filter_Scope_Base\":3:{s:4:\"Core\";a:0:{}s:6:\"Global\";a:0:{}s:8:\"Instance\";a:0:{}}s:9:\"UDirCount\";i:0;s:10:\"UFileCount\";i:0;s:9:\"UExtCount\";i:0;s:8:\"TreeSize\";a:0:{}s:11:\"TreeWarning\";a:0:{}}s:14:\"RecursiveLinks\";a:0:{}s:10:\"file_count\";i:-1;s:10:\"\0*\0Package\";O:11:\"DUP_Package\":25:{s:7:\"Created\";s:19:\"2019-03-29 10:10:42\";s:7:\"Version\";s:6:\"1.3.10\";s:9:\"VersionWP\";s:5:\"5.1.1\";s:9:\"VersionDB\";s:7:\"10.1.38\";s:10:\"VersionPHP\";s:5:\"7.3.3\";s:9:\"VersionOS\";s:5:\"Linux\";s:2:\"ID\";N;s:4:\"Name\";s:12:\"20190329_itq\";s:4:\"Hash\";s:35:\"925668576b01716d7724_20190329101042\";s:8:\"NameHash\";s:48:\"20190329_itq_925668576b01716d7724_20190329101042\";s:4:\"Type\";i:0;s:5:\"Notes\";s:0:\"\";s:9:\"StorePath\";s:38:\"/opt/lampp/htdocs/ITQ/wp-snapshots/tmp\";s:8:\"StoreURL\";s:34:\"http://localhost/ITQ/wp-snapshots/\";s:8:\"ScanFile\";N;s:10:\"TimerStart\";i:-1;s:7:\"Runtime\";N;s:7:\"ExeSize\";N;s:7:\"ZipSize\";N;s:6:\"Status\";i:0;s:6:\"WPUser\";N;s:7:\"Archive\";r:23;s:9:\"Installer\";O:13:\"DUP_Installer\":11:{s:4:\"File\";N;s:4:\"Size\";i:0;s:10:\"OptsDBHost\";s:0:\"\";s:10:\"OptsDBPort\";s:0:\"\";s:10:\"OptsDBName\";s:0:\"\";s:10:\"OptsDBUser\";s:0:\"\";s:12:\"OptsSecureOn\";i:0;s:14:\"OptsSecurePass\";s:0:\"\";s:13:\"numFilesAdded\";i:0;s:12:\"numDirsAdded\";i:0;s:10:\"\0*\0Package\";r:63;}s:8:\"Database\";O:12:\"DUP_Database\":14:{s:4:\"Type\";s:5:\"MySQL\";s:4:\"Size\";N;s:4:\"File\";N;s:4:\"Path\";N;s:12:\"FilterTables\";s:0:\"\";s:8:\"FilterOn\";i:0;s:4:\"Name\";N;s:10:\"Compatible\";s:0:\"\";s:8:\"Comments\";s:19:\"Source distribution\";s:4:\"info\";O:16:\"DUP_DatabaseInfo\":15:{s:9:\"buildMode\";N;s:13:\"collationList\";a:0:{}s:17:\"isTablesUpperCase\";N;s:15:\"isNameUpperCase\";N;s:4:\"name\";N;s:15:\"tablesBaseCount\";N;s:16:\"tablesFinalCount\";N;s:14:\"tablesRowCount\";N;s:16:\"tablesSizeOnDisk\";N;s:18:\"varLowerCaseTables\";i:0;s:7:\"version\";N;s:14:\"versionComment\";N;s:18:\"tableWiseRowCounts\";a:0:{}s:33:\"\0DUP_DatabaseInfo\0intFieldsStruct\";a:0:{}s:42:\"\0DUP_DatabaseInfo\0indexProcessedSchemaSize\";a:0:{}}s:10:\"\0*\0Package\";r:63;s:25:\"\0DUP_Database\0dbStorePath\";N;s:23:\"\0DUP_Database\0EOFMarker\";s:0:\"\";s:26:\"\0DUP_Database\0networkFlush\";b:0;}s:13:\"BuildProgress\";O:18:\"DUP_Build_Progress\":12:{s:17:\"thread_start_time\";N;s:11:\"initialized\";b:0;s:15:\"installer_built\";b:0;s:15:\"archive_started\";b:0;s:20:\"archive_has_database\";b:0;s:13:\"archive_built\";b:0;s:21:\"database_script_built\";b:0;s:6:\"failed\";b:0;s:7:\"retries\";i:0;s:14:\"build_failures\";a:0:{}s:19:\"validation_failures\";a:0:{}s:27:\"\0DUP_Build_Progress\0package\";r:63;}}s:29:\"\0DUP_Archive\0tmpFilterDirsAll\";a:0:{}s:24:\"\0DUP_Archive\0wpCorePaths\";a:5:{i:0;s:30:\"/opt/lampp/htdocs/ITQ/wp-admin\";i:1;s:40:\"/opt/lampp/htdocs/ITQ/wp-content/uploads\";i:2;s:42:\"/opt/lampp/htdocs/ITQ/wp-content/languages\";i:3;s:39:\"/opt/lampp/htdocs/ITQ/wp-content/themes\";i:4;s:33:\"/opt/lampp/htdocs/ITQ/wp-includes\";}s:29:\"\0DUP_Archive\0wpCoreExactPaths\";a:2:{i:0;s:21:\"/opt/lampp/htdocs/ITQ\";i:1;s:32:\"/opt/lampp/htdocs/ITQ/wp-content\";}}s:9:\"Installer\";r:86;s:8:\"Database\";r:98;s:13:\"BuildProgress\";r:128;}", "yes");
INSERT INTO `wp_options` VALUES("872", "duplicator_exe_safe_mode", "0", "yes");
INSERT INTO `wp_options` VALUES("916", "_site_transient_update_core", "O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-5.1.1.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-5.1.1-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-5.1.1-new-bundled.zip\";s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.1.1\";s:7:\"version\";s:5:\"5.1.1\";s:11:\"php_version\";s:5:\"5.2.4\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1553850275;s:15:\"version_checked\";s:5:\"5.1.1\";s:12:\"translations\";a:0:{}}", "no");
INSERT INTO `wp_options` VALUES("917", "_site_transient_update_plugins", "O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1553850275;s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:3:{s:19:\"akismet/akismet.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/akismet\";s:4:\"slug\";s:7:\"akismet\";s:6:\"plugin\";s:19:\"akismet/akismet.php\";s:11:\"new_version\";s:5:\"4.1.1\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/akismet/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/akismet.4.1.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:59:\"https://ps.w.org/akismet/assets/icon-256x256.png?rev=969272\";s:2:\"1x\";s:59:\"https://ps.w.org/akismet/assets/icon-128x128.png?rev=969272\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/akismet/assets/banner-772x250.jpg?rev=479904\";}s:11:\"banners_rtl\";a:0:{}}s:25:\"duplicator/duplicator.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/duplicator\";s:4:\"slug\";s:10:\"duplicator\";s:6:\"plugin\";s:25:\"duplicator/duplicator.php\";s:11:\"new_version\";s:6:\"1.3.10\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/duplicator/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/duplicator.1.3.10.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/duplicator/assets/icon-256x256.png?rev=1298463\";s:2:\"1x\";s:63:\"https://ps.w.org/duplicator/assets/icon-128x128.png?rev=1298463\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:65:\"https://ps.w.org/duplicator/assets/banner-772x250.png?rev=1645055\";}s:11:\"banners_rtl\";a:0:{}}s:9:\"hello.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/hello-dolly\";s:4:\"slug\";s:11:\"hello-dolly\";s:6:\"plugin\";s:9:\"hello.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/hello-dolly/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/hello-dolly.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-256x256.jpg?rev=2052855\";s:2:\"1x\";s:64:\"https://ps.w.org/hello-dolly/assets/icon-128x128.jpg?rev=2052855\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/hello-dolly/assets/banner-772x250.jpg?rev=2052855\";}s:11:\"banners_rtl\";a:0:{}}}}", "no");
INSERT INTO `wp_options` VALUES("918", "_site_transient_timeout_theme_roots", "1553852077", "no");
INSERT INTO `wp_options` VALUES("919", "_site_transient_theme_roots", "a:6:{s:8:\"itqTheme\";s:7:\"/themes\";s:35:\"starry-WORDPRESS/starry-child-theme\";s:7:\"/themes\";s:23:\"starry-WORDPRESS/starry\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";s:15:\"twentyseventeen\";s:7:\"/themes\";s:13:\"twentysixteen\";s:7:\"/themes\";}", "no");
INSERT INTO `wp_options` VALUES("920", "_site_transient_update_themes", "O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1553850278;s:7:\"checked\";a:6:{s:8:\"itqTheme\";s:3:\"1.0\";s:35:\"starry-WORDPRESS/starry-child-theme\";s:0:\"\";s:23:\"starry-WORDPRESS/starry\";s:5:\"1.3.4\";s:14:\"twentynineteen\";s:3:\"1.3\";s:15:\"twentyseventeen\";s:3:\"2.1\";s:13:\"twentysixteen\";s:3:\"1.9\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}", "no");
INSERT INTO `wp_options` VALUES("921", "_transient_is_multi_author", "0", "yes");

/* INSERT TABLE DATA: wp_postmeta */
INSERT INTO `wp_postmeta` VALUES("1", "2", "_wp_page_template", "default");
INSERT INTO `wp_postmeta` VALUES("2", "3", "_wp_page_template", "default");
INSERT INTO `wp_postmeta` VALUES("3", "5", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("4", "5", "_wp_trash_meta_time", "1553247723");
INSERT INTO `wp_postmeta` VALUES("5", "8", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("6", "8", "_wp_trash_meta_time", "1553247807");
INSERT INTO `wp_postmeta` VALUES("7", "10", "_edit_lock", "1553247850:1");
INSERT INTO `wp_postmeta` VALUES("8", "10", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("9", "10", "_wp_trash_meta_time", "1553247866");
INSERT INTO `wp_postmeta` VALUES("10", "12", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("11", "12", "_wp_trash_meta_time", "1553248021");
INSERT INTO `wp_postmeta` VALUES("12", "14", "_edit_lock", "1553248074:1");
INSERT INTO `wp_postmeta` VALUES("13", "14", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("14", "14", "_wp_trash_meta_time", "1553248097");
INSERT INTO `wp_postmeta` VALUES("15", "16", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("16", "16", "_wp_trash_meta_time", "1553248153");
INSERT INTO `wp_postmeta` VALUES("17", "18", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("18", "18", "_wp_trash_meta_time", "1553248187");
INSERT INTO `wp_postmeta` VALUES("19", "20", "_edit_lock", "1553248429:1");
INSERT INTO `wp_postmeta` VALUES("20", "20", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("21", "20", "_wp_trash_meta_time", "1553248432");
INSERT INTO `wp_postmeta` VALUES("22", "22", "_edit_lock", "1553248867:1");
INSERT INTO `wp_postmeta` VALUES("23", "22", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("24", "22", "_wp_trash_meta_time", "1553248868");
INSERT INTO `wp_postmeta` VALUES("25", "24", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("26", "24", "_wp_trash_meta_time", "1553249028");
INSERT INTO `wp_postmeta` VALUES("27", "26", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("28", "26", "_wp_trash_meta_time", "1553249140");
INSERT INTO `wp_postmeta` VALUES("29", "28", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("30", "28", "_wp_trash_meta_time", "1553249241");
INSERT INTO `wp_postmeta` VALUES("31", "30", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("32", "30", "_wp_trash_meta_time", "1553249287");
INSERT INTO `wp_postmeta` VALUES("33", "32", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("34", "32", "_wp_trash_meta_time", "1553249356");
INSERT INTO `wp_postmeta` VALUES("35", "34", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("36", "34", "_wp_trash_meta_time", "1553249409");
INSERT INTO `wp_postmeta` VALUES("37", "36", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("38", "36", "_wp_trash_meta_time", "1553249493");
INSERT INTO `wp_postmeta` VALUES("39", "38", "_edit_lock", "1553578483:1");
INSERT INTO `wp_postmeta` VALUES("49", "70", "_edit_lock", "1553582026:1");
INSERT INTO `wp_postmeta` VALUES("50", "70", "_wp_page_template", "aboutus/aboutUs.php");
INSERT INTO `wp_postmeta` VALUES("51", "38", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("52", "38", "_wp_trash_meta_time", "1553578628");
INSERT INTO `wp_postmeta` VALUES("53", "38", "_wp_desired_post_slug", "about-us");
INSERT INTO `wp_postmeta` VALUES("54", "72", "_edit_lock", "1553583162:1");
INSERT INTO `wp_postmeta` VALUES("55", "72", "_wp_page_template", "aboutus/awards.php");
INSERT INTO `wp_postmeta` VALUES("56", "74", "_edit_lock", "1553584870:1");
INSERT INTO `wp_postmeta` VALUES("57", "74", "_wp_page_template", "aboutus/itqJourney.php");
INSERT INTO `wp_postmeta` VALUES("58", "76", "_edit_lock", "1553695994:1");
INSERT INTO `wp_postmeta` VALUES("59", "76", "_wp_page_template", "aboutus/leadershipTeam.php");
INSERT INTO `wp_postmeta` VALUES("60", "78", "_edit_lock", "1553585964:1");
INSERT INTO `wp_postmeta` VALUES("61", "78", "_wp_page_template", "aboutus/csr.php");
INSERT INTO `wp_postmeta` VALUES("62", "80", "_edit_lock", "1553589393:1");
INSERT INTO `wp_postmeta` VALUES("63", "80", "_wp_page_template", "humanCapital/hr-vision.php");
INSERT INTO `wp_postmeta` VALUES("64", "82", "_edit_lock", "1553589403:1");
INSERT INTO `wp_postmeta` VALUES("65", "83", "_edit_lock", "1553589437:1");
INSERT INTO `wp_postmeta` VALUES("66", "84", "_edit_lock", "1553590122:1");
INSERT INTO `wp_postmeta` VALUES("67", "84", "_wp_page_template", "humanCapital/employee-value.php");
INSERT INTO `wp_postmeta` VALUES("68", "86", "_edit_lock", "1553590617:1");
INSERT INTO `wp_postmeta` VALUES("69", "86", "_wp_page_template", "humanCapital/learning-development.php");
INSERT INTO `wp_postmeta` VALUES("70", "88", "_edit_lock", "1553590648:1");
INSERT INTO `wp_postmeta` VALUES("71", "89", "_edit_lock", "1553590943:1");
INSERT INTO `wp_postmeta` VALUES("72", "89", "_wp_page_template", "humanCapital/diversity-inclusion.php");
INSERT INTO `wp_postmeta` VALUES("73", "91", "_edit_lock", "1553592018:1");
INSERT INTO `wp_postmeta` VALUES("74", "91", "_wp_page_template", "humanCapital/carrer.php");
INSERT INTO `wp_postmeta` VALUES("75", "93", "_edit_lock", "1553592332:1");
INSERT INTO `wp_postmeta` VALUES("76", "93", "_wp_page_template", "ourVision/ourVision.php");
INSERT INTO `wp_postmeta` VALUES("77", "95", "_edit_lock", "1553597824:1");
INSERT INTO `wp_postmeta` VALUES("78", "95", "_wp_page_template", "ourVision/travelEducation.php");
INSERT INTO `wp_postmeta` VALUES("79", "97", "_edit_lock", "1553597835:1");
INSERT INTO `wp_postmeta` VALUES("80", "98", "_edit_lock", "1553597859:1");
INSERT INTO `wp_postmeta` VALUES("81", "99", "_edit_lock", "1553601791:1");
INSERT INTO `wp_postmeta` VALUES("82", "99", "_wp_page_template", "press-room/press-releases.php");
INSERT INTO `wp_postmeta` VALUES("83", "129", "_edit_lock", "1553601811:1");
INSERT INTO `wp_postmeta` VALUES("84", "130", "_edit_lock", "1553604321:1");
INSERT INTO `wp_postmeta` VALUES("85", "130", "_wp_page_template", "products/product-travelport.php");
INSERT INTO `wp_postmeta` VALUES("86", "132", "_edit_lock", "1553604776:1");
INSERT INTO `wp_postmeta` VALUES("87", "132", "_wp_page_template", "products/travelport-ticketing.php");
INSERT INTO `wp_postmeta` VALUES("88", "134", "_edit_lock", "1553605977:1");
INSERT INTO `wp_postmeta` VALUES("89", "134", "_wp_page_template", "products/travelport-fares.php");
INSERT INTO `wp_postmeta` VALUES("90", "136", "_edit_lock", "1553605985:1");
INSERT INTO `wp_postmeta` VALUES("91", "137", "_edit_lock", "1553606002:1");
INSERT INTO `wp_postmeta` VALUES("92", "138", "_edit_lock", "1553606034:1");
INSERT INTO `wp_postmeta` VALUES("93", "139", "_edit_lock", "1553606071:1");
INSERT INTO `wp_postmeta` VALUES("94", "140", "_edit_lock", "1553606124:1");
INSERT INTO `wp_postmeta` VALUES("95", "141", "_edit_lock", "1553606187:1");
INSERT INTO `wp_postmeta` VALUES("96", "142", "_edit_lock", "1553606282:1");
INSERT INTO `wp_postmeta` VALUES("97", "143", "_edit_lock", "1553606692:1");
INSERT INTO `wp_postmeta` VALUES("98", "143", "_wp_page_template", "products/travelport-non-air.php");
INSERT INTO `wp_postmeta` VALUES("99", "145", "_edit_lock", "1553607157:1");
INSERT INTO `wp_postmeta` VALUES("100", "145", "_wp_page_template", "products/travelport-office-management.php");
INSERT INTO `wp_postmeta` VALUES("101", "147", "_edit_lock", "1553607478:1");
INSERT INTO `wp_postmeta` VALUES("102", "147", "_wp_page_template", "products/travelport-value-adds.php");
INSERT INTO `wp_postmeta` VALUES("103", "149", "_edit_lock", "1553660760:1");
INSERT INTO `wp_postmeta` VALUES("104", "149", "_wp_page_template", "products/travelport-galileo-desktop.php");
INSERT INTO `wp_postmeta` VALUES("105", "151", "_edit_lock", "1553661570:1");
INSERT INTO `wp_postmeta` VALUES("106", "151", "_wp_page_template", "products/product-ndc.php");
INSERT INTO `wp_postmeta` VALUES("107", "153", "_edit_lock", "1553661583:1");
INSERT INTO `wp_postmeta` VALUES("108", "154", "_edit_lock", "1553663115:1");
INSERT INTO `wp_postmeta` VALUES("109", "154", "_wp_page_template", "products/airlines.php");
INSERT INTO `wp_postmeta` VALUES("110", "156", "_edit_lock", "1553664050:1");
INSERT INTO `wp_postmeta` VALUES("111", "156", "_wp_page_template", "products/ecommerce.php");
INSERT INTO `wp_postmeta` VALUES("112", "158", "_edit_lock", "1553664698:1");
INSERT INTO `wp_postmeta` VALUES("113", "158", "_wp_page_template", "products/ePricing.php");
INSERT INTO `wp_postmeta` VALUES("114", "160", "_edit_lock", "1553665274:1");
INSERT INTO `wp_postmeta` VALUES("115", "160", "_wp_page_template", "products/product-online-solutions.php");
INSERT INTO `wp_postmeta` VALUES("116", "162", "_edit_lock", "1553665290:1");
INSERT INTO `wp_postmeta` VALUES("117", "163", "_edit_lock", "1553665603:1");
INSERT INTO `wp_postmeta` VALUES("118", "163", "_wp_page_template", "products/online-solution-contents.php");
INSERT INTO `wp_postmeta` VALUES("119", "165", "_edit_lock", "1553669144:1");
INSERT INTO `wp_postmeta` VALUES("120", "165", "_wp_page_template", "products/enhanced-utilities.php");
INSERT INTO `wp_postmeta` VALUES("121", "167", "_edit_lock", "1553673683:1");
INSERT INTO `wp_postmeta` VALUES("122", "167", "_wp_page_template", "products/worldsspan.php");
INSERT INTO `wp_postmeta` VALUES("123", "169", "_edit_lock", "1553673699:1");
INSERT INTO `wp_postmeta` VALUES("124", "170", "_edit_lock", "1553673706:1");
INSERT INTO `wp_postmeta` VALUES("125", "171", "_edit_lock", "1553679370:1");
INSERT INTO `wp_postmeta` VALUES("126", "171", "_wp_page_template", "contact-us.php");
INSERT INTO `wp_postmeta` VALUES("127", "1", "_edit_lock", "1553773520:1");
INSERT INTO `wp_postmeta` VALUES("128", "173", "_edit_lock", "1553679685:1");
INSERT INTO `wp_postmeta` VALUES("131", "173", "_wp_trash_meta_status", "publish");
INSERT INTO `wp_postmeta` VALUES("132", "173", "_wp_trash_meta_time", "1553679865");
INSERT INTO `wp_postmeta` VALUES("133", "173", "_wp_desired_post_slug", "new-blog");
INSERT INTO `wp_postmeta` VALUES("134", "175", "_edit_lock", "1553696038:1");
INSERT INTO `wp_postmeta` VALUES("135", "175", "_wp_page_template", "press-room/articles.php");
INSERT INTO `wp_postmeta` VALUES("136", "177", "_edit_lock", "1553695583:1");
INSERT INTO `wp_postmeta` VALUES("137", "178", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("138", "178", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("139", "178", "_menu_item_object_id", "178");
INSERT INTO `wp_postmeta` VALUES("140", "178", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("141", "178", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("142", "178", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("143", "178", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("144", "178", "_menu_item_url", "http://localhost/ITQ/");
INSERT INTO `wp_postmeta` VALUES("146", "179", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("147", "179", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("148", "179", "_menu_item_object_id", "70");
INSERT INTO `wp_postmeta` VALUES("149", "179", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("150", "179", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("151", "179", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("152", "179", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("153", "179", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("154", "179", "_menu_item_orphaned", "1553696171");
INSERT INTO `wp_postmeta` VALUES("155", "180", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("156", "180", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("157", "180", "_menu_item_object_id", "76");
INSERT INTO `wp_postmeta` VALUES("158", "180", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("159", "180", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("160", "180", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("161", "180", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("162", "180", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("164", "181", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("165", "181", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("166", "181", "_menu_item_object_id", "154");
INSERT INTO `wp_postmeta` VALUES("167", "181", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("168", "181", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("169", "181", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("170", "181", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("171", "181", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("173", "182", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("174", "182", "_menu_item_menu_item_parent", "238");
INSERT INTO `wp_postmeta` VALUES("175", "182", "_menu_item_object_id", "175");
INSERT INTO `wp_postmeta` VALUES("176", "182", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("177", "182", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("178", "182", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("179", "182", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("180", "182", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("182", "183", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("183", "183", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("184", "183", "_menu_item_object_id", "72");
INSERT INTO `wp_postmeta` VALUES("185", "183", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("186", "183", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("187", "183", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("188", "183", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("189", "183", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("191", "184", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("192", "184", "_menu_item_menu_item_parent", "240");
INSERT INTO `wp_postmeta` VALUES("193", "184", "_menu_item_object_id", "91");
INSERT INTO `wp_postmeta` VALUES("194", "184", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("195", "184", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("196", "184", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("197", "184", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("198", "184", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("200", "185", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("201", "185", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("202", "185", "_menu_item_object_id", "171");
INSERT INTO `wp_postmeta` VALUES("203", "185", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("204", "185", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("205", "185", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("206", "185", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("207", "185", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("208", "185", "_menu_item_orphaned", "1553696174");
INSERT INTO `wp_postmeta` VALUES("209", "186", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("210", "186", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("211", "186", "_menu_item_object_id", "78");
INSERT INTO `wp_postmeta` VALUES("212", "186", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("213", "186", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("214", "186", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("215", "186", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("216", "186", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("218", "187", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("219", "187", "_menu_item_menu_item_parent", "240");
INSERT INTO `wp_postmeta` VALUES("220", "187", "_menu_item_object_id", "89");
INSERT INTO `wp_postmeta` VALUES("221", "187", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("222", "187", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("223", "187", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("224", "187", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("225", "187", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("227", "188", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("228", "188", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("229", "188", "_menu_item_object_id", "156");
INSERT INTO `wp_postmeta` VALUES("230", "188", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("231", "188", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("232", "188", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("233", "188", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("234", "188", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("236", "189", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("237", "189", "_menu_item_menu_item_parent", "240");
INSERT INTO `wp_postmeta` VALUES("238", "189", "_menu_item_object_id", "84");
INSERT INTO `wp_postmeta` VALUES("239", "189", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("240", "189", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("241", "189", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("242", "189", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("243", "189", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("245", "190", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("246", "190", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("247", "190", "_menu_item_object_id", "165");
INSERT INTO `wp_postmeta` VALUES("248", "190", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("249", "190", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("250", "190", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("251", "190", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("252", "190", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("254", "191", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("255", "191", "_menu_item_menu_item_parent", "188");
INSERT INTO `wp_postmeta` VALUES("256", "191", "_menu_item_object_id", "158");
INSERT INTO `wp_postmeta` VALUES("257", "191", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("258", "191", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("259", "191", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("260", "191", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("261", "191", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("263", "192", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("264", "192", "_menu_item_menu_item_parent", "240");
INSERT INTO `wp_postmeta` VALUES("265", "192", "_menu_item_object_id", "80");
INSERT INTO `wp_postmeta` VALUES("266", "192", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("267", "192", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("268", "192", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("269", "192", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("270", "192", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("272", "193", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("273", "193", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("274", "193", "_menu_item_object_id", "74");
INSERT INTO `wp_postmeta` VALUES("275", "193", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("276", "193", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("277", "193", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("278", "193", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("279", "193", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("281", "194", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("282", "194", "_menu_item_menu_item_parent", "240");
INSERT INTO `wp_postmeta` VALUES("283", "194", "_menu_item_object_id", "86");
INSERT INTO `wp_postmeta` VALUES("284", "194", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("285", "194", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("286", "194", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("287", "194", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("288", "194", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("290", "195", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("291", "195", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("292", "195", "_menu_item_object_id", "151");
INSERT INTO `wp_postmeta` VALUES("293", "195", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("294", "195", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("295", "195", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("296", "195", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("297", "195", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("299", "196", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("300", "196", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("301", "196", "_menu_item_object_id", "160");
INSERT INTO `wp_postmeta` VALUES("302", "196", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("303", "196", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("304", "196", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("305", "196", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("306", "196", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("308", "197", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("309", "197", "_menu_item_menu_item_parent", "196");
INSERT INTO `wp_postmeta` VALUES("310", "197", "_menu_item_object_id", "163");
INSERT INTO `wp_postmeta` VALUES("311", "197", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("312", "197", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("313", "197", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("314", "197", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("315", "197", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("317", "198", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("318", "198", "_menu_item_menu_item_parent", "241");
INSERT INTO `wp_postmeta` VALUES("319", "198", "_menu_item_object_id", "93");
INSERT INTO `wp_postmeta` VALUES("320", "198", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("321", "198", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("322", "198", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("323", "198", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("324", "198", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("326", "199", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("327", "199", "_menu_item_menu_item_parent", "238");
INSERT INTO `wp_postmeta` VALUES("328", "199", "_menu_item_object_id", "99");
INSERT INTO `wp_postmeta` VALUES("329", "199", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("330", "199", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("331", "199", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("332", "199", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("333", "199", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("335", "200", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("336", "200", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("337", "200", "_menu_item_object_id", "130");
INSERT INTO `wp_postmeta` VALUES("338", "200", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("339", "200", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("340", "200", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("341", "200", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("342", "200", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("344", "201", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("345", "201", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("346", "201", "_menu_item_object_id", "2");
INSERT INTO `wp_postmeta` VALUES("347", "201", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("348", "201", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("349", "201", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("350", "201", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("351", "201", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("352", "201", "_menu_item_orphaned", "1553696184");
INSERT INTO `wp_postmeta` VALUES("353", "202", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("354", "202", "_menu_item_menu_item_parent", "241");
INSERT INTO `wp_postmeta` VALUES("355", "202", "_menu_item_object_id", "95");
INSERT INTO `wp_postmeta` VALUES("356", "202", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("357", "202", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("358", "202", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("359", "202", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("360", "202", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("362", "203", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("363", "203", "_menu_item_menu_item_parent", "200");
INSERT INTO `wp_postmeta` VALUES("364", "203", "_menu_item_object_id", "134");
INSERT INTO `wp_postmeta` VALUES("365", "203", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("366", "203", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("367", "203", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("368", "203", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("369", "203", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("371", "204", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("372", "204", "_menu_item_menu_item_parent", "200");
INSERT INTO `wp_postmeta` VALUES("373", "204", "_menu_item_object_id", "149");
INSERT INTO `wp_postmeta` VALUES("374", "204", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("375", "204", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("376", "204", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("377", "204", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("378", "204", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("380", "205", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("381", "205", "_menu_item_menu_item_parent", "200");
INSERT INTO `wp_postmeta` VALUES("382", "205", "_menu_item_object_id", "143");
INSERT INTO `wp_postmeta` VALUES("383", "205", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("384", "205", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("385", "205", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("386", "205", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("387", "205", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("389", "206", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("390", "206", "_menu_item_menu_item_parent", "200");
INSERT INTO `wp_postmeta` VALUES("391", "206", "_menu_item_object_id", "145");
INSERT INTO `wp_postmeta` VALUES("392", "206", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("393", "206", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("394", "206", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("395", "206", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("396", "206", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("398", "207", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("399", "207", "_menu_item_menu_item_parent", "200");
INSERT INTO `wp_postmeta` VALUES("400", "207", "_menu_item_object_id", "132");
INSERT INTO `wp_postmeta` VALUES("401", "207", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("402", "207", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("403", "207", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("404", "207", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("405", "207", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("407", "208", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("408", "208", "_menu_item_menu_item_parent", "200");
INSERT INTO `wp_postmeta` VALUES("409", "208", "_menu_item_object_id", "147");
INSERT INTO `wp_postmeta` VALUES("410", "208", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("411", "208", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("412", "208", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("413", "208", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("414", "208", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("416", "209", "_menu_item_type", "post_type");
INSERT INTO `wp_postmeta` VALUES("417", "209", "_menu_item_menu_item_parent", "212");
INSERT INTO `wp_postmeta` VALUES("418", "209", "_menu_item_object_id", "167");
INSERT INTO `wp_postmeta` VALUES("419", "209", "_menu_item_object", "page");
INSERT INTO `wp_postmeta` VALUES("420", "209", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("421", "209", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("422", "209", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("423", "209", "_menu_item_url", "");
INSERT INTO `wp_postmeta` VALUES("425", "210", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("426", "210", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("427", "210", "_menu_item_object_id", "210");
INSERT INTO `wp_postmeta` VALUES("428", "210", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("429", "210", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("430", "210", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("431", "210", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("432", "210", "_menu_item_url", "http://localhost/ITQ/index.php/about/");
INSERT INTO `wp_postmeta` VALUES("434", "211", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("435", "211", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("436", "211", "_menu_item_object_id", "211");
INSERT INTO `wp_postmeta` VALUES("437", "211", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("438", "211", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("439", "211", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("440", "211", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("441", "211", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("443", "212", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("444", "212", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("445", "212", "_menu_item_object_id", "212");
INSERT INTO `wp_postmeta` VALUES("446", "212", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("447", "212", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("448", "212", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("449", "212", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("450", "212", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("452", "213", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("453", "213", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("454", "213", "_menu_item_object_id", "213");
INSERT INTO `wp_postmeta` VALUES("455", "213", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("456", "213", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("457", "213", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("458", "213", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("459", "213", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("461", "214", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("462", "214", "_menu_item_menu_item_parent", "211");
INSERT INTO `wp_postmeta` VALUES("463", "214", "_menu_item_object_id", "214");
INSERT INTO `wp_postmeta` VALUES("464", "214", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("465", "214", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("466", "214", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("467", "214", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("468", "214", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("470", "215", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("471", "215", "_menu_item_menu_item_parent", "181");
INSERT INTO `wp_postmeta` VALUES("472", "215", "_menu_item_object_id", "215");
INSERT INTO `wp_postmeta` VALUES("473", "215", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("474", "215", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("475", "215", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("476", "215", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("477", "215", "_menu_item_url", "http://localhost/ITQ/index.php/airlines/");
INSERT INTO `wp_postmeta` VALUES("479", "216", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("480", "216", "_menu_item_menu_item_parent", "181");
INSERT INTO `wp_postmeta` VALUES("481", "216", "_menu_item_object_id", "216");
INSERT INTO `wp_postmeta` VALUES("482", "216", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("483", "216", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("484", "216", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("485", "216", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("486", "216", "_menu_item_url", "http://localhost/ITQ/index.php/airlines/");
INSERT INTO `wp_postmeta` VALUES("488", "217", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("489", "217", "_menu_item_menu_item_parent", "181");
INSERT INTO `wp_postmeta` VALUES("490", "217", "_menu_item_object_id", "217");
INSERT INTO `wp_postmeta` VALUES("491", "217", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("492", "217", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("493", "217", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("494", "217", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("495", "217", "_menu_item_url", "http://localhost/ITQ/index.php/airlines/");
INSERT INTO `wp_postmeta` VALUES("497", "218", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("498", "218", "_menu_item_menu_item_parent", "196");
INSERT INTO `wp_postmeta` VALUES("499", "218", "_menu_item_object_id", "218");
INSERT INTO `wp_postmeta` VALUES("500", "218", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("501", "218", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("502", "218", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("503", "218", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("504", "218", "_menu_item_url", "http://localhost/ITQ/index.php/online-solutions-booking/");
INSERT INTO `wp_postmeta` VALUES("506", "219", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("507", "219", "_menu_item_menu_item_parent", "196");
INSERT INTO `wp_postmeta` VALUES("508", "219", "_menu_item_object_id", "219");
INSERT INTO `wp_postmeta` VALUES("509", "219", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("510", "219", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("511", "219", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("512", "219", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("513", "219", "_menu_item_url", "http://localhost/ITQ/index.php/online-solutions-booking/");
INSERT INTO `wp_postmeta` VALUES("515", "220", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("516", "220", "_menu_item_menu_item_parent", "196");
INSERT INTO `wp_postmeta` VALUES("517", "220", "_menu_item_object_id", "220");
INSERT INTO `wp_postmeta` VALUES("518", "220", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("519", "220", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("520", "220", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("521", "220", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("522", "220", "_menu_item_url", "http://localhost/ITQ/index.php/online-solutions-booking/");
INSERT INTO `wp_postmeta` VALUES("524", "221", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("525", "221", "_menu_item_menu_item_parent", "209");
INSERT INTO `wp_postmeta` VALUES("526", "221", "_menu_item_object_id", "221");
INSERT INTO `wp_postmeta` VALUES("527", "221", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("528", "221", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("529", "221", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("530", "221", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("531", "221", "_menu_item_url", "http://localhost/ITQ/index.php/worldspan/");
INSERT INTO `wp_postmeta` VALUES("533", "222", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("534", "222", "_menu_item_menu_item_parent", "209");
INSERT INTO `wp_postmeta` VALUES("535", "222", "_menu_item_object_id", "222");
INSERT INTO `wp_postmeta` VALUES("536", "222", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("537", "222", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("538", "222", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("539", "222", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("540", "222", "_menu_item_url", "http://localhost/ITQ/index.php/worldspan/");
INSERT INTO `wp_postmeta` VALUES("542", "223", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("543", "223", "_menu_item_menu_item_parent", "209");
INSERT INTO `wp_postmeta` VALUES("544", "223", "_menu_item_object_id", "223");
INSERT INTO `wp_postmeta` VALUES("545", "223", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("546", "223", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("547", "223", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("548", "223", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("549", "223", "_menu_item_url", "http://localhost/ITQ/index.php/worldspan/");
INSERT INTO `wp_postmeta` VALUES("551", "224", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("552", "224", "_menu_item_menu_item_parent", "188");
INSERT INTO `wp_postmeta` VALUES("553", "224", "_menu_item_object_id", "224");
INSERT INTO `wp_postmeta` VALUES("554", "224", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("555", "224", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("556", "224", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("557", "224", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("558", "224", "_menu_item_url", "http://localhost/ITQ/index.php/epricing/");
INSERT INTO `wp_postmeta` VALUES("560", "225", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("561", "225", "_menu_item_menu_item_parent", "188");
INSERT INTO `wp_postmeta` VALUES("562", "225", "_menu_item_object_id", "225");
INSERT INTO `wp_postmeta` VALUES("563", "225", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("564", "225", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("565", "225", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("566", "225", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("567", "225", "_menu_item_url", "http://localhost/ITQ/index.php/epricing/");
INSERT INTO `wp_postmeta` VALUES("569", "226", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("570", "226", "_menu_item_menu_item_parent", "188");
INSERT INTO `wp_postmeta` VALUES("571", "226", "_menu_item_object_id", "226");
INSERT INTO `wp_postmeta` VALUES("572", "226", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("573", "226", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("574", "226", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("575", "226", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("576", "226", "_menu_item_url", "http://localhost/ITQ/index.php/epricing/");
INSERT INTO `wp_postmeta` VALUES("578", "227", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("579", "227", "_menu_item_menu_item_parent", "188");
INSERT INTO `wp_postmeta` VALUES("580", "227", "_menu_item_object_id", "227");
INSERT INTO `wp_postmeta` VALUES("581", "227", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("582", "227", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("583", "227", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("584", "227", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("585", "227", "_menu_item_url", "http://localhost/ITQ/index.php/epricing/");
INSERT INTO `wp_postmeta` VALUES("587", "228", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("588", "228", "_menu_item_menu_item_parent", "188");
INSERT INTO `wp_postmeta` VALUES("589", "228", "_menu_item_object_id", "228");
INSERT INTO `wp_postmeta` VALUES("590", "228", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("591", "228", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("592", "228", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("593", "228", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("594", "228", "_menu_item_url", "http://localhost/ITQ/index.php/epricing/");
INSERT INTO `wp_postmeta` VALUES("596", "229", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("597", "229", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("598", "229", "_menu_item_object_id", "229");
INSERT INTO `wp_postmeta` VALUES("599", "229", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("600", "229", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("601", "229", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("602", "229", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("603", "229", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("605", "230", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("606", "230", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("607", "230", "_menu_item_object_id", "230");
INSERT INTO `wp_postmeta` VALUES("608", "230", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("609", "230", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("610", "230", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("611", "230", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("612", "230", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("614", "231", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("615", "231", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("616", "231", "_menu_item_object_id", "231");
INSERT INTO `wp_postmeta` VALUES("617", "231", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("618", "231", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("619", "231", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("620", "231", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("621", "231", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("623", "232", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("624", "232", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("625", "232", "_menu_item_object_id", "232");
INSERT INTO `wp_postmeta` VALUES("626", "232", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("627", "232", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("628", "232", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("629", "232", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("630", "232", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("632", "233", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("633", "233", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("634", "233", "_menu_item_object_id", "233");
INSERT INTO `wp_postmeta` VALUES("635", "233", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("636", "233", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("637", "233", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("638", "233", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("639", "233", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("641", "234", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("642", "234", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("643", "234", "_menu_item_object_id", "234");
INSERT INTO `wp_postmeta` VALUES("644", "234", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("645", "234", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("646", "234", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("647", "234", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("648", "234", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("650", "235", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("651", "235", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("652", "235", "_menu_item_object_id", "235");
INSERT INTO `wp_postmeta` VALUES("653", "235", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("654", "235", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("655", "235", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("656", "235", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("657", "235", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("659", "236", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("660", "236", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("661", "236", "_menu_item_object_id", "236");
INSERT INTO `wp_postmeta` VALUES("662", "236", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("663", "236", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("664", "236", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("665", "236", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("666", "236", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("668", "237", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("669", "237", "_menu_item_menu_item_parent", "190");
INSERT INTO `wp_postmeta` VALUES("670", "237", "_menu_item_object_id", "237");
INSERT INTO `wp_postmeta` VALUES("671", "237", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("672", "237", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("673", "237", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("674", "237", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("675", "237", "_menu_item_url", "http://localhost/ITQ/index.php/enhanced-utilities/");
INSERT INTO `wp_postmeta` VALUES("677", "238", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("678", "238", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("679", "238", "_menu_item_object_id", "238");
INSERT INTO `wp_postmeta` VALUES("680", "238", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("681", "238", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("682", "238", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("683", "238", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("684", "238", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("686", "239", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("687", "239", "_menu_item_menu_item_parent", "238");
INSERT INTO `wp_postmeta` VALUES("688", "239", "_menu_item_object_id", "239");
INSERT INTO `wp_postmeta` VALUES("689", "239", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("690", "239", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("691", "239", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("692", "239", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("693", "239", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("695", "240", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("696", "240", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("697", "240", "_menu_item_object_id", "240");
INSERT INTO `wp_postmeta` VALUES("698", "240", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("699", "240", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("700", "240", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("701", "240", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("702", "240", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("704", "241", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("705", "241", "_menu_item_menu_item_parent", "0");
INSERT INTO `wp_postmeta` VALUES("706", "241", "_menu_item_object_id", "241");
INSERT INTO `wp_postmeta` VALUES("707", "241", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("708", "241", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("709", "241", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("710", "241", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("711", "241", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("713", "242", "_menu_item_type", "custom");
INSERT INTO `wp_postmeta` VALUES("714", "242", "_menu_item_menu_item_parent", "241");
INSERT INTO `wp_postmeta` VALUES("715", "242", "_menu_item_object_id", "242");
INSERT INTO `wp_postmeta` VALUES("716", "242", "_menu_item_object", "custom");
INSERT INTO `wp_postmeta` VALUES("717", "242", "_menu_item_target", "");
INSERT INTO `wp_postmeta` VALUES("718", "242", "_menu_item_classes", "a:1:{i:0;s:0:\"\";}");
INSERT INTO `wp_postmeta` VALUES("719", "242", "_menu_item_xfn", "");
INSERT INTO `wp_postmeta` VALUES("720", "242", "_menu_item_url", "#");
INSERT INTO `wp_postmeta` VALUES("722", "178", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("723", "211", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("724", "210", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("725", "193", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("726", "180", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("727", "183", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("728", "186", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("729", "213", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("730", "214", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("731", "212", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("732", "200", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("733", "207", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("734", "203", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("735", "205", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("736", "206", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("737", "208", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("738", "204", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("739", "181", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("740", "215", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("741", "216", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("742", "217", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("743", "196", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("744", "197", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("745", "218", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("746", "219", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("747", "220", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("748", "209", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("749", "221", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("750", "222", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("751", "223", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("752", "195", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("753", "188", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("754", "191", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("755", "224", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("756", "225", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("757", "226", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("758", "227", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("759", "228", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("760", "190", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("761", "229", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("762", "230", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("763", "231", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("764", "232", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("765", "233", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("766", "234", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("767", "235", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("768", "236", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("769", "237", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("770", "199", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("771", "182", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("772", "192", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("773", "192", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("774", "189", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("775", "194", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("776", "187", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("777", "184", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("778", "198", "_wp_old_date", "2019-03-27");
INSERT INTO `wp_postmeta` VALUES("779", "202", "_wp_old_date", "2019-03-27");

/* INSERT TABLE DATA: wp_posts */
INSERT INTO `wp_posts` VALUES("1", "1", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "<!-- wp:paragraph -->\n<p>Welcome to WordPress. This is your first post. Edit or delete it, then start writing!</p>\n<!-- /wp:paragraph -->", "Hello world!", "", "publish", "open", "open", "", "hello-world", "", "", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "", "0", "http://localhost/ITQ/?p=1", "0", "post", "", "1");
INSERT INTO `wp_posts` VALUES("2", "1", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "<!-- wp:paragraph -->\n<p>This is an example page. It\'s different from a blog post because it will stay in one place and will show up in your site navigation (in most themes). Most people start with an About page that introduces them to potential site visitors. It might say something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Hi there! I\'m a bike messenger by day, aspiring actor by night, and this is my website. I live in Los Angeles, have a great dog named Jack, and I like pi&#241;a coladas. (And gettin\' caught in the rain.)</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...or something like this:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>The XYZ Doohickey Company was founded in 1971, and has been providing quality doohickeys to the public ever since. Located in Gotham City, XYZ employs over 2,000 people and does all kinds of awesome things for the Gotham community.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>As a new WordPress user, you should go to <a href=\"http://localhost/ITQ/wp-admin/\">your dashboard</a> to delete this page and create new pages for your content. Have fun!</p>\n<!-- /wp:paragraph -->", "Sample Page", "", "publish", "closed", "open", "", "sample-page", "", "", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "", "0", "http://localhost/ITQ/?page_id=2", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("3", "1", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "<!-- wp:heading --><h2>Who we are</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Our website address is: http://localhost/ITQ.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What personal data we collect and why we collect it</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comments</h3><!-- /wp:heading --><!-- wp:paragraph --><p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor&#8217;s IP address and browser user agent string to help spam detection.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>An anonymized string created from your email address (also called a hash) may be provided to the Gravatar service to see if you are using it. The Gravatar service privacy policy is available here: https://automattic.com/privacy/. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Media</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Contact forms</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment on our site you may opt-in to saving your name, email address and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you have an account and you log in to this site, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select &quot;Remember Me&quot;, your login will persist for two weeks. If you log out of your account, the login cookies will be removed.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>If you edit or publish an article, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the post ID of the article you just edited. It expires after 1 day.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Embedded content from other websites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Analytics</h3><!-- /wp:heading --><!-- wp:heading --><h2>Who we share your data with</h2><!-- /wp:heading --><!-- wp:heading --><h2>How long we retain your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time (except they cannot change their username). Website administrators can also see and edit that information.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>What rights you have over your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>If you have an account on this site, or have left comments, you can request to receive an exported file of the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Where we send your data</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Visitor comments may be checked through an automated spam detection service.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Your contact information</h2><!-- /wp:heading --><!-- wp:heading --><h2>Additional information</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>How we protect your data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What data breach procedures we have in place</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What third parties we receive data from</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>What automated decision making and/or profiling we do with user data</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Industry regulatory disclosure requirements</h3><!-- /wp:heading -->", "Privacy Policy", "", "draft", "closed", "open", "", "privacy-policy", "", "", "2019-03-21 14:08:59", "2019-03-21 14:08:59", "", "0", "http://localhost/ITQ/?page_id=3", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("5", "1", "2019-03-22 09:42:03", "2019-03-22 09:42:03", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:42:03\"\n    }\n}", "", "", "trash", "closed", "closed", "", "b4265c38-4ed6-4c3b-90fa-52ed1610c87b", "", "", "2019-03-22 09:42:03", "2019-03-22 09:42:03", "", "0", "http://localhost/ITQ/index.php/2019/03/22/b4265c38-4ed6-4c3b-90fa-52ed1610c87b/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("6", "1", "2019-03-22 09:42:03", "2019-03-22 09:42:03", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n    font-size: 24px;\n    margin-bottom: 25px;\n}\n.whats-new h4{\n	color: #ffffff;\n}\n.carousel-indicators {\n    bottom: -40px;\n}\n.carousel-indicators li {\n    background-color: #dddddd;\n    border-color: #dddddd;\n}\nh5 {\n    color: #632885;\n    font-size: 16px;\n    font-weight: 600;\n}", "starry-WORDPRESS/starry", "", "publish", "closed", "closed", "", "starry-wordpress-starry", "", "", "2019-03-22 10:11:32", "2019-03-22 10:11:32", "", "0", "http://localhost/ITQ/index.php/2019/03/22/starry-wordpress-starry/", "0", "custom_css", "", "0");
INSERT INTO `wp_posts` VALUES("7", "1", "2019-03-22 09:42:03", "2019-03-22 09:42:03", ".home.blog{\n	margin:0px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:42:03", "2019-03-22 09:42:03", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("8", "1", "2019-03-22 09:43:26", "2019-03-22 09:43:26", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:43:26\"\n    }\n}", "", "", "trash", "closed", "closed", "", "42f0d887-918c-4d6c-8b31-3472f4c87f66", "", "", "2019-03-22 09:43:26", "2019-03-22 09:43:26", "", "0", "http://localhost/ITQ/index.php/2019/03/22/42f0d887-918c-4d6c-8b31-3472f4c87f66/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("9", "1", "2019-03-22 09:43:26", "2019-03-22 09:43:26", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:43:26", "2019-03-22 09:43:26", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("10", "1", "2019-03-22 09:44:26", "2019-03-22 09:44:26", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:44:26\"\n    }\n}", "", "", "trash", "closed", "closed", "", "c811d693-c7ee-4cb0-8279-6540f5302890", "", "", "2019-03-22 09:44:26", "2019-03-22 09:44:26", "", "0", "http://localhost/ITQ/?p=10", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("11", "1", "2019-03-22 09:44:26", "2019-03-22 09:44:26", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:44:26", "2019-03-22 09:44:26", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("12", "1", "2019-03-22 09:47:00", "2019-03-22 09:47:00", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:47:00\"\n    }\n}", "", "", "trash", "closed", "closed", "", "a4cfa00c-60cf-4ace-8b5f-2beb7aa507b2", "", "", "2019-03-22 09:47:00", "2019-03-22 09:47:00", "", "0", "http://localhost/ITQ/index.php/2019/03/22/a4cfa00c-60cf-4ace-8b5f-2beb7aa507b2/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("13", "1", "2019-03-22 09:47:00", "2019-03-22 09:47:00", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:47:00", "2019-03-22 09:47:00", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("14", "1", "2019-03-22 09:48:17", "2019-03-22 09:48:17", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:48:17\"\n    }\n}", "", "", "trash", "closed", "closed", "", "4f546c69-7b7c-487b-85ab-66737e52ab47", "", "", "2019-03-22 09:48:17", "2019-03-22 09:48:17", "", "0", "http://localhost/ITQ/?p=14", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("15", "1", "2019-03-22 09:48:17", "2019-03-22 09:48:17", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:48:17", "2019-03-22 09:48:17", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("16", "1", "2019-03-22 09:49:12", "2019-03-22 09:49:12", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:49:12\"\n    }\n}", "", "", "trash", "closed", "closed", "", "ed15b442-1e99-4c81-a4ab-10ca19c8caf1", "", "", "2019-03-22 09:49:12", "2019-03-22 09:49:12", "", "0", "http://localhost/ITQ/index.php/2019/03/22/ed15b442-1e99-4c81-a4ab-10ca19c8caf1/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("17", "1", "2019-03-22 09:49:13", "2019-03-22 09:49:13", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n}\n.search-input:focus{\n	border: none;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:49:13", "2019-03-22 09:49:13", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("18", "1", "2019-03-22 09:49:47", "2019-03-22 09:49:47", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:49:47\"\n    }\n}", "", "", "trash", "closed", "closed", "", "ff5add57-9848-4de6-b5db-d3cf74e8c35f", "", "", "2019-03-22 09:49:47", "2019-03-22 09:49:47", "", "0", "http://localhost/ITQ/index.php/2019/03/22/ff5add57-9848-4de6-b5db-d3cf74e8c35f/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("19", "1", "2019-03-22 09:49:47", "2019-03-22 09:49:47", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:49:47", "2019-03-22 09:49:47", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("20", "1", "2019-03-22 09:53:52", "2019-03-22 09:53:52", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 09:53:52\"\n    }\n}", "", "", "trash", "closed", "closed", "", "22276a69-69f3-4e04-bae5-0010531a20f8", "", "", "2019-03-22 09:53:52", "2019-03-22 09:53:52", "", "0", "http://localhost/ITQ/?p=20", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("21", "1", "2019-03-22 09:53:52", "2019-03-22 09:53:52", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 09:53:52", "2019-03-22 09:53:52", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("22", "1", "2019-03-22 10:01:07", "2019-03-22 10:01:07", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px !important;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:01:07\"\n    }\n}", "", "", "trash", "closed", "closed", "", "b7b6bde3-6be2-4054-8825-de322ee4fe3e", "", "", "2019-03-22 10:01:07", "2019-03-22 10:01:07", "", "0", "http://localhost/ITQ/?p=22", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("23", "1", "2019-03-22 10:01:07", "2019-03-22 10:01:07", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px !important;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:01:07", "2019-03-22 10:01:07", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("24", "1", "2019-03-22 10:03:47", "2019-03-22 10:03:47", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:03:47\"\n    }\n}", "", "", "trash", "closed", "closed", "", "e33d5dd4-7319-4cbc-9fc8-0dcc61a0e5c8", "", "", "2019-03-22 10:03:47", "2019-03-22 10:03:47", "", "0", "http://localhost/ITQ/index.php/2019/03/22/e33d5dd4-7319-4cbc-9fc8-0dcc61a0e5c8/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("25", "1", "2019-03-22 10:03:48", "2019-03-22 10:03:48", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:03:48", "2019-03-22 10:03:48", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("26", "1", "2019-03-22 10:05:40", "2019-03-22 10:05:40", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\\nh4 {\\n    font-size: 24px;\\n    margin-bottom: 25px;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:05:40\"\n    }\n}", "", "", "trash", "closed", "closed", "", "02337e6e-27a5-4da8-b44f-8bc8a118c532", "", "", "2019-03-22 10:05:40", "2019-03-22 10:05:40", "", "0", "http://localhost/ITQ/index.php/2019/03/22/02337e6e-27a5-4da8-b44f-8bc8a118c532/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("27", "1", "2019-03-22 10:05:40", "2019-03-22 10:05:40", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n    font-size: 24px;\n    margin-bottom: 25px;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:05:40", "2019-03-22 10:05:40", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("28", "1", "2019-03-22 10:07:21", "2019-03-22 10:07:21", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\\nh4 {\\n    font-size: 24px;\\n    margin-bottom: 25px;\\n}\\n.carousel-indicators {\\n    bottom: -40px;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:07:21\"\n    }\n}", "", "", "trash", "closed", "closed", "", "1a70eb52-0d75-4a99-bb1d-387f4d04625e", "", "", "2019-03-22 10:07:21", "2019-03-22 10:07:21", "", "0", "http://localhost/ITQ/index.php/2019/03/22/1a70eb52-0d75-4a99-bb1d-387f4d04625e/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("29", "1", "2019-03-22 10:07:21", "2019-03-22 10:07:21", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n    font-size: 24px;\n    margin-bottom: 25px;\n}\n.carousel-indicators {\n    bottom: -40px;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:07:21", "2019-03-22 10:07:21", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("30", "1", "2019-03-22 10:08:06", "2019-03-22 10:08:06", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\\nh4 {\\n    font-size: 24px;\\n    margin-bottom: 25px;\\n}\\n.carousel-indicators {\\n    bottom: -40px;\\n}\\n.carousel-indicators li {\\n    background-color: #dddddd;\\n    border-color: #dddddd;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:08:06\"\n    }\n}", "", "", "trash", "closed", "closed", "", "ea52a1d3-055e-4840-ba16-25f5b6fbce60", "", "", "2019-03-22 10:08:06", "2019-03-22 10:08:06", "", "0", "http://localhost/ITQ/index.php/2019/03/22/ea52a1d3-055e-4840-ba16-25f5b6fbce60/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("31", "1", "2019-03-22 10:08:06", "2019-03-22 10:08:06", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n    font-size: 24px;\n    margin-bottom: 25px;\n}\n.carousel-indicators {\n    bottom: -40px;\n}\n.carousel-indicators li {\n    background-color: #dddddd;\n    border-color: #dddddd;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:08:06", "2019-03-22 10:08:06", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("32", "1", "2019-03-22 10:09:15", "2019-03-22 10:09:15", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\\nh4 {\\n    font-size: 24px;\\n    margin-bottom: 25px;\\n}\\n.carousel-indicators {\\n    bottom: -40px;\\n}\\n.carousel-indicators li {\\n    background-color: #dddddd;\\n    border-color: #dddddd;\\n}\\nh5 {\\n    color: #632885;\\n    font-size: 16px;\\n    font-weight: 600;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:09:15\"\n    }\n}", "", "", "trash", "closed", "closed", "", "525deb05-665e-4a7d-b122-1fa64c22c54a", "", "", "2019-03-22 10:09:15", "2019-03-22 10:09:15", "", "0", "http://localhost/ITQ/index.php/2019/03/22/525deb05-665e-4a7d-b122-1fa64c22c54a/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("33", "1", "2019-03-22 10:09:15", "2019-03-22 10:09:15", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n    font-size: 24px;\n    margin-bottom: 25px;\n}\n.carousel-indicators {\n    bottom: -40px;\n}\n.carousel-indicators li {\n    background-color: #dddddd;\n    border-color: #dddddd;\n}\nh5 {\n    color: #632885;\n    font-size: 16px;\n    font-weight: 600;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:09:15", "2019-03-22 10:09:15", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("34", "1", "2019-03-22 10:10:08", "2019-03-22 10:10:08", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\\nh4 {\\n\\tcolor: #ffffff;\\n    font-size: 24px;\\n    margin-bottom: 25px;\\n}\\n.carousel-indicators {\\n    bottom: -40px;\\n}\\n.carousel-indicators li {\\n    background-color: #dddddd;\\n    border-color: #dddddd;\\n}\\nh5 {\\n    color: #632885;\\n    font-size: 16px;\\n    font-weight: 600;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:10:08\"\n    }\n}", "", "", "trash", "closed", "closed", "", "b5921252-13d6-4c76-8941-7410826ade81", "", "", "2019-03-22 10:10:08", "2019-03-22 10:10:08", "", "0", "http://localhost/ITQ/index.php/2019/03/22/b5921252-13d6-4c76-8941-7410826ade81/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("35", "1", "2019-03-22 10:10:08", "2019-03-22 10:10:08", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n	color: #ffffff;\n    font-size: 24px;\n    margin-bottom: 25px;\n}\n.carousel-indicators {\n    bottom: -40px;\n}\n.carousel-indicators li {\n    background-color: #dddddd;\n    border-color: #dddddd;\n}\nh5 {\n    color: #632885;\n    font-size: 16px;\n    font-weight: 600;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:10:08", "2019-03-22 10:10:08", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("36", "1", "2019-03-22 10:11:32", "2019-03-22 10:11:32", "{\n    \"custom_css[starry-WORDPRESS/starry]\": {\n        \"value\": \".home.blog{\\n\\tmargin:0px !important;\\n}\\n.navbar-brand{\\n\\theight: 100% !important;\\n}\\n.navbar{\\n\\tmargin-bottom: 0px !important;\\n}\\n.navbar-form{\\n\\tmargin-top: 0px !important;\\n}\\n.search-input{\\n\\theight: 64px !important;\\n\\tpadding:10px !important;\\n\\tborder:none;\\n}\\n.search-input:focus{\\n\\tborder: none;\\n}\\n.testimonial-pic {\\n    width: 130px !important;\\n}\\n.carousel-control.left, .carousel-control.right {\\n    background-image: none !important;\\n}\\n.carousel-control {\\n    width: 4% !important;\\n}\\n.carousel-control {\\n    left: -20px;\\n    font-size: 34px !important;\\n    color: #111111 !important;\\n    top: -8px !important;\\n}\\nh4 {\\n    font-size: 24px;\\n    margin-bottom: 25px;\\n}\\n.whats-new h4{\\n\\tcolor: #ffffff;\\n}\\n.carousel-indicators {\\n    bottom: -40px;\\n}\\n.carousel-indicators li {\\n    background-color: #dddddd;\\n    border-color: #dddddd;\\n}\\nh5 {\\n    color: #632885;\\n    font-size: 16px;\\n    font-weight: 600;\\n}\",\n        \"type\": \"custom_css\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2019-03-22 10:11:32\"\n    }\n}", "", "", "trash", "closed", "closed", "", "79743b41-7788-414e-9fa7-dead01edcd6d", "", "", "2019-03-22 10:11:32", "2019-03-22 10:11:32", "", "0", "http://localhost/ITQ/index.php/2019/03/22/79743b41-7788-414e-9fa7-dead01edcd6d/", "0", "customize_changeset", "", "0");
INSERT INTO `wp_posts` VALUES("37", "1", "2019-03-22 10:11:32", "2019-03-22 10:11:32", ".home.blog{\n	margin:0px !important;\n}\n.navbar-brand{\n	height: 100% !important;\n}\n.navbar{\n	margin-bottom: 0px !important;\n}\n.navbar-form{\n	margin-top: 0px !important;\n}\n.search-input{\n	height: 64px !important;\n	padding:10px !important;\n	border:none;\n}\n.search-input:focus{\n	border: none;\n}\n.testimonial-pic {\n    width: 130px !important;\n}\n.carousel-control.left, .carousel-control.right {\n    background-image: none !important;\n}\n.carousel-control {\n    width: 4% !important;\n}\n.carousel-control {\n    left: -20px;\n    font-size: 34px !important;\n    color: #111111 !important;\n    top: -8px !important;\n}\nh4 {\n    font-size: 24px;\n    margin-bottom: 25px;\n}\n.whats-new h4{\n	color: #ffffff;\n}\n.carousel-indicators {\n    bottom: -40px;\n}\n.carousel-indicators li {\n    background-color: #dddddd;\n    border-color: #dddddd;\n}\nh5 {\n    color: #632885;\n    font-size: 16px;\n    font-weight: 600;\n}", "starry-WORDPRESS/starry", "", "inherit", "closed", "closed", "", "6-revision-v1", "", "", "2019-03-22 10:11:32", "2019-03-22 10:11:32", "", "6", "http://localhost/ITQ/index.php/2019/03/22/6-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("38", "1", "2019-03-26 04:16:17", "2019-03-26 04:16:17", "", "About us", "", "trash", "closed", "closed", "", "about-us__trashed", "", "", "2019-03-26 05:37:08", "2019-03-26 05:37:08", "", "0", "http://localhost/ITQ/?page_id=38", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("39", "1", "2019-03-26 04:16:17", "2019-03-26 04:16:17", "", "About us", "", "inherit", "closed", "closed", "", "38-revision-v1", "", "", "2019-03-26 04:16:17", "2019-03-26 04:16:17", "", "38", "http://localhost/ITQ/index.php/2019/03/26/38-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("69", "1", "2019-03-26 05:33:57", "2019-03-26 05:33:57", "", "About us", "", "inherit", "closed", "closed", "", "38-autosave-v1", "", "", "2019-03-26 05:33:57", "2019-03-26 05:33:57", "", "38", "http://localhost/ITQ/index.php/2019/03/26/38-autosave-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("70", "1", "2019-03-26 06:20:48", "2019-03-26 06:20:48", "", "about", "", "publish", "closed", "closed", "", "about", "", "", "2019-03-26 06:20:48", "2019-03-26 06:20:48", "", "0", "http://localhost/ITQ/?page_id=70", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("71", "1", "2019-03-26 05:34:37", "2019-03-26 05:34:37", "", "about", "", "inherit", "closed", "closed", "", "70-revision-v1", "", "", "2019-03-26 05:34:37", "2019-03-26 05:34:37", "", "70", "http://localhost/ITQ/index.php/2019/03/26/70-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("72", "1", "2019-03-26 06:36:23", "2019-03-26 06:36:23", "", "awards", "", "publish", "closed", "closed", "", "awards", "", "", "2019-03-26 06:36:23", "2019-03-26 06:36:23", "", "0", "http://localhost/ITQ/?page_id=72", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("73", "1", "2019-03-26 06:36:23", "2019-03-26 06:36:23", "", "awards", "", "inherit", "closed", "closed", "", "72-revision-v1", "", "", "2019-03-26 06:36:23", "2019-03-26 06:36:23", "", "72", "http://localhost/ITQ/index.php/2019/03/26/72-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("74", "1", "2019-03-26 07:00:59", "2019-03-26 07:00:59", "", "itqjourney", "", "publish", "closed", "closed", "", "itqjourney", "", "", "2019-03-26 07:00:59", "2019-03-26 07:00:59", "", "0", "http://localhost/ITQ/?page_id=74", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("75", "1", "2019-03-26 07:00:59", "2019-03-26 07:00:59", "", "itqjourney", "", "inherit", "closed", "closed", "", "74-revision-v1", "", "", "2019-03-26 07:00:59", "2019-03-26 07:00:59", "", "74", "http://localhost/ITQ/index.php/2019/03/26/74-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("76", "1", "2019-03-26 07:23:47", "2019-03-26 07:23:47", "", "leadershipTeam", "", "publish", "closed", "closed", "", "leadershipteam", "", "", "2019-03-27 14:09:13", "2019-03-27 14:09:13", "", "70", "http://localhost/ITQ/?page_id=76", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("77", "1", "2019-03-26 07:23:47", "2019-03-26 07:23:47", "", "leadershipTeam", "", "inherit", "closed", "closed", "", "76-revision-v1", "", "", "2019-03-26 07:23:47", "2019-03-26 07:23:47", "", "76", "http://localhost/ITQ/index.php/2019/03/26/76-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("78", "1", "2019-03-26 07:30:23", "2019-03-26 07:30:23", "", "csr", "", "publish", "closed", "closed", "", "csr", "", "", "2019-03-26 07:30:23", "2019-03-26 07:30:23", "", "0", "http://localhost/ITQ/?page_id=78", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("79", "1", "2019-03-26 07:30:23", "2019-03-26 07:30:23", "", "csr", "", "inherit", "closed", "closed", "", "78-revision-v1", "", "", "2019-03-26 07:30:23", "2019-03-26 07:30:23", "", "78", "http://localhost/ITQ/index.php/2019/03/26/78-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("80", "1", "2019-03-26 07:41:57", "2019-03-26 07:41:57", "", "hr-vision", "", "publish", "closed", "closed", "", "hr-vision", "", "", "2019-03-26 08:35:34", "2019-03-26 08:35:34", "", "0", "http://localhost/ITQ/?page_id=80", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("81", "1", "2019-03-26 07:41:57", "2019-03-26 07:41:57", "", "hr-vision", "", "inherit", "closed", "closed", "", "80-revision-v1", "", "", "2019-03-26 07:41:57", "2019-03-26 07:41:57", "", "80", "http://localhost/ITQ/index.php/2019/03/26/80-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("82", "1", "2019-03-26 08:38:58", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 08:38:58", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=82", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("83", "1", "2019-03-26 08:39:10", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 08:39:10", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=83", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("84", "1", "2019-03-26 08:39:51", "2019-03-26 08:39:51", "", "employee-value", "", "publish", "closed", "closed", "", "employee-value", "", "", "2019-03-26 08:39:51", "2019-03-26 08:39:51", "", "0", "http://localhost/ITQ/?page_id=84", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("85", "1", "2019-03-26 08:39:51", "2019-03-26 08:39:51", "", "employee-value", "", "inherit", "closed", "closed", "", "84-revision-v1", "", "", "2019-03-26 08:39:51", "2019-03-26 08:39:51", "", "84", "http://localhost/ITQ/index.php/2019/03/26/84-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("86", "1", "2019-03-26 08:51:14", "2019-03-26 08:51:14", "", "learning-development", "", "publish", "closed", "closed", "", "learning-development", "", "", "2019-03-26 08:51:14", "2019-03-26 08:51:14", "", "0", "http://localhost/ITQ/?page_id=86", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("87", "1", "2019-03-26 08:51:14", "2019-03-26 08:51:14", "", "learning-development", "", "inherit", "closed", "closed", "", "86-revision-v1", "", "", "2019-03-26 08:51:14", "2019-03-26 08:51:14", "", "86", "http://localhost/ITQ/index.php/2019/03/26/86-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("88", "1", "2019-03-26 08:59:23", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 08:59:23", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=88", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("89", "1", "2019-03-26 09:00:01", "2019-03-26 09:00:01", "", "diversity-inclusion", "", "publish", "closed", "closed", "", "diversity-inclusion", "", "", "2019-03-26 09:00:01", "2019-03-26 09:00:01", "", "0", "http://localhost/ITQ/?page_id=89", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("90", "1", "2019-03-26 09:00:01", "2019-03-26 09:00:01", "", "diversity-inclusion", "", "inherit", "closed", "closed", "", "89-revision-v1", "", "", "2019-03-26 09:00:01", "2019-03-26 09:00:01", "", "89", "http://localhost/ITQ/index.php/2019/03/26/89-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("91", "1", "2019-03-26 09:05:06", "2019-03-26 09:05:06", "", "carrer", "", "publish", "closed", "closed", "", "carrer", "", "", "2019-03-26 09:05:06", "2019-03-26 09:05:06", "", "0", "http://localhost/ITQ/?page_id=91", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("92", "1", "2019-03-26 09:05:06", "2019-03-26 09:05:06", "", "carrer", "", "inherit", "closed", "closed", "", "91-revision-v1", "", "", "2019-03-26 09:05:06", "2019-03-26 09:05:06", "", "91", "http://localhost/ITQ/index.php/2019/03/26/91-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("93", "1", "2019-03-26 09:22:54", "2019-03-26 09:22:54", "", "our-vision", "", "publish", "closed", "closed", "", "our-vision", "", "", "2019-03-26 09:22:54", "2019-03-26 09:22:54", "", "0", "http://localhost/ITQ/?page_id=93", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("94", "1", "2019-03-26 09:22:54", "2019-03-26 09:22:54", "", "our-vision", "", "inherit", "closed", "closed", "", "93-revision-v1", "", "", "2019-03-26 09:22:54", "2019-03-26 09:22:54", "", "93", "http://localhost/ITQ/index.php/2019/03/26/93-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("95", "1", "2019-03-26 09:28:06", "2019-03-26 09:28:06", "", "travel-education", "", "publish", "closed", "closed", "", "travel-education", "", "", "2019-03-26 09:28:06", "2019-03-26 09:28:06", "", "0", "http://localhost/ITQ/?page_id=95", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("96", "1", "2019-03-26 09:28:06", "2019-03-26 09:28:06", "", "travel-education", "", "inherit", "closed", "closed", "", "95-revision-v1", "", "", "2019-03-26 09:28:06", "2019-03-26 09:28:06", "", "95", "http://localhost/ITQ/index.php/2019/03/26/95-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("97", "1", "2019-03-26 10:59:29", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 10:59:29", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=97", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("98", "1", "2019-03-26 10:59:41", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 10:59:41", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=98", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("99", "1", "2019-03-26 11:00:18", "2019-03-26 11:00:18", "", "press-release", "", "publish", "closed", "closed", "", "press-release", "", "", "2019-03-26 11:00:18", "2019-03-26 11:00:18", "", "0", "http://localhost/ITQ/?page_id=99", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("100", "1", "2019-03-26 11:00:18", "2019-03-26 11:00:18", "", "press-release", "", "inherit", "closed", "closed", "", "99-revision-v1", "", "", "2019-03-26 11:00:18", "2019-03-26 11:00:18", "", "99", "http://localhost/ITQ/index.php/2019/03/26/99-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("128", "1", "2019-03-26 12:05:02", "2019-03-26 12:05:02", "", "press-release", "", "inherit", "closed", "closed", "", "99-autosave-v1", "", "", "2019-03-26 12:05:02", "2019-03-26 12:05:02", "", "99", "http://localhost/ITQ/index.php/2019/03/26/99-autosave-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("129", "1", "2019-03-26 12:05:38", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 12:05:38", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=129", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("130", "1", "2019-03-26 12:06:03", "2019-03-26 12:06:03", "", "product-travelport", "", "publish", "closed", "closed", "", "product-travelport", "", "", "2019-03-26 12:06:03", "2019-03-26 12:06:03", "", "0", "http://localhost/ITQ/?page_id=130", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("131", "1", "2019-03-26 12:06:03", "2019-03-26 12:06:03", "", "product-travelport", "", "inherit", "closed", "closed", "", "130-revision-v1", "", "", "2019-03-26 12:06:03", "2019-03-26 12:06:03", "", "130", "http://localhost/ITQ/index.php/2019/03/26/130-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("132", "1", "2019-03-26 12:47:54", "2019-03-26 12:47:54", "", "travelport-ticketing", "", "publish", "closed", "closed", "", "travelport-ticketing", "", "", "2019-03-26 12:47:54", "2019-03-26 12:47:54", "", "0", "http://localhost/ITQ/?page_id=132", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("133", "1", "2019-03-26 12:47:54", "2019-03-26 12:47:54", "", "travelport-ticketing", "", "inherit", "closed", "closed", "", "132-revision-v1", "", "", "2019-03-26 12:47:54", "2019-03-26 12:47:54", "", "132", "http://localhost/ITQ/index.php/2019/03/26/132-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("134", "1", "2019-03-26 12:55:29", "2019-03-26 12:55:29", "", "travelport-fares", "", "publish", "closed", "closed", "", "travelport-fares", "", "", "2019-03-26 12:55:29", "2019-03-26 12:55:29", "", "0", "http://localhost/ITQ/?page_id=134", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("135", "1", "2019-03-26 12:55:29", "2019-03-26 12:55:29", "", "travelport-fares", "", "inherit", "closed", "closed", "", "134-revision-v1", "", "", "2019-03-26 12:55:29", "2019-03-26 12:55:29", "", "134", "http://localhost/ITQ/index.php/2019/03/26/134-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("136", "1", "2019-03-26 13:15:22", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:15:22", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=136", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("137", "1", "2019-03-26 13:15:32", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:15:32", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=137", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("138", "1", "2019-03-26 13:15:49", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:15:49", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=138", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("139", "1", "2019-03-26 13:16:20", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:16:20", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=139", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("140", "1", "2019-03-26 13:16:58", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:16:58", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=140", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("141", "1", "2019-03-26 13:17:51", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:17:51", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=141", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("142", "1", "2019-03-26 13:18:54", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-26 13:18:54", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=142", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("143", "1", "2019-03-26 13:20:42", "2019-03-26 13:20:42", "", "travelport-non-air", "", "publish", "closed", "closed", "", "travelport-non-air", "", "", "2019-03-26 13:20:42", "2019-03-26 13:20:42", "", "0", "http://localhost/ITQ/?page_id=143", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("144", "1", "2019-03-26 13:20:42", "2019-03-26 13:20:42", "", "travelport-non-air", "", "inherit", "closed", "closed", "", "143-revision-v1", "", "", "2019-03-26 13:20:42", "2019-03-26 13:20:42", "", "143", "http://localhost/ITQ/index.php/2019/03/26/143-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("145", "1", "2019-03-26 13:27:27", "2019-03-26 13:27:27", "", "travelport-office-management", "", "publish", "closed", "closed", "", "travelport-office-management", "", "", "2019-03-26 13:27:27", "2019-03-26 13:27:27", "", "0", "http://localhost/ITQ/?page_id=145", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("146", "1", "2019-03-26 13:27:27", "2019-03-26 13:27:27", "", "travelport-office-management", "", "inherit", "closed", "closed", "", "145-revision-v1", "", "", "2019-03-26 13:27:27", "2019-03-26 13:27:27", "", "145", "http://localhost/ITQ/index.php/2019/03/26/145-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("147", "1", "2019-03-26 13:35:09", "2019-03-26 13:35:09", "", "travelport-value-adds", "", "publish", "closed", "closed", "", "travelport-value-adds", "", "", "2019-03-26 13:35:09", "2019-03-26 13:35:09", "", "0", "http://localhost/ITQ/?page_id=147", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("148", "1", "2019-03-26 13:35:09", "2019-03-26 13:35:09", "", "travelport-value-adds", "", "inherit", "closed", "closed", "", "147-revision-v1", "", "", "2019-03-26 13:35:09", "2019-03-26 13:35:09", "", "147", "http://localhost/ITQ/index.php/2019/03/26/147-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("149", "1", "2019-03-26 13:40:33", "2019-03-26 13:40:33", "", "travelport-galileo-desktop", "", "publish", "closed", "closed", "", "travelport-galileo-desktop", "", "", "2019-03-26 13:40:33", "2019-03-26 13:40:33", "", "0", "http://localhost/ITQ/?page_id=149", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("150", "1", "2019-03-26 13:40:33", "2019-03-26 13:40:33", "", "travelport-galileo-desktop", "", "inherit", "closed", "closed", "", "149-revision-v1", "", "", "2019-03-26 13:40:33", "2019-03-26 13:40:33", "", "149", "http://localhost/ITQ/index.php/2019/03/26/149-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("151", "1", "2019-03-27 04:28:33", "2019-03-27 04:28:33", "", "ndc", "", "publish", "closed", "closed", "", "ndc", "", "", "2019-03-27 04:28:33", "2019-03-27 04:28:33", "", "0", "http://localhost/ITQ/?page_id=151", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("152", "1", "2019-03-27 04:28:33", "2019-03-27 04:28:33", "", "ndc", "", "inherit", "closed", "closed", "", "151-revision-v1", "", "", "2019-03-27 04:28:33", "2019-03-27 04:28:33", "", "151", "http://localhost/ITQ/index.php/2019/03/27/151-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("153", "1", "2019-03-27 04:41:55", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-27 04:41:55", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=153", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("154", "1", "2019-03-27 04:42:21", "2019-03-27 04:42:21", "", "airlines", "", "publish", "closed", "closed", "", "airlines", "", "", "2019-03-27 04:42:21", "2019-03-27 04:42:21", "", "0", "http://localhost/ITQ/?page_id=154", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("155", "1", "2019-03-27 04:42:21", "2019-03-27 04:42:21", "", "airlines", "", "inherit", "closed", "closed", "", "154-revision-v1", "", "", "2019-03-27 04:42:21", "2019-03-27 04:42:21", "", "154", "http://localhost/ITQ/index.php/2019/03/27/154-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("156", "1", "2019-03-27 05:07:51", "2019-03-27 05:07:51", "", "ecommerce-api", "", "publish", "closed", "closed", "", "ecommerce-api", "", "", "2019-03-27 05:07:51", "2019-03-27 05:07:51", "", "0", "http://localhost/ITQ/?page_id=156", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("157", "1", "2019-03-27 05:07:51", "2019-03-27 05:07:51", "", "ecommerce-api", "", "inherit", "closed", "closed", "", "156-revision-v1", "", "", "2019-03-27 05:07:51", "2019-03-27 05:07:51", "", "156", "http://localhost/ITQ/index.php/2019/03/27/156-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("158", "1", "2019-03-27 05:23:24", "2019-03-27 05:23:24", "", "epricing", "", "publish", "closed", "closed", "", "epricing", "", "", "2019-03-27 05:23:24", "2019-03-27 05:23:24", "", "0", "http://localhost/ITQ/?page_id=158", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("159", "1", "2019-03-27 05:23:24", "2019-03-27 05:23:24", "", "epricing", "", "inherit", "closed", "closed", "", "158-revision-v1", "", "", "2019-03-27 05:23:24", "2019-03-27 05:23:24", "", "158", "http://localhost/ITQ/index.php/2019/03/27/158-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("160", "1", "2019-03-27 05:34:14", "2019-03-27 05:34:14", "", "online-solutions", "", "publish", "closed", "closed", "", "online-solutions", "", "", "2019-03-27 05:34:14", "2019-03-27 05:34:14", "", "0", "http://localhost/ITQ/?page_id=160", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("161", "1", "2019-03-27 05:34:14", "2019-03-27 05:34:14", "", "online-solutions", "", "inherit", "closed", "closed", "", "160-revision-v1", "", "", "2019-03-27 05:34:14", "2019-03-27 05:34:14", "", "160", "http://localhost/ITQ/index.php/2019/03/27/160-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("162", "1", "2019-03-27 05:43:39", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-27 05:43:39", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=162", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("163", "1", "2019-03-27 05:44:05", "2019-03-27 05:44:05", "", "online-solutions-booking", "", "publish", "closed", "closed", "", "online-solutions-booking", "", "", "2019-03-27 05:44:05", "2019-03-27 05:44:05", "", "0", "http://localhost/ITQ/?page_id=163", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("164", "1", "2019-03-27 05:44:05", "2019-03-27 05:44:05", "", "online-solutions-booking", "", "inherit", "closed", "closed", "", "163-revision-v1", "", "", "2019-03-27 05:44:05", "2019-03-27 05:44:05", "", "163", "http://localhost/ITQ/index.php/2019/03/27/163-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("165", "1", "2019-03-27 05:49:18", "2019-03-27 05:49:18", "", "enhanced-utilities", "", "publish", "closed", "closed", "", "enhanced-utilities", "", "", "2019-03-27 05:49:18", "2019-03-27 05:49:18", "", "0", "http://localhost/ITQ/?page_id=165", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("166", "1", "2019-03-27 05:49:18", "2019-03-27 05:49:18", "", "enhanced-utilities", "", "inherit", "closed", "closed", "", "165-revision-v1", "", "", "2019-03-27 05:49:18", "2019-03-27 05:49:18", "", "165", "http://localhost/ITQ/index.php/2019/03/27/165-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("167", "1", "2019-03-27 06:45:55", "2019-03-27 06:45:55", "", "worldspan", "", "publish", "closed", "closed", "", "worldspan", "", "", "2019-03-27 06:45:55", "2019-03-27 06:45:55", "", "0", "http://localhost/ITQ/?page_id=167", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("168", "1", "2019-03-27 06:45:55", "2019-03-27 06:45:55", "", "worldspan", "", "inherit", "closed", "closed", "", "167-revision-v1", "", "", "2019-03-27 06:45:55", "2019-03-27 06:45:55", "", "167", "http://localhost/ITQ/index.php/2019/03/27/167-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("169", "1", "2019-03-27 08:03:48", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-27 08:03:48", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=169", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("170", "1", "2019-03-27 08:04:06", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-27 08:04:06", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=170", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("171", "1", "2019-03-27 08:04:25", "2019-03-27 08:04:25", "", "contact-us", "", "publish", "closed", "closed", "", "contact-us", "", "", "2019-03-27 08:04:25", "2019-03-27 08:04:25", "", "0", "http://localhost/ITQ/?page_id=171", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("172", "1", "2019-03-27 08:04:25", "2019-03-27 08:04:25", "", "contact-us", "", "inherit", "closed", "closed", "", "171-revision-v1", "", "", "2019-03-27 08:04:25", "2019-03-27 08:04:25", "", "171", "http://localhost/ITQ/index.php/2019/03/27/171-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("173", "1", "2019-03-27 09:43:44", "2019-03-27 09:43:44", "<!-- wp:paragraph -->\n<p>example blog</p>\n<!-- /wp:paragraph -->", "new blog", "", "trash", "open", "open", "", "new-blog__trashed", "", "", "2019-03-27 09:44:25", "2019-03-27 09:44:25", "", "0", "http://localhost/ITQ/?p=173", "0", "post", "", "0");
INSERT INTO `wp_posts` VALUES("174", "1", "2019-03-27 09:43:44", "2019-03-27 09:43:44", "<!-- wp:paragraph -->\n<p>example blog</p>\n<!-- /wp:paragraph -->", "new blog", "", "inherit", "closed", "closed", "", "173-revision-v1", "", "", "2019-03-27 09:43:44", "2019-03-27 09:43:44", "", "173", "http://localhost/ITQ/index.php/2019/03/27/173-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("175", "1", "2019-03-27 12:42:38", "2019-03-27 12:42:38", "", "articles", "", "publish", "closed", "closed", "", "articles", "", "", "2019-03-27 12:42:38", "2019-03-27 12:42:38", "", "0", "http://localhost/ITQ/?page_id=175", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("176", "1", "2019-03-27 12:42:38", "2019-03-27 12:42:38", "", "articles", "", "inherit", "closed", "closed", "", "175-revision-v1", "", "", "2019-03-27 12:42:38", "2019-03-27 12:42:38", "", "175", "http://localhost/ITQ/index.php/2019/03/27/175-revision-v1/", "0", "revision", "", "0");
INSERT INTO `wp_posts` VALUES("177", "1", "2019-03-27 13:21:37", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "closed", "closed", "", "", "", "", "2019-03-27 13:21:37", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?page_id=177", "0", "page", "", "0");
INSERT INTO `wp_posts` VALUES("178", "1", "2019-03-28 05:01:50", "2019-03-28 05:01:50", "", "HOME", "", "publish", "closed", "closed", "", "home", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=178", "1", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("179", "1", "2019-03-27 14:16:11", "0000-00-00 00:00:00", " ", "", "", "draft", "closed", "closed", "", "", "", "", "2019-03-27 14:16:11", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?p=179", "1", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("180", "1", "2019-03-28 05:01:51", "2019-03-28 05:01:51", "", "LEADERSHIP TEAM", "", "publish", "closed", "closed", "", "leadership-team", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "70", "http://localhost/ITQ/?p=180", "5", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("181", "1", "2019-03-28 05:01:54", "2019-03-28 05:01:54", "", "AIRLINES", "", "publish", "closed", "closed", "", "airlines", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=181", "18", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("182", "1", "2019-03-28 05:02:04", "2019-03-28 05:02:04", "", "ARTICLES", "", "publish", "closed", "closed", "", "articles", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=182", "52", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("183", "1", "2019-03-28 05:01:51", "2019-03-28 05:01:51", "", "AWARDS", "", "publish", "closed", "closed", "", "awards", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=183", "6", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("184", "1", "2019-03-28 05:02:06", "2019-03-28 05:02:06", "", "CAREER", "", "publish", "closed", "closed", "", "career", "", "", "2019-03-28 12:04:51", "2019-03-28 12:04:51", "", "0", "http://localhost/ITQ/?p=184", "58", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("185", "1", "2019-03-27 14:16:14", "0000-00-00 00:00:00", " ", "", "", "draft", "closed", "closed", "", "", "", "", "2019-03-27 14:16:14", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?p=185", "1", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("186", "1", "2019-03-28 05:01:51", "2019-03-28 05:01:51", "", "CSR", "", "publish", "closed", "closed", "", "csr", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=186", "7", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("187", "1", "2019-03-28 05:02:05", "2019-03-28 05:02:05", "", "DIVERSITY AND INCLUSION", "", "publish", "closed", "closed", "", "diversity-and-inclusion", "", "", "2019-03-28 12:04:51", "2019-03-28 12:04:51", "", "0", "http://localhost/ITQ/?p=187", "57", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("188", "1", "2019-03-28 05:01:58", "2019-03-28 05:01:58", "", "ECOMMERCE APIs", "", "publish", "closed", "closed", "", "ecommerce-apis", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=188", "32", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("189", "1", "2019-03-28 05:02:05", "2019-03-28 05:02:05", "", "EMPLOYEE VALUE PROPOSITION & TOTAL REWARDS", "", "publish", "closed", "closed", "", "employee-value-proposition-total-rewards", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=189", "55", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("190", "1", "2019-03-28 05:02:00", "2019-03-28 05:02:00", "", "ENHANCED UTILITIES", "", "publish", "closed", "closed", "", "enhanced-utilities", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=190", "39", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("191", "1", "2019-03-28 05:01:59", "2019-03-28 05:01:59", "", "UNIVERSAL API", "", "publish", "closed", "closed", "", "universal-api", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=191", "33", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("192", "1", "2019-03-28 05:02:04", "2019-03-28 05:02:04", "", "HR VISION & VALUES", "", "publish", "closed", "closed", "", "hr-vision-values", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=192", "54", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("193", "1", "2019-03-28 05:01:50", "2019-03-28 05:01:50", "", "ITQ JOURNEY", "", "publish", "closed", "closed", "", "itq-journey", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=193", "4", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("194", "1", "2019-03-28 05:02:05", "2019-03-28 05:02:05", "", "LEARNING AND DEVELOPMENT", "", "publish", "closed", "closed", "", "learning-and-development", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=194", "56", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("195", "1", "2019-03-28 05:01:58", "2019-03-28 05:01:58", "", "NDC", "", "publish", "closed", "closed", "", "ndc", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=195", "31", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("196", "1", "2019-03-28 05:01:56", "2019-03-28 05:01:56", "", "ONLINE SOLUTIONS", "", "publish", "closed", "closed", "", "online-solutions", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=196", "22", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("197", "1", "2019-03-28 05:01:56", "2019-03-28 05:01:56", "", "CORPORATE BOOKING TOOL", "", "publish", "closed", "closed", "", "corporate-booking-tool", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=197", "23", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("198", "1", "2019-03-28 05:02:06", "2019-03-28 05:02:06", "", "OUR VISION", "", "publish", "closed", "closed", "", "our-vision", "", "", "2019-03-28 12:04:51", "2019-03-28 12:04:51", "", "0", "http://localhost/ITQ/?p=198", "60", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("199", "1", "2019-03-28 05:02:03", "2019-03-28 05:02:03", "", "PRESS RELEASEs", "", "publish", "closed", "closed", "", "press-releases", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=199", "50", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("200", "1", "2019-03-28 05:01:52", "2019-03-28 05:01:52", "", "TRAVELPORT SMARTPOINT", "", "publish", "closed", "closed", "", "travelport-smartpoint", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=200", "11", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("201", "1", "2019-03-27 14:16:24", "0000-00-00 00:00:00", " ", "", "", "draft", "closed", "closed", "", "", "", "", "2019-03-27 14:16:24", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?p=201", "1", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("202", "1", "2019-03-28 05:02:07", "2019-03-28 05:02:07", "", "TRAVEL EDUCATION", "", "publish", "closed", "closed", "", "travel-education", "", "", "2019-03-28 12:04:51", "2019-03-28 12:04:51", "", "0", "http://localhost/ITQ/?p=202", "61", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("203", "1", "2019-03-28 05:01:52", "2019-03-28 05:01:52", "", "FARES", "", "publish", "closed", "closed", "", "fares", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=203", "13", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("204", "1", "2019-03-28 05:01:54", "2019-03-28 05:01:54", "", "GALILEO DESKTOP", "", "publish", "closed", "closed", "", "galileo-desktop", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=204", "17", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("205", "1", "2019-03-28 05:01:53", "2019-03-28 05:01:53", "", "NON-AIR", "", "publish", "closed", "closed", "", "non-air", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=205", "14", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("206", "1", "2019-03-28 05:01:53", "2019-03-28 05:01:53", "", "OFFICE-MANAGEMENT", "", "publish", "closed", "closed", "", "office-management", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=206", "15", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("207", "1", "2019-03-28 05:01:52", "2019-03-28 05:01:52", "", "TICKETING", "", "publish", "closed", "closed", "", "ticketing", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=207", "12", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("208", "1", "2019-03-28 05:01:53", "2019-03-28 05:01:53", "", "VALUE ADDS", "", "publish", "closed", "closed", "", "value-adds", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=208", "16", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("209", "1", "2019-03-28 05:01:57", "2019-03-28 05:01:57", "", "WORLDSPAN", "", "publish", "closed", "closed", "", "worldspan", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=209", "27", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("210", "1", "2019-03-28 05:01:50", "2019-03-28 05:01:50", "", "COMPANY PROFILE", "", "publish", "closed", "closed", "", "company-profile", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=210", "3", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("211", "1", "2019-03-28 05:01:50", "2019-03-28 05:01:50", "", "ABOUT US", "", "publish", "closed", "closed", "", "about-us", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=211", "2", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("212", "1", "2019-03-28 05:01:52", "2019-03-28 05:01:52", "", "PRODUCTS", "", "publish", "closed", "closed", "", "products", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=212", "10", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("213", "1", "2019-03-28 05:01:51", "2019-03-28 05:01:51", "", "BLOGS", "", "publish", "closed", "closed", "", "blogs", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=213", "8", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("214", "1", "2019-03-28 05:01:51", "2019-03-28 05:01:51", "", "EVENTS", "", "publish", "closed", "closed", "", "events", "", "", "2019-03-28 12:04:48", "2019-03-28 12:04:48", "", "0", "http://localhost/ITQ/?p=214", "9", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("215", "1", "2019-03-28 05:01:55", "2019-03-28 05:01:55", "", "JET AIRWAYS BOOKING PROCEDURES", "", "publish", "closed", "closed", "", "jet-airways-booking-procedures", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=215", "19", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("216", "1", "2019-03-28 05:01:55", "2019-03-28 05:01:55", "", "AIR INDIA BOOKING PROCEDURES", "", "publish", "closed", "closed", "", "air-india-booking-procedures", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=216", "20", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("217", "1", "2019-03-28 05:01:55", "2019-03-28 05:01:55", "", "INDIGO BOOKING PROCEDURES", "", "publish", "closed", "closed", "", "indigo-booking-procedures", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=217", "21", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("218", "1", "2019-03-28 05:01:56", "2019-03-28 05:01:56", "", "SELF BOOKING TOOL", "", "publish", "closed", "closed", "", "self-booking-tool", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=218", "24", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("219", "1", "2019-03-28 05:01:56", "2019-03-28 05:01:56", "", "GALSTAR (B2B / B2C WHITE LABEL)", "", "publish", "closed", "closed", "", "galstar-b2b-b2c-white-label", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=219", "25", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("220", "1", "2019-03-28 05:01:57", "2019-03-28 05:01:57", "", "B2B INTERNET BOOKING ENGINE", "", "publish", "closed", "closed", "", "b2b-internet-booking-engine", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=220", "26", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("221", "1", "2019-03-28 05:01:57", "2019-03-28 05:01:57", "", "TRAVEL AGENCY", "", "publish", "closed", "closed", "", "travel-agency", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=221", "28", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("222", "1", "2019-03-28 05:01:58", "2019-03-28 05:01:58", "", "TRAVEL SUPPLIER", "", "publish", "closed", "closed", "", "travel-supplier", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=222", "29", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("223", "1", "2019-03-28 05:01:58", "2019-03-28 05:01:58", "", "ONLINE SOLUTION", "", "publish", "closed", "closed", "", "online-solution", "", "", "2019-03-28 12:04:49", "2019-03-28 12:04:49", "", "0", "http://localhost/ITQ/?p=223", "30", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("224", "1", "2019-03-28 05:01:59", "2019-03-28 05:01:59", "", "GALILEO WEB SERVICES", "", "publish", "closed", "closed", "", "galileo-web-services", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=224", "34", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("225", "1", "2019-03-28 05:01:59", "2019-03-28 05:01:59", "", "E-PRICING", "", "publish", "closed", "closed", "", "e-pricing", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=225", "35", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("226", "1", "2019-03-28 05:01:59", "2019-03-28 05:01:59", "", "XML API DESKTOP", "", "publish", "closed", "closed", "", "xml-api-desktop", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=226", "36", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("227", "1", "2019-03-28 05:02:00", "2019-03-28 05:02:00", "", "MINI RULE SERVICE", "", "publish", "closed", "closed", "", "mini-rule-service", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=227", "37", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("228", "1", "2019-03-28 05:02:00", "2019-03-28 05:02:00", "", "TICKET XCHANGE API - VR3", "", "publish", "closed", "closed", "", "ticket-xchange-api-vr3", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=228", "38", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("229", "1", "2019-03-28 05:02:01", "2019-03-28 05:02:01", "", "ADVANCED HMPR", "", "publish", "closed", "closed", "", "advanced-hmpr", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=229", "40", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("230", "1", "2019-03-28 05:02:01", "2019-03-28 05:02:01", "", "E-TICKET APPLICATION", "", "publish", "closed", "closed", "", "e-ticket-application", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=230", "41", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("231", "1", "2019-03-28 05:02:01", "2019-03-28 05:02:01", "", "GALILEO FLIGHT INFO (PNR SMS)", "", "publish", "closed", "closed", "", "galileo-flight-info-pnr-sms", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=231", "42", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("232", "1", "2019-03-28 05:02:05", "2019-03-28 05:02:05", "", "TRAVEL INSURANCE", "", "publish", "closed", "closed", "", "travel-insurance", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=232", "43", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("233", "1", "2019-03-28 05:02:02", "2019-03-28 05:02:02", "", "ITQ FINANCIAL", "", "publish", "closed", "closed", "", "itq-financial", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=233", "44", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("234", "1", "2019-03-28 05:02:02", "2019-03-28 05:02:02", "", "LCC INTEGRATOR", "", "publish", "closed", "closed", "", "lcc-integrator", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=234", "45", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("235", "1", "2019-03-28 05:02:02", "2019-03-28 05:02:02", "", "VR3", "", "publish", "closed", "closed", "", "vr3", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=235", "46", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("236", "1", "2019-03-28 05:02:03", "2019-03-28 05:02:03", "", "BSP QUOTA TOOL", "", "publish", "closed", "closed", "", "bsp-quota-tool", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=236", "47", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("237", "1", "2019-03-28 05:02:03", "2019-03-28 05:02:03", "", "LOW FARE FINDER", "", "publish", "closed", "closed", "", "low-fare-finder", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=237", "48", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("238", "1", "2019-03-28 05:02:03", "2019-03-28 05:02:03", "", "PRESS ROOM", "", "publish", "closed", "closed", "", "press-room", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=238", "49", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("239", "1", "2019-03-28 05:02:04", "2019-03-28 05:02:04", "", "ADVERTISEMENTS", "", "publish", "closed", "closed", "", "advertisements", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=239", "51", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("240", "1", "2019-03-28 05:02:04", "2019-03-28 05:02:04", "", "HUMAN CAPITAL", "", "publish", "closed", "closed", "", "human-capital", "", "", "2019-03-28 12:04:50", "2019-03-28 12:04:50", "", "0", "http://localhost/ITQ/?p=240", "53", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("241", "1", "2019-03-28 05:02:06", "2019-03-28 05:02:06", "", "TRAINING", "", "publish", "closed", "closed", "", "training", "", "", "2019-03-28 12:04:51", "2019-03-28 12:04:51", "", "0", "http://localhost/ITQ/?p=241", "59", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("242", "1", "2019-03-28 05:02:07", "2019-03-28 05:02:07", "", "LINK TO NEW PORTAL", "", "publish", "closed", "closed", "", "link-to-new-portal", "", "", "2019-03-28 12:04:51", "2019-03-28 12:04:51", "", "0", "http://localhost/ITQ/?p=242", "62", "nav_menu_item", "", "0");
INSERT INTO `wp_posts` VALUES("243", "1", "2019-03-28 16:59:25", "0000-00-00 00:00:00", "", "Auto Draft", "", "auto-draft", "open", "open", "", "", "", "", "2019-03-28 16:59:25", "0000-00-00 00:00:00", "", "0", "http://localhost/ITQ/?p=243", "0", "post", "", "0");

/* INSERT TABLE DATA: wp_term_relationships */
INSERT INTO `wp_term_relationships` VALUES("1", "1", "0");
INSERT INTO `wp_term_relationships` VALUES("173", "1", "0");
INSERT INTO `wp_term_relationships` VALUES("178", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("180", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("181", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("182", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("183", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("184", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("186", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("187", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("188", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("189", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("190", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("191", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("192", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("193", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("194", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("195", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("196", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("197", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("198", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("199", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("200", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("202", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("203", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("204", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("205", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("206", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("207", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("208", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("209", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("210", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("211", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("212", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("213", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("214", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("215", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("216", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("217", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("218", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("219", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("220", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("221", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("222", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("223", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("224", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("225", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("226", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("227", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("228", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("229", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("230", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("231", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("232", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("233", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("234", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("235", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("236", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("237", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("238", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("239", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("240", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("241", "3", "0");
INSERT INTO `wp_term_relationships` VALUES("242", "3", "0");

/* INSERT TABLE DATA: wp_term_taxonomy */
INSERT INTO `wp_term_taxonomy` VALUES("1", "1", "category", "", "0", "1");
INSERT INTO `wp_term_taxonomy` VALUES("3", "3", "nav_menu", "", "0", "62");

/* INSERT TABLE DATA: wp_terms */
INSERT INTO `wp_terms` VALUES("1", "Uncategorized", "uncategorized", "0");
INSERT INTO `wp_terms` VALUES("3", "Primary-Menu", "primary-menu", "0");

/* INSERT TABLE DATA: wp_usermeta */
INSERT INTO `wp_usermeta` VALUES("1", "1", "nickname", "admin");
INSERT INTO `wp_usermeta` VALUES("2", "1", "first_name", "");
INSERT INTO `wp_usermeta` VALUES("3", "1", "last_name", "");
INSERT INTO `wp_usermeta` VALUES("4", "1", "description", "");
INSERT INTO `wp_usermeta` VALUES("5", "1", "rich_editing", "true");
INSERT INTO `wp_usermeta` VALUES("6", "1", "syntax_highlighting", "true");
INSERT INTO `wp_usermeta` VALUES("7", "1", "comment_shortcuts", "false");
INSERT INTO `wp_usermeta` VALUES("8", "1", "admin_color", "fresh");
INSERT INTO `wp_usermeta` VALUES("9", "1", "use_ssl", "0");
INSERT INTO `wp_usermeta` VALUES("10", "1", "show_admin_bar_front", "false");
INSERT INTO `wp_usermeta` VALUES("11", "1", "locale", "");
INSERT INTO `wp_usermeta` VALUES("12", "1", "wp_capabilities", "a:1:{s:13:\"administrator\";b:1;}");
INSERT INTO `wp_usermeta` VALUES("13", "1", "wp_user_level", "10");
INSERT INTO `wp_usermeta` VALUES("14", "1", "dismissed_wp_pointers", "wp496_privacy,theme_editor_notice");
INSERT INTO `wp_usermeta` VALUES("15", "1", "show_welcome_panel", "1");
INSERT INTO `wp_usermeta` VALUES("17", "1", "wp_dashboard_quick_press_last_post_id", "243");
INSERT INTO `wp_usermeta` VALUES("18", "1", "session_tokens", "a:2:{s:64:\"2a4736937d0df0ead6e95291c76f7ec13963432846746338146d9b0786bb34e7\";a:4:{s:10:\"expiration\";i:1553965072;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36\";s:5:\"login\";i:1553792272;}s:64:\"547baa82ccc9a1dfbaedb280ae4b113116e4efc61d738f8b92d2c49fae156967\";a:4:{s:10:\"expiration\";i:1554023074;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:105:\"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36\";s:5:\"login\";i:1553850274;}}");
INSERT INTO `wp_usermeta` VALUES("19", "1", "managenav-menuscolumnshidden", "a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}");
INSERT INTO `wp_usermeta` VALUES("20", "1", "metaboxhidden_nav-menus", "a:1:{i:0;s:12:\"add-post_tag\";}");
INSERT INTO `wp_usermeta` VALUES("21", "1", "nav_menu_recently_edited", "3");
INSERT INTO `wp_usermeta` VALUES("22", "1", "tgmpa_dismissed_notice_tgmpa", "1");

/* INSERT TABLE DATA: wp_users */
INSERT INTO `wp_users` VALUES("1", "admin", "$P$BqbJVqcHbFM9F8Bsa8fK7c.uj9b2HI/", "admin", "sandya.mn@mobinius.com", "", "2019-03-21 14:08:58", "", "0", "admin");

SET FOREIGN_KEY_CHECKS = 1; 

/* Duplicator WordPress Timestamp: 2019-03-29 10:10:44*/
/* DUPLICATOR_MYSQLDUMP_EOF */
